/// \file uhf2repeater.h
/// This is the header file of the uhf2repeater module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2012/06/26
/// - Module       : UHF2REPEATER
/// - Description  : uhf2repeater interface header
///
/// \details This module handles UHF2 messages on a Mobile Radio configured as a 
///          UNIT_TYPE_UHF2_REPEATER. The specific DLL types that get repeated is configurable.
///
/// \note Confluence link: https://jira.parsec.co.za:8446/display/PRJCASII/UHF2REPEATER+Module
///
/// \author Michael Manthey (MM)
/// \version $Id: uhf2repeater.h,v 1.1 2012-06-29 12:00:20+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _UHF2REPEATER_H_
#define _UHF2REPEATER_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"       ///< SCAS II Message types

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
/// \enum UHF2 Repeater Configurations
typedef enum _UHF2REPEATER_Config {
	REPEATER_NONE           =   0x00,       /// Repeats no messages (can be used to 'disable' a
	                                        /// UHF2 repeater temporarily, etc.)
	REPEATER_ALL            =   0x01,       /// Repeats everything received over UHF2
	REPEATER_CA_ONLY        =   0x02,       /// Repeats only CA messages (BEACON, CAM, CAACK, etc.)
	REPEATER_SCASII_ONLY    =   0x03,       /// Repeats SCASII and SCASII_LONG messages
	REPEATER_CA_SCASII      =   0x04,       /// Repeats CA, SCASII and SCASII_LONG messages
	REPEATER_P2_ONLY        =   0x05,       /// Repeats messages on MESSAGES_UHF2_DLL_TYPE_P2_SHORT
	                                        /// \todo Add MESSAGES_UHF2_DLL_TYPE_P2_LONG when it
											///       starts getting used
	REPEATER_CA_P2          =   0x06,       /// Repeats CA messages and protocol 2 messages
	REPEATER_RU_ONLY        =   0x07,       /// Repeats messages between RU RFM's on remote LMU's
} UHF2REPEATER_Config;

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initialises the UHF2REPEATER module, and should be called after 
///        UHF2_Init()
void UHF2REPEATER_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function processes all messages received over UHF2 and repeats any messages which
///        must be repeated according to the configuration settings
/// \param rxMsg [in] A pointer to the received message including preamble, payload and RSSI
void UHF2REPEATER_ProcessMessage(MESSAGES_UHF2Msg *rxMsg);

#ifdef __cplusplus
};
#endif

#endif	// _UHF2REPEATER_H_