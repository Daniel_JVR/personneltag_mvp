/// \file cbit.c
/// This is the implementation file of the cbit module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/09/07
/// - Module        : CBIT
/// - Description   : cbit implementation
///
/// The continuous build in test is responsible for system integrity tests on a continuous
/// basis, specifically for the Mobile Radio.
///
/// \author Richard Roche (RR)
/// \version $Id: cbit.c,v 1.7 2015-03-05 12:02:29+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: none
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "cbit.h"

#include "board.h"      ///< HW Definitions

#include "adc.h"        ///< ADC module
#include "msghandler.h" ///< SCAS II message handler
#include "timer.h"      ///< Timer module header file
#include "calog.h"      ///< CALOG module header file

//-------------------------------------------------------------------------------------------------
// Defines
#define MAX_ADC_VAL         (2047)          ///< 12-bit ADC, 1 bit for sign indication
#define CBIT_VDDANA         (3.15)          ///< Voltage applied to VDDANA pin of MCU
#define R1_VAL              (82000)         ///< 82k resistor according to schematics
#define R2_VAL              (18000)         ///< 39k resistor according to schematics
#define VOLTAGE_MAX         (float)(5.0)    ///< The uppervoltage limit for the Mobile Radio.
#define VOLTAGE_MIN         (float)(3.2)    ///< The lowervoltage limit for the Mobile Radio.
#define VOLTAGE_AVERAGE     (5)             ///< Number of times to average the ADC value
#define LOG_SPAM_TIMEOUT_S  (300)           ///< Do not log cbit events more than once in 5 minutes

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
/// \enum _CBIT_PowerVoltage
typedef enum _CBIT_PowerVoltage {
    CBIT_VOLTAGE_LOW,                       ///< MR powerVoltage below VOLTAGE_MIN
    CBIT_VOLTAGE_NORMAL,                    ///< MR powerVoltage between VOLTAGE_MAX & VOLTAGE_MIN
    CBIT_VOLTAGE_HIGH                       ///< MR powerVoltage above VOLTAGE_MAX
} CBIT_PowerVoltage;

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;                           ///< Configuration parameters

//-------------------------------------------------------------------------------------------------
// Global variables
static CBIT_PowerVoltage powerMeasured = CBIT_VOLTAGE_LOW;  ///< Power state initially at low voltage
static U32 lastLogTs;                       ///< Stores the timestamp at each cbit logging event
U8 boardInitStatus = FAILURE;

//-------------------------------------------------------------------------------------------------
// C function prototypes
void cbit_SystemPower(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
/// This function initialises the continuous build in test module
void CBIT_Init(void)
{
	// Modules that are required for cbit only can be initialized in this function
    ADC_Init(ADC_REF_06V, ADC_FREQUENCY);
}

//-------------------------------------------------------------------------------------------------
/// This function services hardware/software tests required on a continuous basis
void CBIT_Service(void)
{
	static U32 serviceCounter = 0;
	
	/// Only perform this service routine about every 1000th time
	if(serviceCounter < 1000) {
	    serviceCounter++;
	} else {
	    cbit_SystemPower();
	    serviceCounter = 0;
	}
}

//-------------------------------------------------------------------------------------------------
/// This function re-initialises the continuous build in test service
void CBIT_Reinitialize(void)
{
    powerMeasured = CBIT_VOLTAGE_LOW;                        // Set the power state to low voltage
}

//-------------------------------------------------------------------------------------------------
void CBIT_SetBoardInitStatus(U8 initStatus)
{
	boardInitStatus = initStatus;
}

//-------------------------------------------------------------------------------------------------
U8 CBIT_GetBoardInitStatus(void)
{
	return boardInitStatus;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function checks the system power by ADC measurements of power rails
/// \note  Its the main test to check if the system`s power supply is adequate for proper operation.
void cbit_SystemPower(void)
{
    S16 adcReadings = 0;
	float powerNew;

	// Get current voltage rail measurements
	for(U8 i=0; i<VOLTAGE_AVERAGE; i++) {
		adcReadings = adcReadings + ADC_Read(ADC_CHANNEL_VBAT);
	}

	// Average the readings and convert to a voltage
	adcReadings = (adcReadings / VOLTAGE_AVERAGE);
	///< \note: adcReadings ~= 2047 * Vbat * R2 / (R1 + R2)
	powerNew = (adcReadings / (float)MAX_ADC_VAL) * 0.6*CBIT_VDDANA * ((R1_VAL + R2_VAL) / ((float)R2_VAL));

	if(powerNew > (VOLTAGE_MAX)) {                  // Power voltage > VOLTAGE_MAX
        powerMeasured = CBIT_VOLTAGE_HIGH;
		
		///< Not really concerned with high voltage level measurements.
		if(TIMER_GetSecondsElapsed(lastLogTs) > LOG_SPAM_TIMEOUT_S) {
			lastLogTs = TIMER_GetSecondsCounter();
			/*MESSAGES_SystemError data = {
				.errorType = MESSAGES_ERR_MINOR,
				.errorCode = ERROR_MR_BATTERY_HIGH
			};*/
			// Don't log BATTERY_HIGH condition. Issue 03 (and earlier) of Mobile Radio PCB had 
			// different resistor values in the voltage divider connected to ADC pin. On these older
			// boards, the voltage will be measured higher than the actual value.
			//CALOG_Write(SYSTEM_ERROR_EVENT, (U8 *)&data, SYSTEM_ERROR_LEN);
		}

	} else if((powerNew >= (VOLTAGE_MIN)) && (powerNew <= (VOLTAGE_MAX))) {
		powerMeasured = CBIT_VOLTAGE_NORMAL;

	} else {                                        // Power voltage < VOLTAGE_MIN
		powerMeasured = CBIT_VOLTAGE_LOW;
		// log this event
		if(TIMER_GetSecondsElapsed(lastLogTs) > LOG_SPAM_TIMEOUT_S) {
			lastLogTs = TIMER_GetSecondsCounter();
			MESSAGES_SystemError data = {
				.errorType = MESSAGES_ERR_MAJOR,
				.errorCode = ERROR_MR_BATTERY_LOW
			};
			CALOG_Write(SYSTEM_ERROR_EVENT, (U8 *)&data, SYSTEM_ERROR_LEN);
		}
	}
}
