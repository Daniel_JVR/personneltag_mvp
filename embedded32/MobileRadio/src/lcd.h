/// \file lcd.h
/// This is the header file of the lcd module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2015/09/01
/// - Module        : LCD
/// - Description   : lcd header file
///
/// \detail	LCD
///
///
/// \author Casey Stephens (CS)
/// \version 
//-------------------------------------------------------------------------------------------------

#ifndef _LCD_H_
#define _LCD_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"
#include "msgdispatch.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief 
void LCD_Init(void);
void LCD_Service(void);
void LCD_MessageProcess(MESSAGES_PacketG2 *packet, MSGHANDLER_RxChannel rxChannel);

#ifdef __cplusplus
};
#endif

#endif	// _LCD_H_