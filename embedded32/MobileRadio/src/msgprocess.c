/// \file msgprocess.c
/// This is the implementation file of the msgprocess module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/08/29
/// - Module        : MSGPROCESS
/// - Description   : MsgProcess implementation for the Mobile Radio
///
/// \details This module handles messages from the Messages module to & from the
/// appropriate communications handler on a Mobile Radio.
///
/// \note Only the IrDA port (UART1) connects to this message handler. To use diag_terminal on
///       the Mobile Radio, MSGPROCESS_Service() must be commented out in main.c
///
/// Messages that are processed (received) are:
///     MESSAGES_TYPE_UNIT_TEST                 (Test unit -> MR)
///     MESSAGES_TYPE_UNIT_CONFIRM              (Test unit -> MR)
///     MESSAGES_TYPE_ACK                       (Ignored)
///     MESSAGES_TYPE_NACK                      (Ignored)
///     MESSAGES_TYPE_ALIVE                     (Ignored)
///     MESSAGES_TYPE_TIME_SYNC
///     MESSAGES_TYPE_SET_CA_VISIBILITY         (SDBIF -> MR, needs improvement)
///     MESSAGES_TYPE_IMAGE_DOWNLOAD_ENABLE     (SDBIF -> MR)
///     MESSAGES_TYPE_IMAGE_PACKET              (SDBIF -> MR)
///     MESSAGES_TYPE_IMAGE_SWMAINT_RESET       (SDBIF -> MR)
///     MESSAGES_TYPE_CONFIG_BLOCK_DL           (SDBIF -> MR)
///     MESSAGES_TYPE_CONFIG_BLOCK_REQ          (SDBIF -> MR)

///     MESSAGES_TEST_ROUNDTRIP_UHF1            (Test unit -> MR)
///     MESSAGES_TEST_ROUNDTRIP_UHF2_INIT       (Test unit -> MR)
///     MESSAGES_TEST_ROUNDTRIP_UHF2            (Test unit -> MR)
///     MESSAGES_TEST_RANGING_INIT              (Test unit -> MR)

/// UHF1 DataMsg types handled in MSGPROCESS_Service()
///     UHF1_TYPE_RANGE_RESULT
///     UHF1_TYPE_TEST_ROUNDTRIP
///     UHF1_TYPE_TEST_RANGING_RESULTS
///
/// \author Michael Manthey
/// \version $Id: msgprocess.c,v 1.50 2015-03-05 13:50:19+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: [1] 32-bit Ranging Unit MsgProcess module
///                       Richard Roche
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "msgprocess.h"
#include "msgdispatch.h"

#include "common.h"             ///< Common defines such as TRUE, FALSE etc
#include "board.h"              ///< HW definitions


#include "rangetag.h"
#include "sw_maintenance_uc3.h" ///< SCAS II software maintenance

#include "msghandler.h"         ///< SCAS II message handler
#include "uhf2repeater.h"       ///< UHF2 Repeater Module

#include "mrtest.h"             ///< Mobile Radio test module header file
#include "wdt.h"                ///< Used for MESSAGES_TYPE_DO_WDT_RESET
#include "calog.h"
#include "ptcstatus.h"
#include "lcd.h"

#include <string.h>             ///< Used for memcpy

// for debugging
#include <gpio.h>
#include "ca_msg.h"
#include "ioctl.h"
#include "iophy.h"
#include "paging.h"
#include "regionctrl.h"
#include "flashdump.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define MAX_TEST_ATTEMPTS       (5)             ///< When the Mobile Radio is being tested, the max
                                                ///  number of times to run a specific test is 5.
//#define UHF2_MAX_FRAGMENT_LEN   (MESSAGES_UHF2_MAX_LEN - 2) ///< Two bytes used for numPacket and totalPackets

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct _msgprocess_TestCounters {
	U8 uhf1Roundtrip;
	U8 uhf2Roundtrip;
	U8 ranging;
} ATTR_PACKED msgprocess_TestCounters;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;       ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// Global variables
static msgprocess_TestCounters testCnt = {0};       // For future use during testing



//-------------------------------------------------------------------------------------------------
// C function prototypes

void    msgprocess_RetAck(MESSAGES_Dest destination, U8 seqByte);
void    msgprocess_RetNack(MESSAGES_Dest destination, U8 seqByte);
void    msgprocess_RetAlive(MESSAGES_Dest destination);
//U8      msgprocess_CaMsgBufferCount(void);

//void    msgprocess_SetMobileRadioVisibility(U8 newVal);
//void    msgprocess_AccumulateUHF2LongMessage(MESSAGES_UHF2Msg* rxMsg);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
/// This function initializes the communications ports used by the message
/// handler on the Mobile Radio
U8 MSGPROCESS_Init(void)
{
	// Register a callback on the MSGDISPATCH module
	return MSGDISPATCH_RegisterCallbackForMessageClass(MSGDISPATCH_CLASS_LEGACY_PROTOCOL, &MSGPROCESS_Process);
}

//-------------------------------------------------------------------------------------------------
/// This function processes the data packet received based on the message type
void MSGPROCESS_Process(MESSAGES_PacketG2 *packet, MSGHANDLER_RxChannel rxChannel)
{
    //U8 *data = packet->data;                              ///< Packet data pointer
	MESSAGES_SetCaEnabledState *caEnabledMsg;               ///< Received over UHF2/IrDA to set collision avoidance on/off

	UHF1_TestRoundtripMsg rtMsgUHF1;                        ///< Used for testing UHF1 data messaging
	UHF1_DataMsg msgUHF1;                                   ///< Used for testing UHF1 data messaging
	MESSAGES_RoundtripMsgUHF1 *rtMsgPayloadUHF1;            ///< Used for testing UHF1 data messaging

	UHF1_TestRangingMsg testRangingMsg;                     ///< Used for testing UHF1 ranging
	MESSAGES_TestRangingInitMsg *testRangingInitMsg;        ///< Used for testing UHF1 ranging

	MESSAGES_RoundtripMsgUHF2Init *rtMsgPayloadUHF2Init;    ///< Used for testing UHF2
	MESSAGES_RoundtripMsgUHF2 rtMsgUHF2;                    ///< Used for testing UHF2

	MESSAGES_TestMrLeds *testLedMsg;                        ///< Used for testing MR warning LED's

	static MESSAGES_ConfigBlockReq configBlockReq;          ///< Configblock request
	SYSCONFIG_InitStateMsg *initStateReq;
	U8 *data = packet->data;                                ///< Packet data pointer
	TIMER_TimeStamp newTime;                                ///< New time used for updating timer module
	MESSAGES_UHF2PingResponse pingResponse;
	MESSAGES_UHF2Ping *pingPtr;

	MESSAGES_RangeResult *rangeResult;

	U32 logCount;                                           ///< Number of log entries
    U32 logIndex;                                           ///< Log index
	CALOG_DataFormat readLog;

	MESSAGES_VerConfirm *verConfirmPtr;                     ///< Version confirm message
	REGIONCTRL_RegionCtrlMsg *regionCtrlMsg;
	S32 rcuDist;
	
	SYSCONFIG_InitStateMsg initStateMsg;

    // Validate that the message is for a Miner Unit or Hazard Unit
    if((packet->destType == MESSAGES_DEST_MU) || (packet->destType == MESSAGES_DEST_UHF2) || (packet->destType == MESSAGES_DEST_INTERNAL)) {
        switch(packet->msgType) {
		    case MESSAGES_TYPE_ACK:             // Acknowledge received

                break;

		    case MESSAGES_TYPE_NACK:            // Not acknowledged received

                break;

		    case MESSAGES_TYPE_ALIVE:           // Alive message received
                //gpio_toggle_pin(LU_LED_PIN);
				//TIMER_DELAY_MS(100);
				MSGHANDLER_TransmitPacket(MESSAGES_DEST_IRDA, MESSAGES_TYPE_ALIVE, MESSAGES_LEN_ALIVE, &g_configBlock.deviceID.IDByte1);
                break;

			case MESSAGES_TYPE_DO_WDT_RESET:
				wdt_disable();
				wdt_opt_t opt = {
                    ///< Disable WDT after a watchdog reset.
                    .dar   = TRUE,
		            ///< The WDT is in basic mode, only PSEL time is used.
                    .mode  = WDT_BASIC_MODE,
		            ///< WDT Control Register is now locked(can only be unlocked with reset).
                    .sfv   = TRUE,
		            ///< The flash calibration will be redone after a watchdog reset.
                    .fcd   = FALSE,
		            ///< Select the system RC oscillator (RCSYS) as clock source.
                    .cssel = WDT_CLOCK_SOURCE_SELECT_RCSYS,
		            ///< TimeOut Value
                    .us_timeout_period = 1000000        // 1 second
                };
	            wdt_enable(&opt);

	            while(1);
				break;
				
			case MESSAGES_TYPE_UNIT_TEST:               // Test message packet received
		            switch(packet->data[0]) {
				        case MESSAGES_TEST_REPORT:
				            MRTEST_EchoReport();        // Provide the current initReport
				            break;

				        default:    // Do nothing
				            break;
			        }
		            break;

            case MESSAGES_TYPE_TIME_SYNC:           // Time sync message received
                memcpy(&newTime, packet->data, sizeof(newTime));
                //  Set local Timer timestamp
                TIMER_SetTimeStamp(newTime);
				// Also send the updated time value to the PTC Display
				////MSGHANDLER_TransmitPacketNewDestination(MESSAGES_DEST_MR_PTC, packet);
                break;

			case MESSAGES_TYPE_SET_CA_ENABLED_STATE:
			    caEnabledMsg = (MESSAGES_SetCaEnabledState *)&packet->data[0];
			    /// Do software address matching
				if(memcmp((U8*)&(caEnabledMsg->destID), (U8*)&(g_configBlock.deviceID), sizeof(DeviceID)) == 0) {
					RANGETAG_SetCaEnabledState(caEnabledMsg->caEnabled);
					IOCTL_PulseWarnLedsBlocking();
				}
				break;

			case MESSAGES_TYPE_IMAGE_DOWNLOAD_ENABLE:   // Download enable received
                // Kick off the SW maintenance state machine
				SWMAINT_StartDownload(data, packet->destType, packet->seqByte);
				break;

			case MESSAGES_TYPE_IMAGE_PACKET:            // Image packet received
			    // Process the packet
				SWMAINT_ProcessPacket(data, packet->seqByte);
				break;

            case MESSAGES_TYPE_IMAGE_SWMAINT_RESET:     // Reset the SW maintenance module
                // Reset
				SWMAINT_Reset();
				break;

			case MESSAGES_TYPE_CONFIG_BLOCK_DL:         // Config block dl received
			    // Update the config block
				SYSCONFIG_UpdateConfigBlock(data);
			    break;

			case MESSAGES_TYPE_CONFIG_BLOCK_REQ:        // Config block request received

				memcpy(&configBlockReq, data, sizeof(MESSAGES_ConfigBlockReq));

				// Check the ID
				if(((configBlockReq.sourceDest == MESSAGES_DEST_UHF2) || (configBlockReq.sourceDest == MESSAGES_DEST_DEBUG_RS485)) &&
                    (configBlockReq.destID.IDByte1 == g_configBlock.deviceID.IDByte1) &&
                    (configBlockReq.destID.IDByte2 == g_configBlock.deviceID.IDByte2) &&
                    (configBlockReq.destID.IDByte3 == g_configBlock.deviceID.IDByte3) &&
                    (configBlockReq.destID.IDByte4 == g_configBlock.deviceID.IDByte4) &&
                    (configBlockReq.destID.IDByte5 == g_configBlock.deviceID.IDByte5)) {

                    // Package the current config block and transmit it back
                        MESSAGES_ConfigBlockDl configBlockDl;

                        configBlockDl.destID = configBlockReq.sourceID;
                        configBlockDl.configBlock = g_configBlock;

                        // Transmit the packet
                        MSGHANDLER_TransmitPacket(configBlockReq.sourceDest,
                                                    MESSAGES_TYPE_CONFIG_BLOCK_DL,
                                                    MESSAGES_LEN_CONFIG_BLOCK_DL,
                                                    (U8*)&configBlockDl);

                // Else respond always
                } else if((configBlockReq.sourceDest != MESSAGES_DEST_UHF2) && (configBlockReq.sourceDest != MESSAGES_DEST_DEBUG_RS485)) {
                    // Package the current config block and transmit it back
                    MESSAGES_ConfigBlockDl configBlockDl;

                    configBlockDl.destID = configBlockReq.sourceID;
                    configBlockDl.configBlock = g_configBlock;

                    // Transmit the packet
                    MSGHANDLER_TransmitPacket(configBlockReq.sourceDest,
                                                MESSAGES_TYPE_CONFIG_BLOCK_DL,
                                                MESSAGES_LEN_CONFIG_BLOCK_DL,
                                                (U8*)&configBlockDl);

                    // Now also send the SysInit status to configUtility
					// Return the SYSCONFIG init state
					initStateMsg.sourceDest = MESSAGES_DEST_MU;  // Not really necessary
					initStateMsg.sourceID = g_configBlock.deviceID;
					initStateMsg.destId = configBlockReq.sourceID;
					initStateMsg.initState = SYSCONFIG_GetInitState();
					initStateMsg.version = SYSCONFIG_GetVersion();
					
					MSGHANDLER_TransmitPacket(configBlockReq.sourceDest,
					                            MESSAGES_TYPE_SYSCONFIG_INIT_STATE,
											    MESSAGES_LEN_SYSCONFIG_INIT_STATE,
											    (U8*)&initStateMsg);
                }
                break;
				
            case MESSAGES_TYPE_SYSCONFIG_INIT_STATE:
                // Get the request parameters
                initStateReq = (SYSCONFIG_InitStateMsg*)data;
					
                if(initStateReq->sourceDest != MESSAGES_DEST_UHF2) {
                    // Return the SYSCONFIG init state
	                initStateMsg.sourceDest = MESSAGES_DEST_MU;  // Not really necessary
	                initStateMsg.sourceID = g_configBlock.deviceID;
	                initStateMsg.destId = initStateReq->sourceID;
	                initStateMsg.initState = SYSCONFIG_GetInitState();
	                initStateMsg.version = SYSCONFIG_GetVersion();
					
	                MSGHANDLER_TransmitPacket(initStateReq->sourceDest,
					                            MESSAGES_TYPE_SYSCONFIG_INIT_STATE,
								                MESSAGES_LEN_SYSCONFIG_INIT_STATE,
								                (U8*)&initStateMsg);
                }
                break;
					
            case MESSAGES_TYPE_RESET_ALL_CONFIG:
                SYSCONFIG_ResetAllConfig(&g_configBlock, SYSCONFIG_RESET_MSG_RECEIVED);
                break;

			case MESSAGES_TYPE_UHF2_PING:
	            // This will never work. Need to add ability to reply in an assigned time slot
	            // based on deviceID.
			    pingPtr = (MESSAGES_UHF2Ping*)data;
			    pingResponse.srcID = g_configBlock.deviceID;
				pingResponse.destID = pingPtr->srcID;
				pingResponse.version = g_configBlock.version;
				MSGHANDLER_TransmitPacket(MESSAGES_DEST_UHF2, MESSAGES_TYPE_UHF2_PING_RESPONSE,
				                          MESSAGES_LEN_UHF2_PING_RESPONSE, (U8*)&pingResponse);
				break;

			case MESSAGES_TYPE_RANGE_RESULT:
			    ///< Ranging result messages are being moved from UHF1 to UHF2. This will be done
				///  in two phases since all existing software at Impala shaft #14 will have to be
				///  updated before the UHF1 result messages can be removed completely. For now, we
				/// will use UHF1 and UHF2. The code below temporarily tricks the Mobile Radio software
				/// into thinking a range result message was received over UHF1.
				rangeResult = (MESSAGES_RangeResult*)data;

				///< Do software address matching
				if(memcmp((U8*)&(rangeResult->destID), (U8*)&(g_configBlock.deviceID), sizeof(DeviceID)) == 0) {

					if(rangeResult->legacyIndicationByte == TRUE) {
						// Force the result to be within zone 2 to turn on the LED
				        rangeResult->distance = (g_configBlock.proximityZone2_cm - 1);

					} else {
						rangeResult->distance = (rangeResult->distance);
					}

				    RANGETAG_TableUpdateUHF1(rangeResult);
				}
				break;

            case MESSAGES_TYPE_REGION_CTRL:
			    regionCtrlMsg = (REGIONCTRL_RegionCtrlMsg *)&(packet->data);
				// Check whether the message must be processed based on RCU region distances
				if(RANGETAG_GetDeviceDist(regionCtrlMsg->srcID, &rcuDist) == TRUE) {
					if((rcuDist > regionCtrlMsg->minDist) && (rcuDist < regionCtrlMsg->maxDist)) {
						
						// Check the required command to perform
				        switch(regionCtrlMsg->cmd) {
					        case ENTER_DISCREET_MODE:
						        // Disable the cap lamp laser/LED
						        IOCTL_SetDiscreetMode();
						        break;
								
							case MINER_GLOBAL_DEREG:
							    // An RCU does not send an RCU Command when configured to trigger MINER_GLOBAL_DEREG
				                // In stead, the range result messages sent to Miner Units will indicate in the usual
				                // exclusion bits whether the device must be excluded
								break;
								
					        case NO_COMMAND:
							case ENTER_STOP_MODE:
							case ENTER_CRAWL_MODE:
					        default:
						        // Do nothing
						        break;
				        }
					}
				}
				break;

			case MESSAGES_TYPE_RET_RANGING_START:  // Ranging return start received
                // Do nothing
                break;

		    case MESSAGES_TYPE_RET_RANGING_ENTRY:  // Ranging return entry received
                // Update the table in the RangeTag module
                RANGETAG_TableUpdateRangeAlgo(data);
                break;

		    case MESSAGES_TYPE_RET_RANGING_END:    // Ranging return end of list received
                // Start ranging again
                RANGETAG_ConcludeRanging();
                break;

// Log uploads via IRDA
			case MESSAGES_TYPE_REQ_NUM_LOGS:        // Request num logs message received (PC)
                //CALOG_Count();
                break;

		    case MESSAGES_TYPE_REQ_LOGS:            // Request logs message received (PC)

				// Check the request type
				switch(packet->data[0]) {
				    case MESSAGES_LOG_UPLOAD:
					    // Download logs from the current manual log upload index up to current log index
                        logIndex = CALOG_GetNextLogIndex();

					    // This should go from the upload index
					    for(logCount = CALOG_GetUploadIndex(CALOG_UPL_INDEX_MANUAL); logCount < logIndex; logCount++) {
								
							// Clear the WDT to avoid a reset during the download
						    wdt_clear();

						    // Read the next log
                            CALOG_Read(logCount, &(readLog));
                                
							// Transmit the log to the specified destination
							MSGHANDLER_TransmitPacket(MESSAGES_DEST_IRDA,
                                                        MESSAGES_TYPE_RET_LOGS,
                                                        MESSAGES_LEN_RET_LOGS,
														(U8*)&readLog);

							TIMER_DELAY_32(CALOG_LOG_UPL_DELAY_MS);
                        }

						// Update the manual log upload index
                        CALOG_SetUploadIndex(CALOG_UPL_INDEX_MANUAL, logCount, TRUE);
						
				        break;

					case MESSAGES_LOG_ALL:
					
					    // Download all logs in every position in the DataFlash
						for(logCount = 0; logCount < 58363; logCount++) {
							
							// Clear the WDT to avoid a reset during the download
						    wdt_clear();

						    // Read the log
                            CALOG_Read(logCount, &(readLog));
								
                            // Transmit the log to the specified destination
							MSGHANDLER_TransmitPacket(MESSAGES_DEST_IRDA,
                                                        MESSAGES_TYPE_RET_LOGS,
                                                        MESSAGES_LEN_RET_LOGS,
														(U8*)&readLog);

							TIMER_DELAY_32(CALOG_LOG_UPL_DELAY_MS);
                        }

						// Do not update the log upload index after uploading ALL logs
						
				        break;

					default:
						// Do nothing
						break;
				}



                break;

			case MESSAGES_TYPE_CLR_LOGS:            // Clear logs
				CALOG_Clear();
				break;

//-----------------------------------------------------------------------------------------------
// The following are Mobile Radio test messages
		    case MESSAGES_TEST_ROUNDTRIP_UHF1:
		        ///< Test hardware has requested a UHF1 roundtrip test, and provided a DeviceID
				///  whith which to do the roundtrip test.
				rtMsgPayloadUHF1 = (MESSAGES_RoundtripMsgUHF1 *)&(packet->data[0]);
				rtMsgUHF1.sourceID = g_configBlock.deviceID;
				msgUHF1.len = UHF1_LEN_TEST_ROUNDTRIP;
				msgUHF1.msgType = UHF1_TYPE_TEST_ROUNDTRIP;
				memcpy(&(msgUHF1.payload[0]), (U8*)&(rtMsgUHF1.sourceID), UHF1_LEN_TEST_ROUNDTRIP);
				UHF1_SendDataMsg(rtMsgPayloadUHF1->remoteDeviceID, msgUHF1);
				testCnt.uhf1Roundtrip++;
				break;

		    case MESSAGES_TEST_ROUNDTRIP_UHF2_INIT:
		        ///< Test hardware has requested a UHF2 roundtrip test, and provided a DeviceID
				///  whith which to do the roundtrip test.
				rtMsgPayloadUHF2Init = (MESSAGES_RoundtripMsgUHF2Init *)&(packet->data[0]);
				/// Compose a MESSAGES_TEST_ROUNDTRIP_UHF2 message to send to remote device
				rtMsgUHF2.sourceID = g_configBlock.deviceID;
				rtMsgUHF2.destID = rtMsgPayloadUHF2Init->remoteDeviceID;
				MSGHANDLER_TransmitPacket(MESSAGES_DEST_UHF2, MESSAGES_TEST_ROUNDTRIP_UHF2,
				                          MESSAGES_LEN_ROUNDTRIP_UHF2, (U8*)&(rtMsgUHF2));
				testCnt.uhf2Roundtrip++;
		        break;

		    case MESSAGES_TEST_ROUNDTRIP_UHF2:
		        ///< A remote device might have replied to a MESSAGES_TEST_ROUNDTRIP_UHF2 message from
				///  this specific Mobile Radio. Check whether that is the case (do software address
				///  matching) and notify the test equipment that the roundtrip is complete.
				memcpy((U8*)&(rtMsgUHF2), (U8*)&(packet->data[0]), MESSAGES_LEN_ROUNDTRIP_UHF2);
				if(memcmp((U8*)&(rtMsgUHF2.destID), (U8*)&(g_configBlock.deviceID), sizeof(DeviceID)) == 0) {
					///< Notify the test equipment that the roundtrip is complete
					rtMsgUHF2.sourceID = g_configBlock.deviceID;
					memset((U8*)&(rtMsgUHF2.destID), 0x00, sizeof(DeviceID));   ///< Empty destination ID
					////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC, MESSAGES_TEST_ROUNDTRIP_UHF2,
				    ////                      MESSAGES_LEN_ROUNDTRIP_UHF2, (U8*)&(rtMsgUHF2));
				}
		        break;

			case MESSAGES_TEST_RANGING_INIT:
			    ///< Test hardware has requested a UHF1 ranging test sequence, and provided a DeviceID
				///  of the remote device which must renage with the UUT.
				testRangingInitMsg = (MESSAGES_TestRangingInitMsg *)&(packet->data[0]);

				///< Send a UHF1_TYPE_TEST_RANGING message to the remote device
				testRangingMsg.uutID = g_configBlock.deviceID;
				testRangingMsg.maxCount = testRangingInitMsg->maxCount;
				testRangingMsg.stopCount = testRangingInitMsg->stopCount;
                msgUHF1.len = UHF1_LEN_TEST_RANGING;
				msgUHF1.msgType = UHF1_TYPE_TEST_RANGING;
				memcpy(&(msgUHF1.payload[0]), (U8*)&(testRangingMsg), UHF1_LEN_TEST_RANGING);
				UHF1_SendDataMsg(testRangingInitMsg->remoteID, msgUHF1);
				testCnt.ranging++;
				break;

			case MESSAGES_TEST_MR_LEDS:
			    testLedMsg = (MESSAGES_TestMrLeds*)&(packet->data[0]);
				IOPHY_VehicleWarningLED(testLedMsg->vuLED);
				IOPHY_LocoWarningLED(testLedMsg->luLED);
				///< NOTE: HU and EXCL LED's switched around on client request
				IOPHY_WrkrExclDrvrDeregLED(testLedMsg->huLED);
				IOPHY_HazardWarningLED(testLedMsg->exclLED);
				IOPHY_DigitalOut1(testLedMsg->caplamp);
				// Suspend updating of warning LED's for specified period
				IOCTL_SuspendWarnLedUpdates(testLedMsg->timeoutMs);
				break;

			case MESSAGES_TYPE_DISPLAY_VER:
			    // Suspend updating of warning LED's for specified period
			    IOCTL_DisplayVer();

				// Each version of the MR code must display a different LED sequence
                break;

            case MESSAGES_TYPE_VER_CONFIRM:
				verConfirmPtr = (MESSAGES_VerConfirm *)&(packet->data[0]);

				U8 versionMatch = FALSE;
				// Check if the version matches
				Version currentVersion = SYSCONFIG_GetVersion();
				if(memcmp(&(currentVersion), &(verConfirmPtr->version), sizeof(Version)) == 0) {
					versionMatch = TRUE;
				}

                // Check the logic that must be applied
				if(verConfirmPtr->reverseLogic == FALSE) {
                    if(versionMatch == TRUE) {
                        IOCTL_DisplayVer();
					}

				// Reverse logic
				} else {
                    if(versionMatch == FALSE) {
                        IOCTL_DisplayVer();
					}
				}
			    break;
				
			case MESSAGES_TYPE_PTC_KEEP_ALIVE_REQ:
	        case MESSAGES_TYPE_PTC_KEEP_ALIVE_RES:
	        case MESSAGES_TYPE_PTC_DISPLAY_MSG:
	        case MESSAGES_TYPE_PTC_USER_RESPONSE:
	        case MESSAGES_TYPE_PTC_SEND_PRESET_MSG:
			    // The PAGING module handles all PTC paging related messages
				PAGING_MessageProcess(packet);
				PTCSTATUS_MessageProcess(packet);
				break;
				
			// Message received to read a sector of flash memory
			case MESSAGES_TYPE_READ_FLASH_SECTOR:
                FLASHDUMP_EchoSectorToDest(MESSAGES_DEST_IRDA, (FLASHDUMP_SectorReadParams *)data);
                break;	

			default:
                // Unknown message type, transmit a NACK
                // MM Do not transmit nack - Mobile Radio interfaces are UHF1, UHF2 and IrDA, all
				// of which should not be saturated with nack messages.
				//msgprocess_RetNack(packet->destType, packet->seqByte);
                break;
        }
    } else if(packet->destType == MESSAGES_DEST_DIRECT_DEBUG_NO_CRC) {
		switch(packet->msgType) {
			
			// Message received to read a sector of flash memory
		    case MESSAGES_TYPE_READ_FLASH_SECTOR:
                FLASHDUMP_EchoSectorToDest(MESSAGES_DEST_IRDA, (FLASHDUMP_SectorReadParams *)data);
                break;
			
			// Message received to perform a debugging action on UHF1
		    case MESSAGES_TYPE_NANOLOC_DEBUG:
                switch(data[0]) {
                	case UHF1_DEBUG_ACTION_SHUTDOWN:
					    UHF1_Shutdown();
                		break;
						
                	case UHF1_DEBUG_ACTION_RESTART:
					    UHF1_Restart();
                		break;
                	
					case UHF1_DEBUG_ACTION_PA_HIGH_GAIN:
#ifndef SCASII_OC_TAG					
					    UHF1_ConfigurePa(UHF1_PA_HIGH_GAIN);
#endif
                		break;
						
                	case UHF1_DEBUG_ACTION_PA_LOW_GAIN:
#ifndef SCASII_OC_TAG
					    UHF1_ConfigurePa(UHF1_PA_LOW_GAIN);
#endif
                		break;
						
                	case UHF1_DEBUG_ACTION_PA_DISABLED:
#ifndef SCASII_OC_TAG					
					    UHF1_ConfigurePa(UHF1_PA_DISABLE);
#endif
                		break;
						
                	default:
                		break;
                }
                break;
				
			/*case MESSAGES_TYPE_TEST:
				if(packet->data[0] == 0xA3) {
					U32 logCount;
					memcpy(&logCount, &data[1], sizeof(U32));
					CALOG_DebuggingWriteRandomLogs(logCount, data[5]);
				}
				break;*/
			
			default:
			    break;
		}			
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function transmits an ACK
/// \param destination Destination type of the message
/// \param seqByte Sequence byte of packet ACK is for
void msgprocess_RetAck(MESSAGES_Dest destination, U8 seqByte)
{
    MSGHANDLER_TransmitPacket(destination,
	                          MESSAGES_TYPE_ACK,
							  MESSAGES_LEN_ACK,
							  &seqByte);
}

//-------------------------------------------------------------------------------------------------
/// \brief This function transmits a NACK
/// \param destination Destination type of the message
/// \param seqByte Sequence byte of packet ACK is for
void msgprocess_RetNack(MESSAGES_Dest destination, U8 seqByte)
{
    MSGHANDLER_TransmitPacket(destination,
	                          MESSAGES_TYPE_NACK,
							  MESSAGES_LEN_NACK,
							  &seqByte);
}

//-------------------------------------------------------------------------------------------------
/// \brief This function transmits a keep alive message
/// \param destination Destination type of the message
void msgprocess_RetAlive(MESSAGES_Dest destination)
{
    MSGHANDLER_TransmitPacket(destination,
	                          MESSAGES_TYPE_ALIVE,
							  MESSAGES_LEN_ALIVE,
							  (U8*)(&g_configBlock.deviceID));
}

