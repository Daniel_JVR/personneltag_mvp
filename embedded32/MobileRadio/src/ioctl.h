/// \file ioctl.h
/// This is the header file of the ioctl module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/09/13
/// - Module       : IOCTL
/// - Description  : ioctl interface header
///
/// \details  This module provides functions for manipulating and accessing inputs and outputs on
///           the Mobile Radio PCB. For example, warning LED's can be controlled according to the
///           contents of an IOCTL_WarnBits variable. It also manages the blinking of a run LED and
///           other LED's such as driver deregistration LED.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: ioctl.h,v 1.16 2015-02-25 16:04:34+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _IOCTL_H_
#define _IOCTL_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "common.h"         ///< Contains common defines such as TRUE, FALSE, and ATTR_PACKED

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
#define IOCTL_FAST_BLINK_MS     (200)       ///< Fast blinking period in milliseconds
#define IOCTL_SLOW_BLINK_MS     (1500)      ///< Slow blinking period in milliseconds

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
/// \enum IO State 
/// The IOState enumeration identifies the states as ON or OFF
typedef enum _IOState {
    IO_OFF          = 0,    ///< OFF State
    IO_ON           = 1,    ///< ON State
    IO_RISING_EDGE  = 2,    ///< Input has changed from low to high
    IO_FALLING_EDGE = 3,    ///< Input has changed from high to low
    IO_BLINKING     = 4,
} IOState;

//-------------------------------------------------------------------------------------------------
/// \enum IO Service Modes
/// The IOState enumeration identifies the states as ON or OFF
typedef enum _IOCTL_ServiceModes {
    IOCTL_SERVICE_FULL,
	IOCTL_SERVICE_HEARTBEAT,
} IOCTL_ServiceModes;

//-------------------------------------------------------------------------------------------------
/// \enum IOCTL_DigOutFunction 
/// The IOCTL_DigOutFunction enumeration describes functions that can be assigned to a digital 
/// output pin
typedef enum _IOCTL_DigOutFunction {
	IOCTL_FUNC_CAPLAMP_LED,     // Digital out pin is used for Miner Unit caplamp LED
	IOCTL_FUNC_HU_RELAY,        // Digital out pin used for toggling relay in a Hazard Unit
} IOCTL_DigOutFunction;

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
/// \union IO Control warning bits
/// Contains the warning bits: a set bit is indicative of a warning 
typedef union _IOCTL_WarnBits {
    struct {
        U16 SingleLoco         : 1;  ///< Single Locomotive Waring
        U16 MultipleLoco       : 1;  ///< Multiple Locomotive warning
        U16 SingleVehicle      : 1;  ///< Single vehicle warning 
        U16 MultipleVehicle    : 1;  ///< Multiple vehicle warning
        U16 Hazard             : 1;  ///< Hazard warning
        U16 TestUnit           : 1;  ///< Test unit warning
        U16 FixedUnit          : 1;  ///< Fixed unit warning
        U16 Spare1             : 5;  ///< Not used
		U16 wrkrExcl           : 1;  ///< Indicates Worker Exclusion
        U16 drvrDereg          : 1;  ///< Indicates Driver Deregistration
        U16 Spare2             : 2;  ///< Not used
    } bit;
    U16 word;
} ATTR_PACKED IOCTL_WarnBits;

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initializes the IOCTL module and assigns default states to Inputs
///        and outputs.
void IOCTL_Init(void);
//-------------------------------------------------------------------------------------------------
/// \brief Services the IOCTL module to debounce Emergency Stop Button and blink LED's
void IOCTL_Service(IOCTL_ServiceModes mode);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the Miner Unit warning LEDs and Caplamp laser according to 
///        nearby devices, both nanoLOC equipped devices and old SCAS I devices.
/// \param warnBits [in] Indicates which types of devices were detected on UHF1 and UHF2
void IOCTL_UpdateWarning(IOCTL_WarnBits warnBits);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the debounced value of the Emergency Stop Button (DI1_PIN)
/// \return debounced state of Emergency Stop Button (IO_ON or IO_OFF)
IOState IOCTL_ReadEmergencyStopBtn(void);

//-------------------------------------------------------------------------------------------------
/// \brief Sets the heartbeat LED flash period
/// \param heartbeatPer [in] Heartbeat period in milliseconds
void IOCTL_SetHeartbeatMs(U16 heartbeatPer);

//-------------------------------------------------------------------------------------------------
/// \brief Briefly flashes all four warning LED's on the MR
void IOCTL_PulseWarnLedsBlocking(void);

//-------------------------------------------------------------------------------------------------
/// \brief Briefly sweep all four warning LED's on the MR and also the digital output (caplamp LED)
void IOCTL_SweepWarnLedsBlocking(void);

//-------------------------------------------------------------------------------------------------
/// \brief Briefly sweep all four warning LED's on the MR
void IOCTL_SweepWarnLedsBlockingNoCaplamp(void);

//-------------------------------------------------------------------------------------------------
/// \brief Briefly flashes the Dig Out 1 pin on the MR (normally conencted to Cap Lamp laser)
void IOCTL_PulseDigOut1Blocking(void);

//-------------------------------------------------------------------------------------------------
/// \brief Suspend warning LED updates for specified time
/// \param timeoutMs The number of milliseconds to suspend warning LED updates
void IOCTL_SuspendWarnLedUpdates(U32 timeoutMs);

//-------------------------------------------------------------------------------------------------
/// \brief Toggles the HU LED, only if warning LED updates are not being suspended
void IOCTL_ToggleHazardLed(void);

//-------------------------------------------------------------------------------------------------
/// \brief Initiates a warning LED sequence which displays the software version
void IOCTL_DisplayVer(void);

//-------------------------------------------------------------------------------------------------
/// \brief Allows external modules to set the DIG_OUT1 pin's function.
/// \param newFunction Either IOCTL_FUNC_CAPLAMP_LED or IOCTL_FUNC_HU_RELAY
void IOCTL_SetDigOut1Function(IOCTL_DigOutFunction newFunction);

//-------------------------------------------------------------------------------------------------
/// \brief Allows external modules to set the DIG_OUT1 pin status to active (i.e. toggling at a
///         configurable rate) or off.
/// \param newState Boolean value indicating whether the DIG_OUT1 pin must be active (TRUE) or not 
///         (FALSE)
void IOCTL_SetDigOut1Active(U8 newState);

//-------------------------------------------------------------------------------------------------
/// \brief This function activates discreet mode on the Mobile Radio, meaning the cap lamp LED is.
///         disabled. Discreet mode will time out if not confirmed for masterCATimeout_s.
void IOCTL_SetDiscreetMode(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function activates a gas warning on the Mobile Radio, meaning the cap lamp LED must
///        blink 3 times per second, as defined in the RSD requirement R2.45.
/// \param state Specified whether gas warning must be set (TRUE) or cleared (FALSE)
/// \note Gas warning will time out automatically after XX seconds. \todo
void IOCTL_SetGasWarning(U8 state, U8 seconds);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets a flag indicating a special software release, which will cause the
///         vehcile (red) and loco (blue) indicator LEDs to flash alongside the heartbeat LED.
void IOCTL_SetSpecialReleaseFlag(U8 state);

#ifdef __cplusplus
};
#endif

#endif	// _IOCTL_H_
