/// \file rangetag.h
/// This is the header file of the rangetag module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/09/16
/// - Module       : RANGETAG
/// - Description  : rangetag interface header
///
/// \details This module is responsible for processing all collision avoidance messages received
///          over UHF1 and UHF2. It maintains a warning table of all devices detected in the
///          mobile radio's vicinity, and uses rssi measurements and range result messages to
///          issue warnings for both SCAS I devices (no nanoLOC) and SCAS II devices (nanoLOC
///          equipped). It also performs logging of all collision avoidance events such as UHF2
///          registration (RSSI over threshold) and UHF1 registration (distance within zone 2),
///          worker exclusion and driver deregistration additions, termination and timeouts, etc. 
///
/// \note    This module replaces CAALGO for the Mobile Radio in order to keep collision avoidance
///          information in a single module. The only UHF2 messages received/transmitted by the
///          Mobile Radio are:
///          - CA_MSG_BEACON: Received from all CA masters 
///          - CA_MSG_CAACK: Received from Fixed Units, after which FU beacons must be ignored for
///            a determined period of time.
///          - CA_MSG_CAM: Sent to CA masters whenever average RSSI is measured over threshold
///
///
/// \author Michael Manthey (MM)
/// \version $Id: rangetag.h,v 1.15 2015-03-05 11:49:27+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _RANGETAG_H_
#define _RANGETAG_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "uhf1.h"
#include "messages.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
typedef enum _RANGETAG_AirInterfaceRole {
	RANGETAG_CA_MASTER,
	RANGETAG_CA_SLAVE,
	RANGETAG_REPEATER,
} RANGETAG_AirInterfaceRole;

//-------------------------------------------------------------------------------------------------
// Struct for other modules to get warning table information
typedef struct RANGETAG_Warning_Table_Entry {
	U8 threatType;		// according to UnitType
	U8 zone;			// according to MESSAGES_SpatialZone
	S32 latitude;		// decimal degrees
	S32 longitude;	// decimal degrees
} ATTR_PACKED RANGETAG_Warning_Table_Entry;

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initialises the RANGETAG module by clearing the warning table and also
///        initialises all lower-level modules required for collision avoidance.
/// \return TRUE if successful, FALSE otherwise.
U8 RANGETAG_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function services the RANGETAG module by checking for new messages on UHF2 and UHF1
///        and updates the warning table accordingly. It also services the warning table by 
///        handling timeouts and finally, updates the warning LED's on the Mobile Radio PCB.
void RANGETAG_Service(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function places a received UHF1 Data message onto a buffer in the RANGETAG module.
///        The only relevant message type at the moment is UHF1_TYPE_RANGE_RESULT
/// \param msg [in] A pointer to the UHF1_MsgType
/// \return TRUE if the operation was successful, FAILURE if the buffer is already full.
U8 RANGETAG_PutUHF1DataMsg(UHF1_DataMsg* msg);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the Mobile Radio CA enabled state. If state is set to FALSE, all UHF2 
///         CA messages will be ignored.
/// \param  newVal The values to set CA enabled state to
void RANGETAG_SetCaEnabledState(U8 newVal);

//-------------------------------------------------------------------------------------------------
/// \brief When a Mobile Radio acts as a RANGETAG_CA_SLAVE device, this function updates an entry 
//         in the warning table based on UHF1 ranging results received from a nearby CA Master. If 
///        the device is not yet in the table, a new entry is created, but this should rarely occur
///        since UHF1 ranging will only be performed after devices are registered over UHF2.
/// \param rangeResult [in] the ranging result message containing device ID and measured distance.
void RANGETAG_TableUpdateUHF1(MESSAGES_RangeResult *rangeResult);

//-------------------------------------------------------------------------------------------------
/// \brief This function starts a ranging cycle on the Mobile Radio by sending the current 
///        warnTable entries to the RANGEALGO module.
void RANGETAG_StartRanging(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function concludes a ranging cycle on the Mobile Radio by updating the RANGETAG 
///        state and related timestamps.
void RANGETAG_ConcludeRanging(void);

//-------------------------------------------------------------------------------------------------
/// \brief When a Mobile Radio acts as a RANGETAG_CA_MASTER device, this function updates an entry 
//         in the warning table based on UHF1 ranging results.
/// \param rangingData [in] the ranging result message containing device ID and measured distance.
void RANGETAG_TableUpdateRangeAlgo(U8 *rangingData);

//-------------------------------------------------------------------------------------------------
/// \brief This function disables the RANGETAG low power mode, which will prevent NanoLOC from 
///        being shut down when zero threats are detected on UHF2. Useful in test station.
void RANGETAG_DisableLowPowerMode(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function searches for a DeviceID in the warning table. If the device is found, the
///        distance to the device is returned via an S32 pointer.
/// \param id [in] the DeviceID to search for in the warning table.
/// \param dist [out] The distance to the device, if found.
/// \return SUCCESS if DeviceID was found, FAILURE otherwise.
U8 RANGETAG_GetDeviceDist(DeviceID id, S32 *dist);

//-------------------------------------------------------------------------------------------------
/// \brief This function searches for all units of a specific type in the warning table and returns
///         the maximum signal strength of type (based on UHF2 RSSI) as a percentage value.
///         A return value of zero means no devices of specified type are being detected.
U8 RANGETAG_GetMaxSignalPercentageByType(UnitType type);

//-------------------------------------------------------------------------------------------------
/// \brief This function set whether the ranging module should be kept enabled
/// \param state [in] TRUE if it should remain enabled
void RANGETAG_ForceUHF1Enabled(U8 state);

//-------------------------------------------------------------------------------------------------
/// \brief This function finds the device in the warning table with the highest average RSSI
/// \param type [in] The device type for which to search in warning table
/// \param devId [in] A pointer to the device ID variable into which return value must be written
///         (if found)
/// \return TRUE if device of the specified type is found, FALSE otherwise
U8 RANGETAG_TableFindMostPowerfulByType(UnitType type, DeviceID *devId);

void RANGETAG_GetWarningTable(RANGETAG_Warning_Table_Entry *buffer, U8 *threatCount);

#ifdef __cplusplus
};
#endif

#endif	// _RANGETAG_H_