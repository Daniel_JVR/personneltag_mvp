/// \file captod.h
/// This is the header file of the captod module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2015/01/29
/// - Module       : CAPTOD
/// - Description  : captod interface header
///
/// \detail	Cap Lamp Test On Demand (CAPTOD) Module.
///         This is the implementation file for the CAPTOD module which runs on the Mobile
///         Radio PCB. It executes the test sequence for the Cap Lamp.
///
/// \author Wouter van Wyk (WW)
//-------------------------------------------------------------------------------------------------

#ifndef CAPTOD_H_
#define CAPTOD_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"
#include "msghandler.h"
#include "p2messages.h"
#include "messages_ptcslave.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------

// \enum TOD_TestState
typedef enum _TOD_TestState {
	TOD_STATE_INACTIVE,
	TOD_STATE_MR_UHF_TESTS,
	TOD_STATE_MR_LED_HU_TEST,
	TOD_STATE_MR_LED_EXL_TEST,
	TOD_STATE_MR_LED_LU_TEST,		
	TOD_STATE_MR_LED_VU_TEST,
	TOD_STATE_MR_INDICATOR_TEST,
	TOD_STATE_MR_LAMP_TEST,	
	TOD_STATE_PTC_LED_RED_TEST,
	TOD_STATE_PTC_LED_GREEN_TEST,
	TOD_STATE_PTC_LED_BLUE_TEST,
	TOD_STATE_PTC_VIBRATE_TEST,
	TOD_STATE_PTC_BUZZER_TEST,
	TOD_STATE_PTC_BUTTON_LEFT_TEST,
	TOD_STATE_PTC_BUTTON_RIGHT_TEST,
	TOD_STATE_PTC_LCD_TEST,
	TOD_STATE_PTC_LCD_TEST_INVERTED,	
	TOD_STATE_PTC_WAIT_RANGING,
	TOD_STATE_PTC_LOG_RESULTS,
	TOD_STATE_FAILED
} TOD_TestState;

// \enum TOD_FailTypes
typedef enum _TOD_FailTypes {
	TOD_FAIL_MR_LED_TEST,
	TOD_FAIL_MR_INDICATOR_TEST,
	TOD_FAIL_MR_LAMP_TEST,
	TOD_FAIL_PTC_LED_TEST,
	TOD_FAIL_PTC_VIBRATE_TEST,
	TOD_FAIL_PTC_BUZZER_TEST,
	TOD_FAIL_PTC_BUTTON_LEFT_TEST,
	TOD_FAIL_PTC_BUTTON_RIGHT_TEST,
	TOD_FAIL_PTC_LCD_TEST,
	TOD_FAIL_UHF2_TEST,
	TOD_FAIL_UHF1_TEST
} TOD_FailTypes;

// \enum TOD_TestResult
typedef enum _TOD_TestResult {
	TOD_RESULT_SKIPPED  = 0x00,				// Test was skipped
	TOD_RESULT_PASSED  = 0x01,	                // Test passed
	TOD_RESULT_FAILED_TIMEOUT  = 0x02,          // Test failed - timeout 
	TOD_RESULT_FAILED_USER_FEEDBACK  = 0x03,    // (PTC) Test failed due to user feedback - failed button
	TOD_RESULT_FAILED_NO_RESULT   = 0x04,       // (PTC) Test failed due to user not pressing any button
	TOD_RESULT_FAILED_NOT_AVAILABLE   = 0x05    // Test failed due to unavailable equipment, e.g. no S-Link available	
} TOD_TestResult;


//-------------------------------------------------------------------------------------------------
// Structures

//-------------------------------------------------------------------------------------------------

// \struct CAPTOD_RangeRequest
//  This message is sent to a S-Link to request ranging test
typedef struct _CAPTOD_RangeRequest {
	
} ATTR_PACKED CAPTOD_RangeRequest;

// \struct CAPTOD_FailLog
//  Log entry for test failure
typedef struct _CAPTOD_FailLog {
	U8 component;	// Component that failed
	U8 reason;		// Reason for failure
} ATTR_PACKED CAPTOD_FailLog;

// \struct CAPTOD_TestResultLog
//  Log entry for test result
typedef struct _CAPTOD_TestResultLog {
	U8  passed;			// 0: Failed, 1:Passed
	U8  rssi;			// UHF2 RSSI
	U32 range;			// UHF1 Range in cm
	DeviceID slinkId;	// ID of S-Link ranged with
} ATTR_PACKED CAPTOD_TestResultLog;

// \struct CAPTOD_RangeResponse
//  This message is sent from the S-Link to report ranging test result
typedef struct _CAPTOD_RangeResponse {
	U8 type;
	U32 range;              // Ranging result in cm
} ATTR_PACKED CAPTOD_RangeResponse;

// \struct CAPTOD_MrTestResults
//  Mobile Radio test results
typedef struct _CAPTOD_MrTestResults {
	U32  uhf1Range;				// UHF1 range in mm
	U8   uhf2Rssi;				// UHF2 RSSI
	U8   uhf1Result : 4;		// UHF1 test result
	U8   uhf2Result : 4;		// UHF2 test result
	U8   indicatorLed : 4;		// MR Indicator test result
	U8   caplampLeds : 4;		// MR LEDs test result
	U8   caplamp : 4;			// MR Cap lamp test result
	U8   spare : 4;				// Spare
	DeviceID slinkId;			// Device ID of S-Link used in ranging test
} ATTR_PACKED CAPTOD_MrTestResults;

// \struct CAPTOD_PtcTestResults
//  PTC test results
typedef struct _CAPTOD_PtcTestResults {
	U8   spare : 4;				// Spare
	U8   buzzer : 4;			// PTC buzzer test result
	U8   vibratorMotor : 4;		// PTC vibrator motor test result
	U8   lcdDisplay : 4;		// PTC lcd test result
	U8   leftSensor : 4;		// PTC left sensor test result
	U8   rightSensor : 4;		// PTC right sensor test result
	U8   backLight : 4;			// PTC backlight test result
	U8   leds : 4;				// PTC LEDs test result
} ATTR_PACKED CAPTOD_PtcTestResults;

// \struct CAPTOD_PtcTestResults
//  Cap lamp assembly test results
typedef struct _CAPTOD_AssemblyTestResults {
	U8   hasPTC;				// 0: No PTC attached, 1: PTC attached
	CAPTOD_MrTestResults  mr;	// Mobile radio test results
	CAPTOD_PtcTestResults ptc;	// PTC test results
} ATTR_PACKED CAPTOD_AssemblyTestResults;


typedef struct _CAPTOD_params {
	U8 hasPTC;							///< TRUE if the caplamp is equipped with a PTC
	TOD_TestState captodState;			///< State of the state machine
	TOD_TestState captodPreviousState;	///< Previous State of the state machine
	U32 timerRange;						///< Timer for Ranging timeout
	U32 timerKeepAlive;					///< Timer for Ranging timeout
	U32 timerTestDuration;				///< Timer used for testing the LEDs
	DeviceID slinkDeviceId;				///< Device ID of the S-Link to range with
	U8 rangeTestRunning;				///< TRUE if a ranging test is running
	U8 rangeAttempts;				///< Number of times ranging has been retried if it timed-out
	CAPTOD_AssemblyTestResults testResults;   ///< Test results
	PTCSLAVE_UpdateRequest ptcUpdateRequest;  ///< Update Request sent to the PTC
	U8 ptcInSlaveMode;					///< TRUE if PTC in slave mode
	DeviceID testAgentDevId;			///< ID of test agent to send the results to
	U8 ptcInitiated;                    ///< TRUE if test was initiated from the PTC
	U8 failIndicatorOn;                 ///< Indicate toggle state of the indicators showing that the test has failed
	U8 testFinished;
	U8 testPassed;
	U8 ignorePtcActivated;             ///< TRUE if PTC activated test must be ignored
} CAPTOD_params;

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

/// \brief This function should be called to initialize the module
void CAPTOD_Init(void);

/// \brief This function should be called in the service routine
void CAPTOD_Service(void);

/// \brief Handle ranging result from S-Link
void CAPTOD_HandleRangingResult(P2MESSAGES_ShortMessage *p2Msg, U8 rssi);

/// \brief Switch all MR LEDs off
void CAPTOD_OffMrLeds(void);

/// \brief Test whether a test is in progress
U8 CAPTOD_IsBusyTesting(void);

#ifdef __cplusplus
};
#endif



#endif /* CAPTOD_H_ */