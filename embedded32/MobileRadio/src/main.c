/// \file main.c
/// This is the implementation file of the main module for the SCASII MobileRadio
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/08/29
/// - Module        : MAIN
/// - Description   : Main for the MobileRadio application
///
/// \detail This module contains the main program loop of the MobileRadio application
///         It initializes and maintains all the other modules used on the Mobile Radio.
///
///         The Mobile Radio software is developed for the AVR32 series of micros, specifically 
///         for the UC3C device. The unit makes use of the Parsec drivers and software library as 
///         well as the AVR Software Framework (ASF).
///
/// \author Michael Manthey (MM)
/// \version $Id: main.c,v 1.30 2015-03-04 13:39:25+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include <asf.h>                ///< All drivers that have been imported from the ASF
#include "board.h"
#include "sysconfig_uc3.h"      ///< Configuration parameters
#include "sw_maintenance_uc3.h" ///< SCAS II SW maintenance module
#include "msgprocess.h"         ///< MSGPROCESS Module header file
#include "rangetag.h"           ///< RANGETAG Module for the Mobile Radio
#include "cbit.h"               ///< CBIT Module header file
#include "mrtest.h"             ///< MRTEST module header file
#include "ioctl.h"              ///< IOCTL module header file
#include <gpio.h>               ///< GPIO module header file, to toggle LEDs while in SW_MAINT mode
#include "calog.h"              ///< CALOG module header file
#include "paging.h"             ///< SCAS II Paging Module
#include "ptcstatus.h"
#include "uhf2_msghandler.h"
#include "serialhandler.h"
#include "captod.h"
#include "gps.h"
#include "lcd.h"
#include "BQ24250.h"
#include "buzzerCtrl.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define SW_UPDATE_LED_TGL_TIME_MS   TIMER_MS(250)

//-------------------------------------------------------------------------------------------------
// Global variables
ConfigBlock g_configBlock;   ///< Global configuration items of unit

//static U8 txData[5] = {0x01, 0x02, 0x03, 0x04, 0x05};
static U8 wdtReset = FALSE;

//-------------------------------------------------------------------------------------------------
// C function prototypes
void main_ConfigureRunTimeWDT(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// This function is the main loop
int main(void)
{
	static volatile U8 initStatus = FAILURE;
	U32 swUpdateTmrLed = 0;
	static U8 startupUnitType;			///< Stores DeviceID.IDByte1 as at startup
	// Disable the Watchdog timer
	wdt_disable();
	
	// Check whether the MR was restarted by means of the Watchdog timer (reset event must be logged)
	if((AVR32_PM.RCAUSE.wdt)) {
		wdtReset = TRUE;
	}


	// Initialise the Mobile Radio hardware/software modules
	initStatus = board_init();
	
	// Notify CBIT of the board initialisation result
	if((initStatus == SUCCESS) && (SYSCONFIG_IsHealthy() == TRUE)) {
		CBIT_SetBoardInitStatus(SUCCESS);
	} else {
		CBIT_SetBoardInitStatus(FAILURE);
	}
	
	
	if(initStatus == FAILURE) {
		// If board_init() was unsuccessful, flash run LED at a faster rate.
		IOCTL_SetHeartbeatMs(IOCTL_FAST_BLINK_MS);
	}
	
	// Store the Mobile Radio's Unit Type at start-up. If this changes, MR will be reset by WDT
	startupUnitType = g_configBlock.deviceID.IDByte1;
	
	// Setup WDT to reset system if services are not run for 3 seconds
	main_ConfigureRunTimeWDT();
	
	// Write a log if the Mobile Radio was restarted by means of a Watchdog timer reset
	if(wdtReset == TRUE) {
		CALOG_Write(SYSTEM_RESET_EVENT, &wdtReset, SYSTEM_RESET_LEN);
		wdtReset = FALSE;
	}
	
    // Run service routines
	while(1) {
		
		if((initStatus == FAILURE) && (MRTEST_GetState() == TEST_MODE_INACTIVE)) {
		    // MR not in test jig and init failed, do not clear WDT to achieve a reset, unless
			// an OTA update is triggered
			if(SWMAINT_IsBusy() == TRUE) {
				wdt_clear();
			}
		} else {
			// In all other cases, keep Mobile Radio running by regularly clearing WDT
			wdt_clear();
		}
		if(CAPTOD_IsBusyTesting() == FALSE) {
		    IOCTL_Service(IOCTL_SERVICE_FULL);          // Run the IOCTL module's full service routine
		} else {
		    IOCTL_Service(IOCTL_SERVICE_HEARTBEAT);     // Run only the heartbeat portion of IOCTL module's service routine
		}
		
		SERIALHANDLER_Service();
		UHF2_MSGHANDLER_Service();
		RANGETAG_Service();                 // Run the RANGETAG service                 -> Beacon_Service();    -> CA_MSG_Service();    -> UHF1_Service();
		CBIT_Service();                     // Run the CBIT service
		PAGING_Service();                   // Run the paging service to send buffered pages
		PTCSTATUS_Service();                // Run the PTCSTATUS service to send keep alive requests to PTC
		CAPTOD_Service();                   // Run the CAPTOD service
		BUZZER_Service();
		GPS_Service();
		LCD_Service();
		BQ24250_Service();
		
		
		///< Run in SWMAINT mode while an OTA update is in progress
		while(SWMAINT_IsBusy() == TRUE) {
			SWMAINT_Service();              // Only run the SW maintenance service if a download is busy
			UHF1_Service();                 // Must run UHF1_Service() aswell
			SERIALHANDLER_Service();
		    UHF2_MSGHANDLER_Service();
			
			if(TIMER_Elapsed32(swUpdateTmrLed) > SW_UPDATE_LED_TGL_TIME_MS) {
				swUpdateTmrLed = TIMER_Now32();
				gpio_toggle_pin(VU_LED_PIN);
				gpio_toggle_pin(LU_LED_PIN);
				gpio_toggle_pin(EXCL_LED_PIN);
			}
		}
		
		///< Check for changes in unit type, and reboot MCU if unit type changes
		if(g_configBlock.deviceID.IDByte1 != startupUnitType) {
			while(1);	///< WDT will cause a software reset
		}
	}
	return 0;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function configures and enables the Watchdog Timer
void main_ConfigureRunTimeWDT(void)
{
	// Disable the WDT which was enabled during board_init()
	wdt_disable();
    wdt_opt_t wdtOptions = {
	    .cssel = WDT_CLOCK_SOURCE_SELECT_RCSYS,     ///< Select the 32kHz oscillator as clock source
		.dar   = false,                             ///< After a watchdog reset, the WDT will still be enabled
		.fcd   = false,                             ///< The flash calibration will be redone after a watchdog reset
		.mode  = WDT_BASIC_MODE,                    ///< The WDT is in basic mode, only PSEL time is used.
		.sfv   = false,                             ///< WDT Control Register is not locked
		.us_timeout_period  = 3000000               ///< Time out value of 3 seconds
	};
	// Enable the 3 second run-time WDT
    wdt_enable(&wdtOptions);
}