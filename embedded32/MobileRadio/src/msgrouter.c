/// \file msgrouter.c
/// This is the implementation file of the msgrouter module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2015/01/27
/// - Module        : MSGROUTER
/// - Description   : msgrouter implementation file
///
/// \detail	msgrouter
///
///
/// \author Michael Manthey (MM)
/// \version $Id: msgrouter.c,v 1.0 2015-01-27 10:33:40+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "msgrouter.h"
#include "serialhandler.h"

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Global variables
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void MSGROUTER_RoutePacket(MESSAGES_PacketG2 *g2Packet, MSGHANDLER_RxChannel rxChannel)
{
	// Do not allow rerouting of packets over the same port as they were received
	if(rxChannel != SERIALHANDLER_GetDestSerialChannel(g2Packet->destType)) {
		MSGHANDLER_RouteG2Packet(g2Packet);
	}
}


//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 