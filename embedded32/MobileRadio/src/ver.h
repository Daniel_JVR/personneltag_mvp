/// \file ver.h
/// This is version definition file for the Mobile Radio in the SCAS II
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/08/28
/// - Module       : VERSION
/// - Description  : Version definition specific to the Mobile Radio
///
/// \details This file defines the version of the Mobile Radio software and keeps track
/// of build history.
///
/// \author Michael Manthey (MM)
/// \version $Id: ver.h,v 1.53 2015-03-05 13:53:38+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
#ifndef _VER_H_
#define _VER_H_

//-------------------------------------------------------------------------------------------------
// Defines

#define VER_SOFTWARE_DESCRIPTION_STR    "MR Software"
#define VER_MAJOR_VERSION               (3)
#define VER_MINOR_VERSION               (2)
#define VER_BUGFIX_VERSION              (0)
#define VER_BUILD_NUMBER                (0)

#define VER_UNIT_TYPE                   (UNIT_TYPE_MU)

//-------------------------------------------------------------------------------------------------
// Build History
// ---------------
// 03 March 2015
// ---------------
// Version :
//      3.2.0 Build 0
// Changed by
// WVW, MM
// Comment :
//  - Added CAPTOD (CAP lamp Test On Demand) module responsible for testing of the Cap lamp, as 
//      described in DEVMMII-33. (WVW)
//  - Includes a bug-fix with the index returned during log downloads after log wrap around has
//      occurred. Also, corrupt or unknown logs are uploaded as logs with event type 0x00 and also
//      with zero length. The Download Agent interprets an event type of 0x00 as an illegal SCAS II 
//      log
//  - CA_MSG bug-fix which affects the random staggering of responses to beacon messages 
//      on UHF2
//  - UHF2 driver updated to check the UHF2MUX module to determine whether messages must be 
//      transmitted directly or relayed to another device in a vehicle system.
//  - UHF2 driver also updated to better handle random back-off times using mini slots within
//      the response period in which random back-off applies. 
//  - No longer logs BATTERY_HIGH condition. Issue 03 (and earlier) of Mobile Radio PCB had 
//      different resistor values in the voltage divider connected to ADC pin. On these older 
//      boards, the voltage will be measured higher than the actual value. This also resolves the 
//      issue that issue 04 Mobile Radios would periodically log the BATTERY_LOW condition.
//  - Now stores beacon cycle time and beacon ignore parameters for each device in the warning 
//      table when available. 
//  - Timeout events on UHF2 are now based on the largest duration between 
//      i) the configured slaveCATimeout_s parameter and 
//      ii) three times the beacon cycle time of the remote device, if known. 
//  - During the global beacon ignore period, beacon and CAACK messages will now still be processed, 
//      albeit without sending CAM message responses to beacons from S-Links. 
//  - The UHF2 last seen timestamp of a device in the warning table is now refreshed whenever a 
//      range result message is received from the device. 
//  - Beacon ignore times can now apply to individual devices in the warning table without the 
//      global beacon ignore being activated. 
//  - Increased the size of UHF2_MSGHANDLER_LongMessageBuffer to allow reception of larger packets 
//      via fragmented UHF2 packets. 
//  - Updated UHF2_MSGHANDLER_CheckBroadcastID() function to allow broadcast to specific device 
//      types, for example 05.FF.FF.FF.FF or 05.00.00.00.00 will qualify as a broadcast ID to 
//      Miner Units. 
//  - As a precaution, the continuous resetting of the Mobile Radio PCB in the case of failed
//      board initialisation can now be disabled by triggering an OTA update. This means that
//      if a software release should ever cause board initialisation to fail, at least an OTA
//      update can still be performed to rectify the situation. 
//
// ---------------
// 26 February 2015
// ---------------
// Version :
//      3.1.0 Build 1
// Changed by
// MM
// Comment :
//  - For demonstration of PTC Distress call causing vehicles to stop. Use in conjunction with
//      MCB Slave version 3.5.0.1
//  - Included many of the changes implemented for 3.2.0.0
//
// ---------------
// 28 January 2015
// ---------------
// Version :
//      3.0.0 Build 0
// Changed by
// MM
// Comment :
//  - REMEMBER 3second hysteresis is still included on relay output since updated rangetag.c was
//      checked into RCS. Do a quick review of the changes to RANGETAG and confirm with 
//      Schauenburg whether this fixed 3 second hysteresis should be in all future releases or
//      perhaps become configurable.
//  - Updated for new internal message protocol
//  - Because UHF2 is no longer serviced in MSGPROCESS, passing of UHF2 messages to the UHF2REPEATER
//      module when DeviceID.IDByte1 == UNIT_TYPE_UHF2_REPEATER was removed, meaning UHF2 repeater
//      functionality is now disabled. It was however not used as far as I know and would probably
//      be quite dangerous to use due to the excessive levels of traffic which could be introduced.
//
//  -   THIS RELEASE HAS NOT BEEN THOROUGHLY TESTED AFTER IMPLEMENTATION OF NEW INTERNAL MESSAGE
//      PROTOCOL.
//
//
//
// ---------------
// 21 January 2015
// ---------------
// Version :
//      2.17.0 Build 1
// Changed by
// MM
// Comment :
//  - TEMPORARY RELEASE
//  - Crudely implemented a fixed 3 second hysteresis on relay output when the Mobile Radio is 
//      not configured as a Miner Unit (e.g. when configured as a VMU, LMU, HU etc.)
//
// ---------------
// 16 January 2015
// ---------------
// Version :
//      2.17.0 Build 0
// Changed by
// MM
// Comment :
//  - First official release since 2.16.0.0
//  - As with all official releases, this release will not force config parameters to defaults at 
//      start-up. 
//  - Fixed bugs in logging by improving the initialisation and management of log indexes in CALOG
//      module. This is an important update and should be deployed as soon as possible as described
//      on DEVSCASII-736.
//  - Log requests with an index below the available logs will now be responded to by returning the
//      oldest available log(s)
//
// ---------------
// 16 January 2015
// ---------------
// Version :
//      2.16.0 Build 3
// Changed by
// MM
// Comment :
//  - TEMPORARY RELEASE
//  - Sets configuration parameters to defaults at start-up and commits to DataFlash, if the values
//      are not already defaults. The defaults have been modified for use at Bathopele:
//      - MR laser on/off times: 400ms
//      - Zone 2 threshold distance: 2000cm (20m)
//
// ---------------
// 15 December 2014
// ---------------
// Version :
//      2.16.0 Build 2
// Changed by
// MM
// Comment :
//  - FOR TESTING ONLY
//  - This version shares information about the signal strength received from S-Links with an attached
//      PTC Display, which will show this information if it is loaded with the correct version (PTC 
//      Display version 3.1.0.1).
//
// ---------------
// 04 November 2014
// ---------------
// Version :
//      2.16.0 Build 1
// Changed by
// MM
// Comment :
//      - ==================================
//      - ==== USE WITH CARE ===============
//      - ==================================
//      - This release forces config block parameters to defaults during startup and sets zone 
//          boundaries and cap lamp LED on/off times according to Bathopele requirements.
//
//      - Fixed bugs in storage and management of log upload indexes (CALOG module) which could
//          result in a device logging from index position 0 each time the device restarts, leading
//          to logs being overwritten.
//      - Improved handling of manual log download requests via IrDA
//      
// ---------------
// 11 August 2014
// ---------------
// Version :
//      2.16.0 Build 0
// Changed by
// MM
// Comment :
//      - CAM messages now also contain "excluded" and "excludedGlobally" parameters (previously only
//          "deregistered" to indicate global exclusion)
//
// ---------------
// 31 July 2014
// ---------------
// Version :
//      2.15.0 Build 0
// Changed by
// MM
// Comment :
//      - Can now be configured as a UNIT_TYPE_CU (Caution Unit).
//
// ---------------
// 21 July 2014
// ---------------
// Version :
//      2.14.0 Build 0
// Changed by
// MM
// Comment :
//      - Added two new options to huRelayActuators config parameter: DISABLED and BOARD OPERATIONAL
//      - huRelayActuators parameter will now apply to all Unit Types except Miner Units
//      - The digital output (i.e. relay output) on a Hazard Unit will no longer toggle at start-up
//      - Battery low/high logs are now generated at 5 minute interval rather than 30 seconds interval
//
// ---------------
// 25 June 2014
// ---------------
// Version :
//      2.13.0 Build 0
// Changed by
// MM
// Comment :
//      - Added new RCU command: MINER_GLOBAL_DEREG, which will cause global worker exclusion of
//          any Miner Unit within the RCU's configurable zone.
//      - Updated the sending of ACKnowledge messages when an early warning/page (EWP) message is 
//          received to ensure that the ACK is transmitted each time the same EWP is received.
//
// ---------------
// 17 June 2014
// ---------------
// Version :
//      2.12.0 Build 0
// Changed by
// MM
// Comment :
//      - A keep-alive mechanism is now implemented between Mobile Radio and PTC Display.
//      - Keep-alive messages sent to PTC include signal level of nearest SAP/AID, message counter,
//          Time Sync value, and other fields for future use.
//      - Requires PTC Display version 1.3.0.0 or later due to change in messaging protocol.
//
// ---------------
// 06 June 2014
// ---------------
// Version :
//      2.11.0 Build 2
// Changed by
// MM
// Comment :
//      - For testing only. Reception of early warning/page messages will now be logged, provided 
//          that the message is not supposed to be ignored according to the ignoreDurationS 
//          parameter in the message.
//
// ---------------
// 06 June 2014
// ---------------
// Version :
//      2.11.0 Build 1
// Changed by
// MM
// Comment :
//      - For testing only. Reception of P2 messages on PORT_PAGING (0x30) is now handled by new
//          EWPHANDLER module. Early Warning/Page message format updated as per 
//          https://jira.parsec.co.za:8446/display/PRJCASII/MIMACS+II+Early+Warning+and+Paging+Message
//      - For testing of integration between Sentinel and Miner Units. To be tested with Sentinel
//          version 2.28.0.0
//
// ---------------
// 16 April 2014
// ---------------
// Version :
//      2.11.0 Build 0
// Changed by
// MM
// Comment :
//      - Mobile Radio can now be configured as a UNIT_TYPE_CA_EXTENDER to extend the collision 
//          avoidance range of other SCAS II devices around bends and in longer tunnels
//
// ---------------
// 02 April 2014
// ---------------
// Version :
//      2.10.0 Build 2
// Changed by
// MM
// Comment :
//      - Gas warnings received from Sentinel GDI will now cause reception of an early warning page
//          message to be simulated. This was added for a demonstration of the integration between
//          GDI and SCAS II while the Sentinel GDI software still needs to be updated to send gas
//          warnings in the new early warning/page format. See DEVSCASII-527.
//
// ---------------
// 02 April 2014
// ---------------
// Version :
//      2.10.0 Build 1
// Changed by
// MM
// Comment :
//      - Added handling of MESSAGES_TYPE_SYSCONFIG_INIT_STATE, for use in the test jig to verify
//          that SYSCONFIG initialised correctly, i.e. to catch corrupt config parameters
//      - Also note that since version 2.10.0.0, the latest revision of the CC1101 transceiver IC
//          is supported.
//
// ---------------
// 28 March 2014
// ---------------
// Version :
//      2.10.0 Build 0
// Changed by
// MM
// Comment :
//      - Includes paging support as per Rev0B of the paging air interface document.
//      - Can be configured as a BRAKE_TEST_UNIT (BTU) to trigger brake tests on passing vehicles
//          if enabled and configured. 
//
// ---------------
// 13 March 2014
// ---------------
// Version :
//      2.9.0 Build 1
// Changed by
// MM
// Comment :
//      - Built for a demo of paging at Anglo offices (Hexriver). DISTRESS call stored in call
//          message index 0. 
//      - Updated to support latest definition of the paging air interface (Rev0B of the paging air 
//          interface document).
//
// ----------------
// 19 February 2014
// ----------------
// Version :
//      2.9.0 Build 0
// Changed by
//  MM
// Comment :
//  - The default value for proximityZone2_cm changed from 2500cm to 3000cm, as per DEVSCASII-629.
//
// ----------------
// 19 February 2014
// ----------------
// Version :
//      2.8.0 Build 7
// Changed by
//  MM
// Comment :
//  - ==================================
//  - ==== USE WITH CARE ===============
//  - ==================================
//  - This release forces config block parameters to defaults during startup (Samancor)
//  - Zone 2 threshold defaults to 2000cm. 
//  - MR Laser ON Time defaults to 1000ms.
//  - MR Laser OFF Time defaults to 1000ms.
//
// ----------------
// 08 December 2013
// ----------------
// Version :
//      2.8.0 Build 6
// Changed by
//  MM
// Comment :
//  - Special release, forces type to UNIT_TYPE_LMU with a device ID 01.01.A5.A5.A5, and will only
//      perform ranging with a list of 15 Miner Units (device IDs identified in vehicle logs after
//      a fatal accident at Impala #14). 
//
// ----------------
// 06 December 2013
// ----------------
// Version :
//      2.8.0 Build 5
// Changed by
//  MM
// Comment :
//  - Normal release in which config parameters can again be set.
//  - Made minor updates to the "version confirm" feature. 
//  - Added support for a dataflash downloading mechanism to download parts of, or the entire 
//      dataflash content on a Mobile Radio over IrDA.
//
// ----------------
// 06 December 2013
// ----------------
// Version :
//      2.8.0 Build 4
// Changed by
//  MM
// Comment :
//  - ==================================
//  - ==== USE WITH CARE ===============
//  - ==================================
//  - This release forces config block parameters to defaults during startup. Zone 2 threshold defaults 
//      to 3000cm. 
//
// ----------------
// 07 October 2013
// ----------------
// Version :
//      2.8.0 Build 0
// Changed by
//  MM
// Comment :
//  - Updated to support global worker exclusion and global driver deregistration (see DEVSCASII-562)
//  - Updated to support SCAS I type paging with the new SCAS II PTC Display PCB.
//  - Support for ranging result messages over UHF1 has been removed.
//
// ----------------
// 29 August 2013
// ----------------
// Version :
//      2.7.2 Build 1
// Changed by
//  MM
// Comment :
//  - For demonstration purposes. If a gas warning is received, the caplamp LED will flash at 3Hz 
//      for 5 seconds. If a custom or preset page is received, the caplamp LED will stay solid on 
//      for 5 seconds.
//
// ----------------
// 29 August 2013
// ----------------
// Version :
//      2.7.2 Build 0
// Changed by
//  MM
// Comment :
//  - THIS VERSION AND PREVIOUS VERSIONS DID'T SEND AN ACK TO THE CONTROLLER FOR EACH RECEIVED PAGE
//  - For demonstration purposes. If a gas warning is received, the caplamp LED will flash at 3Hz 
//      for 5 seconds. If a custom or preset page is received, the caplamp LED will stay solid on 
//      for 5 seconds.
//
// ----------------
// 19 August 2013
// ----------------
// Version :
//      2.7.1 Build 0
// Changed by
//  MM
// Comment :
//  - For demonstration purposes, added toggling of the caplamp LED at 3x per second when gas 
//      warnings are received from a Sentinel 4 device. Gas warning are canceled when Sentinel 4
//      reports that gas levels have been restored to safe levels, otherwise it will time out in
//      60 seconds.
//  - This release also re-enabled the sending of "custom" pages when gas warnings are received
//      from Sentinel.
//
// ----------------
// 31 May 2013
// ----------------
// Version :
//      2.7.0 Build 0
// Changed by
//  MM
// Comment :
//  - Mobile Radio can now be configured as UNIT_TYPE_RCU (Region Control Unit) and can broadcast
//      commands to ENTER_DISCREET_MODE, ENTER_STOP_MODE and ENTER_CRAWL_MODE to nearby SCAS II 
//      devices, which they will process provided that they are within a configurable range from the 
//      RCU.
//  - When configured as a Miner Unit, the cap lamp laser/LED will be disabled if a command to enter
//      DISCREET_MODE is received from a nearby RCU, within the configurable distance from the RCU.
//
// ----------------
// 05 March 2013
// ----------------
// Version :
//      2.6.1 Build 1
// Changed by
//  MM
// Comment :
//  - Not an official release
//  - Once-off build for use in a demonstration, in which a "vent door Hazard Unit" will not cause
//      Miner Units to indicate the presence of a Hazard Unit by illuminating the amber LED.
//
// ----------------
// 04 March 2013
// ----------------
// Version :
//      2.6.1 Build 0
// Changed by
//  MM
// Comment :
//  - Fixed a UHF1 timeout bug on Hazard Units with relay outputs.
//  - Fixed the RSSI based prioritisation of devices when a new device is detected while the 
//      warnTable is already full.
//  - Improved the algorithm which converts the CC1101 RSSI register value into a usable integer 
//      value.
//  - Made a minor change to Hazard Unit relay toggling - if configurable parameter huRelayThreshold_cm
//      is set to 0cm, the relay toggling will be based on UHF2 RSSI values instead of ranging results
//
// ----------------
// 20 February 2013
// ----------------
// Version :
//      2.6.0 Build 1
// Changed by
//  MM
// Comment :
//  - Not an official release
//  - Once-off build for use in a demonstration, in which a "vent door Hazard Unit" will not cause
//      Miner Units to indicate the presence of a Hazard Unit by illuminating the amber LED.
//
// ----------------
// 13 February 2013
// ----------------
// Version :
//      2.6.0 Build 0
// Changed by
//  MM
// Comment :
//  - Fixed an issue which caused the PLL to start-up and lock on the incorrect frequency. At the
//      start of the application software, after the bootloader has executed, the main clock source
//      is set to the internal RC oscillator and the PLL is reset before configuring the MCU for
//      48MHz operation. The PLL now also scales the 12MHz crystal frequency up to 96MHz before 
//      dividing down to 48MHz.
//  - Updated the previously implemented PAGING module. PAGING is now a complete port of SCAS I 
//      paging software for the Miner Unit. Some minor improvements have also been included. The 
//      basic PAGING functionality has been tested and verified. Testing is currently ongoing with
//      simultaneous updates to MIMACS and the PAGING GUI application.
//
// ----------------
// 21 January 2013
// ----------------
// Version :
//      2.5.0 Build 0
// Changed by
//  MM
// Comment :
//  - If configured as a Hazard Unit, the Mobile Radio will now toggle the DIG_OUT1 pin based on 
//      the detection of specific (configurable) device types within a configurable distance. This 
//      pin output is used for toggling a relay.
//
// ----------------
// 07 September 2012
// ----------------
// Version :
//      2.4.1 Build 0
// Changed by
//  MM
// Comment :
//  - Fixed a configuration bug which could cause the UHF1 (NanoLOC) TX power to be set to its 
//      minimum. Ranging Unit and Mobile Radio UHF1 TX power will now always be set to maximum.
//  - Now logs transmission of emergency stop request and worker self-exclusion request.
// ----------------
// 04 September 2012
// ----------------
// Version :
//      2.4.0 Build 1
// Changed by
//  MM
// Comment :
//  - Specifically built for a demonstration at Impala, where a Mobile Radio configured as a Hazard 
//      Unit will trigger a relay at the rate (specified by config block parameters mrLaserOnTime_ms
//      and mrLaserOffTime_ms) when a Locomotive is detected around the HU based on UHF2 RSSI). 
//      This relay will be connected to lights mounted at vent doors.
//
// ----------------
// 04 September 2012
// ----------------
// Version :
//      2.4.0 Build 0
// Changed by
//  MM
// Comment :
//  - Expanded the PAGING module to work around bugs found on the PTC display software. Pages are
//      now buffered and sent at times when the PTC display is expected to receive the page
//      correctly.
//  - Added the Mobile Radio Self-Exclusion feature. A button attached to the Mobile Radio is
//      now configurable to perform either Emergency Stop or Worker Self Exclusion.
//
// ----------------
// 23 August 2012
// ----------------
// Version :
//      2.3.0 Build 0
// Changed by
//  MM
// Comment :
//  - Various changes to board initialisation and managing of digital IO pins to improve the 
//      testability of the Mobile Radio in the MR test jig. Test sequence also updated 
//      simultaneously.
//  - Power management is disabled when an MR is placed in a test jig
//  - WDT reset after failed initialisation is disabled when an MR is placed in a test jig
//  - Changed defaults for configBlock->nanoLocTxPwr - If this value is set to zero, it will revert
//      to the default value of 63 (max output power)
// ----------------
// 16 August 2012
// ----------------
// Version :
//      2.2.0 Build 0
// Changed by
//  RR
// Comment :
//  - Added checking on the rangeResult->legacyIndicationByte to allow SCAS I systems and SCAS II
//    systems without ranging units to be indicated
//
// ----------------
// 29 June 2012
// ----------------
// Version :
//      2.1.0 Build 0
// Changed by
//  MM
// Comment :
//  - Added a PAGING module which sends messages to an attached PTC Display when a nearby Sentinel
//      gas sensor detects an over range level of CH4 or CO gas.
//  - Added UNIT_TYPE_UHF2_REPEATER functionality by means of the new UHF2REPEATER module. User can
//      specify (in configBlock) which UHF2 DLL types must be repeated.
//  - UHF2 RSSI threshold checking is now disabled by default to allow quicker registration on UHF2.
// ----------------
// 21 June 2012
// ----------------
// Version :
//      2.0.0 Build 0
// Changed by
//  RR
// Comment :
//  - UHF1 updates
//      * nanoLOC interrupt is now enabled, this affected init.c as well
//      * Major improvements made to the handling of failed ranging resulting in a system-wide improvement
//  - RangeTag updates
//      * UNIT_TYPE_AID handled the same as fixed units
//  - MsgProcess updates
//      * MESSAGES_TYPE_VER_CONFIRM added
//  - P2MsgProcess updates
//      * Ping & time sync messages handled via AID
//  - CALOG updates
//      * Log uploads via the AID has been tested
//  - AST failure workarounds
//      * Affected ioctl.c
//      * Affected timer.c
//      * Affected uhf2.c
//      * Affected cc1101.c
//
// ----------------
// 20 May 2012
// ----------------
// Version :
//      1.6.0 Build 0
// Changed by
//  RR
// Comment :
//  - MSGHANDLER updates
//      * The improved message handler state machine now has a fail safe time out
//  - SysConfig updates
//      * The validation of new config parameters has been extended to check the entire config block
//        increasing the stability of the system by avoiding human error
//  - UHF2 updates
//      * Changed global variable to static variables. This specifically solved a problem where the
//        RX buffer head was erroneously incremented at start-up with no code specifically writing
//        to the head up to that point.
//      * Added #define UHF2_DEFAULT_CHANNEL, which is now used for setting CC1101_CHANNR register
//        value and for initialising the channelInfo struct.
//      * Minor bug fix - ensure that UHF2 packets are sent out after switching UHF2 channels
//  - CA Log updates
//      * Bug fixes in the clearing of logs
//      * Multiple erase types added
//      * AID requests for log uploads implemented, still needs to be tested
//
// ---------------
// 08 May 2012
// ---------------
// Version :
//      1.5.3 Build 0
// Changed by
// MM
// Comments :
//  - Increased IrDA data buffer sizes for new UART driver
//  - Changes made to decrease the power usage of the Mobile Radio. NanoLOC is now switched off
//      whenever the warning table is empty or only test units are in the table and ranging with
//      these test units has been performed successfully. Green debug LED is now no longer toggled
//      each time UHF1 ranging results are received, but rather very briefly pulsed to use less power.
//  - MSGPROCESS now uses the new MSGHANDLER which features multiple parser channels.
//  - UHF2 and CC1101 were further improved by making minor changes suggested by CC1101 errata, and
//      fixing a couple of small bugs.
//  - CC1101 now goes from RX to IDLE when a packet is received, as it did in versions before v1.3.0.0.
//      This was done to catch a specific case only confirmed/seen on the Schauenburg Debug Interface, where
//      corrupt packages were received over UHF2 during times of heavy simultaneous TX and RX communications.
//  - UART driver was considerably changed and improved to resolve issues where duplicate/corrupt data was
//      sometimes received. The DMA mechanism on both the RX and TX side was updated. Updated the RX side to
//      utilise a 50/50 switch of reloads. Transmit side now uses a single circular buffer and does not disable
//      PDCA in the UART_TxData() function. Tested by successfully transmitting over a million 10-byte long
//      messages without lost/duplicated/corrupt packets.
//
// ---------------
// 20 March 2012
// ---------------
// Version :
//      1.5.2 Build 0
// Changed by
// MM
// Comments :
//  - IRDAHANDLER bug fixed: The delay inserted before switching from TX mode to RX mode was
//    effectively shortened in the previous release due to the use of the 48MHz clock as source for
//    the TIMER module. This caused some devices to fail the test sequence due to an IrDA fault.
//    The delay has now been made proportional to the TIMER source clock frequency.
//  - MSGPROCESS bug fix: When RANGEALGO was initially added to Mobile Radio software, MSGPROCESS
//    had to process messages destined for MESSAGES_DEST_SLAVE. This requirement was later
//    removed with the addition of MESSAGES_DEST_INTERNAL, but MESSAGES_DEST_SLAVE was still
//    processed in MSGPROCESS. This will lead to OTA updates destined for an MCB Slave to also be
//    loaded onto Mobile Radios, and it was therefore removed.
//  - Added handling of CA_MSG_LMU_BEACON and CA_MSG_LMU_CAM which will be used in wireless
//    locomotive systems.
//  - Other software library changes made while implementing the wireless locomotive software. This
//    should have a minimal effect on the Mobile Radio software.
// ---------------
// 06 March 2012
// ---------------
// Version :
//      1.5.1 Build 3
// Changed by
// MM
// Comments :
//  - Same as version 1.5.1.3 with the following changes to enable production testing of boards with
//      AST failures:
//      * Fixed LED start-up sequence to accommodate the HU and EXCL warning LED switch requested by
//        client.
//      * Activated software alternative to Hardware AST by making changes in timer.h and timer.c:
//          - #define TIMER_CLK_HZ      (48000000)
//          - #define TIMER_PRESCALER   (128)
//          - #define TIMER_CLK_SRC     TC_CLOCK_SOURCE_TC5  ---> connected to fPBA / 128
//          - timerLib.calMode = SW_CALENDAR;
// ---------------
// 06 March 2012
// ---------------
// Version :
//      1.5.1 Build 2
// Changed by
// MM
// Comments :
//  - A fresh build with various files modified in the software lib, but no major changes to the
//      Mobile Radio's functionality. (Mostly changes for Mponeng Loco system and Impala VDU bug fixes)
//  - Handling of MESSAGES_TEST_MR_LEDS updated to reflect the HU and EXCL warning LED switch
//      requested by client.
// ---------------
// 29 February 2012
// ---------------
// Version :
//      1.5.1 Build 1
// Changed by
// MM
// Comments :
//  - Updated the way Test Units are handled. Mobile Radio will stop sending CAM messages to TU
//      once the TU successfully ranged with the Mobile Radio. TU will not time out of zone 2, only
//      UHF2 timeouts are applied.
//  - Mobile Radio will now also turn on vehicle LED in response to a UNIT_TYPE_TU_UHF2 beacon,
//      without sending a CAM to the UNIT_TYPE_TU_UHF2. UNIT_TYPE_TU_UHF2 also does not perform
//      ranging.
// ---------------
// 22 February 2012
// ---------------
// Version :
//      1.5.1 Build 0
// Changed by
// MM
// Comments :
//  - Watchdog Timer bug was fixed in wdt4.c - The WDT is now also used during initialisation
//      to reset the MCU if an initialisation step takes more than 8 seconds.
//  - Updated LED handling for UNIT_TYPE_HU (Hazard Unit) - STATUS LED on HU enclosure blinks as a
//      heartbeat LED and caplamp laser is only serviced if unit type is UNIT_TYPE_MU (Miner Unit).
//  - Added a short LED blinking sequence at start-up, before CALOG_Init(), when configured as
//      UNIT_TYPE_HU. This results in the HU enclosure's STATUS LED blinking at start-up to indicate
//      that a working board is installed and is about to perform initialisation.
// ---------------
// 20 February 2012
// ---------------
// Version :
//      1.5.0 Build 2
// Changed by
// MM
// Comments :
//  - Changed two conditional statements to be more precise when checking UHF1 range result messages
//      for worker exclusion and driver deregistration status (in rangetag.c).
// ---------------
// 17 February 2012
// ---------------
// Version :
//      1.5.0 Build 1
// Changed by
// MM
// Comments :
//  - See DEVSCASII-124: Switched HU and Excl LED's to achieve warning indication as set out in Impala
//      standard:
//          Red - Vehicle
//          Blue - Loco
//          Amber - Driver de-registration
//          Yellow - Hazard unit
// ---------------
// 15 February 2012
// ---------------
// Version :
//      1.5.0 Build 0
// Changed by
// MM
// Comments :
//  - Device class is now selected at start-up as either a collision avoidance master or slave
//      (RANGETAG_CA_MASTER or RANGETAG_CA_SLAVE)
//  - BEACON module and RANGEALGO module is initialised and serviced if device class is RANGETAG_CA_MASTER
//  - CA_MSG_CAM messages are now also handled
//  - Ranging is performed with all collision avoidance masters and slaves, and ranging result
//      messages are transmitted (ONLY OVER UHF2) to all ranging slaves (i.e. Miner Units)
//  - MSGPROCESS now handles MESSAGES_TYPE_RET_RANGING_START, MESSAGES_TYPE_RET_RANGING_ENTRY,
//      MESSAGES_TYPE_RET_RANGING_END, MESSAGES_TYPE_REQ_LOGS, MESSAGES_TYPE_CLR_LOGS
//  - Ranging states are now defined to keep track of ranging cycles.
//  - When setting Mobile Radio up as TU/VMU/LMU unit types, the UHF2 transmit power will be
//      adjusted according to the value of g_configBlock.relTxPower
//  - If unit type is TU/LMU/VMU, Miner Units will only be added to warning table if their RSSI
//  - is initially over threshold.
//  - Toggle the HU LED when a TU beacon is received
//  - Vehicle warning LED is now also switched on when a Test Unit (TU) is in range
//  - Updated version display sequence to use different LED colours for major, minor, bug-fix, an build
//      numbers each time the version changes
//  - NanoLOC's 2.5V regulator nSHDN pin now held low from start-up of board until UHF1_Init().
//  - UHF2 no longer reinitialises periodically when there is radio silence
// ---------------
// 07 February 2012
// ---------------
// Version :
//      1.4.0 Build 0
// Changed by
// MM
// Comments :
//  - Added handling of MESSAGES_TYPE_RANGE_RESULT, sent from vehicle units (or any other ranging
//      master) to Mobile Radios over UHF2. This is the first phase of moving range result messages
//      from UHF1 to UHF2.
//  - UHF2 TX buffer size now increased from 8 to 50 entries.
//  - HU LED no longer flashes when any UHF2 packet is received, but only if Test Unit (TU) beacons
//      are received.
//  - Added init_SmartStart() function to keep nanoLOC 2V5 regulator in shutdown until UHF1_Init()
// ---------------
// 03 February 2012
// ---------------
// Version :
//      1.3.0 Build 0
// Changed by
// MM
// Comments :
//  - DEREG LED now blinks when worker excluded. Used to be solid ON when worker excluded.
//  - UHF2 now uses a more advanced transmit queue which allows removal of the "blocking" TX_WAIT state in
//      the UHF2 state machine. Previously, messages would not be transmitted while another message was
//      waiting to be transmitted.
//  - Implemented a simple retry mechanism (random back-off type) for retransmitting messages if clear channel
//      assessment fails while using the new transmit queue. The mechanism is not perfect yet but works.
//      (tx_queue sorting is not performed when the txTimestamp is updated in the retry mechanism)
//  - Added a short 50us delay after each SIDLE strobe command.
//  - Implemented a new mechanism for servicing CC1101 interrupts. The interrupt will only be serviced (i.e. RX FIFO
//      will get read) if the MCU is not busy with an SPI transaction to the CC1101. If an SPI transaction is in progress
//      when the interrupt triggers, a counter gets incremented, and as soon as the SPI transaction completes, the counter
//      is checked. If necessary, the RX FIFO is serviced on the CC1101.
//  - Renamed some UHF2 state machine states, now using UHF2_TX_PREPARE and UHF2_TX_EXECUTE.
//  - CC1101 now goes into RX mode after receiving a packet (used to be IDLE). This improves the CC1101 RX
//      performance under heavy UHF2 traffic since the chip is not IDLE while the RX FIFO gets read. UHF2_Service()
//      also no longer needs to strobe the CC1101 into RX state every time it executes, which reduces the total
//      number of SPI transactions to the chip.
//  - FIFO under/overflow state checking now only done every 20th time the UHF2_Service() runs, to reduce the total
//      number of SPI transactions to the chip.
//  - Interrupt is now on rising edge on function 0x0F (see CC1101 datasheet, table 41)
//  - Renamed states TX_HIGH to TX_PREPARE and TX_NORMAL to TX_EXECUTE.
//  - USPI Driver change: DMA Rx interrupt handlers added as a failsafe. Bug fix in USPI_MasterTransmitDataNoRx.
//      Driver now requires USPIx_PDCA_RX_IRQ define.
//  - Updated CA_MSG module to use new UHF2 transmit functions.
//  - Updated the logging downloads, its a bit smarter now! (RR)
//  - Logging messages changed in length.
//  - Logging Indexes moved out of the Flash User Page, log format now includes index.
//  - Dataflash module is pulled out of reset at a slower rate. This GPIO MUST be initialised LOW in the
//      processors initialisation!
// ---------------
// 17 January 2012
// ---------------
// Version :
//      1.2.0 Build 0
// Changed by
// MM
// Comments :
//      - Added MESSAGES_TYPE_DISPLAY_VER. When an MR receives the messages, it briefly displays it's
//        software version by toggling the warning LED's. This feature is useful when doing OTA
//        updates in a lamproom.
//      - UHF2 and CC1101 are now reinitialised from time to time when the interrupt is not triggered
//        for 5s or 60s, depending on whether the radio has been idle for a while or not. This was
//        added to resolve a known issue
//      - The number of CC1101_ReadFifo() function calls inside the CC1101 interrupt service routine
//        was reduced, which effectively reduces the number of SPI transactions in the ISR. This
//        was done to resolve an issue where heavy UHF2 traffic would cause the microcontroller to
//        hang up completely.
//      - CC1101_Init() function updated to more closely follow the instructions in the datasheet.
//      - UHF2 and CA_MSG modules were updated to implement the improved BEACON and CAM message
//        structures, which allow tracking of a large number of radios in for example a lamproom.
//        BEACON messages now contain fields that specify the beacon cycle time, etc. and  enables
//        the Mobile Radio (or other device) to calculate a time slot in which to reply (based on
//        device ID).
//      - Minor change to dataflash memory structure
// ---------------
// 30 November 2011
// ---------------
// Version :
//      1.1.2 Build 0
// Changed by
// MM
// Comments :
//      - Uses new sw_maintenance_uc3 with timeout bug fix
//      - Made warning LED's blink during SW updates
// ---------------
// 23 November 2011
// ---------------
// Version :
//      1.1.1 Build 0
// Changed by
// MM
// Comments :
//      - CC1101 SPI interface now running at 10MHz
//      - Changed default value of beaconCycleTime_ms to 500.
//      - As a temporary fix for the problem that SCAS II devices hang-up during heavy UHF2 traffic,
//        the WDT (WATCHDOG TIMER) is now enabled to reset the MCU after 3 seconds pass without
//        running services. The reset event is logged at start-up.
//      - Changed CC1101 interrupt edge back to FALLING_EDGE, since this proved to yield more reliable
//        OTA communication. According to CC1101 configuration the interrupt should be on rising edge.
//        This issue should be investigated.
//      - Minor changes to IOPHY, IOCTL and MSGPROCESS to allow testing of LED's in bed of nails
// ---------------
// 18 November 2011
// ---------------
// Version :
//      1.1.0 Build 0
// Changed by
// MM
// Comments :
//      - Software updated for second roll-out to Impala shaft #14
//      - Added data whitening
//      - Added UHF2_MSGHANDLER functionality
//      - Fixed hwdefs.h to swop around D3 and D4 as originally designed in schematics
//      - Removed hardcoding of default configBlock variables in init.c
//      - CA_MSG messages are no longer buffered in msgprocess.c, but rather in ca_msg.c
//      - Changed the way CA visibility is managed. RANGETAG module ignores CA_MSG messages when invisible
// ---------------
// xx XXXX 2011
// ---------------
// Version :
//      1.0.0 Build 0
// Changed by
// MM
// Comment :
//      - SCAS II 32-bit baseline
//-------------------------------------------------------------------------------------------------
#endif //_VER_H_
