/// \file ptcstatus.c
/// This is the implementation file of the ptcstatus module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/06/10
/// - Module        : PTCSTATUS
/// - Description   : ptcstatus implementation file
///
/// \detail	The PTCSTATUS module is responsible for managing a keep-alive protocol which runs
///         between the Mobile Radio and the PTC Display. Some additional information is shared
///         between the two devices in the keep-alive request and response messages, such as message
///         counters, date and time updates, and so on.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: ptcstatus.c,v 1.3 2015-03-02 14:17:59+02 woutervw Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "ptcstatus.h"
#include "common.h"
#include "timer.h"
#include "ptcctrl.h"
#include "rangetag.h"
#include "paging.h"
#include "msghandler.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define PTCSTATUS_COMMS_TIMEOUT_MS      (1000)      // Assume PTC awol after 1 second of no comms
#define PTCSTATUS_BEACON_INTERVAL_MS    (300)       // Send a keep alive request to PTC Display at 300ms interval

#define PTC_MIN_VERSION_REQUIRED        {1, 3, 0, 0}

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
// \struct PTCSTATUS_RuntimeVariables
typedef struct _PTCSTATUS_RuntimeVariables {
	U8 sequence;
	Version minExpectedVersion;
	U32 beaconTxTimestamp;
	U32 lastRxTimestamp;
	
	U8 ptcPresent;              // Boolean indicating whether PTC is believed to be present based on comms
} ATTR_PACKED PTCSTATUS_RuntimeVariables;

//-------------------------------------------------------------------------------------------------
// Global variables
//-------------------------------------------------------------------------------------------------
static PTCSTATUS_RuntimeVariables ptcLib = {0};


//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
static void ptcstatus_TransmitKeepAliveRequest(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void PTCSTATUS_Init(void)
{
	// Initialise all fields in the runtime variables struct
	ptcLib.sequence = 0;
	Version tempVersion = PTC_MIN_VERSION_REQUIRED;
	ptcLib.minExpectedVersion = tempVersion;
	ptcLib.beaconTxTimestamp = TIMER_Now32();
	ptcLib.lastRxTimestamp = TIMER_Now32();
	ptcLib.ptcPresent = FALSE;                          // Assume false until comms is established
}

//-------------------------------------------------------------------------------------------------
void PTCSTATUS_Service(void)
{
	// Check whether it is time to send a keep alive request (beacon)
	if(TIMER_Elapsed32(ptcLib.beaconTxTimestamp) > TIMER_MS(PTCSTATUS_BEACON_INTERVAL_MS)) {
		// Send a beacon
		ptcstatus_TransmitKeepAliveRequest();
		// Update the timer
		ptcLib.beaconTxTimestamp = TIMER_Now32();
	}
	
	// Check whether comms timeout has elapsed
	if((ptcLib.ptcPresent == TRUE) && (TIMER_Elapsed32(ptcLib.lastRxTimestamp) > TIMER_MS(PTCSTATUS_COMMS_TIMEOUT_MS))) {
		// Comms timeout occurred
		ptcLib.ptcPresent = FALSE;
	}
}

//-------------------------------------------------------------------------------------------------
void PTCSTATUS_MessageProcess(MESSAGES_PacketG2 *packet)
{
	PTCCTRL_MSG_KeepAliveRes *response;
	
	switch(packet->msgType) {
		case MESSAGES_TYPE_PTC_KEEP_ALIVE_REQ:
		    // This message is usually transmitted to the PTC and not received by the Mobile Radio
			break;
			
		case MESSAGES_TYPE_PTC_KEEP_ALIVE_RES:
		    // Process the Keep Alive Response
			response = (PTCCTRL_MSG_KeepAliveRes*)&(packet->data[0]);
			ptcLib.lastRxTimestamp = TIMER_Now32();
			ptcLib.ptcPresent = TRUE;
			if(g_configBlock.hasPtc == FALSE) {
				g_configBlock.hasPtc = TRUE;
				// Save to FLASH
				SYSCONFIG_UpdateConfigBlock((U8*)&g_configBlock);
			}
			break;
		
		default:
		    break;
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function transmits a beacon to the PTC Display
void ptcstatus_TransmitKeepAliveRequest(void)
{
	// Build a keep alive request (beacon) message
	PTCCTRL_MSG_KeepAliveReq reqMsg;
	
	// Set all the relevant fields of the request message
	reqMsg.seq = ptcLib.sequence;
	reqMsg.unixTimestamp = TIMER_GetUnixTime();
	reqMsg.minExpectedVersion = ptcLib.minExpectedVersion;
	reqMsg.commsTimeoutMs = PTCSTATUS_COMMS_TIMEOUT_MS;
	reqMsg.commsHealthy = ptcLib.ptcPresent;
	U8 maxSignal = max(RANGETAG_GetMaxSignalPercentageByType(UNIT_TYPE_AID),
	                   RANGETAG_GetMaxSignalPercentageByType(UNIT_TYPE_SAP));
	reqMsg.uplinkSignalPercentage = maxSignal;
	reqMsg.inboxCount = PAGING_GetPendingDisplayMessageCount();
	reqMsg.outboxCount = PAGING_GetQueuedP2MessageCount();
	
	// Send the message
	////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC,
	////                            MESSAGES_TYPE_PTC_KEEP_ALIVE_REQ,
	////							sizeof(PTCCTRL_MSG_KeepAliveReq),
	////							(U8*)&reqMsg);
	
	
}

//-------------------------------------------------------------------------------------------------
U8 PTCSTATUS_PTCIsPresent(void)
{
	return ptcLib.ptcPresent;
}
