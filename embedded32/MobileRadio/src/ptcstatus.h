/// \file ptcstatus.h
/// This is the header file of the ptcstatus module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/06/10
/// - Module        : PTCSTATUS
/// - Description   : ptcstatus header file
///
/// \detail	The PTCSTATUS module is responsible for managing a keep-alive protocol which runs
///         between the Mobile Radio and the PTC Display. Some additional information is shared
///         between the two devices in the keep-alive request and response messages, such as message
///         counters, date and time updates, and so on.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: ptcstatus.h,v 1.2 2015-02-04 08:49:06+02 woutervw Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _PTCSTATUS_H_
#define _PTCSTATUS_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initialises runtime variables in the PTCSTATUS module
void PTCSTATUS_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function services the PTCSTATUS module to check for comms timeouts and to send
///         beacons to PTC Display periodically.
void PTCSTATUS_Service(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function processes messages received from the PTC Display
/// \param rxMsg [in] A pointer to the received MESSAGES_Packet.
void PTCSTATUS_MessageProcess(MESSAGES_PacketG2 *packet);

//-------------------------------------------------------------------------------------------------
/// \brief This function indicates whether a PTC Display is present
U8 PTCSTATUS_PTCIsPresent(void);

#ifdef __cplusplus
};
#endif

#endif	// _PTCSTATUS_H_