/// \file paging.h
///
/// \details For testing/demonstration purposes only

/// \author Michael Manthey (MM)
/// \version $Id: paging.h,v 1.11 2015-01-27 09:16:00+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _PAGING_H_
#define _PAGING_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"       ///< SCAS II Message types
#include "ptcctrl.h"
#include "p2messages.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
/// Gas types reported by Sentinel devices in the old format of sending gas warnings to nearby
/// devices.
typedef enum _PAGING_GasSensorTypes {
	PAGING_GAS_TYPE_H2S     = 0x03,
	PAGING_GAS_TYPE_CO      = 0x04,
	PAGING_GAS_TYPE_CH4     = 0x05,
	PAGING_GAS_TYPE_O2      = 0x06,
	PAGING_GAS_TYPE_NO      = 0x07,
	PAGING_GAS_TYPE_NO2     = 0x08,
} PAGING_GasSensorTypes;

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initialises the PAGING module.
/// \return Current implementation always returns SUCCESS
U8 PAGING_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function services the paging buffer and sends pages to the PTC display when it
///         expects the PTC display to receive it correctly (i.e. when the known bugs on the PTC 
///         display are not expected to cause problems)
void PAGING_Service(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function processes messages received from the PTCCTRL module on the PTC Display
/// \param rxMsg [in] A pointer to the received MESSAGES_TYPE_PTC_PROTOCOL message payload.
void PAGING_MessageProcess(MESSAGES_PacketG2 *packet);

//-------------------------------------------------------------------------------------------------
/// \brief This function processes paging messages received over UHF2. 
void PAGING_P2PageMessageReceived(P2MESSAGES_ShortMessage *p2PageMsg);

//-------------------------------------------------------------------------------------------------
/// \brief This function should be called when a user responds to a displayed message
void PAGING_UserResponseReceived(PTCCTRL_MSG_UserResponse *response);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the number of pages ready to be sent to PTC Display (i.e. "inbox"
///         count).
U8 PAGING_GetPendingDisplayMessageCount(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the number of paging related messages queued to be sent to the
///         Paging Agent, i.e. the "outbox" count.
U8 PAGING_GetQueuedP2MessageCount(void);


#ifdef __cplusplus
};
#endif

#endif	// _PAGING_H_