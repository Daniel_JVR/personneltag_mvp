/// \file serialhandler.c
/// This is the implementation file of the serialhandler module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2015/01/27
/// - Module        : SERIALHANDLER
/// - Description   : serialhandler implementation file
///
/// \detail	serialhandler
///
///
/// \author Michael Manthey (MM)
/// \version $Id: serialhandler.c,v 1.0 2015-01-27 11:11:12+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "serialhandler.h"

#include "uart_uc3.h"           ///< UC3 series UART driver
#include "irdahandler.h"        ///< IrDA handler for the UC3 series
#include "uhf1.h"               ///< UHF1 driver header file
#include "uhf2_msghandler.h"    ///< UHF2 message handler
#include "scasdbg.h"

//-------------------------------------------------------------------------------------------------
// Defines
//-------------------------------------------------------------------------------------------------
#define SERIALHANDLER_BUFFER    (1024)

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Global variables
//-------------------------------------------------------------------------------------------------
static MESSAGES_Packet rxPacketIrda;                ///< IrDA RxPacket
static MESSAGES_PacketG2 rxPacketIrdaG2;            ///< IrDA RxPacket
static MESSAGES_Packet rxPacketPtcUart;             ///< PTC UART RxPacket
static MESSAGES_PacketG2 rxPacketPtcUartG2;         ///< PTC UART RxPacket

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
U8 SERIALHANDLER_Init(void)
{
	// Init PTC UART, also used by test station
	if(UART_Init(PTC_UART, PTC_BAUDRATE) == FAILURE) {
		return FAILURE;
	} else {
		// Enable the dead time ISR since the interim PTC parser expects full packets from the PTC
		UART_EnableRxDeadTimeIsr(PTC_UART);
	}
	
	// Init IrDA UART 1
	if(IRDAHANDLER_Init() == FAILURE) {
		return FAILURE;
	}

	// Optionally, enable RxDeadTime ISR for IrDA port
	//UART_EnableRxDeadTimeIsr(IRDA_UART);

	// Enable the message parsers
	MSGHANDLER_EnableParser(MSGHANDLER_RX_CHAN_IRDA, &rxPacketIrda, &rxPacketIrdaG2);
	MSGHANDLER_EnableParser(MSGHANDLER_RX_CHAN_UART_PTC, &rxPacketPtcUart, &rxPacketPtcUartG2);

    return SUCCESS;
}

//-------------------------------------------------------------------------------------------------
void SERIALHANDLER_Service(void)
{
	U8 rxBuffer[SERIALHANDLER_BUFFER];                      ///< Receive buffer
	U16 rxLength;                                           ///< Received length
	U8 status;                                              ///< Status variable for function returns

	// Check the queues of all the interfaces

	/// Check the UHF1 queue first
	while(UHF1_IsReceivedBufEmpty() == FALSE) {
		UHF1_DataMsg rxMsgUHF1;
		/// Get the received message from the UHF1 message buffer
        UHF1_GetReceivedMsg(&rxMsgUHF1);

        switch(rxMsgUHF1.msgType) {
            case UHF1_TYPE_RANGE_RESULT:
			    // UHF1 range result message no longer supported

				break;
			case UHF1_TYPE_TEST_ROUNDTRIP:
			    /// A remote device replied to a UHF1_TYPE_TEST_ROUNDTRIP message from this
				/// specific Mobile Radio. Notify the test equipment that the roundtrip is complete
				////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC, MESSAGES_TEST_ROUNDTRIP_UHF1,
				////                          MESSAGES_LEN_ROUNDTRIP_UHF1, &(rxMsgUHF1.payload[0]));
			    break;
			case UHF1_TYPE_TEST_RANGING_RESULTS:
			    /// A remote device performed a ranging sequence with this specific Mobile Radio, and
				/// sent the ranging results. Notify the test equipment of the results achieved.
				////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC, MESSAGES_TEST_RANGING_RESULTS,
				////                          MESSAGES_LEN_RANGING_RESULTS, &(rxMsgUHF1.payload[0]));

		    default:
			    /// Do nothing. Only range result messages defined so far...
				break;
		}
	}


	/// Check the IrDA queue second
    // If there is a new byte available send it to the parser
    while(!UART_RxBufEmpty(IRDA_UART)) {
        rxLength = UART_RxBufLen(IRDA_UART);

		if(rxLength < SERIALHANDLER_BUFFER) {
			status = UART_GetRxData(IRDA_UART, (U8*)&rxBuffer, rxLength);
			//IRDAHANDLER_TxData((U8*)&rxBuffer, rxLength);
			status = MSGHANDLER_Parser(MSGHANDLER_RX_CHAN_IRDA, (U8*)&rxBuffer, rxLength);

		} else {
			status = UART_GetRxData(IRDA_UART, (U8*)&rxBuffer, SERIALHANDLER_BUFFER);
			status = MSGHANDLER_Parser(MSGHANDLER_RX_CHAN_IRDA, (U8*)&rxBuffer, SERIALHANDLER_BUFFER);
		}
    }


	/// Check the PTC UART queue third
    // If there is a new byte available send it to the parser
    while(!UART_RxBufEmpty(PTC_UART)) {
        rxLength = UART_RxBufLen(PTC_UART);
		
		if(rxLength < SERIALHANDLER_BUFFER) {
			status = UART_GetRxData(PTC_UART, (U8*)&rxBuffer, rxLength);
			// Even though messages received from current PTC Display are not in the SCAS II message
			// format, parser should be called for PTC_UART use in the Mobile Radio test station
			status = MSGHANDLER_Parser(MSGHANDLER_RX_CHAN_UART_PTC, (U8*)&rxBuffer, rxLength);
		} else {
			status = UART_GetRxData(PTC_UART, (U8*)&rxBuffer, SERIALHANDLER_BUFFER);
			// Even though messages received from current PTC Display are not in the SCAS II message
			// format, parser should be called for PTC_UART use in the Mobile Radio test station
			status = MSGHANDLER_Parser(MSGHANDLER_RX_CHAN_UART_PTC, (U8*)&rxBuffer, SERIALHANDLER_BUFFER);
		}
    }
}

//-------------------------------------------------------------------------------------------------
void SERIALHANDLER_SendPacket(U8 *packet, U16 length, MESSAGES_Dest destination)
{
	switch(SERIALHANDLER_GetDestSerialChannel(destination)) {
		case MSGHANDLER_RX_CHAN_IRDA:        ///< Transmit a packet out via the IrDA port
		    // Manage IrDA direction, transmit using IrDA UART
			IRDAHANDLER_TxData(packet, length);
		    break;

		case MSGHANDLER_RX_CHAN_UHF2:        ///< Transmit via UHF2 as a BROADCAST
            UHF2_MSGHANDLER_TransmitUHF2Packet(destination, length, packet, 0xFF);
		    break;

		case MSGHANDLER_RX_CHAN_UART_PTC:
		    UART_TxData(PTC_UART, packet, length);
            break;
			
		default:
            // Do nothing
            break;
    }
}

//-------------------------------------------------------------------------------------------------
MSGHANDLER_RxChannel SERIALHANDLER_GetDestSerialChannel(MESSAGES_Dest dest)
{
	switch(dest) {
		case MESSAGES_DEST_IRDA:        ///< Transmit a packet out via the IrDA port
		    return MSGHANDLER_RX_CHAN_IRDA;
		    break;

		case MESSAGES_DEST_UHF2:        ///< Transmit via UHF2 as a BROADCAST
		    return MSGHANDLER_RX_CHAN_UHF2;
		    break;

		case MESSAGES_DEST_MR_PTC:
		case MESSAGES_DEST_MR_LCD:
		    return MSGHANDLER_RX_CHAN_UART_PTC;
            break;
			
		default:
            return MSGHANDLER_RX_CHAN_UNKNOWN;
            break;
    }
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 