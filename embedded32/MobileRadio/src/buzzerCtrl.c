/// \file buzzerCtrl.h
/// This is the header file of the buzzer control for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2015/09/02
/// - Module        : buzzerCtlr
/// - Description   : Buzzer Control header file
///
/// \detail	GPS
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "buzzerCtrl.h"
#include "scasdbg.h"

#include "serialhandler.h"
#include "hwdefs.h"
#include "timer.h"
#include "ioctl.h"
#include "iophy.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define SERIALHANDLER_BUFFER    (255)

//-------------------------------------------------------------------------------------------------
// Enums
// none

typedef enum {
	BUZZER_SATE_IDLE = 0,
	BUZZER_STATE_OFF,
	BUZZER_STATE_CAUTION,
	BUZZER_STATE_WARNING,
	BUZZER_STATE_CRITICAL
	
	}BUZZER_state;

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Local variables                                       ///< Received length
BUZZER_state buzzer_CurrentState = BUZZER_STATE_OFF;

U8	buzzer_BeepCnt = 0;

U32 digOut1Ts = 0;
//-------------------------------------------------------------------------------------------------
// Global variables

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes


//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------

//U32 digOut1Ts;
//digOut1Ts = TIMER_Now32();

//TIMER_Elapsed32( digOut1Ts) > TIMER_MS(GAS_WARNING_BLINK_MS)
//
//IOPHY_DigitalOut1_Buzz(IOState state);
//void IOPHY_DigitalOut1Toggle_Buzz(void);

void BUZZER_Service(void)
{
	switch (buzzer_CurrentState)
	{
		case BUZZER_SATE_IDLE:
			//do nothing
			break;
			
		case BUZZER_STATE_OFF:
			//SCASDBG_PrintAscii("BUZZER_STATE_OFF \r\n ");
			IOPHY_DigitalOut1_Buzz(IO_OFF);
			buzzer_CurrentState = BUZZER_SATE_IDLE;
			break;
		
		case BUZZER_STATE_CAUTION:
			if (TIMER_Elapsed32( digOut1Ts) > TIMER_MS(250))
			{
				//SCASDBG_PrintAscii("BUZZER_STATE_CAUTION \r\n ");
				IOPHY_DigitalOut1_Buzz(!(buzzer_BeepCnt % 2));
				// Incrament the beep counter
				buzzer_BeepCnt++;
				// Get current time for time difference calc
				digOut1Ts = TIMER_Now32();
				// have we beeped twice?
				if(buzzer_BeepCnt > 4)
				{
					// clear the beep counter
					buzzer_BeepCnt = 0;
					// switch buzzer off
					buzzer_CurrentState = BUZZER_STATE_OFF;
				}
			}

			break;
		
		case BUZZER_STATE_WARNING:
			if (TIMER_Elapsed32( digOut1Ts) > TIMER_MS(500))
			{
				//SCASDBG_PrintAscii("BUZZER_STATE_WARNING \r\n ");
				IOPHY_DigitalOut1Toggle_Buzz();
				// Get current time for time difference calc
				digOut1Ts = TIMER_Now32();
			}
			break;
		
		case BUZZER_STATE_CRITICAL:
			if (TIMER_Elapsed32( digOut1Ts) > TIMER_MS(100))
			{
				//SCASDBG_PrintAscii("BUZZER_STATE_CRITICAL \r\n ");
				IOPHY_DigitalOut1Toggle_Buzz();
				// Get current time for time difference calc
				digOut1Ts = TIMER_Now32();
			}
			break;
		
		default:
			//SCASDBG_PrintAscii("default \r\n ");
			// unknown state so switchbuzzer off
			buzzer_CurrentState = BUZZER_STATE_OFF;
			break;
	}
}

//void BUZZER_CycleStates(void)
//{
	//buzzer_CurrentState++;
	//
	//if(buzzer_CurrentState == BUZZER_STATE_OFF)
	//{
		//buzzer_CurrentState++;		
	//}
	//if(buzzer_CurrentState > BUZZER_STATE_CRITICAL)
	//{
		//buzzer_CurrentState = BUZZER_SATE_IDLE;
	//}
	//
//}

void BUZZER_SoundOff(void)
{
	IOPHY_DigitalOut1_Buzz(IO_OFF);
	buzzer_CurrentState = BUZZER_STATE_OFF;
	
}

void BUZZER_SoundCaution(void)
{
	if (buzzer_CurrentState == BUZZER_STATE_CAUTION) return;
	digOut1Ts = TIMER_Now32();
	IOPHY_DigitalOut1_Buzz(IO_ON);
	buzzer_BeepCnt = 1;
	buzzer_CurrentState = BUZZER_STATE_CAUTION;
}

void BUZZER_SoundWarning(void)
{
	if (buzzer_CurrentState == BUZZER_STATE_WARNING) return;
	digOut1Ts = TIMER_Now32();
	IOPHY_DigitalOut1_Buzz(IO_ON);
	buzzer_CurrentState = BUZZER_STATE_WARNING;	
}

void BUZZER_SoundOCritical(void)
{
	if (buzzer_CurrentState == BUZZER_STATE_CRITICAL) return;
	digOut1Ts = TIMER_Now32();
	IOPHY_DigitalOut1_Buzz(IO_ON);
	buzzer_CurrentState = BUZZER_STATE_CRITICAL;
	
}

