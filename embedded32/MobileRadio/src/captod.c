/*
 * captod.c
 *
 * Created: 2015/01/29 10:25:36 AM
 *  Author: woutervw
 */ 

/// \file captod.c
/// This is the implementation file of the captod module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2015/01/29
/// - Module        : CAPTOD
/// - Description   : captod implementation file
///
/// \detail	Cap Lamp Test On Demand (CAPTOD) Module.
///         This is the implementation file for the CAPTOD module which runs on the Mobile
///         Radio PCB. It executes the test sequence for the Cap Lamp.
///
///
/// \author Wouter van Wyk (WW)
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "captod.h"
#include "uhf2_msghandler.h"
#include "msghandler.h"
#include "msgdispatch.h"
#include "messages_runtimediag.h"
#include "messages_ptcslave.h"
#include "iophy.h"
#include "gpio.h"
#include "calog.h"
#include "ptcstatus.h"
#include "rangetag.h"

#include "board.h"
#include "scasdbg.h"

#include <string.h>

//-------------------------------------------------------------------------------------------------
// Defines
#define CAPTOD_PTC_ACTIVATE_VALID_TIME_SEC (3600)   // Time after which PTC test activation will be ignored 
#define CAPTOD_RANGING_TIMEOUT_MS (3000)
#define CAPTOD_RANGING_MAX_ATTEMPTS (5)
#define CAPTOD_TEST_DURATION_MS (2000)
#define CAPTOD_BUTTEST_DURATION_MS (10000)
#define CAPTOD_TEST_FAIL_TOGGLE_DURATION_MS (1000)
#define CAPTOD_KEEPALIVE_TIMEOUT_MS (3000)
#define CAPTOD_KEEPALIVE_INTERVAL_MS (500)    // Interval at which slave mode keep alive messages are sent to the PTC 

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Global variables
// none
CAPTOD_params captodInfo;


//-------------------------------------------------------------------------------------------------
// External variables
//-------------------------------------------------------------------------------------------------
extern ConfigBlock g_configBlock;

//-------------------------------------------------------------------------------------------------
// C function prototypes

/// \brief This function should be called whenever a relevant message is received.
void captod_MessageProcess(MESSAGES_PacketG2 *g2Packet, MSGHANDLER_RxChannel rxChannel);

/// \brief Send message to S-Link to request a ranging test
void captod_SendUhf2RangeRequest(DeviceID devId);

/// \brief Handle the LED tests
void captod_ToggleMrLeds(void);

/// \brief Switch all MR LEDs off
void captod_OffMrLeds(void);

/// \brief Switch all MR LEDs on
void captod_OnMrLeds(void);

/// \brief Reset the test results
void captod_ResetTestResults(void);

/// \brief Change to a new state
void captod_ChangeState(TOD_TestState newState);

/// \brief Send a PTC update message to the PTC
void captod_TransmitPTCUpdate(PTCSLAVE_UpdateRequest *request);

/// \brief Enable or Disable PTC slave Mode
/// \param mode [in] TRUE to enable slave mode, FALSE to disable slave mode
void captod_SetPTCSlaveMode(U8 mode);

/// \brief Handle PTC button presses
void captod_HandleButton(PTCSLAVE_QTOUCH_Button button, PTCSLAVE_QTOUCH_Button_State state );

/// \brief Log test results
U8 captod_LogTestResults(void);

/// \brief Send keep alive messages
void captod_SendKeepAlive(void);

void captod_HandleStateEnter(TOD_TestState state);

void captod_HandleStateExit(TOD_TestState state);

void captod_NextState(void);



//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
void CAPTOD_Init(void)
{
	captodInfo.hasPTC = g_configBlock.hasPtc;
	
    MSGDISPATCH_RegisterCallbackForMessageClass(MSGDISPATCH_CLASS_RUNTIME_DIAGNOSTICS, &captod_MessageProcess);
	MSGDISPATCH_RegisterCallbackForMessageClass(MSGDISPATCH_CLASS_PTC_SLAVE, &captod_MessageProcess);
		
	memset(&captodInfo,0x00,sizeof(CAPTOD_params));
	
	captodInfo.testAgentDevId.IDByte1 = UNIT_TYPE_AID;
	captodInfo.testAgentDevId.IDByte2 = 0x00;
	captodInfo.testAgentDevId.IDByte3 = 0x00;
	captodInfo.testAgentDevId.IDByte4 = 0x26;
	captodInfo.testAgentDevId.IDByte5 = 0x95;
	
	captodInfo.rangeTestRunning = FALSE;
	captodInfo.ptcInSlaveMode = FALSE;
	captodInfo.ptcInitiated = FALSE;
	captodInfo.failIndicatorOn = FALSE;
	captodInfo.ignorePtcActivated = FALSE;
	
	captodInfo.captodState = TOD_STATE_INACTIVE;
	captodInfo.captodPreviousState = TOD_STATE_INACTIVE;
	
	captod_ResetTestResults();
	
}

//-------------------------------------------------------------------------------------------------
void captod_MessageProcess(MESSAGES_PacketG2 *g2Packet, MSGHANDLER_RxChannel rxChannel) 
{
	MESSAGES_PTCSLAVE_TouchIndication *buttonEvent;
		
	// Ensure that the message class is correct
	if(g2Packet->msgClass == MSGDISPATCH_CLASS_RUNTIME_DIAGNOSTICS) {
       
		// Filter based on the message type
		switch(g2Packet->msgType) {
                           
			case MSG_RUNTIMEDIAG_TYPE_CAPTEST_REQUEST:
				if((captodInfo.captodState != TOD_STATE_INACTIVE) && (captodInfo.captodState != TOD_STATE_FAILED)) {
				   // Busy testing
				   return;	
				}
												
				if(g2Packet->senderType == MESSAGES_DEST_MR_PTC ) {
					captodInfo.ptcInitiated = TRUE;
					if(captodInfo.ignorePtcActivated == TRUE) {
						return;
					}
				} else {
					captodInfo.ptcInitiated = FALSE;
				}								
				
				// Reset the state variables
				captod_ChangeState(TOD_STATE_INACTIVE);					
				
				// Change to first test state
				captod_ChangeState(TOD_STATE_MR_UHF_TESTS);				
		
			break;
									
			default:
			// Unknown message
			break;
		}
			
	} else if(g2Packet->msgClass == MSGDISPATCH_CLASS_PTC_SLAVE) {
	
		// Filter based on the message type
		switch(g2Packet->msgType) {
		
			case MSG_PTCSLAVE_TYPE_QTOUCH_RESPONSE:
				  buttonEvent = (MESSAGES_PTCSLAVE_TouchIndication*)&g2Packet->data[0];
		  
				  // Call into state machine
				  captod_HandleButton(buttonEvent->button,buttonEvent->state);
			break;
		
			default:
			// Unknown message
			break;						
		}
   } 
}


//-------------------------------------------------------------------------------------------------
void CAPTOD_Service(void) {
	
	// PTC present is only indicated when the first keep alive messages is received
	captodInfo.hasPTC = g_configBlock.hasPtc;
	
	if(captodInfo.ignorePtcActivated == FALSE) {
		if(TIMER_GetSecondsCounter() > CAPTOD_PTC_ACTIVATE_VALID_TIME_SEC) {
			captodInfo.ignorePtcActivated = TRUE;
		}
	}
	
	if(captodInfo.captodState == TOD_STATE_INACTIVE) {
		return;
	}
		
	if(captodInfo.rangeTestRunning == TRUE) {		
		if(TIMER_Elapsed32(captodInfo.timerRange) > TIMER_MS(CAPTOD_RANGING_TIMEOUT_MS)) {
			// Ranging request timed out
			captodInfo.rangeAttempts++;
			if(captodInfo.rangeAttempts >= CAPTOD_RANGING_MAX_ATTEMPTS) {
				// Ranging failed due to maximum attempts			
				captodInfo.testResults.mr.uhf1Result = TOD_RESULT_FAILED_NO_RESULT;					
				captodInfo.testResults.mr.uhf1Range = 0;
				captodInfo.rangeTestRunning = FALSE;				
			} else {
				// Retry
				captod_SendUhf2RangeRequest(captodInfo.testResults.mr.slinkId);
 		        captodInfo.timerRange = TIMER_Now32();
			}				
		}
	}
	
	if((TIMER_Elapsed32(captodInfo.timerKeepAlive) > TIMER_MS(CAPTOD_KEEPALIVE_INTERVAL_MS) && (captodInfo.ptcInSlaveMode == TRUE))) {
		captod_SendKeepAlive();
		captodInfo.timerKeepAlive = TIMER_Now32();
	}
	
	// Check the state machine
	switch (captodInfo.captodState) {
		
		case TOD_STATE_INACTIVE:
		         // No test running
				 break;
				 
		case TOD_STATE_MR_UHF_TESTS:
		// UHF tests started - goto next test
		   captod_ChangeState(TOD_STATE_MR_LED_HU_TEST);	
		break;				 
				 				 				 				 		
		case TOD_STATE_MR_LED_VU_TEST:
		case TOD_STATE_MR_LED_HU_TEST:
		case TOD_STATE_MR_LED_LU_TEST:
		case TOD_STATE_MR_LED_EXL_TEST:
		case TOD_STATE_MR_INDICATOR_TEST:
        case TOD_STATE_PTC_LED_RED_TEST:
        case TOD_STATE_PTC_LED_GREEN_TEST:
        case TOD_STATE_PTC_LED_BLUE_TEST:	
	    case TOD_STATE_PTC_VIBRATE_TEST:		
		    // Continue with the next test if timer expired
		    if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_TEST_DURATION_MS)) {
			    captod_NextState();
		    }							
		break;
					
		case TOD_STATE_MR_LAMP_TEST:
		    if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_TEST_DURATION_MS)) {
			    if(!captodInfo.hasPTC) {
					if(captodInfo.rangeTestRunning == TRUE) {
						captod_ChangeState(TOD_STATE_PTC_WAIT_RANGING);
					} else {
						captod_ChangeState(TOD_STATE_PTC_LOG_RESULTS);
					}					
		       } else {
				   captod_ChangeState(TOD_STATE_PTC_LED_RED_TEST);
			   }
			}
		break;	
						   
       case TOD_STATE_PTC_BUZZER_TEST:
		   if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_TEST_DURATION_MS)) {
                if(captodInfo.ptcInitiated == FALSE) {
					// Kick off the next test
					captod_ChangeState(TOD_STATE_PTC_BUTTON_LEFT_TEST);
				} else {
					if(captodInfo.rangeTestRunning == TRUE) {
						captod_ChangeState(TOD_STATE_PTC_WAIT_RANGING);
						} else {
						captod_ChangeState(TOD_STATE_PTC_LOG_RESULTS);
					}
				}			   
		   }		          
       break;	
	   
       case TOD_STATE_PTC_BUTTON_LEFT_TEST:
		   if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_BUTTEST_DURATION_MS)) {
			   // Timed out waiting for user input - failed
			   captodInfo.testResults.ptc.leftSensor = TOD_RESULT_FAILED_NO_RESULT;
	       
			   // Kick off the next test
			   captod_ChangeState(TOD_STATE_PTC_BUTTON_RIGHT_TEST);
		   }
       
       break;	
	   
       case TOD_STATE_PTC_BUTTON_RIGHT_TEST:
		   if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_BUTTEST_DURATION_MS)) {
			   // Timed out waiting for user input - failed
			   captodInfo.testResults.ptc.rightSensor = TOD_RESULT_FAILED_NO_RESULT;
				if(captodInfo.rangeTestRunning == TRUE) {
					captod_ChangeState(TOD_STATE_PTC_WAIT_RANGING);
					} else {
					captod_ChangeState(TOD_STATE_PTC_LOG_RESULTS);
				}
		   }
       
       break;	
	   
	   case	TOD_STATE_PTC_WAIT_RANGING:
	      if(captodInfo.rangeTestRunning == FALSE) {
			  // Finished with the test
			  captod_ChangeState(TOD_STATE_PTC_LOG_RESULTS);
		  }
	   
	   break;
	   
	    case TOD_STATE_PTC_LOG_RESULTS:
	   		if(captodInfo.testPassed == TRUE) {
		   		   captod_ChangeState(TOD_STATE_INACTIVE);
		   		} else {
		   		   captod_ChangeState(TOD_STATE_FAILED);
	   		}
			   break;
	   	   			
        case TOD_STATE_FAILED:
		   // Toggle the LEDs to indicate failure
		   if(TIMER_Elapsed32(captodInfo.timerTestDuration) > TIMER_MS(CAPTOD_TEST_FAIL_TOGGLE_DURATION_MS)) {
			   	// Disable LED local control for duration of LED test
			   IOCTL_SuspendWarnLedUpdates(CAPTOD_TEST_FAIL_TOGGLE_DURATION_MS);
			   if(captodInfo.failIndicatorOn == FALSE) {
				   #ifdef SCASII_OC_TAG
				   gpio_set_pin_low(VU_LED_PIN);
				   #else
				   gpio_set_pin_high(VU_LED_PIN);
				   #endif
				   IOPHY_DigitalOut1(IO_ON);
				   captodInfo.failIndicatorOn = TRUE;
			   } else {
				   	#ifdef SCASII_OC_TAG
				   	gpio_set_pin_high(VU_LED_PIN);
				   	#else
				   	gpio_set_pin_low(VU_LED_PIN);
				   	#endif
				   	IOPHY_DigitalOut1(IO_OFF);
				   	captodInfo.failIndicatorOn = FALSE;
			   }
			   captodInfo.timerTestDuration = TIMER_Now32();			   			   
		   }
		   
		break;
					
						
		default:
		// Invalid state
		break;
	}					
}

//-------------------------------------------------------------------------------------------------
void captod_SendUhf2RangeRequest(DeviceID devId) {
	
	// Create the ranging request message
    CAPTOD_RangeRequest requestMsg;
	memset((void*)&requestMsg, 0x00, sizeof(CAPTOD_RangeRequest));
	
	
	// Broadcast the result message over UHF2 using Protocol 2
	UHF2_MSGHANDLER_TransmitP2ShortMsg(devId, 
	                                   P2MESSAGES_PORT_SCASII_OPTEST,
									   P2MESSAGES_TYPE_RANGE_TEST_REQUEST,
									   sizeof(CAPTOD_RangeRequest),
									   (U8*)&requestMsg);
}

//-------------------------------------------------------------------------------------------------
void CAPTOD_HandleRangingResult(P2MESSAGES_ShortMessage *p2Msg, U8 rssi)
{
	CAPTOD_RangeResponse *response;
	
	switch(p2Msg->type) {
		case P2MESSAGES_TYPE_RANGE_TEST_RESPONSE:
		    response = (CAPTOD_RangeResponse*)&(p2Msg->payload[0]);
			
			if(response->type == 0x01) {  // Range Request Ack
				captodInfo.testResults.mr.uhf2Rssi = rssi;
				captodInfo.testResults.mr.uhf2Result = TOD_RESULT_PASSED;
				
			} else if(response->type == 0x02) { // Range result
				captodInfo.testResults.mr.uhf1Range = response->range;
				// Check if valid range??
				captodInfo.testResults.mr.uhf1Result = TOD_RESULT_PASSED;
				captodInfo.rangeTestRunning = FALSE;				
			}
			break;
			
		default:
		    // Do not process the message
			break;
	}
}

//-------------------------------------------------------------------------------------------------
void captod_ToggleMrLeds(void) {
	gpio_toggle_pin(HU_LED_PIN);	
	gpio_toggle_pin(VU_LED_PIN);
	gpio_toggle_pin(LU_LED_PIN);
	gpio_toggle_pin(EXCL_LED_PIN);	
}

//-------------------------------------------------------------------------------------------------
void captod_OffMrLeds(void) {
#ifdef SCASII_OC_TAG
	gpio_set_pin_high(VU_LED_PIN);
	gpio_set_pin_high(HU_LED_PIN);
	gpio_set_pin_high(LU_LED_PIN);
	gpio_set_pin_high(EXCL_LED_PIN);
	#else
	gpio_set_pin_low(VU_LED_PIN);
	gpio_set_pin_low(HU_LED_PIN);
	gpio_set_pin_low(LU_LED_PIN);
	gpio_set_pin_low(EXCL_LED_PIN);
#endif	
}

//-------------------------------------------------------------------------------------------------
void captod_OnMrLeds(void) {
#ifdef SCASII_OC_TAG	
	gpio_set_pin_low(VU_LED_PIN);
	gpio_set_pin_low(HU_LED_PIN);
	gpio_set_pin_low(LU_LED_PIN);
	gpio_set_pin_low(EXCL_LED_PIN);
#else
	gpio_set_pin_high(VU_LED_PIN);
	gpio_set_pin_high(HU_LED_PIN);
	gpio_set_pin_high(LU_LED_PIN);
	gpio_set_pin_high(EXCL_LED_PIN);
#endif	
	
}

//-------------------------------------------------------------------------------------------------
void captod_ResetTestResults(void) {
	
	captodInfo.testResults.hasPTC = captodInfo.hasPTC;

	captodInfo.testResults.mr.caplampLeds = TOD_RESULT_SKIPPED;
	captodInfo.testResults.mr.indicatorLed = TOD_RESULT_SKIPPED;
	captodInfo.testResults.mr.uhf1Result = TOD_RESULT_SKIPPED;
	captodInfo.testResults.mr.uhf2Result = TOD_RESULT_SKIPPED;
	captodInfo.testResults.mr.caplamp = TOD_RESULT_SKIPPED;
	captodInfo.testResults.mr.uhf1Range = 0;
	captodInfo.testResults.mr.uhf2Rssi = 0;
	
	captodInfo.testResults.ptc.buzzer = TOD_RESULT_SKIPPED;
	captodInfo.testResults.ptc.spare = TOD_RESULT_SKIPPED;
	captodInfo.testResults.ptc.vibratorMotor = TOD_RESULT_SKIPPED;
	captodInfo.testResults.ptc.lcdDisplay = TOD_RESULT_SKIPPED;			
}

//-------------------------------------------------------------------------------------------------
void captod_HandleStateEnter(TOD_TestState state) {
		
		// Handle the Enter events
		switch (state) {
			
			case TOD_STATE_INACTIVE:
				if(captodInfo.ptcInSlaveMode == TRUE) {
					// Exit PTC slave mode
					PTCSLAVE_ClearLCD(&captodInfo.ptcUpdateRequest);
					captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
					captod_SetPTCSlaveMode(FALSE);
				}
				captodInfo.rangeTestRunning = FALSE;
				captodInfo.ptcInSlaveMode = FALSE;
				captodInfo.failIndicatorOn = FALSE;
			break;
			
			case TOD_STATE_MR_UHF_TESTS:
															
			// Enable Ranging module
			RANGETAG_ForceUHF1Enabled(TRUE);
												
			// Find the closes S-Link
			U8 found = RANGETAG_TableFindMostPowerfulByType(UNIT_TYPE_SAP, &captodInfo.testResults.mr.slinkId);
				
			if(found == TRUE) {
				// Might contain the Device ID of the S-Link to range with??
				captod_SendUhf2RangeRequest(captodInfo.testResults.mr.slinkId);
				captodInfo.rangeAttempts = 0;
				captodInfo.rangeTestRunning = TRUE;
				captodInfo.timerRange = TIMER_Now32();
				} else {
				captodInfo.testResults.mr.uhf1Result = TOD_RESULT_FAILED_NOT_AVAILABLE;
				captodInfo.testResults.mr.uhf2Result = TOD_RESULT_FAILED_NOT_AVAILABLE;
			}					
			break;
			
			case TOD_STATE_MR_LED_VU_TEST:
			// Disable LED local control for duration of LED test
			IOCTL_SuspendWarnLedUpdates(CAPTOD_TEST_DURATION_MS);						
				   #ifdef SCASII_OC_TAG
				   gpio_set_pin_low(VU_LED_PIN);
				   #else
				   gpio_set_pin_high(VU_LED_PIN);
				   #endif
			break;
			
			case TOD_STATE_MR_LED_HU_TEST:
			// Disable LED local control for duration of LED test
			captod_OffMrLeds();				
			IOCTL_SuspendWarnLedUpdates(CAPTOD_TEST_DURATION_MS * 4);		
//			gpio_set_pin_high(EXCL_LED_PIN);
				#ifdef SCASII_OC_TAG
				gpio_set_pin_low(EXCL_LED_PIN);
				#else
				gpio_set_pin_high(EXCL_LED_PIN);
				#endif
			break;
			
			case TOD_STATE_MR_LED_LU_TEST:
			//gpio_set_pin_high(LU_LED_PIN);
				#ifdef SCASII_OC_TAG
				//SCASDBG_PrintAscii("TOD_STATE_MR_LED_LU_TEST\r\n");
				gpio_set_pin_low(LU_LED_PIN);
				#else
				gpio_set_pin_high(LU_LED_PIN);
				#endif
			break;
			
			case TOD_STATE_MR_LED_EXL_TEST:
//			gpio_set_pin_high(HU_LED_PIN);
				#ifdef SCASII_OC_TAG
				gpio_set_pin_low(HU_LED_PIN);
				#else
				gpio_set_pin_high(HU_LED_PIN);
				#endif
			break;
						
			case TOD_STATE_MR_INDICATOR_TEST:
			IOPHY_DigitalOut1(IO_ON);
			break;
			
			case TOD_STATE_MR_LAMP_TEST:
			// Check the caplamp
			
			break;
			
			case TOD_STATE_PTC_LED_RED_TEST:
			// Send LED on
			PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_RED,COMMON_RATE_MED);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			break;
			
			case TOD_STATE_PTC_LED_GREEN_TEST:
			// Send LED on
			PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_GREEN,COMMON_RATE_MED);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			break;
			
			case TOD_STATE_PTC_LED_BLUE_TEST:
			// Send LED on
			PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_BLUE,COMMON_RATE_MED);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			break;
			
			case TOD_STATE_PTC_VIBRATE_TEST:
			PTCSLAVE_SetVibratorState(&captodInfo.ptcUpdateRequest,TRUE);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			break;
			
			case TOD_STATE_PTC_BUZZER_TEST:
			// Switch on the buzzer
			PTCSLAVE_SetBuzzerState(&captodInfo.ptcUpdateRequest,COMMON_RATE_MED);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			break;
			
			case TOD_STATE_PTC_BUTTON_LEFT_TEST:
			PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"","Press Left Button","Press","",FALSE,FALSE);
			captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);

			
			// -------------------------------
			// Send message to PTC
			// Text: Press Left Button
			// Left: Press
			// Right:
			// ---------------------------------
			
			break;
			
			case TOD_STATE_PTC_BUTTON_RIGHT_TEST:
				PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"","Press Right Button","","Press",FALSE,FALSE);
				captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			
			// -------------------------------
			// Send message to PTC
			// Text: Press Right Button
			// Left:
			// Right: Press
			// ---------------------------------
			
			break;
			
			case TOD_STATE_PTC_LCD_TEST:
			captodInfo.timerTestDuration = TIMER_Now32();
			
				PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"abcdefghijklmnopqrstuvwxz","abcdefghijklmnopqrstuvwxz","OK","Not OK",FALSE,FALSE);
				captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			
			// -------------------------------
			// Send message to PTC
			// Line1: abcdefghijklmnopqrstuvwxyz
			// Line2: abcdefghijklmnopqrstuvwxyz
			// Left: OK
			// Right: NOT OK
			// ---------------------------------
			
			break;
			
			case TOD_STATE_PTC_LCD_TEST_INVERTED:
			captodInfo.timerTestDuration = TIMER_Now32();
			
				PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"abcdefghijklmnopqrstuvwxz","abcdefghijklmnopqrstuvwxz","OK","Not OK",TRUE,TRUE);
				captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			
			// -------------------------------
			// Send message to PTC
			// Line1: abcdefghijklmnopqrstuvwxyz
			// Line2: abcdefghijklmnopqrstuvwxyz
			// Left: OK
			// Right: NOT OK
			// ---------------------------------
			
			break;
			
			case TOD_STATE_PTC_WAIT_RANGING:
			// Waiting for ranging test to finish
			if(captodInfo.ptcInSlaveMode == TRUE) {
				PTCSLAVE_ClearLCD(&captodInfo.ptcUpdateRequest);
				captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			}
			break;
			
			case TOD_STATE_PTC_LOG_RESULTS:
			// Testing is finished - log results
			   captodInfo.testPassed = captod_LogTestResults();
			break;
			
			case TOD_STATE_FAILED:
			// Disable LED local control for duration of LED test
			IOCTL_SuspendWarnLedUpdates(CAPTOD_TEST_FAIL_TOGGLE_DURATION_MS);
				   #ifdef SCASII_OC_TAG
				   gpio_set_pin_low(VU_LED_PIN);
				   #else
				   gpio_set_pin_high(VU_LED_PIN);
				   #endif
			IOPHY_DigitalOut1(IO_ON);
			captodInfo.failIndicatorOn = TRUE;
			if(captodInfo.hasPTC == TRUE) {
			   PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"Cap lamp test failed","Return to repairs or re-test","Test","",FALSE,FALSE);
			   captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
			}
			
			break;		
			
									
			default:
			// Invalid state
			break;
		}			
}


//-------------------------------------------------------------------------------------------------
void captod_HandleStateExit(TOD_TestState state) {
	
	// Handle the Exit events
	switch (state) {
		
		case TOD_STATE_INACTIVE:
		
			// Clear test results
			captod_ResetTestResults();
			memset(&captodInfo.ptcUpdateRequest,0x00,sizeof(PTCSLAVE_UpdateRequest));
					
		    // Go into test mode
			if(captodInfo.hasPTC == TRUE) {
				// Set the PTC in slave mode
				captod_SetPTCSlaveMode(TRUE);
				captodInfo.ptcInSlaveMode = TRUE;
				
				// Start keepalive timer
				captodInfo.timerKeepAlive = TIMER_Now32();
				
				PTCSLAVE_LCDMessage(&captodInfo.ptcUpdateRequest,"","Cap lamp in Test Mode","","",FALSE,FALSE);
				captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);				
			}										
		break;
		
		
		case TOD_STATE_MR_LED_VU_TEST:
			#ifdef SCASII_OC_TAG
			gpio_set_pin_high(VU_LED_PIN);
			#else
			gpio_set_pin_low(VU_LED_PIN);
			#endif
		break;
		
		case TOD_STATE_MR_LED_HU_TEST:				   
			#ifdef SCASII_OC_TAG
			gpio_set_pin_high(EXCL_LED_PIN);
			#else
			gpio_set_pin_low(EXCL_LED_PIN);
			#endif
		break;
		
		case TOD_STATE_MR_LED_LU_TEST:
						   
			#ifdef SCASII_OC_TAG
			//SCASDBG_PrintAscii("TOD_STATE_MR_LED_LU_TEST\r\n");
			gpio_set_pin_high(LU_LED_PIN);
			#else
			gpio_set_pin_low(LU_LED_PIN);
			#endif
		break;
		
		case TOD_STATE_MR_LED_EXL_TEST:
						   
			#ifdef SCASII_OC_TAG
			gpio_set_pin_high(HU_LED_PIN);
			#else		
			gpio_set_pin_low(HU_LED_PIN);
			#endif
		break;
				
		case TOD_STATE_MR_INDICATOR_TEST:
		IOPHY_DigitalOut1(IO_OFF);
		break;
		
		case TOD_STATE_MR_LAMP_TEST:		
		break;
		
		case TOD_STATE_PTC_LED_RED_TEST:
		PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_RED,COMMON_RATE_OFF);
		captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
		break;
		
		case TOD_STATE_PTC_LED_GREEN_TEST:
		PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_GREEN,COMMON_RATE_OFF);
		captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
		break;
		
		case TOD_STATE_PTC_LED_BLUE_TEST:
		PTCSLAVE_SetLedState(&captodInfo.ptcUpdateRequest,LED_BLUE,COMMON_RATE_OFF);
		captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
		break;
		
		case TOD_STATE_PTC_VIBRATE_TEST:
		PTCSLAVE_SetVibratorState(&captodInfo.ptcUpdateRequest,FALSE);
		captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
		break;
		
		case TOD_STATE_PTC_BUZZER_TEST:
		PTCSLAVE_SetBuzzerState(&captodInfo.ptcUpdateRequest,COMMON_RATE_OFF);
		captod_TransmitPTCUpdate(&captodInfo.ptcUpdateRequest);
		break;
		
		case TOD_STATE_PTC_BUTTON_LEFT_TEST:
		break;
		
		case TOD_STATE_PTC_BUTTON_RIGHT_TEST:
		break;
		
		case TOD_STATE_PTC_LCD_TEST:
		break;
		
		case TOD_STATE_PTC_LCD_TEST_INVERTED:
		break;
		
		case TOD_STATE_PTC_WAIT_RANGING:
		break;
		
		case TOD_STATE_PTC_LOG_RESULTS:
		break;
		
		case TOD_STATE_FAILED:
			#ifdef SCASII_OC_TAG
			gpio_set_pin_high(VU_LED_PIN);
			#else
			gpio_set_pin_low(VU_LED_PIN);
			#endif
			IOPHY_DigitalOut1(IO_OFF);
			captodInfo.failIndicatorOn = FALSE;	
		break;		
						
		default:
		// Invalid state
		break;
	}
}

// ------------------------------------------------------------------------------------------------
void captod_NextState() {
	captod_ChangeState(captodInfo.captodState + 1);	
}

//-------------------------------------------------------------------------------------------------
void captod_ChangeState(TOD_TestState newState) {
	
	if(newState == captodInfo.captodState) {
		// Already in this state
		return;
	}
	
	TOD_TestState currentState = captodInfo.captodState;
	
	// Handle EXIT from current state	
	captod_HandleStateExit(currentState);
	
	// Handle ENTER into new state 
	captod_HandleStateEnter(newState);
	   
	captodInfo.captodPreviousState = currentState;
	captodInfo.captodState = newState;
	captodInfo.timerTestDuration = TIMER_Now32();	   	   	   	   	 
}

//-------------------------------------------------------------------------------------------------
U8 captod_LogTestResults() {
	
	// For debug
	// CALOG_Write(CAPLAMP_TEST_RESULT,(U8 *)&testResults,sizeof(CAPTOD_AssemblyTestResults));
	
	CAPTOD_FailLog log;
	U8 passed = TRUE; 
	
#if rem	
	if(captodInfo.testResults.hasPTC == TRUE) {
		if(captodInfo.testResults.mr.caplamp != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_LAMP_TEST;
			log.reason = captodInfo.testResults.mr.caplamp;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
		
		if(captodInfo.testResults.mr.caplampLeds != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_LED_TEST;
			log.reason = captodInfo.testResults.mr.caplampLeds;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}		
		
		if(captodInfo.testResults.mr.indicatorLed != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_INDICATOR_TEST;
			log.reason = captodInfo.testResults.mr.indicatorLed;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}		
		
		if(captodInfo.testResults.mr.uhf1Result != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_UHF1_TEST;
			log.reason = captodInfo.testResults.mr.uhf1Result;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}		
		
		if(captodInfo.testResults.mr.uhf2Result != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_UHF2_TEST;
			log.reason = captodInfo.testResults.mr.uhf2Result;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}	
		
		if(captodInfo.testResults.ptc.buzzer != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_BUZZER_TEST;
			log.reason = captodInfo.testResults.ptc.buzzer;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
		
		if(captodInfo.testResults.ptc.lcdDisplay != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_LCD_TEST;
			log.reason = captodInfo.testResults.ptc.lcdDisplay;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
		
		if(captodInfo.testResults.ptc.leds != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_LED_TEST;
			log.reason = captodInfo.testResults.ptc.leds;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}	
		
		if(captodInfo.testResults.ptc.leftSensor != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_BUTTON_LEFT_TEST;
			log.reason = captodInfo.testResults.ptc.leftSensor;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}	
		
		if(captodInfo.testResults.ptc.rightSensor != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_BUTTON_RIGHT_TEST;
			log.reason = captodInfo.testResults.ptc.rightSensor;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}	
		
		if(captodInfo.testResults.ptc.vibratorMotor != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_VIBRATE_TEST;
			log.reason = captodInfo.testResults.ptc.vibratorMotor;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}										
		
	} else {
		
		if(captodInfo.testResults.mr.caplamp != TOD_RESULT_SKIPPED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_LAMP_TEST;
			log.reason = captodInfo.testResults.mr.caplamp;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
				
		if(captodInfo.testResults.mr.caplampLeds != TOD_RESULT_SKIPPED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_LED_TEST;
			log.reason = captodInfo.testResults.mr.caplampLeds;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
				
		if(captodInfo.testResults.mr.indicatorLed != TOD_RESULT_SKIPPED) {
			passed = FALSE;
			log.component = TOD_FAIL_MR_INDICATOR_TEST;
			log.reason = captodInfo.testResults.mr.indicatorLed;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
				
		if(captodInfo.testResults.mr.uhf1Result != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_UHF1_TEST;
			log.reason = captodInfo.testResults.mr.uhf1Result;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
				
		if(captodInfo.testResults.mr.uhf2Result != TOD_RESULT_PASSED) {
			passed = FALSE;
			log.component = TOD_FAIL_UHF2_TEST;
			log.reason = captodInfo.testResults.mr.uhf2Result;
			CALOG_Write(CAPLAMP_TEST_FAILURE,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}						
	}	
#endif	

	if((captodInfo.testResults.hasPTC == TRUE) && (captodInfo.ptcInitiated == FALSE)) {
		if(captodInfo.testResults.ptc.rightSensor == TOD_RESULT_FAILED_NO_RESULT) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_BUTTON_RIGHT_TEST;
			log.reason = captodInfo.testResults.ptc.rightSensor;
			CALOG_Write(CAPLAMP_TEST_FAILURE_EVENT,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
		
		if(captodInfo.testResults.ptc.leftSensor == TOD_RESULT_FAILED_NO_RESULT) {
			passed = FALSE;
			log.component = TOD_FAIL_PTC_BUTTON_LEFT_TEST;
			log.reason = captodInfo.testResults.ptc.leftSensor;
			CALOG_Write(CAPLAMP_TEST_FAILURE_EVENT,(U8 *)&log,sizeof(CAPTOD_FailLog));
		}
	}
	
	if(captodInfo.testResults.mr.uhf1Result != TOD_RESULT_PASSED) {
		passed = FALSE;
		log.component = TOD_FAIL_UHF1_TEST;
		log.reason = captodInfo.testResults.mr.uhf1Result;
		CALOG_Write(CAPLAMP_TEST_FAILURE_EVENT,(U8 *)&log,sizeof(CAPTOD_FailLog));
	}
	
	if(captodInfo.testResults.mr.uhf2Result != TOD_RESULT_PASSED) {
		passed = FALSE;
		log.component = TOD_FAIL_UHF2_TEST;
		log.reason = captodInfo.testResults.mr.uhf2Result;
		CALOG_Write(CAPLAMP_TEST_FAILURE_EVENT,(U8 *)&log,sizeof(CAPTOD_FailLog));
	}	
			
	CAPTOD_TestResultLog result;
	result.passed = passed;
	result.range = captodInfo.testResults.mr.uhf1Range;
	result.rssi = (U8)((U16)captodInfo.testResults.mr.uhf2Rssi + 128);
	result.slinkId.IDByte1 = captodInfo.testResults.mr.slinkId.IDByte1;
	result.slinkId.IDByte2 = captodInfo.testResults.mr.slinkId.IDByte2;
	result.slinkId.IDByte3 = captodInfo.testResults.mr.slinkId.IDByte3;
	result.slinkId.IDByte4 = captodInfo.testResults.mr.slinkId.IDByte4;			
	result.slinkId.IDByte5 = captodInfo.testResults.mr.slinkId.IDByte5;
		
    CALOG_Write(CAPLAMP_TEST_RESULT_EVENT,(U8*)&result,sizeof(CAPTOD_TestResultLog));
	U32 idx = CALOG_GetNextLogIndex();
	// Get previous index
	idx--;
	CALOG_DataFormat logData;
	// Read log
	U8 retVal = CALOG_Read(idx,&logData);
	
	if(retVal == SUCCESS) {
		// Send log over UHF2
		UHF2_MSGHANDLER_TransmitP2ShortMsg(captodInfo.testAgentDevId, P2MESSAGES_PORT_SCASII_OPTEST, P2MESSAGES_TYPE_OPTEST_TEST_RESULT, sizeof(CALOG_DataFormat), (U8*)&logData);
	}
	
	return passed;
	
	// Send result to agent on UHF2	 
	// UHF2_MSGHANDLER_TransmitP2ShortMsg(captodInfo.testAgentDevId, P2MESSAGES_PORT_SCASII_OPTEST, P2MESSAGES_TYPE_OPTEST_TEST_RESULT, sizeof(CAPTOD_TestResultLog), (U8*)&result);
	
		
}

//-------------------------------------------------------------------------------------------------
void captod_HandleButton(PTCSLAVE_QTOUCH_Button button, PTCSLAVE_QTOUCH_Button_State state ) {
	

	// Check the state machine
	switch (captodInfo.captodState) {
						
		case TOD_STATE_PTC_BUTTON_LEFT_TEST:
			
			if(state == QTOUCH_DOWN) {
				// Only interested in the release event
				break;
			}
			if(button == QTOUCH_LEFT) {
				// PASS
				captodInfo.testResults.ptc.leftSensor = TOD_RESULT_PASSED;
				} else {
				
					return;
				}			
			
			// Kick off the next test
			captod_ChangeState(TOD_STATE_PTC_BUTTON_RIGHT_TEST);						
		break;
			
		case TOD_STATE_PTC_BUTTON_RIGHT_TEST:			
			if(state == QTOUCH_DOWN) {
				// Only interested in the release event
				break;
			}
			if(button == QTOUCH_RIGHT) {
				// PASS
				captodInfo.testResults.ptc.rightSensor = TOD_RESULT_PASSED;
				} else {
				
				return;
			}			
			
			// Kick off the next test
			if(captodInfo.rangeTestRunning == TRUE) {
				   captod_ChangeState(TOD_STATE_PTC_WAIT_RANGING);
				} else {
				   captod_ChangeState(TOD_STATE_PTC_LOG_RESULTS);
			}
		break;	
		
		case TOD_STATE_FAILED:
		
		if(state == QTOUCH_DOWN) {
			// Only interested in the release event
			break;
		}
		if(button == QTOUCH_LEFT) {
			// PASS
			
			} else {
			
			return;
		}
		
		// Kick off the next test
		captod_ChangeState(TOD_STATE_INACTIVE);
		captod_ChangeState(TOD_STATE_MR_UHF_TESTS);
		break;						
		
		default: 
		
		break;
	}

}

//-------------------------------------------------------------------------------------------------
void captod_TransmitPTCUpdate(PTCSLAVE_UpdateRequest *request) {
	
	MSGHANDLER_TransmitPacketG2(MESSAGES_DEST_MU,MESSAGES_DEST_MR_PTC,MSGDISPATCH_CLASS_PTC_SLAVE,MSG_PTCSLAVE_TYPE_SLAVEMODE_UPDATE,sizeof(PTCSLAVE_UpdateRequest),(U8 *)request);
	memset(&captodInfo.ptcUpdateRequest,0x00,sizeof(PTCSLAVE_UpdateRequest));
}


//-------------------------------------------------------------------------------------------------
void captod_SetPTCSlaveMode(U8 mode) {
	
    MESSAGES_PTCSLAVE_ModeRequest modeRequest;
	
	modeRequest.mode = mode;
	modeRequest.keepAliveTimout = CAPTOD_KEEPALIVE_TIMEOUT_MS;
	
	MSGHANDLER_TransmitPacketG2(MESSAGES_DEST_MU,MESSAGES_DEST_MR_PTC,MSGDISPATCH_CLASS_PTC_SLAVE,MSG_PTCSLAVE_TYPE_SLAVEMODE_SET,sizeof(MESSAGES_PTCSLAVE_ModeRequest),(U8 *)&modeRequest);		
}

//-------------------------------------------------------------------------------------------------
void captod_SendKeepAlive() {
	
	MESSAGES_PTCSLAVE_KeepAlive alive;
	
	MSGHANDLER_TransmitPacketG2(MESSAGES_DEST_MU,MESSAGES_DEST_MR_PTC,MSGDISPATCH_CLASS_PTC_SLAVE,MSG_PTCSLAVE_TYPE_SLAVEMODE_KEEPALIVE,sizeof(MESSAGES_PTCSLAVE_KeepAlive),(U8 *)&alive);		
}

//-------------------------------------------------------------------------------------------------
U8 CAPTOD_IsBusyTesting() {
	
	if(captodInfo.captodState == TOD_STATE_INACTIVE) {
		return FALSE;
	}
	return TRUE;
}
