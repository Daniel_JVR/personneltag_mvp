/// \file ioctl.c
/// This is the implementation file of the ioctl module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/09/13
/// - Module        : IOCTL
/// - Description   : ioctl implementation
///
/// \details  This module provides functions for manipulating and accessing inputs and outputs on
///           the Mobile Radio PCB. For example, warning LED's can be controlled according to the
///           contents of an IOCTL_WarnBits variable. It also manages the blinking of a run LED and
///           other LED's such as driver deregistration LED.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: ioctl.c,v 1.33 2015-02-25 16:04:35+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: [1]
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "ioctl.h"
#include "timer.h"          ///< Timer module used for measuring delays, etc.
#include "iophy.h"          ///< IOPHY module for hardware level manipulation of MCU pins
#include "sysconfig_uc3.h"  ///< Configuration parameters
#include "ca_msg.h"         ///< Used to transmit an emergency stop message over UHF2
#include "msghandler.h"     ///< SCAS II Message Handler
#include "gpio.h"
#include "ver.h"
#include "calog.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define HEARTBEAT_MS            TIMER_MS(1000)  ///< Default run LED blinking period
#define HEARTBEAT_DUTY_CYCLE    (1.0/8.0)       ///< Run LED must be 12.5% on, 87.5% off (low current)

#define BLINKING_MS             TIMER_MS(200)   ///< General blinking period for other LED's

#define GAS_WARNING_BLINK_MS    (U32)(1000/6.0) ///< Blink cap lamp 3x per second for 
                                                            ///  gas warnings (RSD R2.45)
#define GAS_WARNING_TIMEOUT_S   (5)            ///< Timeout gas warnings after 5s


#define DEBOUNCE_PERIOD         TIMER_MS(50)    ///< Debounce period for emergency stop button

#define SWEEP_PERIOD_MS         (200)

#define IOCLT_VER_STEP_DURATION_MS  (750)
#define IOCLT_VER_OFF_DURATION_MS   (100)
#define IOCLT_VER_WAIT_DURATION_MS  (1500)

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
typedef enum _IOCLT_ButtonFunction {
    IOCLT_EMERGENCY_STOP,
	IOCLT_WORKER_EXCLUSION,
} IOCLT_ButtonFunction;

//-------------------------------------------------------------------------------------------------
// Structures
// \struct IOCLT_VerDisplayLib
typedef struct _IOCLT_VerDisplayLib {
	U8  active;
	U8  maxVerDigit;
	U8  displayStep;
	U32 stepStartTs;
	U8  verArray[4];                        // Stores the version parameters in a specified order
} ATTR_PACKED IOCLT_VerDisplayLib;

typedef struct _IOCTL_RuntimeVariables {
	IOState runLedState;                    // Current state of run LED
	U32 runLedTs;                           // Timestamp when run LED blinking was last toggled
	U32 heartbeatPeriodMs;                  // Period of run LED blinking
	
	IOState generalLedState;                // Current state of general LED blinking flag
	U32 generalLedTs;                       // Timestamp when general blinking was last toggled
	
	IOCTL_DigOutFunction digOut1Func;       // Specifies the function assigned to DIG_OUT1 pin
	U8 digOut1Active;                       // Boolean, indicates whether DIG_OUT1 pin is active
	U32 digOut1Ts;                          // Timestamp when DIG_OUT1 pin was last toggled
	
	U8 blinkDeregLed;                       // Boolean indicating whether deregistration LED must blink
	
	U8 suspendFlag;                         // Specifies whether updating of LED's must be suspended
    U32 suspendStopTs;                      // Timestamp when LED update suspension must be ended
	
	U8 specialReleaseFlag;                  // Indicates whether a special release of sogftware is
	                                        // running on the MR which means that it should not be
											// used for normal Collision Warning purposes.
	
	U8 discreetEnabled;                     // TRUE if command was received to disable cap lamp LED, 
	                                        // FALSE if discreet mode not confirmed for masterCATimeout_s
    U32 discreetTimestamp;                  // Timestamp when discreet mode was last confirmed
	
	IOState gasWarningState;                // Indicates that a gas warning must be indicated using
	                                        // the cap lamp LED/laser, either ON, BLINKING, or OFF
											
    U32 gasWarningTimestamp;                // Timestamp when gas warning was last confirmed
	U8 gasWarningTimeoutS;                  // Duration for which gas warning must be active
} ATTR_PACKED IOCTL_RuntimeVariables;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;           // Global config block to obtain parameters

//-------------------------------------------------------------------------------------------------
// Global variables
static IOCTL_RuntimeVariables ioctlLib = {0};
static IOCLT_VerDisplayLib verDispLib = {0};

// Emergency stop button debouncing variables
static U8       emergencyStopBtnDown;       // TRUE/FALSE
static U32      emergencyStopBtnStart;      // Time reference when button went down
static IOState  emergencyStopBtnState;      // Debounced state of the button

//-------------------------------------------------------------------------------------------------
// C function prototypes
void ioctl_Debounce(void);
void ioctl_SetExclusionIndicators(U8 workerExcluded, U8 driverDeregistered);
void ioctl_DigOut1Service(void);
void ioctl_VerDisplayService(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void IOCTL_Init()
{
    IOPHY_Init(); // Initialise all IO pins, including LED's

    emergencyStopBtnDown = FALSE;
    emergencyStopBtnStart = 0;
    emergencyStopBtnState = IO_OFF;

    ioctlLib.heartbeatPeriodMs = HEARTBEAT_MS;  /// Assign default value. Value can be changed
                                                /// by IOCTL_SetHeartbeatMs();

	// By default, the DIG_OUT1 pin indicates warnings on a Miner Unit caplamp
	ioctlLib.digOut1Func = IOCTL_FUNC_CAPLAMP_LED;

    // Initialise the maxVerDigit variable
    verDispLib.maxVerDigit = VER_MAJOR_VERSION;
	if(verDispLib.maxVerDigit < VER_MINOR_VERSION) {
	    verDispLib.maxVerDigit = VER_MINOR_VERSION;
	}
	if(verDispLib.maxVerDigit < VER_BUGFIX_VERSION) {
	    verDispLib.maxVerDigit = VER_BUGFIX_VERSION;
	}
	if(verDispLib.maxVerDigit < VER_BUILD_NUMBER) {
	    verDispLib.maxVerDigit = VER_BUILD_NUMBER;
	}
}

//-------------------------------------------------------------------------------------------------
void IOCTL_Service(IOCTL_ServiceModes mode)
{
    /// Debounce the emergency stop button
	ioctl_Debounce();

    /// Blink the RunLED
    if(ioctlLib.runLedState == IO_OFF) {
		if(TIMER_Elapsed32(ioctlLib.runLedTs) > ((1 - HEARTBEAT_DUTY_CYCLE)*ioctlLib.heartbeatPeriodMs)) {
            ioctlLib.runLedState = IO_ON;
            IOPHY_RunLED(IO_ON);
			ioctlLib.runLedTs = TIMER_Now32();
			
			if(ioctlLib.specialReleaseFlag == TRUE) {
				IOPHY_VehicleWarningLED(IO_ON);
				IOPHY_LocoWarningLED(IO_ON);
			}
			
		}
    } else {
		if(TIMER_Elapsed32(ioctlLib.runLedTs) > ((HEARTBEAT_DUTY_CYCLE)*ioctlLib.heartbeatPeriodMs)) {
            ioctlLib.runLedState = IO_OFF;
            IOPHY_RunLED(IO_OFF);
			ioctlLib.runLedTs = TIMER_Now32();
			
			if(ioctlLib.specialReleaseFlag == TRUE) {
				IOPHY_VehicleWarningLED(IO_OFF);
				IOPHY_LocoWarningLED(IO_OFF);
			}
		}
    }
	
	if(mode == IOCTL_SERVICE_HEARTBEAT) {
		// skip the rest of the services
		return;
	}

    /// Toggle static variable for blinking other LEDs
    if(TIMER_Elapsed32(ioctlLib.generalLedTs) > BLINKING_MS) {
        if(ioctlLib.generalLedState == IO_ON) {
            ioctlLib.generalLedState = IO_OFF;
        } else {
            ioctlLib.generalLedState = IO_ON;
        }
        ioctlLib.generalLedTs = TIMER_Now32();
    }

	/// Handle blinking of driver deregistration LED
    if((ioctlLib.blinkDeregLed == TRUE) && (ioctlLib.suspendFlag == FALSE)) {
        if(ioctlLib.generalLedState == IO_ON) {
            IOPHY_WrkrExclDrvrDeregLED(IO_ON);
        } else {
            IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
        }
    }

	// Check whether the Miner Unit button has been pressed
    if(IOCTL_ReadEmergencyStopBtn() == IO_RISING_EDGE) {
        
		if(g_configBlock.mobileRadioButtonFunction == IOCLT_EMERGENCY_STOP) {
		    /// If the emergency stop button has been pressed, send an emergency stop
			/// message over UHF2 and log the event
			MSGHANDLER_TransmitPacket(MESSAGES_DEST_UHF2,
			                          MESSAGES_TYPE_CA_MSG_EMERGENCY_STOP,
									  MESSAGES_LEN_CA_MSG_EMERGENCY_STOP,
									  (U8*)&(g_configBlock.deviceID.IDByte1));
            
			CALOG_Write(EM_STOP_EVENT, (U8*)&(g_configBlock.deviceID), EM_STOP_LEN);

		} else if(g_configBlock.mobileRadioButtonFunction == IOCLT_WORKER_EXCLUSION) {
            /// If the worker self-exclusion (WSE) button has been pressed, send a WSE request
			/// message over UHF2 and log the event
			MSGHANDLER_TransmitPacket(MESSAGES_DEST_UHF2,
			                          MESSAGES_TYPE_WORKER_EXCLUSION_MR,
									  MESSAGES_LEN_WORKER_EXCLUSION_MR,
									  (U8*)&(g_configBlock.deviceID));
									  
            CALOG_UHF1Log newUHF1Log;
            newUHF1Log.deviceID = g_configBlock.deviceID;
            newUHF1Log.zone = CA_ZONE_2;
            newUHF1Log.quad = QUADRANT_TBD;
            newUHF1Log.type = CA_WORKER_SELF_EXCL_REQ;
            newUHF1Log.finalDist = 0;
			newUHF1Log.globalExclusion = FALSE;     // Always FALSE for worker self-exclusion
            CALOG_Write(CA_UHF1_EVENT, (U8*)&newUHF1Log, CA_UHF1_LEN);
		}
    }

	if(ioctlLib.suspendFlag == TRUE) {
		if(TIMER_Now32() > ioctlLib.suspendStopTs) {
			ioctlLib.suspendFlag = FALSE;
		}
	}

	ioctl_DigOut1Service();

	// Run the service for showing software version on warning LED's
	ioctl_VerDisplayService();
}

//-------------------------------------------------------------------------------------------------
void IOCTL_UpdateWarning(IOCTL_WarnBits warnBits)
{
    U8 laserOnTemp = FALSE;     /// Temporary variable to store the required state of the caplamp laser

	if((ioctlLib.suspendFlag == FALSE) && (ioctlLib.specialReleaseFlag == FALSE)) {
        if( warnBits.bit.SingleLoco || warnBits.bit.MultipleLoco) {
            IOPHY_LocoWarningLED(IO_ON);
			laserOnTemp = TRUE;
        } else {
            IOPHY_LocoWarningLED(IO_OFF);
        }

        if( warnBits.bit.SingleVehicle || warnBits.bit.MultipleVehicle || warnBits.bit.TestUnit) {
            IOPHY_VehicleWarningLED(IO_ON);
            laserOnTemp = TRUE;
        } else {
            IOPHY_VehicleWarningLED(IO_OFF);
        }

        if( warnBits.bit.Hazard) {
            IOPHY_HazardWarningLED(IO_ON);
            laserOnTemp = TRUE;
        } else {
            IOPHY_HazardWarningLED(IO_OFF);
        }
		
		// Check whether discreet mode is enabled
		if(ioctlLib.discreetEnabled == TRUE) {
			// Do not switch on cap lamp LED
			laserOnTemp = FALSE;
			
			// Manage the timeout of discreet mode
		    if(TIMER_Elapsed32(ioctlLib.discreetTimestamp) > TIMER_S(g_configBlock.masterCATimeout_s)) {
			    ioctlLib.discreetEnabled = FALSE;
		    }
		}

	    if(ioctlLib.digOut1Func == IOCTL_FUNC_CAPLAMP_LED) {
			// Update the status of the DIG_OUT1 pin
			ioctlLib.digOut1Active = laserOnTemp;
	    }

	    /// Finally, handle the Worker Exclusion/Driver deregistration LED status
	    ioctl_SetExclusionIndicators(warnBits.bit.wrkrExcl, warnBits.bit.drvrDereg);
	}
}

//-------------------------------------------------------------------------------------------------
IOState IOCTL_ReadEmergencyStopBtn(void)
{
    if(emergencyStopBtnState == IO_RISING_EDGE) {
        emergencyStopBtnState = IO_ON;
        return IO_RISING_EDGE;
    } else {
        return emergencyStopBtnState;
    }
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetHeartbeatMs(U16 heartbeatPer) {
    ioctlLib.heartbeatPeriodMs = TIMER_MS(heartbeatPer);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_PulseWarnLedsBlocking(void) {
    IOPHY_SetAllWarnleds(IO_ON);
	TIMER_DELAY_MS(25);
	IOPHY_SetAllWarnleds(IO_OFF);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SweepWarnLedsBlocking(void)
{
    IOPHY_VehicleWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_LocoWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_WrkrExclDrvrDeregLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_HazardWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_DigitalOut1(IO_ON);

	TIMER_DELAY_MS(SWEEP_PERIOD_MS);

	IOPHY_VehicleWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_LocoWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_HazardWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_DigitalOut1(IO_OFF);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SweepWarnLedsBlockingNoCaplamp(void)
{
	IOPHY_VehicleWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_LocoWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_WrkrExclDrvrDeregLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_HazardWarningLED(IO_ON);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);

	TIMER_DELAY_MS(SWEEP_PERIOD_MS);

	IOPHY_VehicleWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_LocoWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
	IOPHY_HazardWarningLED(IO_OFF);
	TIMER_DELAY_MS(SWEEP_PERIOD_MS);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_PulseDigOut1Blocking(void)
{
	IOPHY_DigitalOut1(IO_ON);
	TIMER_DELAY_MS(250);
	IOPHY_DigitalOut1(IO_OFF);
	TIMER_DELAY_MS(250);
	IOPHY_DigitalOut1(IO_ON);
	TIMER_DELAY_MS(250);
	IOPHY_DigitalOut1(IO_OFF);
	TIMER_DELAY_MS(250);
	IOPHY_DigitalOut1(IO_ON);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SuspendWarnLedUpdates(U32 timeoutMs)
{
	ioctlLib.suspendFlag = TRUE;
	ioctlLib.suspendStopTs = TIMER_Now32() + TIMER_MS(timeoutMs);
}

//-------------------------------------------------------------------------------------------------
void IOCTL_ToggleHazardLed(void)
{
	if(ioctlLib.suspendFlag == FALSE) {
	    gpio_toggle_pin(HU_LED_PIN);
	}
}

//-------------------------------------------------------------------------------------------------
void IOCTL_DisplayVer(void)
{
	// Suspend warning LED updates for the duration of the version display
	U32 suspendPeriodMs = (IOCLT_VER_STEP_DURATION_MS * verDispLib.maxVerDigit) + IOCLT_VER_WAIT_DURATION_MS;
	IOCTL_SuspendWarnLedUpdates(suspendPeriodMs);

	// Determine which LED will be used for major, minor, bugfix, and build numbers
	U32 verSum = VER_MAJOR_VERSION + VER_MINOR_VERSION + VER_BUGFIX_VERSION + VER_BUILD_NUMBER;
	U32 verShift = verSum % 4;  ///< There are 4 warning LED's

	switch(verShift) {
	    default:
		case 0:
		    verDispLib.verArray[0] = VER_MAJOR_VERSION;
			verDispLib.verArray[1] = VER_MINOR_VERSION;
			verDispLib.verArray[2] = VER_BUGFIX_VERSION;
			verDispLib.verArray[3] = VER_BUILD_NUMBER;
	        break;
	    case 1:
		    verDispLib.verArray[1] = VER_MAJOR_VERSION;
			verDispLib.verArray[2] = VER_MINOR_VERSION;
			verDispLib.verArray[3] = VER_BUGFIX_VERSION;
			verDispLib.verArray[0] = VER_BUILD_NUMBER;
	        break;
	    case 2:
		    verDispLib.verArray[2] = VER_MAJOR_VERSION;
			verDispLib.verArray[3] = VER_MINOR_VERSION;
			verDispLib.verArray[0] = VER_BUGFIX_VERSION;
			verDispLib.verArray[1] = VER_BUILD_NUMBER;
	        break;
	    case 3:
		    verDispLib.verArray[3] = VER_MAJOR_VERSION;
			verDispLib.verArray[0] = VER_MINOR_VERSION;
			verDispLib.verArray[1] = VER_BUGFIX_VERSION;
			verDispLib.verArray[2] = VER_BUILD_NUMBER;
	        break;
	}

	verDispLib.active = TRUE;
	verDispLib.displayStep = 1;                 ///< Begin at step 1
	verDispLib.stepStartTs = TIMER_Now32();
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetDigOut1Function(IOCTL_DigOutFunction newFunction)
{
	ioctlLib.digOut1Func = newFunction;
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetDigOut1Active(U8 newState)
{
	ioctlLib.digOut1Active = newState;
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetDiscreetMode(void)
{
	ioctlLib.discreetEnabled = TRUE;
	ioctlLib.discreetTimestamp = TIMER_Now32();
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetGasWarning(IOState state, U8 seconds)
{
	switch(state) {
		case IO_ON:
		    ioctlLib.gasWarningState = IO_ON;
	        ioctlLib.gasWarningTimestamp = TIMER_Now32();
			ioctlLib.gasWarningTimeoutS = seconds;
			break;
		case IO_BLINKING:
		    ioctlLib.gasWarningState = IO_BLINKING;
	        ioctlLib.gasWarningTimestamp = TIMER_Now32();
			ioctlLib.gasWarningTimeoutS = seconds;
			break;
		case IO_OFF:
		default:
		    ioctlLib.gasWarningState = IO_OFF;
			break;
	}
}

//-------------------------------------------------------------------------------------------------
void IOCTL_SetSpecialReleaseFlag(U8 state)
{
	ioctlLib.specialReleaseFlag = state;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function debounces the emergency stop button on the Mobile Radio.
void ioctl_Debounce(void)
{
    if(IOPHY_EmergencyStopBtn() == IO_ON) {
        if(emergencyStopBtnDown == FALSE) {
            emergencyStopBtnDown = TRUE;
            emergencyStopBtnStart = TIMER_Now32();
        }

        if(TIMER_Elapsed32(emergencyStopBtnStart) > DEBOUNCE_PERIOD) {
            switch(emergencyStopBtnState) {
                case IO_OFF:
                case IO_FALLING_EDGE:
                    emergencyStopBtnState = IO_RISING_EDGE;
                    break;
                default:
                    break;
            }
        }
    } else {
        emergencyStopBtnDown = FALSE;
        emergencyStopBtnStart = 0;
        emergencyStopBtnState = IO_OFF;
    }
}

//-------------------------------------------------------------------------------------------------
/// \brief Sets the exclusion LED (EXCL_LED) to 'on', 'off', or 'blinking'. To indicate driver
///        deregistration, the LED must blink. For Worker Exclusion, the LED must be constantly on.
/// \param workerExcluded [in] indicates whether this unit is currently Worker Excluded
/// \param driverDeregistered [in] indicates whether this unit is currently Deregistered as driver
void ioctl_SetExclusionIndicators(U8 workerExcluded, U8 driverDeregistered)
{
	if(driverDeregistered == TRUE) {
        //IOPHY_WrkrExclDrvrDeregLED(IO_BLINKING);
        ioctlLib.blinkDeregLed = TRUE;
    } else if(workerExcluded == TRUE) {
        //IOPHY_WrkrExclDrvrDeregLED(IO_ON);
        //ioctlLib.blinkDeregLed = FALSE;
        /// Also blink the dereg LED for Worker Exclusion
		ioctlLib.blinkDeregLed = TRUE;
    } else {
        IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
        ioctlLib.blinkDeregLed = FALSE;
    }
}

//-------------------------------------------------------------------------------------------------
/// \brief Controls the toggling of the DIG_OUT1 pin according to configurable on and off periods.
void ioctl_DigOut1Service(void)
{
	if(ioctlLib.suspendFlag == FALSE) {
	    if((ioctlLib.digOut1Active == FALSE) && (ioctlLib.gasWarningState == IO_OFF)) {
		    IOPHY_DigitalOut1(IO_OFF);
	    } else {
			if(ioctlLib.gasWarningState != IO_OFF) {
				// Toggle cap lamp LED on/off 3 times per second
				if(TIMER_Elapsed32(ioctlLib.digOut1Ts) > TIMER_MS(GAS_WARNING_BLINK_MS)) {
					if(ioctlLib.gasWarningState == IO_BLINKING) {
						// Blink the caplamp LED
						IOPHY_DigitalOut1Toggle();
					    ioctlLib.digOut1Ts = TIMER_Now32();
					} else {
						// Leave LED on
						IOPHY_DigitalOut1(IO_ON);
						ioctlLib.digOut1Ts = TIMER_Now32();
					}
				}
			} else {
		        // Toggle cap lamp LED according to configurable parameters
				if(TIMER_Elapsed32(ioctlLib.digOut1Ts) <= TIMER_MS(g_configBlock.mrLaserOnTime_ms)) {
			        IOPHY_DigitalOut1(IO_ON);
		        } else if(TIMER_Elapsed32(ioctlLib.digOut1Ts) <= TIMER_MS(g_configBlock.mrLaserOnTime_ms + g_configBlock.mrLaserOffTime_ms)) {
			        IOPHY_DigitalOut1(IO_OFF);
		        } else {
			        ioctlLib.digOut1Ts = TIMER_Now32();
		        }
			}				
	    }
	}
	
	// Check the gas warning timeout
	if(ioctlLib.gasWarningState != IO_OFF) {
	    if(TIMER_Elapsed32(ioctlLib.gasWarningTimestamp) > TIMER_S(ioctlLib.gasWarningTimeoutS)) {
		    ioctlLib.gasWarningState = IO_OFF;
	    }
	}
	
}

//-------------------------------------------------------------------------------------------------
/// \brief Displays the software version using the four warning LED's
void ioctl_VerDisplayService(void)
{
	// If version is being displayed
	if(verDispLib.active == TRUE) {
		if(TIMER_Elapsed32(verDispLib.stepStartTs) < TIMER_MS(IOCLT_VER_OFF_DURATION_MS)) {
			IOPHY_VehicleWarningLED(IO_OFF);
			IOPHY_LocoWarningLED(IO_OFF);
			IOPHY_HazardWarningLED(IO_OFF);
			IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
		} else if(TIMER_Elapsed32(verDispLib.stepStartTs) > TIMER_MS(IOCLT_VER_STEP_DURATION_MS)) {
		    // Time to display the next step (if any)
			verDispLib.displayStep++;
			verDispLib.stepStartTs = TIMER_Now32();

			IOPHY_VehicleWarningLED(IO_OFF);
			IOPHY_LocoWarningLED(IO_OFF);
			IOPHY_HazardWarningLED(IO_OFF);
			IOPHY_WrkrExclDrvrDeregLED(IO_OFF);

			if(verDispLib.displayStep > verDispLib.maxVerDigit) {
				// exit the version display state
				verDispLib.active = FALSE;
			}
		} else {
			if(verDispLib.verArray[0] >= verDispLib.displayStep) {
				IOPHY_VehicleWarningLED(IO_ON);
			} else {
				IOPHY_VehicleWarningLED(IO_OFF);
			}

			if(verDispLib.verArray[1] >= verDispLib.displayStep) {
				IOPHY_LocoWarningLED(IO_ON);
			} else {
				IOPHY_LocoWarningLED(IO_OFF);
			}

			if(verDispLib.verArray[2] >= verDispLib.displayStep) {
				IOPHY_HazardWarningLED(IO_ON);
			} else {
				IOPHY_HazardWarningLED(IO_OFF);
			}

			if(verDispLib.verArray[3] >= verDispLib.displayStep) {
				IOPHY_WrkrExclDrvrDeregLED(IO_ON);
			} else {
				IOPHY_WrkrExclDrvrDeregLED(IO_OFF);
			}
		}
	}
}