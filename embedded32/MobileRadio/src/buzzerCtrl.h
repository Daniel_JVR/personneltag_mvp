/// \file buzzerCtrl.h
/// This is the header file of the buzzer control for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2015/09/02
/// - Module        : buzzerCtlr
/// - Description   : Buzzer Control header file
///
/// \detail	GPS
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------

#ifndef BUZZERCTRL_H_
#define BUZZERCTRL_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "common.h"         ///< Common defines such as TRUE, FALSE etc
#include "timer.h"

#ifdef __cplusplus
extern "C" {    // C++
	#endif

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums	
	
//-------------------------------------------------------------------------------------------------
// Structures

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------

void BUZZER_SoundOff(void);
void BUZZER_SoundCaution(void);
void BUZZER_SoundWarning(void);
void BUZZER_SoundOCritical(void);

void BUZZER_Service(void);

void BUZZER_CycleStates(void);


#ifdef __cplusplus
};
#endif

#endif /* BUZZERCTRL_H_ */