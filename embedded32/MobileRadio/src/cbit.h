/// \file cbit.h
/// This is the header file of the cbit module for the SCASII Mobile Radio
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2013/01/11
/// - Module        : CBIT
/// - Description   : cbit header file
///
/// \detail	The CBIT (Continuous Built-In Testing) module on the Mobile Radio only checks the 
///         battery voltage, and logs an error event when the voltage drops below a set level.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: cbit.h,v 1.1 2014-07-21 08:49:00+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _CBIT_H_
#define _CBIT_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "config_block_uc3.h"           ///< Config block

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums

//-------------------------------------------------------------------------------------------------
// External variables

//-------------------------------------------------------------------------------------------------
// Structures

//-------------------------------------------------------------------------------------------------
// C function prototypes

#ifdef __cplusplus
extern "C" {                       
#endif

//-------------------------------------------------------------------------------------------------
/// \brief This function initialize the continuous build in test module
void CBIT_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function service hardware/software tests that`s required on a continuous bases
void CBIT_Service(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function re-initializes the continuous build in test service
/// \note  This function is required when the display received a error message and went into
///        a state where it overrides the display output from the continuous build in tests.
void CBIT_Reinitialize(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the initialisation status variable held in CBIT according to whether
///         the hardware on the unit initialised successfully or not.
/// \param initStatus SUCCESS if board initialised successfully, FAILURE otherwise
void CBIT_SetBoardInitStatus(U8 initStatus);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the initialisation status variable held in CBIT according to whether
///         the hardware on the unit initialised successfully or not.
/// \return SUCCESS if board initialised successfully, FAILURE otherwise (provided that 
///         CBIT_SetBoardInitStatus() was called after board initialisation.
U8 CBIT_GetBoardInitStatus(void);

#ifdef __cplusplus
};
#endif

#endif // _CBIT_H_
