/// \file gps.c
/// This is the implementation file of the gps module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2015/08/28
/// - Module        : GPS
/// - Description   : gps implementation file
///
/// \detail	GPS
///
///
/// \author Casey Stephens (CS), Rossen Ivanov (RPI)
/// \version 
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "gps.h"
#include "scasdbg.h"
#include <math.h>
#include "serialhandler.h"
#include "hwdefs.h"

#include "uart_uc3.h"           ///< UC3 series UART driver
#include "irdahandler.h"        ///< IrDA handler for the UC3 series
#include "string.h"

#include "gpio.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define SERIALHANDLER_BUFFER    (255)

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Local variables
static	char GPS_rxBuffer[SERIALHANDLER_BUFFER];                      ///< Receive
static U16 GPS_rxNmeaLength;                                           ///< Received length


//-------------------------------------------------------------------------------------------------
// Global variables
GPS_Data deviceLocationData;

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
U8 GPS_ServiceRxBuffer(void);

U8 GPS_ServiceNMEA(void);

U8 parseHex(char c);

U8 GPS_NMEA_ServiceGGA(void);
U8 GPS_NMEA_ServiceRMC(void);


//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
U8 GPS_Init()
{
	// Clear any GPS data
	memset(&deviceLocationData, 0, sizeof(GPS_Data));
	
	//Init GPS UART, also used by test station
	if(UART_Init(GPS_UART, GPS_BAUDRATE) == FAILURE)
	{
		return FAILURE;
	}
	else
	{
		////Enable the dead time ISR since the interim GPS parser expects full packets from the GPS
		UART_EnableRxDeadTimeIsr(GPS_UART);
	}
	
	#ifdef SCASII_OC_TAG
	// Initialise the RST and Standby pins.
	gpio_configure_pin(GPS_STANDBY_PIN, GPIO_DIR_OUTPUT);
	gpio_configure_pin(GPS_RESET_PIN, GPIO_DIR_OUTPUT);
	//gpio_set_pin_low(GPS_STANDBY_PIN);						//Keep GPS module in standby mode for now.
	gpio_set_pin_high(GPS_STANDBY_PIN);						//Switch on the GPS module
	gpio_set_pin_high(GPS_RESET_PIN);
	#endif 
	
	//disable all messages from GPS module		 
	UART_TxData(GPS_UART, (U8*)&PMTK_SET_NMEA_OUTPUT_OFF, strlen(PMTK_SET_NMEA_OUTPUT_OFF));
	//turn on GPRMC and GGA messages
	UART_TxData(GPS_UART, (U8*)&PMTK_SET_NMEA_OUTPUT_RMCGGA, strlen(PMTK_SET_NMEA_OUTPUT_RMCGGA));
		 
	//#ifndef HAULTRUCK
		//deviceLocationData.latitude = -261191130;//-261191130;//-261191110;	// bakkie latitude
		//deviceLocationData.longitude = 282029870;//282029870;//282029860;	// bakkie longitude
		//deviceLocationData.altitude = 1540;			// bakkie altitude
		//deviceLocationData.speed = 100;				// bakkie speed
		//deviceLocationData.gpsFixValid = GPS_3D_FIX;
	//#endif
	
	//#ifdef HAULTRUCK
		//deviceLocationData.latitude = -261195860;	// haul truck latitude
		//deviceLocationData.longitude = 282016560;	// haul truck longitude
		//deviceLocationData.altitude = 1543;			// haul truck altitude
		//deviceLocationData.speed = 45;				// haul truck speed
		//deviceLocationData.gpsFixValid = GPS_3D_FIX;
		//deviceLocationData.latitude = -261202640;	// haul truck latitude
		//deviceLocationData.longitude = 282028560;	// haul truck longitude
		//deviceLocationData.altitude = 1543;			// haul truck altitude
		//deviceLocationData.speed = 45;				// haul truck speed
		//deviceLocationData.gpsFixValid = GPS_3D_FIX;
		
	//#endif
	
	return SUCCESS;
}

U8 GPS_Service()
{
	U8 status = 0;		///< Status variable for function returns
	
	GPS_ServiceRxBuffer();
		
	//UART_TxData(GPS_UART, (unsigned char *)"Hello WORLD!!!\r\n", 16);
	
	//if (deviceLocationData.gpsFixValid == GPS_3D_FIX) {
		//SCASDBG_PrintAscii("latitude - ");
		//SCASDBG_PrintU32ToAscii(deviceLocationData.latitude,0);
		//SCASDBG_PrintAscii("\r\n");
//
		//SCASDBG_PrintAscii("longitude - ");
		//SCASDBG_PrintU32ToAscii(deviceLocationData.longitude,0);
		//SCASDBG_PrintAscii("\r\n");
	//}
	
	return status;
}

U8 GPS_ServiceRxBuffer(void)
{	
	U8 status;										///< Status variable for function returns
	U16	gpsUartDataCount = UART_RxBufLen(GPS_UART); // Get the amount of Rx bytes on the GPS UART
	//U16 gpsUartLfPos = 0;
	
	// If there is Rx data in the GPS buffer and there is a complete NMEA packet
	if(gpsUartDataCount )
	{
		// Find position of LF if present in RX buffer
		GPS_rxNmeaLength = UART_RxData_CheckForLF(GPS_UART, gpsUartDataCount);
		// If LF was found then get the NMEA packet
		if (GPS_rxNmeaLength)
		{
				status = UART_GetRxData(GPS_UART, (U8*)&GPS_rxBuffer, GPS_rxNmeaLength);
				//Add a nul terminator since we haven't cleared the buffer before.
				GPS_rxBuffer[GPS_rxNmeaLength] = '\0';
				//Echo back the RX Data
				//status = UART_TxData(GPS_UART, (U8*)&GPS_rxBuffer, GPS_rxNmeaLength);
				//Handle the received NMMEA packet
				GPS_ServiceNMEA();
		}
	}
		return status;
}

U8 GPS_ServiceNMEA(void)
{
	U8	i;
	U16 pcktCheckSum = 0;
	U16 rxCheckSum = 0;
	
	
	//GPS_rxNmeaLength
	//GPS_rxBuffer
	
	//UART_TxData(GPS_UART, &GPS_rxBuffer[strlen(GPS_rxBuffer)-5], 1);
	
	//Check the checksum of the packet
	if (GPS_rxBuffer[strlen(GPS_rxBuffer)-5] == '*') 
	{
		pcktCheckSum = parseHex(GPS_rxBuffer[strlen(GPS_rxBuffer)-4]) * 16;
		 
		pcktCheckSum += parseHex(GPS_rxBuffer[strlen(GPS_rxBuffer)-3]);
		 
		// check checksum
		for ( i=1; i < (strlen(GPS_rxBuffer)-5); i++) 
		{
			rxCheckSum ^= GPS_rxBuffer[i];
		}
		if (rxCheckSum != pcktCheckSum) 
		{
			// bad checksum :(
			//UART_TxData(GPS_UART, (U8*)"Bad Checksum\r\n", 14);
			return false;
		}
		if (strstr(GPS_rxBuffer, "$GPGGA")) 
		{
			GPS_NMEA_ServiceGGA();
						
			//SCASDBG_PrintAscii(GPS_rxBuffer);
			//SCASDBG_PrintAscii("\r\n");

			//SCASDBG_PrintAscii(".latitude - ");
			//SCASDBG_PrintU32ToAscii(deviceLocationData.latitude,0);
//
			//SCASDBG_PrintAscii("\r\n.longitude - ");
			//SCASDBG_PrintU32ToAscii(deviceLocationData.longitude,0);
//
			//SCASDBG_PrintAscii("\r\n.altitude - ");
			//SCASDBG_PrintU32ToAscii(deviceLocationData.altitude,0);
			//SCASDBG_PrintAscii("\r\n");
				
		}
			
		if (strstr(GPS_rxBuffer, "$GPRMC")) 
		{
			GPS_NMEA_ServiceRMC();
			
			//SCASDBG_PrintAscii(GPS_rxBuffer);
			//SCASDBG_PrintAscii("\r\n");

			//SCASDBG_PrintAscii("deviceLocationData.speed - ");
			//SCASDBG_PrintU32ToAscii(deviceLocationData.speed,0);
			//SCASDBG_PrintAscii("\r\n");

			//SCASDBG_PrintAscii("deviceLocationData.heading - ");
			//SCASDBG_PrintU32ToAscii(deviceLocationData.heading,0);
			//SCASDBG_PrintAscii("\r\n");
		}
			
		//if (strstr(GPS_rxBuffer, "$GPVTG")) 
		//{
			////deviceLocationData.speed
			////deviceLocationData.course
		//}
			 
	}
	return 0;
}

U8 GPS_NMEA_ServiceRMC(void)
{
	char *p = GPS_rxBuffer;
	float timef;
	uint32_t time;
			
	// get time
	p = strchr(p, ',')+1;
	timef = atof(p);
	time = timef;
	deviceLocationData.gpsTimeDate.tm_hour = time / 10000;
	deviceLocationData.gpsTimeDate.tm_min = (time % 10000) / 100;
	deviceLocationData.gpsTimeDate.tm_sec = (time % 100);

	//milliseconds = fmod(timef, 1.0) * 1000;

	// check the fix validity
	p = strchr(p, ',')+1;
	if (p[0] == 'V')
	{
		deviceLocationData.gpsFixValid = 0;
		deviceLocationData.gpsTimeDate.tm_hour = 0;
		deviceLocationData.gpsTimeDate.tm_min = 0;
		deviceLocationData.gpsTimeDate.tm_sec = 0;
		return false;
	}

	// parse out latitude
	p = strchr(p, ',')+1;

	// check N or S hemisphere	    
	p = strchr(p, ',')+1;
	    
	// parse out longitude
	p = strchr(p, ',')+1;

	// check E or W hemisphere	    
	p = strchr(p, ',')+1;

	// speed
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		// get speed as a float and convert from knot to Km/h
		deviceLocationData.speed = (U8)((atof(p)*1.852));
	}
	    
	// angle
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		deviceLocationData.heading = atof(p);
	}
	    
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		uint32_t fulldate = atof(p);
		deviceLocationData.gpsTimeDate.tm_mday = fulldate / 10000;
		//need to subtract 1 since our time structure uses 0-11 for Jan to Dec instead of 1 - 12
		deviceLocationData.gpsTimeDate.tm_mon = ((fulldate % 10000) / 100)-1; 
		// need to add 100 since our time structure uses year as year since 1900
		deviceLocationData.gpsTimeDate.tm_year = (fulldate % 100)+100;
	}
	
	return TRUE;
}

U8 GPS_NMEA_ServiceGGA(void)
{
	char *p = GPS_rxBuffer;
	int32_t degree;
	long minutes;
	char degreebuff[10];
	// Fixed point latitude and longitude value with degrees stored in units of 1/100000 degrees,
	int32_t latitude_fixed, longitude_fixed;
//	float geoidheight;
//	float HDOP;
	U8	fixquality = 0;
		
	// get time
	p = strchr(p, ',')+1;
		
	// parse out latitude
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		strncpy(degreebuff, p, 2);
		p += 2;
		degreebuff[2] = '\0';
		degree = atol(degreebuff) * 10000000;
			
		strncpy(degreebuff, p, 2); // minutes
		p += 3; // skip decimal point
			
		strncpy(degreebuff + 2, p, 4);
		degreebuff[6] = '\0';
		minutes = 50 * atol(degreebuff) / 3;
			
		latitude_fixed = degree + minutes;
	}
	    
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		if (p[0] == 'S') latitude_fixed *= -1.0;
		deviceLocationData.latitude = latitude_fixed;
	}
	    
	// parse out longitude
	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		strncpy(degreebuff, p, 3);
		p += 3;
		degreebuff[3] = '\0';
		degree = atol(degreebuff) * 10000000;
		strncpy(degreebuff, p, 2); // minutes
		p += 3; // skip decimal point
		strncpy(degreebuff + 2, p, 4);
		degreebuff[6] = '\0';
		minutes = 50 * atol(degreebuff) / 3;
		longitude_fixed = degree + minutes;			
	}

	p = strchr(p, ',')+1;
	if (',' != *p)
	{
		if (p[0] == 'W') longitude_fixed *= -1.0;
		deviceLocationData.longitude = longitude_fixed;
    }
    
    p = strchr(p, ',')+1;
    if (',' != *p)
    {
	    fixquality = atoi(p);
		//if(fixquality > 0)
		//{
			deviceLocationData.gpsFixValid = fixquality;
			//SCASDBG_PrintAscii("GPS Fix: ");
			//SCASDBG_PrintU8ToAscii(deviceLocationData.gpsFixValid, FALSE);
			//SCASDBG_PrintAscii("\r\n");			
		//}
    }
    
    p = strchr(p, ',')+1;
    //if (',' != *p)
    //{
	    //satellites = atoi(p);
    //}
    
    p = strchr(p, ',')+1;
    //if (',' != *p)
    //{
	    //HDOP = atof(p);
    //}
    
    p = strchr(p, ',')+1;
    if (',' != *p)
    {
	    deviceLocationData.altitude = atof(p);
    }
    
	return TRUE;
}

U8 parseHex(char c) 
{
	if (c < '0')
	return 0;
	if (c <= '9')
	return c - '0';
	if (c < 'A')
	return 0;
	if (c <= 'F')
	return (c - 'A')+10;
	// if (c > 'F')
	return 0;
}


S32 GPS_GetLatitude()
{
	return deviceLocationData.latitude;
}

S32 GPS_GetLongitude()
{
	return deviceLocationData.longitude;
}

U32 GPS_GetAltitude()
{
	return deviceLocationData.altitude;
}

U32 GPS_GetSpeed()
{
	return deviceLocationData.speed;
}

U8 GPS_GetFixValid()
{
	return deviceLocationData.gpsFixValid;
}

S32 GPS_GetDistance(S32 latitude, S32 longitude)
{
	// Uses the haversine formula as described on http://www.movable-type.co.uk/scripts/latlong.html
	//SCASDBG_PrintU32ToAscii((U32)latitude, FALSE);
	//SCASDBG_PrintAscii("\r\n");
	//SCASDBG_PrintU32ToAscii((U32)longitude, FALSE);
	//SCASDBG_PrintAscii("\r\n");
	//SCASDBG_PrintU32ToAscii((U32)GPS_GetLatitude(), FALSE);
	//SCASDBG_PrintAscii("\r\n");
	//SCASDBG_PrintU32ToAscii((U32)GPS_GetLongitude(), FALSE);
	//SCASDBG_PrintAscii("\r\n");
	//SCASDBG_PrintU32ToAscii((U32)deviceLocationData.gpsFixValid, FALSE);
	//SCASDBG_PrintAscii("\r\n");
	
	if ((deviceLocationData.gpsFixValid >= GPS_2D_FIX) && (deviceLocationData.latitude != 0 && deviceLocationData.longitude != 0) && (latitude != 0 && longitude != 0)) {
		U32 r = 6371000;
		double PI = 3.14159265359;
		double decLat1 = ((double)deviceLocationData.latitude)/10000000.0;
		double decLat2 = ((double)latitude)/10000000.0;
		double decLong1 = ((double)deviceLocationData.longitude)/10000000.0;
		double decLong2 = ((double)longitude)/10000000.0;
		double radLat1 = decLat1 * (PI/180.0);
		double radLat2 = decLat2 * (PI/180.0);
		double diffLat = (decLat2 - decLat1) * (PI/180.0);
		double diffLong = (decLong2 - decLong1) * (PI/180.0);
	
		double a = sin(diffLat/2) * sin(diffLat/2) + cos(radLat1) * cos(radLat2) * sin(diffLong/2) * sin(diffLong/2);

		double c = 2 * atan2(sqrt(a), sqrt(1-a)); 

		double d = r * c;	// Distance between the two points in meters
	
		SCASDBG_PrintAscii("GPS Distance: ");
		SCASDBG_PrintU32ToAscii((S32)(d * 100), FALSE);
		SCASDBG_PrintAscii("\r\n");
	
		// return distance in cm
		return (S32)(d * 100);
	}
	else {
		return 0;
	}	
}
//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 