/// \file M24SR04.h
/// This is the implementation file of the M24SR04 module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2016/02/01
/// - Module        : M24SR04
/// - Description   : M24SR04 RFID IC module
///
/// \detail	M24SR04
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------


#ifndef M24SR04_H_
#define M24SR04_H_

#include <stdint.h>

//-------------------------------------------------------------------------------------------------
// Defines

#define M24SR04_I2C_ADDRESS			(0xAC >> 1)

#define USER_ADDRESS				0xA6
#define SECURITY_ADDRESS			0xAE
#define EEPROM_PASSWORD				0x00000000
#define PASSWORD_ADDRESS			0x900
#define CONFIG_ADDRESS				0x910
#define VALIDATE_PASSWORD			0x09
#define E2_ADDR_WRITE_LOCK_BITS		0x800
#define E2_ADDR_UID					0x914
#define E2_ADDR_SECTOR1_SECURITY	0x0000

#define INDEX_ADDR					0x0000
#define SERVICE_ADDR				0x0020
#define CALIBRATION_ADDR			0x0020

#define EEPROM_SIZE					0x1FFF
#define EEPROM_PAGE_SIZE			4
#define WRITE_DELAY					5	//10
#define VERIFY						1
#define DONTVERIFY					0

/* ---------------------- status code ----------------------------------------*/
#define M24SR_ACTION_COMPLETED		0x9000
#define UB_STATUS_OFFSET			4
#define LB_STATUS_OFFSET			3

#define M24SR_NBBYTE_INVALID		0xFFFE

/*-------------------------- File Identifier ---------------------------------*/
#define SYSTEM_FILE_ID		0xE101
#define CC_FILE_ID			0xE103
#define NDEF_FILE_ID		0x0001

/*-------------------------- Password Management -----------------------------*/
#define READ_PWD		0x0001
#define WRITE_PWD		0x0002
#define I2C_PWD			0x0003

/*-------------------------- Verify command answer ----------------------------*/
#define M24SR_PWD_NOT_NEEDED		0x9000
#define M24SR_PWD_NEEDED			0x6300

#define M24SR_PWD_CORRECT			0x9000

/* special M24SR command ----------------------------------------------------------------------*/
#define M24SR_OPENSESSION		0x26
#define M24SR_KILLSESSION		0x52

/*  Length	----------------------------------------------------------------------------------*/
#define M24SR_STATUS_NBBYTE					2
#define M24SR_CRC_NBBYTE					2
#define M24SR_STATUSRESPONSE_NBBYTE			5
#define M24SR_DESELECTREQUEST_NBBYTE		3
#define M24SR_DESELECTRESPONSE_NBBYTE		3
#define M24SR_WATINGTIMEEXTRESPONSE_NBBYTE	4
#define M24SR_PASSWORD_NBBYTE				0x10

/*  Command structure	------------------------------------------------------------------------*/
#define M24SR_CMDSTRUCT_SELECTAPPLICATION		0x01FF
#define M24SR_CMDSTRUCT_SELECTCCFILE			0x017F
#define M24SR_CMDSTRUCT_SELECTNDEFFILE			0x017F
#define M24SR_CMDSTRUCT_READBINARY				0x019F
#define M24SR_CMDSTRUCT_UPDATEBINARY			0x017F
#define M24SR_CMDSTRUCT_VERIFYBINARYWOPWD		0x013F
#define M24SR_CMDSTRUCT_VERIFYBINARYWITHPWD		0x017F
#define M24SR_CMDSTRUCT_CHANGEREFDATA			0x017F
#define M24SR_CMDSTRUCT_ENABLEVERIFREQ			0x011F
#define M24SR_CMDSTRUCT_DISABLEVERIFREQ			0x011F
#define M24SR_CMDSTRUCT_SENDINTERRUPT			0x013F
#define M24SR_CMDSTRUCT_GPOSTATE				0x017F

/*  mask	------------------------------------------------------------------------------------*/
#define M24SR_MASK_BLOCK													0xC0
#define M24SR_MASK_IBLOCK													0x00
#define M24SR_MASK_RBLOCK													0x80
#define M24SR_MASK_SBLOCK													0xC0


/* APDU Command: class list -------------------------------------------*/
#define C_APDU_CLA_DEFAULT	  0x00
#define C_APDU_CLA_ST					0xA2

/*------------------------ Data Area Management Commands ---------------------*/
#define C_APDU_SELECT_FILE     0xA4
#define C_APDU_GET_RESPONCE    0xC0
#define C_APDU_STATUS          0xF2
#define C_APDU_UPDATE_BINARY   0xD6
#define C_APDU_READ_BINARY     0xB0
#define C_APDU_WRITE_BINARY    0xD0
#define C_APDU_UPDATE_RECORD   0xDC
#define C_APDU_READ_RECORD     0xB2

/*-------------------------- Safety Management Commands ----------------------*/
#define C_APDU_VERIFY          0x20
#define C_APDU_CHANGE          0x24
#define C_APDU_DISABLE         0x26
#define C_APDU_ENABLE          0x28

//-------------------------------------------------------------------------------------------------
typedef enum EEPROM_status
{
	EEPROM_IDLE				= 0,
	EEPROM_WRITE_SUCCESS	= 1,
	EEPROM_READ_SUCCESS		= 2,
	EEPROM_WRITE_UNSUCCESS	= 3,
	EEPROM_READ_UNSUCCESS	= 4,
	EEPROM_ADDRESS_FAULT	= 5,
	EEPROM_WRITE_BUSY		= 6,
	EEPROM_READ_BUSY		= 7,
}EEPROM_status_e;

//-------------------------------------------------------------------------------------------------
typedef struct eeprom_control
{
	uint8_t		write;
	uint8_t		read;
	uint8_t		qued_write;
	uint8_t		verify;
}__attribute__ ((packed)) eeprom_control_t, *eeprom_control_p;

typedef struct eeprom_status
{
	uint8_t		write;
	uint8_t		read;
	uint8_t		mode;
}__attribute__ ((packed)) eeprom_status_t, *eeprom_status_p;

typedef struct eeprom
{
	eeprom_control_t	control;
	eeprom_status_t		status;
	uint8_t		i2c_address;
	uint32_t		password;
	uint16_t		write_lockbits;
	uint64_t		uid;
	uint32_t		sector_security;
	uint8_t		configuration;
	uint16_t		length;
	uint16_t		read_address;
	uint16_t		write_address;
	uint8_t	   *buff_p;
	uint8_t		buff[256];
	uint8_t		buff_cmp[256];
	uint16_t		qued_write_address;
	uint8_t		qued_lenght;
	uint8_t	   *qued_buff_p;
}eeprom_ctx_t;

extern eeprom_ctx_t eeprom_ctx;

//-------------------------------------------------------------------------------------------------
// Enums


//-------------------------------------------------------------------------------------------------
// Structures


//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
// Function prototypes
uint32_t NFC_EEPROM_init(void);
uint32_t EEPROM_polling_service(void);
uint32_t EEPROM_service(void);
uint32_t EEPROM_read(uint16_t read_address, uint8_t *string, uint16_t length);
void EEPROM_write(uint16_t write_address, uint8_t *string, uint16_t length, uint8_t verify);
uint32_t EEPROM_read_state_machine(void);
uint32_t EEPROM_save_state_machine(void);
uint32_t EEPROM_read_nxdosi(void);
uint32_t EEPROM_write_nxdosi(void);

//-------------------------------------------------------------------------------------------------

#endif /* M24SR04_H_ */