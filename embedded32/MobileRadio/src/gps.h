/// \file gps.h
/// This is the header file of the gps module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems 
/// - Client        : Schauenburg
/// - Date created  : 2015/08/28
/// - Module        : GPS
/// - Description   : gps header file
///
/// \detail	GPS
///
///
/// \author Casey Stephens (CS), Rossen Ivanov (RPI)
/// \version 
//-------------------------------------------------------------------------------------------------

#ifndef _GPS_H_
#define _GPS_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "common.h"         ///< Common defines such as TRUE, FALSE etc
#include "timer.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
#define GPS_NO_FIX	0
#define GPS_2D_FIX	1
#define GPS_3D_FIX	2

// different commands to set the update rate from once a second (1 Hz) to 10 times a second (10Hz)
// Note that these only control the rate at which the position is echoed, to actually speed up the
// position fix you must also send one of the position fix rate commands below too.
#define PMTK_SET_NMEA_UPDATE_100_MILLIHERTZ  "$PMTK220,10000*2F" // Once every 10 seconds, 100 millihertz.
#define PMTK_SET_NMEA_UPDATE_200_MILLIHERTZ  "$PMTK220,5000*1B"  // Once every 5 seconds, 200 millihertz.
#define PMTK_SET_NMEA_UPDATE_1HZ  "$PMTK220,1000*1F"
#define PMTK_SET_NMEA_UPDATE_5HZ  "$PMTK220,200*2C"
#define PMTK_SET_NMEA_UPDATE_10HZ "$PMTK220,100*2F"
// Position fix update rate commands.
#define PMTK_API_SET_FIX_CTL_100_MILLIHERTZ  "$PMTK300,10000,0,0,0,0*2C" // Once every 10 seconds, 100 millihertz.
#define PMTK_API_SET_FIX_CTL_200_MILLIHERTZ  "$PMTK300,5000,0,0,0,0*18"  // Once every 5 seconds, 200 millihertz.
#define PMTK_API_SET_FIX_CTL_1HZ  "$PMTK300,1000,0,0,0,0*1C"
#define PMTK_API_SET_FIX_CTL_5HZ  "$PMTK300,200,0,0,0,0*2F"
// Can't fix position faster than 5 times a second!

#define PMTK_SET_BAUD_57600 "$PMTK251,57600*2C"
#define PMTK_SET_BAUD_9600 "$PMTK251,9600*17"

// turn on only the second sentence (GPRMC)
#define PMTK_SET_NMEA_OUTPUT_RMCONLY "$PMTK314,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"
// turn on GPRMC and GGA
#define PMTK_SET_NMEA_OUTPUT_RMCGGA "$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"
// turn on GPRMC and GGA and VTG
#define PMTK_SET_NMEA_OUTPUT_RMCGGAVTG "$PMTK314,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n"
// turn on ALL THE DATA
#define PMTK_SET_NMEA_OUTPUT_ALLDATA "$PMTK314,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"
// turn off output
#define PMTK_SET_NMEA_OUTPUT_OFF "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28\r\n"


#define PMTK_LOCUS_STARTLOG  "$PMTK185,0*22"
#define PMTK_LOCUS_STOPLOG "$PMTK185,1*23"
#define PMTK_LOCUS_STARTSTOPACK "$PMTK001,185,3*3C"
#define PMTK_LOCUS_QUERY_STATUS "$PMTK183*38"
#define PMTK_LOCUS_ERASE_FLASH "$PMTK184,1*22"
#define LOCUS_OVERLAP 0
#define LOCUS_FULLSTOP 1

#define PMTK_ENABLE_SBAS "$PMTK313,1*2E"
#define PMTK_ENABLE_WAAS "$PMTK301,2*2E"

// standby command & boot successful message
#define PMTK_STANDBY "$PMTK161,0*28"
#define PMTK_STANDBY_SUCCESS "$PMTK001,161,3*36"  // Not needed currently
#define PMTK_AWAKE "$PMTK010,002*2D"

// ask for the release and version
#define PMTK_Q_RELEASE "$PMTK605*31"

// request for updates on antenna status
#define PGCMD_ANTENNA "$PGCMD,33,1*6C"
#define PGCMD_NOANTENNA "$PGCMD,33,0*6D"

// how long to wait when we're looking for a response
#define MAXWAITSENTENCE 5

//-------------------------------------------------------------------------------------------------
// Enums


//-------------------------------------------------------------------------------------------------
// Structures
/// \struct gps_Data
///  A struct used to store the latest GPS position of this device
typedef struct _GPS_Data {
	U8	gpsFixValid;		// 0 = no valid fix available, 1 = 2D fix, 2 = Differential fix available
	S32 latitude;		// signed decimal degrees 1e7
	S32 longitude;		// signed decimal degrees 1e7
	U32 altitude;		// meters		
	U8 speed;			// km/h
	U16	heading;			// Measured heading in degrees.
	tm	gpsTimeDate;	// GSP time and date
} ATTR_PACKED GPS_Data;

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
U8 GPS_Init(void);
U8 GPS_Service(void);
S32 GPS_GetLatitude(void);
S32 GPS_GetLongitude(void);
U32 GPS_GetAltitude(void);
U32 GPS_GetSpeed(void);
S32 GPS_GetDistance(S32 latitude, S32 longitude);
U8 GPS_GetFixValid(void);

#ifdef __cplusplus
};
#endif

#endif	// _GPS_H_