/// \file mrtest.h
/// This is the header file of the mrtest module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/09/06
/// - Module       : MRTEST
/// - Description  : mrtest interface header
///
/// \details This module performs tasks required by the test station software, such as storing
///          the initialisation results of other software modules.
///
/// \author Michael Manthey (MM), Richard Roche (RR)
/// \version $Id: mrtest.h,v 1.3 2012-08-23 12:33:53+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _MRTEST_H_
#define _MRTEST_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
typedef enum _MRTEST_TestState {
	TEST_MODE_INACTIVE,
	TEST_MODE_ACTIVE,
} MRTEST_TestState;

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function resets the initialisation report for the Mobile Radio
void MRTEST_ResetReport(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the Mobile Radio initialisation report
/// \param report The value to set the initReport to
void MRTEST_SetReport(MESSAGES_Test_Report report);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the init state of the UHF1 module
/// \param initResult SUCCESS of FAILURE
void MRTEST_SetInitResultUHF1(U8 initResult);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the init state of the UHF2 module
/// \param initResult SUCCESS of FAILURE
void MRTEST_SetInitResultUHF2(U8 initResult);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the init state of the DataFlash module
/// \param initResult SUCCESS of FAILURE
void MRTEST_SetInitResultDataFlash(U8 initResult);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the Mobile Radio initialisation report
/// \return The current contents of the initReport
MESSAGES_Test_Report MRTEST_EchoReport(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the current MRTEST state, indicating whether the Mobile Radio is
///        running in test mode or not
/// \return The current MRTEST state
MRTEST_TestState MRTEST_GetState(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the MRTEST state
/// \param report The value to set the state to
void MRTEST_SetState(MRTEST_TestState state);

#ifdef __cplusplus
};
#endif

#endif	// _MRTEST_H_