/// \file p2msgprocess.c
/// This is the implementation file of the p2msgprocess module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2012/05/08
/// - Module        : P2MSGPROCESS
/// - Description   : p2msgprocess implementation
///
///
///
/// \author Richard Roche (RR)
/// \version $Id: p2msgprocess.c,v 1.12 2015-02-17 09:22:16+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: [1]
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "p2msgprocess.h"

#include "calog.h"              ///< SCAS II logging mechanism
#include "msghandler.h"         ///< SCAS II message handler
#include "uhf2_msghandler.h"    ///< UHF2 message handler
#include <string.h>             ///< Used for memcpy etc
#include "paging.h"
#include "ewphandler.h"
#include "ioctl.h"
#include "btuctrl.h"
#include "caectrl.h"
#include "captod.h"

#include <string.h>

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
/// \enum

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
/// \struct

//-------------------------------------------------------------------------------------------------
// Global variables

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;   ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// C function prototypes

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void P2MSGPROCESS_ProcessShort(MESSAGES_UHF2Msg *rxMsg)
{
	TIMER_TimeStamp newTime;                        ///< New time used for updating timer module
	
	//Create a pointer to the received UHF2 packet payload
	U8 *packet = (U8*)&(rxMsg->payload[0]);
	
    // Cast the received packet into the P2 short packet struct
	P2MESSAGES_ShortMessage *p2ShortMsg = (P2MESSAGES_ShortMessage*)packet;
	
	
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
	// For simulating reception of a Protocol 2 Early Warning/Page message when a Sentinel sends
	// a P2MESSAGES_TYPE_SEND_868_RANGE_ALERT message
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
	P2MESSAGES_ShortMessage gasMsg;
	P2MESSAGES_GenericP2PageMessage gasPage;
	
	memcpy((void*)&(gasMsg.destID), (void*)&(g_configBlock.deviceID),sizeof(DeviceID));
	gasMsg.srcID.IDByte1 = UNIT_TYPE_SENTINEL_GDI;
	gasMsg.srcID.IDByte2 = 0x00;
	gasMsg.srcID.IDByte3 = 0x01;
	gasMsg.srcID.IDByte4 = 0x02;
	gasMsg.srcID.IDByte5 = 0x03;
	gasMsg.destPort = P2MESSAGES_PORT_PAGING;
	gasMsg.srcPort = P2MESSAGES_PORT_PAGING;
	gasMsg.type = P2MESSAGES_TYPE_EARLY_WARN_PAGE;
	
	gasPage.actions.threatType = 0x02;      // GAS_EXPLOSIVE
	gasPage.actions.userResponseType = P2MESSAGES_PAGE_RESPONSE_NONE;
	gasPage.actions.ackDelivery = FALSE;
	gasPage.warnDurationS = 0;
	gasPage.threatDistanceCm = -1;
	gasPage.token.sourceDeviceID.IDByte1 = UNIT_TYPE_SENTINEL_GDI;
	gasPage.token.sourceDeviceID.IDByte2 = 0x00;
	gasPage.token.sourceDeviceID.IDByte3 = 0x01;
	gasPage.token.sourceDeviceID.IDByte4 = 0x02;
	gasPage.token.sourceDeviceID.IDByte5 = 0x03;
	gasPage.token.unixTimestamp = TIMER_GetUnixTime();
	gasPage.token.sequence = 0x01;
	strcpy((void*)&(gasPage.str[0]), "\0");
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 

    // Filter initially based on the port
	switch(p2ShortMsg->destPort) {

		case P2MESSAGES_PORT_PING:
		    // Filter based on the message type
			switch(p2ShortMsg->type) {
			    case P2MESSAGES_TYPE_PING_REQ:
				    // Received a request, reply on it, Transmit the P2 short message
			        UHF2_MSGHANDLER_TransmitP2ShortMsg(p2ShortMsg->srcID,
			                                           P2MESSAGES_PORT_PING,
											           P2MESSAGES_TYPE_PING_REPLY,
											           P2MESSAGES_LEN_PING_REPLY,
											           NULL);
			        break;

				case P2MESSAGES_TYPE_PING_REPLY:
				    // A SCAS II device should only transmit this message type
			        break;

				default:
				    // Do nothing
				    break;
			}

	        break;
			
		case P2MESSAGES_PORT_PEER_2_PEER:
		    // Filter based on the message type
		    switch(p2ShortMsg->type) {
				// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
				// Simulate reception of a Protocol 2 Early Warning/Page message
				// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
			    case P2MESSAGES_TYPE_SEND_868_RANGE_ALERT:
				    switch(p2ShortMsg->payload[0]) {
						case PAGING_GAS_TYPE_CH4:
						    // CH4 over range
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "CH4") == 0) {
								// Send to PTC Display, activate red flashing LED, vibrator and buzzer "Methane (CH4) EVACUATE"
						        strcpy((void*)&(gasPage.str[0]), "Methane (CH4) EVACUATE\0");
								memcpy((void*)&(gasMsg.payload[0]), (void*)&(gasPage), sizeof(P2MESSAGES_GenericP2PageMessage));
								// Send the message to the EWPHANDLER module
			                    EWPHANDLER_P2MessageReceived(&gasMsg);
                                IOCTL_SetGasWarning(IO_BLINKING, 5);														 
						    }
							
							// CH4 warning level exceeded
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "ch4") == 0) {
								// Send to PTC Display, activate red flashing LED, vibrator and buzzer "Methane (CH4) EVACUATE"
						        strcpy((void*)&(gasPage.str[0]), "Methane (CH4) EVACUATE\0");
								memcpy((void*)&(gasMsg.payload[0]), (void*)&(gasPage), sizeof(P2MESSAGES_GenericP2PageMessage));
								// Send the message to the EWPHANDLER module
			                    EWPHANDLER_P2MessageReceived(&gasMsg);
                                IOCTL_SetGasWarning(IO_BLINKING, 5);
						    }
							
							// CH4 level safe
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "_ch4_") == 0) {
								
                                IOCTL_SetGasWarning(IO_OFF, 5);
						    }
						    
							break;
							
						case PAGING_GAS_TYPE_CO:
						    // CO over range
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "CO") == 0) {
							    // Send to PTC Display, activate red flashing LED, vibrator and buzzer "Carbon Monox. EVACUATE"
						        strcpy((void*)&(gasPage.str[0]), "Carbon Monox. EVACUATE\0");
								memcpy((void*)&(gasMsg.payload[0]), (void*)&(gasPage), sizeof(P2MESSAGES_GenericP2PageMessage));
								// Send the message to the EWPHANDLER module
			                    EWPHANDLER_P2MessageReceived(&gasMsg);
                                IOCTL_SetGasWarning(IO_BLINKING, 5);
						    }
						    
							// CO warning level exceeded
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "co") == 0) {
							    // Send to PTC Display, activate red flashing LED, vibrator and buzzer "Carbon Monox. EVACUATE"
						        strcpy((void*)&(gasPage.str[0]), "Carbon Monox. EVACUATE\0");
								memcpy((void*)&(gasMsg.payload[0]), (void*)&(gasPage), sizeof(P2MESSAGES_GenericP2PageMessage));
								// Send the message to the EWPHANDLER module
			                    EWPHANDLER_P2MessageReceived(&gasMsg);
								IOCTL_SetGasWarning(IO_BLINKING, 5);
						    }
							
							// CO level safe
						    if(strcmp((const char *)&(p2ShortMsg->payload[1]), "_co_") == 0) {
							    
                                IOCTL_SetGasWarning(IO_OFF, 5);
						    }
							break;
						default:
						    break;
				    }
			        break;

				default:
				    // Do nothing
				    break;
			}
		    break;

		case P2MESSAGES_PORT_SCASII:
		    // Filter based on the message type
			switch(p2ShortMsg->type) {
			    case P2MESSAGES_TYPE_REPLY_SYS_LOGS:
				    // A SCAS II device should only transmit this message type
			        break;

				case P2MESSAGES_TYPE_REQ_NEXT_SYS_LOGS_FROM:
				    CALOG_UploadLogs(p2ShortMsg->srcID, &(p2ShortMsg->payload[0]));
			        break;

				default:
				    // Do nothing
				    break;
			}
	        break;
			
		case P2MESSAGES_PORT_SCASII_OPTEST:
		    // Filter based on the message type
			switch(p2ShortMsg->type) {
			    case P2MESSAGES_TYPE_BTU_TEST_COMMAND:
				    // A Brake Test Unit should only transmit this message type
			        break;

				case P2MESSAGES_TYPE_BTU_TEST_RESPONSE:
				    // Pass the message on to the BTUCTRL module
					BTUCTRL_P2MsgProcess(p2ShortMsg);
			        break;
					
				case P2MESSAGES_TYPE_RANGE_TEST_RESPONSE:
				    // Pass the message on to the BTUCTRL module
					CAPTOD_HandleRangingResult(p2ShortMsg,rxMsg->rssi_lqi[0]);
			        break;
					
			    case P2MESSAGES_TYPE_RANGE_TEST_REQUEST:
				    // Should only transmit this message type
			        break;						

				default:
				    // Do nothing
				    break;
			}					
			break;
			
		case P2MESSAGES_PORT_SCASII_CA_RESULTS:
		    // Filter based on the message type
		    switch(p2ShortMsg->type) {
		    	case P2MESSAGES_TYPE_CAE_TRANSFER_RANGE_RESULT:
				    CAECTRL_P2MsgProcess(p2ShortMsg);
		    		break;
					
		    	default:
				    // Do nothing
		    		break;
		    }
			break;
			
		case P2MESSAGES_PORT_PAGING:

			// Send the message to the EWPHANDLER module
			EWPHANDLER_P2MessageReceived(p2ShortMsg);
			break;

		case P2MESSAGES_PORT_BROADCAST:
		    // Filter based on the message type
			switch(p2ShortMsg->type) {
			    case P2MESSAGES_TYPE_SET_TIME:
				    //  Set local Timer timestamp
					memcpy(&newTime, &(p2ShortMsg->payload[0]), sizeof(newTime));
                    TIMER_SetTimeStamp(newTime);
			        break;

				default:
				    // Do nothing
				    break;
			}
	        break;

		default:
		    // Ignore the message
	        break;
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief

