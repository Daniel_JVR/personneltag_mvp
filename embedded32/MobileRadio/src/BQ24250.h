/// \file BQ24250.h
/// This is the header file of the BQ24250 module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2016/01/19
/// - Module        : BQ24250
/// - Description   : TI BQ24250 battery charger module
///
/// \detail	BQ24250
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------

#ifndef _BQ24250_H_
#define _BQ24250_H_

//-------------------------------------------------------------------------------------------------
// Include files

#include "hwdefs.h"
#include "status_codes.h"

#ifdef __cplusplus
extern "C" {    // C++
	#endif

//-------------------------------------------------------------------------------------------------
// Defines

#define BQ24250_I2C_ADDRESS	0x6A

#define BQ24250_I2C_REG1	0x00
#define BQ24250_I2C_REG2	0x01
#define BQ24250_I2C_REG3	0x02
#define BQ24250_I2C_REG4	0x03
#define BQ24250_I2C_REG5	0x04
#define BQ24250_I2C_REG6	0x05
#define BQ24250_I2C_REG7	0x06

//BQ24250_I2C_REG1 bit locations
#define REG1_WD_FAULT		7
#define REG1_WD_EN			6
//00 � Ready
//01 � Charge in progress
//10 � Charge done
//11 � Fault
#define REG1_STAT1			5
#define REG1_STAT0			4
#define REG1_STAT_MASK		0x30
//0000 � Normal
//0001 � Input OVP
//0010 � Input UVLO
//0011 � Sleep
//0100 � Battery Temperature (TS) Fault
//0101 � Battery OVP
//0110 � Thermal Shutdown
//0111 � Timer Fault
//1000 � No Battery connected
//1001 � ISET short
//1010 � Input Fault and LDO low
#define REG1_FAULT3			3
#define REG1_FAULT2			2
#define REG1_FAULT1			1
#define REG1_FAULT0			0
#define REG1_FAULT_MASK		0x0F

#define BQ_FAULT_NO_FAULT			0x00
#define BQ_FAULT_VIN_OVP			0x01
#define BQ_FAULT_VIN_UVLO			0x02
#define BQ_FAULT_SLEEP				0x03
#define BQ_FAULT_BAT_TEMP_FAULT		0x04
#define BQ_FAULT_BAT_OVP			0x05
#define BQ_FAULT_THERM_SHDN			0x06
#define BQ_FAULT_TMR_FAULT			0x07
#define BQ_FAULT_NO_BATTERY			0x08
#define BQ_FAULT_ISET_SHORT			0x09
#define BQ_FAULT_IN_FAULT			0x10

#define BQ_CHARGE_CNTRL_READY		0x00
#define BQ_CHARGE_CNTRL_CHARGING	0x10
#define BQ_CHARGE_CNTRL_DONE		0x20
#define BQ_CHARGE_CNTRL_FAULT		0x30


//BQ24250 BQ24250_I2C_REG2 bit locations
#define REG2_RST			7
//000 � USB2.0 host with 100mA current limit
//001 � USB3.0 host with 150mA current limit IN_ILIMIT_1 Read/Write
//010 � USB2.0 host with 500mA current limit
//011 � USB3.0 host with 900mA current limit
//100 � Charger with 1500mA current limit
//101 � Charger with 2000mA current limit
//110 � External ILIM current limit
//111 - No input current limit with internal clamp at 3A (PTM MODE)
#define REG2_IN_ILIM2		6
#define REG2_IN_ILIM1		5
#define REG2_IN_ILIM0		4
#define REG2_IN_ILIM_MASK	0x70
#define REG2_EN_STAT		3
#define REG2_EN_TERM		2	//	
#define REG2_EN_CE			1	// 0 - charge enabled, 1 - charge disabled
#define REG2_HZ_MODE		0	//	1 - high impedance mode

//BQ24250 BQ24250_I2C_REG3 bit locations
//Charge voltage range is 3.5V�4.44V with the offset of 3.5V and step of 20mV (default 4.2V)
#define REG3_VBATREG_5		7	//640mV (default 1)
#define REG3_VBATREG_4		6	//320mV (default 0)
#define REG3_VBATREG_3		5	//160mV (default 0)
#define REG3_VBATREG_2		4	//80mV (default 0)
#define REG3_VBATREG_1		3	//40mV (default 1)
#define REG3_VBATREG_0		2	//20mV (default 1)
#define REG3_VBATREG_MASK	0xFC
#define REG3_EN2_PIN		1
#define REG3_EN1_PIN		0

//BQ24250 BQ24250_I2C_REG4 bit locations
//Charge current offset is 500 mA and default charge current is external (maximum is 2.0A)
//When all bits are 1�s, it is external ISET charging mode
#define REG4_ICHG_4			7	// 800mA � (default 1)
#define REG4_ICHG_3			6	// 400mA � (default 1)
#define REG4_ICHG_2			5	// 200mA � (default 1)
#define REG4_ICHG_1			4	// 100mA � (default 1)
#define REG4_ICHG_0			3	// 50mA � (default 1)
#define REG4_ICHG_MASK		0xF8
//Termination threshold voltage offset is 50mA. The default termination current is 50mA if the charge is selected from I2C. Otherwise,
//termination is set to 10% of ICHG in external I_set mode with +/-10% accuracy.
#define REG4_ITERM_2		2	// 100mA (default 0)
#define REG4_ITERM_1		1	// 50mA (default 0)
#define REG4_ITERM_0		0	// 25mA (default 0)
#define REG4_ITERM_MASK		0x07

//BQ24250 BQ24250_I2C_REG5 bit locations
//LOOP_STATUS bits show if there are any loop is active that slow down the safety timer. If a status occurs, these bits announce the
//status and do not clear until read. If more than one occurs, the first one is shown.
//00 � No loop is active that slows down timer
//01 � VIN_DPM regulation loop is active
//10 � Input current limit loop is active
//11 � Thermal regulation loop is active
#define REG5_LOOP_STATUS1	7
#define REG5_LOOP_STATUS0	6
#define REG5_LOOP_STATUS_MASK	0xC0

#define REG5_LOW_CHG		5
#define REG5_DPDM_EN		4
#define REG5_CE_STATUS		3
//VIN-DPM voltage offset is 4.20V and default VIN_DPM threshold is 4.36V.
#define REG5_VINDPM_2		2	// 320mV (default 0)
#define REG5_VINDPM_1		1	// 160mV (default 1)
#define REG5_VINDPM_0		0	// 80mV (default 0)
#define REG5_VINDPM_MASK	0x07

//BQ24250 BQ24250_I2C_REG6 bit locations
#define REG6_2XTMR_EN		7
//Safety Timer Time Limit
//00 � 0.75 hour fast charge
//01 � 6 hour fast charge (default 01)
//10 � 9 hour fast charge
//11 � Disable safety timers
#define REG6_TMR_1			6
#define REG6_TMR_2			5
#define REG6_TMR_MASK		0x60

#define REG6_SYSOFF			4	// 0 � SYSOFF disabled, 1 � SYSOFF enabled
#define	REG6_TS_EN			3
//TS Fault Mode:
//000 � Normal, No TS fault
//001 � TS temp > THOT (Charging suspended for JEITA and Standard TS)
//010 � TWARM < TS temp < THOT (Regulation voltage is reduced for JEITA standard)
//011 � TCOLD < TS temp < TCOOL (Charge current is reduced for JEITA standard)
//100 � TS temp < TCOLD (Charging suspended for JEITA and Standard TS)
//101 � TFREEZE < TS temp < TCOLD (Charging at 3.9V and 100mA and only for PSE option only)
//110 � TS temp < TFREEZE (Charging suspended for PSE option only)
//111 � TS open (TS disabled)
#define REG6_TS_STAT2		2
#define REG6_TS_STAT1		1
#define REG6_TS_STAT0		0
#define REG6_TS_STAT_MASK	0x07

//BQ24250 BQ24250_I2C_REG7 bit locations
//OVP voltage:
//B6 VOVP_1 Read/Write 000 � 6.0V; 001 � 6.5V; 010 � 7.0V; 011 � 8.0V
//B5 VOVP_0 Read/Write 100 � 9.0V; 101 � 9.5V; 110 � 10.0V; 111 �10.5V
#define REG7_VOVP_2			7
#define REG7_VOVP_1			6
#define REG7_VOVP_0			5
#define REG7_VOVP_MASK		0xE0

#define REG7_CLR_VDP		4	//1 � Turn off D+ voltage source to release D+ line
#define REG7_FORCE_BAT_DET	3	// Read/Write 0 � Enter the battery detection routine only if TERM is true or Force PTM is true 
#define REG_FORCE_PTM		2	//1 � PTM (production test mode) mode is enabled

//-------------------------------------------------------------------------------------------------
// Enums

typedef enum{
	NO_FAULT = 0,
	VIN_OVP,
	VIN_UVLO,
	SLEEP,
	TEMP_FAULT,
	BAT_OVP,
	THERM_SHDN,
	TMR_FAULT,
	NO_BATTERY,
	ISET_SHORT,
	IN_FAULT,
	
	UNKNOWN_ERROR

}bq24250FaultEnum;

typedef enum{
	CHARGE_CNTRL_READY = 0,
	CHARGE_CNTRL_CHARGING,
	CHARGE_CNTRL_DONE,
	CHARGE_CNTRL_FAULT,
	UNKNOWN_STATE
}bq24250ChargeControllerStatus;

//-------------------------------------------------------------------------------------------------
// Structures


//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
void BQ24250_Init_GPIO(void);
status_code_t BQ24250_Init(void);
status_code_t BQ24250_Service(void);

bq24250FaultEnum BQ24250_CheckFault(void);
bq24250ChargeControllerStatus BQ24250_ChargerStatus(void);


//-------------------------------------------------------------------------------------------------
#ifdef __cplusplus
};
#endif

#endif	// _BQ24250_H_