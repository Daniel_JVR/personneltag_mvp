/// \file slinkmon.h
/// This is the header file of the slinkmon module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/12/15
/// - Module        : SLINKMON
/// - Description   : slinkmon header file
///
/// \detail	slinkmon
///
///
/// \author Michael Manthey (MM)
/// \version $Id: slinkmon.h,v 1.0 2014-12-15 12:52:12+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _SLINKMON_H_
#define _SLINKMON_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "messages.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
void SLINKMON_PrintArray(void);

//-------------------------------------------------------------------------------------------------
/// \brief 
void SLINKMON_UHF2PacketReceived(MESSAGES_UHF2Msg *rxMsg);

//-------------------------------------------------------------------------------------------------
/// \brief 
void SLINKMON_SLinkMessageReceived(DeviceID slinkID, U8 rssi);

#ifdef __cplusplus
};
#endif

#endif	// _SLINKMON_H_