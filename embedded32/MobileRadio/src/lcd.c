/// \file lcd.c
/// This is the implementation file of the lcd module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2015/09/01
/// - Module        : LCD
/// - Description   : lcd implementation file
///
/// \detail	lcd
///
///
/// \author Casey Stephens (CS)
/// \version 
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "lcd.h"
#include "messages.h"
#include "msghandler.h"
#include "msgdispatch.h"
#include "scasdbg.h"
#include "config_block_uc3.h"
#include "rangetag.h"
#include "gps.h"
#include "buzzerCtrl.h"
#include <string.h>             ///< Used for memcmp and memset

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct LCD_Threat {
	U8 threatType;		// according to UnitType
	U8 zone;			// according to MESSAGES_SpatialZone
	double latitude;		// decimal degrees
	double longitude;	// decimal degrees
} ATTR_PACKED LCD_Threat;

typedef struct LCD_Control_Message {
	bool workerExclusion;
	bool driverExclusion;
	bool ignitionInterlock;
} ATTR_PACKED LCD_Control_Message;

// TODO Need to add an error message as well

//-------------------------------------------------------------------------------------------------
// Global variables
RANGETAG_Warning_Table_Entry mThreatBuffer[CONFIG_MAX_CA_DEVICES];	 
LCD_Threat mThreatMessageBuffer[CONFIG_MAX_CA_DEVICES + 1];				// First threat in the message is this device 
U8 mThreatCount;
U8 mThreatMessageCount;
LCD_Control_Message mControlMessage;

U8 threatsInCaution = 0;
U8 threatsInWarning = 0;
U8 threatsInCritical = 0;
U8 newThreatsInCaution = 0;
U8 newThreatsInWarning = 0;
U8 newThreatsInCritical = 0;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;           ///< Global configuration items of unit
static U32 startTime; 

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
/// \ brief This function 
void LCD_Init() {
	MSGDISPATCH_RegisterCallbackForMessageClass(MSGDISPATCH_CLASS_LCD_COMMS, &LCD_MessageProcess);
	
	// clear threat buffer
	memset(&mThreatBuffer, 0, sizeof(RANGETAG_Warning_Table_Entry));
	memset(&mThreatMessageBuffer, 0, sizeof(LCD_Threat));
	mThreatCount = 0;
	mThreatMessageCount = 0;
	
	// clear control message
	memset(&mControlMessage, 0, sizeof(LCD_Control_Message));
	
	startTime = TIMER_LongTimeNow();
}

void LCD_Service(void) {
	newThreatsInCaution = 0;
	newThreatsInWarning = 0;
	newThreatsInCritical = 0;
	
	memset(&mThreatBuffer, 0, sizeof(RANGETAG_Warning_Table_Entry));
	memset(&mThreatMessageBuffer, 0, sizeof(LCD_Threat));
	mThreatCount = 0;
	
	if (GPS_GetFixValid() >= GPS_2D_FIX) {
		mThreatMessageBuffer[0].latitude = ((double)(GPS_GetLatitude()))/10000000.0;
		mThreatMessageBuffer[0].longitude = ((double)(GPS_GetLongitude()))/10000000.0;
		mThreatMessageBuffer[0].threatType = (U8)g_configBlock.deviceID.IDByte1;
		mThreatMessageBuffer[0].zone = (U8)0x00;	
	}	
	else {
		mThreatMessageBuffer[0].latitude = 0;
		mThreatMessageBuffer[0].longitude = 0;
		mThreatMessageBuffer[0].threatType = (U8)g_configBlock.deviceID.IDByte1;
		mThreatMessageBuffer[0].zone = (U8)0x00;
	}
	
	mThreatMessageCount = 1; // First message in the buffer is for this device location
	
	RANGETAG_GetWarningTable(&mThreatBuffer, &mThreatCount);	
	
	for (int i = 0; i < mThreatCount; i++) {	
		// Only consider threats that have coordinates or have been ranged with (i.e. in a spatial zone other than 0)
		if (((mThreatBuffer[i].latitude != (S32)0) && (mThreatBuffer[i].longitude != (S32)0)) || (mThreatBuffer[i].zone != ((U8)MESSAGES_SPATIAL_ZONE_0))) {
			mThreatMessageBuffer[mThreatMessageCount].latitude = ((double)(mThreatBuffer[i].latitude))/10000000.0;
			mThreatMessageBuffer[mThreatMessageCount].longitude = ((double)(mThreatBuffer[i].longitude))/10000000.0;
			mThreatMessageBuffer[mThreatMessageCount].threatType = mThreatBuffer[i].threatType;
			mThreatMessageBuffer[mThreatMessageCount].zone = mThreatBuffer[i].zone;
			
			switch (mThreatMessageBuffer[mThreatMessageCount].zone) {
				case MESSAGES_SPATIAL_ZONE_1:
					newThreatsInCaution++;
					break;
				case MESSAGES_SPATIAL_ZONE_2:
					newThreatsInWarning++;
					break;
				case MESSAGES_SPATIAL_ZONE_3:
					newThreatsInCritical++;
					break;
				default:
					break;				
			}
			
			mThreatMessageCount++;
		}			
	}	   
	
	//if(TIMER_LongTimeElapsed(startTime) >= 0 && TIMER_LongTimeElapsed(startTime) < 1) {
		//mThreatMessageBuffer[1].latitude = -26.120664;
		//mThreatMessageBuffer[1].longitude = 28.201853;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x00;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 1 && TIMER_LongTimeElapsed(startTime) < 2) {
		//mThreatMessageBuffer[1].latitude = -26.120611;
		//mThreatMessageBuffer[1].longitude = 28.201981;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x01;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 2 && TIMER_LongTimeElapsed(startTime) < 3) {
		//mThreatMessageBuffer[1].latitude = -26.120514;
		//mThreatMessageBuffer[1].longitude = 28.202203;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x01;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 3 && TIMER_LongTimeElapsed(startTime) < 4) {
		//mThreatMessageBuffer[1].latitude = -26.120386;
		//mThreatMessageBuffer[1].longitude = 28.202419;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x02;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 4 && TIMER_LongTimeElapsed(startTime) < 5) {
		//mThreatMessageBuffer[1].latitude = -26.120444;
		//mThreatMessageBuffer[1].longitude = 28.202506;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x02;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 5 && TIMER_LongTimeElapsed(startTime) < 6) {
		//mThreatMessageBuffer[1].latitude = -26.120267;
		//mThreatMessageBuffer[1].longitude = 28.202622;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x03;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 6 && TIMER_LongTimeElapsed(startTime) < 7) {
		//mThreatMessageBuffer[1].latitude = -26.120172;
		//mThreatMessageBuffer[1].longitude = 28.202722;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x03;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 7 && TIMER_LongTimeElapsed(startTime) < 8) {
		//mThreatMessageBuffer[1].latitude = -26.120031;
		//mThreatMessageBuffer[1].longitude = 28.202744;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x02;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 8 && TIMER_LongTimeElapsed(startTime) < 9) {
		//mThreatMessageBuffer[1].latitude = -26.119856;
		//mThreatMessageBuffer[1].longitude = 28.202858;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x02;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 9 && TIMER_LongTimeElapsed(startTime) < 10) {
		//mThreatMessageBuffer[1].latitude = -26.119689;
		//mThreatMessageBuffer[1].longitude = 28.202939;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x01;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 10 && TIMER_LongTimeElapsed(startTime) < 11) {
		//mThreatMessageBuffer[1].latitude = -26.119497;
		//mThreatMessageBuffer[1].longitude = 28.202989;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x01;
		//mThreatMessageCount = 2; 
	//}
	//
	//else if(TIMER_LongTimeElapsed(startTime) >= 11 && TIMER_LongTimeElapsed(startTime) < 12) {
		//mThreatMessageBuffer[1].latitude = -26.119331;
		//mThreatMessageBuffer[1].longitude = 28.203125;
		//mThreatMessageBuffer[1].threatType = 0x03;
		//mThreatMessageBuffer[1].zone = (U8)0x01;
		//mThreatMessageCount = 2; 
	//}
	//
	//else {
		//startTime = TIMER_LongTimeNow();
		//mThreatMessageCount = 1; 
	//}
	
	//mThreatMessageBuffer[1].latitude = -26.120253;	// 100 horizontal
	//mThreatMessageBuffer[1].longitude = 28.201856;
	//mThreatMessageBuffer[1].threatType = 0x03;
	//mThreatMessageBuffer[1].zone = (U8)0x01;
		//
	//mThreatMessageCount++;
		//
	//mThreatMessageBuffer[2].latitude = -26.119361; // 100 vertical
	//mThreatMessageBuffer[2].longitude = 28.202864;
	//mThreatMessageBuffer[2].threatType = 0x03;
	//mThreatMessageBuffer[2].zone = (U8)0x01;
		//
	//mThreatMessageCount++;
	
	//mThreatMessageBuffer[3].latitude = -26.120264;	// 50 horizontal
	//mThreatMessageBuffer[3].longitude = 28.202358;
	//mThreatMessageBuffer[3].threatType = 0x03;
	//mThreatMessageBuffer[3].zone = (U8)0x02;
		//
	//mThreatMessageCount++;
		//
	//mThreatMessageBuffer[4].latitude = -26.119361; // 50 vertical
	//mThreatMessageBuffer[4].longitude = 28.202864;
	//mThreatMessageBuffer[4].threatType = 0x03;
	//mThreatMessageBuffer[4].zone = (U8)0x02;
		//
	//mThreatMessageCount++;
	
	if (newThreatsInCritical != 0) {
		//SCASDBG_PrintAscii("Turning on CRITICAL Buzzer\r\n");
		BUZZER_SoundOCritical();
	}
	else if (newThreatsInWarning != 0) {
		//SCASDBG_PrintAscii("Turning on WARNING Buzzer\r\n");
		BUZZER_SoundWarning();
	}
	else if (newThreatsInCaution > threatsInCaution) {
		//SCASDBG_PrintAscii("Turning on CAUTION Buzzer\r\n");
		BUZZER_SoundCaution();
	}
	else if (newThreatsInCritical == 0 && newThreatsInWarning == 0 && newThreatsInCaution == 0 && (threatsInCritical != 0 || threatsInWarning != 0 || threatsInCaution != 0)){
		//SCASDBG_PrintAscii("Turning OFF Buzzer\r\n");
		BUZZER_SoundOff();
	}
	
	threatsInCaution = newThreatsInCaution;
	threatsInWarning = newThreatsInWarning;
	threatsInCritical = newThreatsInCritical;
	
	//MSGHANDLER_TransmitPacketG2(MESSAGES_DEST_MU, MESSAGES_DEST_MR_LCD, MSGDISPATCH_CLASS_LCD_COMMS, MESSAGES_TYPE_LCD_THREATS_RES, (U16)(0), (U8*)mThreatMessageBuffer); 
}

void LCD_MessageProcess(MESSAGES_PacketG2 *packet, MSGHANDLER_RxChannel rxChannel) {
	switch (packet->msgType) {
		case MESSAGES_TYPE_LCD_THREATS_REQ:
			MSGHANDLER_TransmitPacketG2(MESSAGES_DEST_MU, MESSAGES_DEST_MR_LCD, MSGDISPATCH_CLASS_LCD_COMMS, MESSAGES_TYPE_LCD_THREATS_RES, (U16)(mThreatMessageCount * sizeof(LCD_Threat)), (U8*)mThreatMessageBuffer); 
			break;
		case MESSAGES_TYPE_LCD_CONTROL_REQ:
			break;
		default:
			break;
	}
}
//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 