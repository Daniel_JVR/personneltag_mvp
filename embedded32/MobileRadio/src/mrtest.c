/// \file mrtest.c
/// This is the implementation file of the mrtest module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/09/06
/// - Module        : MRTEST
/// - Description   : mrtest implementation
///
/// \details This module performs tasks required by the test station software, such as storing
///          the initialisation results of other software modules.
///
/// \author Michael Manthey (MM), Richard Roche (RR)
/// \version $Id: mrtest.c,v 1.5 2015-01-20 10:53:24+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: RANGINGTEST module for the Ranging Unit
///                   Richard Roche
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "mrtest.h"

#include "timer.h"                              ///< Handles system time
#include "adc.h"                                ///< ADC software driver for AVR UC3
#include "flash_uc3.h"                          ///< Handles the dataFlash
#include "msghandler.h"                         ///< SCAS II message handler
#include "uhf1.h"                               ///< UHF1 module

#include <string.h>                             ///< Used for memset

//-------------------------------------------------------------------------------------------------
// Defines
#define VOLTAGE_MIN             ((float)(3.3))  ///< The lower voltage limit for the Mobile Radio
#define UHF1_TEST_TIMEOUT       (1024)          ///< Timeout for the UHF1 channel to respond

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct _MRTEST_TestLib {
	MESSAGES_Test_Report initReport;
    MRTEST_TestState state;
} ATTR_PACKED MRTEST_TestLib;

//-------------------------------------------------------------------------------------------------
// Global variables
static MRTEST_TestLib testLib = {{0},0};

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void MRTEST_ResetReport(void)
{
	// Set the report to FAILURE on all counts
	memset(&(testLib.initReport), FAILURE, sizeof(testLib.initReport));
}

//-------------------------------------------------------------------------------------------------
void MRTEST_SetReport(MESSAGES_Test_Report report)
{
	testLib.initReport = report;
}

//-------------------------------------------------------------------------------------------------
void MRTEST_SetInitResultUHF1(U8 initResult)
{
	testLib.initReport.UHF1 = initResult;
}

//-------------------------------------------------------------------------------------------------
void MRTEST_SetInitResultUHF2(U8 initResult)
{
	testLib.initReport.UHF2 = initResult;
}

//-------------------------------------------------------------------------------------------------
void MRTEST_SetInitResultDataFlash(U8 initResult)
{
	testLib.initReport.dataFlash = initResult;
}

//-------------------------------------------------------------------------------------------------
MESSAGES_Test_Report MRTEST_EchoReport(void)
{
	// Transmit over IRDA
	MSGHANDLER_TransmitPacket(MESSAGES_DEST_IRDA,
		                      MESSAGES_TYPE_UNIT_REPORT,
		                      MESSAGES_LEN_UNIT_REPORT,
							  (U8*)&(testLib.initReport));
    // Transmit over PTC UART which is used to communicate to the Test Jig during production testing
    ////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC,
	////	                      MESSAGES_TYPE_UNIT_REPORT,
	////	                      MESSAGES_LEN_UNIT_REPORT,
	////						  (U8*)&(testLib.initReport));
	// Return initReport for (optional) external use
	return testLib.initReport;
}

//-------------------------------------------------------------------------------------------------
MRTEST_TestState MRTEST_GetState(void)
{
	return testLib.state;
}

//-------------------------------------------------------------------------------------------------
void MRTEST_SetState(MRTEST_TestState state)
{
	testLib.state = state;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
// none
