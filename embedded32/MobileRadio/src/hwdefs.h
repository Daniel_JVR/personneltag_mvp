/// \file hwdefs.h
/// This is the hardware definitions file of the MobileRadio module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/08/29
/// - Module       : HWDEFS
/// - Description  : hwdefs interface header
///
/// \details This is the hardware definitions file for the SCAS II Mobile Radio PCB.
///
/// \note:  Communication peripherals used are:
///         ___________________________________________________________________
///         | Peripheral  |  Interface                  | DMA Channels        |
///         |-----------------------------------------------------------------|
///         | SPI Port 0  |  UHF2                       | Rx:  1;    Tx:  0   |
///         | SPI Port 1  |  UHF1                       | Rx:  3;    Tx:  2   |
///         | USPI Port 0 |  DataFlash                  | Rx:  5;    Tx:  4   |
///         | UART Port 1 |  IrDA                       | Rx:  7;    Tx:  6   |
///         | UART Port 2 |  Unused                     | Rx:  _;    Tx:  _   |
///         | USPI Port 3 |  VLF (future use)           | Rx:  _;    Tx:  _   |
///         | UART Port 4 |  PTC Display (future use)   | Rx: 13;    Tx: 12   |
///         -------------------------------------------------------------------
///
///
/// \author Michael Manthey (MM)
/// \version $Id: hwdefs.h,v 1.18 2013-09-19 11:46:31+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _HWDEFS_H_
#define _HWDEFS_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros


#include "board.h"

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none
//#define UHF1_CHIRP_TEST

// PAGING_ENABLED must be defined to ensure that paging messages received over UHF2 on the collision
// avoidance DLL are routed to the PAGING module
#define PAGING_ENABLED

//-------------------------------------------------------------------------------------------------
/// Clock definitions

///< Osc0 frequency [Hz]
#define FOSC0                   (12000000)
///< Osc0 startup time [RCOsc periods]
#define OSC0_STARTUP            (AVR32_SCIF_OSCCTRL0_STARTUP_32768_RCOSC)

///< Osc32 frequency [Hz]
#define FOSC32                  (32768)
///< Osc32 startup time [RCOsc periods]
#define OSC32_STARTUP           (AVR32_SCIF_OSCCTRL32_STARTUP_8192_RCOSC)

#define BOARD_OSC0_HZ           (FOSC0)
#define BOARD_OSC0_STARTUP_US   (71000)
#define BOARD_OSC0_IS_XTAL      true
#define BOARD_OSC32_HZ          (FOSC32)
#define BOARD_OSC32_STARTUP_US  (71000)
#define BOARD_OSC32_IS_XTAL     true

///*******************************
///< NB: The defines below will cause the TIMER module to use a software alternative to the AST.
#define TIMER_CLK_HZ            (48000000)              // fPBA
#define TIMER_PRESCALER         (128)                   // prescaler
#define TIMER_CLK_SRC           (TC_CLOCK_SOURCE_TC5)   // Internal source clock 5, connected to fPBA / 128.
// #define USE_SW_CALENDAR      // No longer needed, SW_CALENDAR used by default
///*******************************

#define DBG_USART               (&AVR32_USART1)
#define DBG_USART_RX_PIN        AVR32_USART1_RXD_1_PIN
#define DBG_USART_RX_FUNCTION   AVR32_USART1_RXD_1_FUNCTION
#define DBG_USART_TX_PIN        AVR32_USART1_TXD_1_PIN
#define DBG_USART_TX_FUNCTION   AVR32_USART1_TXD_1_FUNCTION
#define DBG_USART_BAUDRATE      (115200)

/// Diag_Terminal defines
#define DEBUG_UART              (1)
#define DEBUG_BAUD_RATE         (DBG_USART_BAUDRATE)
#define NO_MSG_HANDLER

//-------------------------------------------------------------------------------------------------
/// LED's
#ifdef SCASII_OC_TAG
#define VU_LED_PIN              (AVR32_PIN_PC11)
#else
#define VU_LED_PIN              (AVR32_PIN_PC07)
#endif 

#ifdef SCASII_OC_TAG
#define LU_LED_PIN              (AVR32_PIN_PC07)
#else
#define LU_LED_PIN              (AVR32_PIN_PC00)
#endif 

#define HU_LED_PIN              (AVR32_PIN_PC01)

#ifdef SCASII_OC_TAG
#define EXCL_LED_PIN            (AVR32_PIN_PC24)
#else
#define EXCL_LED_PIN            (AVR32_PIN_PC11)
#endif

#define DLED1_PIN               (AVR32_PIN_PB02)
#define DLED2_PIN               (AVR32_PIN_PB03)
#define RUN_LED_PIN             (DLED1_PIN)
#define LED_PLLCHECK_PIN        (DLED2_PIN)

//-------------------------------------------------------------------------------------------------
/// DIO's
#ifdef SCASII_OC_TAG
#define DO1_PIN                 (AVR32_PIN_PD28)
#else
#define DO1_PIN                 (AVR32_PIN_PC02)
#endif

#ifdef SCASII_OC_TAG
#define DI1_PIN                 (AVR32_PIN_PD27)
#else
#define DI1_PIN                 (AVR32_PIN_PD14)
#endif 

#define TEST_PIN_TP42           (AVR32_PIN_PA11)    // Used in Mobile Radio test station

//-------------------------------------------------------------------------------------------------
/// TWI
#ifdef SCASII_OC_TAG
// TWI0 communicates with the BQ24250, M24SR04 and the battery protection IC
#define MR_TWD0                 (AVR32_PIN_PC02)
#define MR_TWCK0                (AVR32_PIN_PC03)

#define TWI0_TWD_PIN            (AVR32_TWIMS0_TWD_PIN)
#define TWI0_TWD_FUNCTION       (AVR32_TWIMS0_TWD_FUNCTION)
#define TWI0_TWCK_PIN           (AVR32_TWIMS0_TWCK_PIN)
#define TWI0_TWCK_FUNCTION      (AVR32_TWIMS0_TWCK_FUNCTION)

#define BCHRG_INT_PIN			(AVR32_PIN_PD30)
#define BCHRG_CE_PIN			(AVR32_PIN_PC31)

#define RFID_RF_DIS_PIN			(AVR32_PIN_PA22)
#define RFID_INT_PIN			(AVR32_PIN_PA23)
//----
//TWI1 communicates with the LIS3DH accelerometer
#define MR_TWD1                 (AVR32_PIN_PC04)
#define MR_TWCK1                (AVR32_PIN_PC05)
#define TWI1_TWD_PIN            (AVR32_TWIMS1_TWD_PIN)
#define TWI1_TWD_FUNCTION       (AVR32_TWIMS1_TWD_FUNCTION)
#define TWI1_TWCK_PIN           (AVR32_TWIMS1_TWCK_PIN)
#define TWI1_TWCK_FUNCTION      (AVR32_TWIMS1_TWCK_FUNCTION)

#define ACC_INT1_PIN			(AVR32_PIN_PD29)
#define ACC_INT2_PIN			(AVR32_PIN_PC21)
#endif

//-------------------------------------------------------------------------------------------------
/// ADC
#define ADC_NUM_CHANNELS        (4)
#define ADC_CHANNEL_VBAT        (0)
#define ADC_CHANNEL_D2V5        (1)
#define ADC_CHANNEL_DD3V1       (2)
#define ADC_CHANNEL_D3V1        (3)
#define ADC_FREQUENCY           (1000000)       ///< ADC sampling frequency [Hz]
#define ADC_VDDANA              (3.1)

//-------------------------------------------------------------------------------------------------
/// SPI0: UHF2

///< SPI0
#define ENABLE_SPI0

#define SPI0_PDCA_CHANNEL_RX    (1)
#define SPI0_PDCA_CHANNEL_TX    (0)
#define SPI0_PDCA_RX_IRQ        (AVR32_PDCA_IRQ_1)  ///< IRQ must reflect channel
#define SPI0_PDCA_TX_IRQ        (AVR32_PDCA_IRQ_0)  ///< IRQ must reflect channel
#define SPI0_INT_LEVEL          (AVR32_INTC_INT2)

#define SPI0_SCK_PIN            (AVR32_SPI0_SCK_PIN)
#define SPI0_SCK_FUNCTION       (AVR32_SPI0_SCK_FUNCTION)
#define SPI0_MISO_PIN           (AVR32_SPI0_MISO_PIN)
#define SPI0_MISO_FUNCTION      (AVR32_SPI0_MISO_FUNCTION)
#define SPI0_MOSI_PIN           (AVR32_SPI0_MOSI_PIN)
#define SPI0_MOSI_FUNCTION      (AVR32_SPI0_MOSI_FUNCTION)

#define SPI0_NPCS0_PIN          (AVR32_SPI0_NPCS_0_PIN)
#define SPI0_NPCS0_FUNCTION     (AVR32_SPI0_NPCS_0_FUNCTION)
// Unused: Only NPCS0 is used
#define SPI0_NPCS1_PIN          AVR32_SPI0_NPCS_0_PIN
#define SPI0_NPCS1_FUNCTION     AVR32_SPI0_NPCS_0_FUNCTION
#define SPI0_NPCS2_PIN          AVR32_SPI0_NPCS_0_PIN
#define SPI0_NPCS2_FUNCTION     AVR32_SPI0_NPCS_0_FUNCTION
#define SPI0_NPCS3_PIN          AVR32_SPI0_NPCS_0_PIN
#define SPI0_NPCS3_FUNCTION     AVR32_SPI0_NPCS_0_FUNCTION

///< CC1101 mapping for SPI
#define CC1101_USES_SPI
#define CC1101_BAUDRATE         (3000000)//(3000000)          ///< SPI baudrate
#define CC1101_PORT             (0)                ///< CC1101 connected to SPI0
#define CC1101_CS_MODE          (SPI_CS_FIXED)
#define CC1101_CS_LINE          (0)
#define CC1101_CS_PIN           (SPI0_NPCS0_PIN)
#define CC1101_MISO_PIN         (SPI0_MISO_PIN)
#define CC1101_MOSI_PIN         (SPI0_MOSI_PIN)
#define CC1101_SCK_PIN          (SPI0_SCK_PIN)
#ifdef SCASII_OC_TAG
	#define CC1101_GDO2_PIN         (AVR32_PIN_PA19)
	#define CC1101_GDO0_PIN         (AVR32_PIN_PA25)
#else
	#define CC1101_GDO0_PIN         (AVR32_PIN_PA19)
	#define CC1101_GDO2_PIN         (AVR32_PIN_PA25)
#endif

///< CC1101 GDO2 interrupt defines
#define CC1101_IRQ_INT_LEVEL    (AVR32_INTC_INT1)
#ifdef SCASII_OC_TAG
#define CC1101_GPIO_IRQ         (AVR32_GPIO_IRQ_2)
#else
#define CC1101_GPIO_IRQ         (AVR32_GPIO_IRQ_3)
#endif

//#define CC1101_IRQ              (AVR32_EIC_IRQ_1) ///< Match the line
//#define CC1101_IRQ_FUNCTION     (AVR32_EIC_EXTINT_0_2_FUNCTION)
//#define CC1101_IRQ_LINE         (EXT_NMI)
//#define CC1101_IRQ_NUM_EIC      (1)

//-------------------------------------------------------------------------------------------------
/// SPI1: UHF1

///< SPI1
#define ENABLE_SPI1

#define SPI1_PDCA_CHANNEL_RX    (3)                     ///< This channel is not used
#define SPI1_PDCA_CHANNEL_TX    (2)
#define SPI1_PDCA_RX_IRQ        (AVR32_PDCA_IRQ_3)      ///< IRQ must reflect channel
#define SPI1_PDCA_TX_IRQ        (AVR32_PDCA_IRQ_2)
#define SPI1_INT_LEVEL          (AVR32_INTC_INT2)

#define SPI1_SCK_PIN            (AVR32_SPI1_SCK_PIN)
#define SPI1_SCK_FUNCTION       (AVR32_SPI1_SCK_FUNCTION)
#define SPI1_MISO_PIN           (AVR32_SPI1_MISO_PIN)
#define SPI1_MISO_FUNCTION      (AVR32_SPI1_MISO_FUNCTION)
#define SPI1_MOSI_PIN           (AVR32_SPI1_MOSI_PIN)
#define SPI1_MOSI_FUNCTION      (AVR32_SPI1_MOSI_FUNCTION)

#define SPI1_NPCS0_PIN          (AVR32_SPI1_NPCS_0_1_PIN)
#define SPI1_NPCS0_FUNCTION     (AVR32_SPI1_NPCS_0_1_FUNCTION)
// Unused: Only NPCS0 is used
#define SPI1_NPCS1_PIN          (AVR32_SPI1_NPCS_0_1_PIN)
#define SPI1_NPCS1_FUNCTION     (AVR32_SPI1_NPCS_0_1_FUNCTION)
#define SPI1_NPCS2_PIN          (AVR32_SPI1_NPCS_0_1_PIN)
#define SPI1_NPCS2_FUNCTION     (AVR32_SPI1_NPCS_0_1_FUNCTION)
#define SPI1_NPCS3_PIN          (AVR32_SPI1_NPCS_0_1_PIN)
#define SPI1_NPCS3_FUNCTION     (AVR32_SPI1_NPCS_0_1_FUNCTION)

///< nanoLOC Definitions

#define NANO_USES_SPI

// RR USE THE ISR
#define USE_NANOLOC_ISR

#define NANO_PORT               (1)
#define NANO_BAUDRATE           (20000000)
#define NANO_SPI_CS_CHANNEL     (0)

/* EIC interrupt */
#define NANO_IRQ                (AVR32_EIC_IRQ_2)               ///< Match the line
#define NANO_IRQ_PIN            (AVR32_EIC_EXTINT_2_2_PIN)
#define NANO_IRQ_FUNCTION       (AVR32_EIC_EXTINT_2_2_FUNCTION)
#define NANO_IRQ_LINE           (EXT_INT1)                      //EXT_INT2//EXT_NMI//(EXT_INT2)
#define NANO_IRQ_INT_LEVEL      (AVR32_INTC_INT2)               //(AVR32_INTC_INT2)//(AVR32_INTC_INT3)

#define NANO_IRQ_NUM_EIC        (1)                             ///<

/* GPIO interrupt
#define NANO_IRQ_PIN            (AVR32_PIN_PA08)
#define NANO_GPIO_IRQ           (AVR32_GPIO_IRQ_1)
#define NANO_IRQ_INT_LEVEL      (AVR32_INTC_INT1)
*/

#ifndef SCASII_OC_TAG
#define NANO_PA_GAIN_PIN        (AVR32_PIN_PD24)
#define NANO_PA_CEN_PIN         (AVR32_PIN_PD23)
#define NANO_PA_EN_PIN          (AVR32_PIN_PD22)
#endif
#define NANO_DIO_PIN            (AVR32_PIN_PB20)
#define NANO_NRESET_PIN         (AVR32_PIN_PB19)    ///< Power on reset (was PON_RST_PIN)
#define NANO_NSHDN_PIN          (AVR32_PIN_PB21)

///< LED mapping -> used in the nanoLOC API
#define LED_ALIVE_PIN           (AVR32_PIN_PB22)

#define LED0_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED1_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED2_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED3_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED4_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED5_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED6_PIN                (AVR32_PIN_PB22)    // Unconnected pin
#define LED7_PIN                (AVR32_PIN_PB22)    // Unconnected pin

#define LED_TX_PIN              (AVR32_PIN_PB22)    // Unconnected pin
#define LED_RX_PIN              (AVR32_PIN_PB22)    // Unconnected pin
#define LED_ERR_PIN             (AVR32_PIN_PB22)    // Unconnected pin
#define LED_CAL_PIN             (AVR32_PIN_PB22)    // Unconnected pin

//-------------------------------------------------------------------------------------------------
/// USART0: Dataflash (USPI)

#define FLASH_16MBIT
#define FLASH_BAUDRATE          (12000000)           ///< SPI baudrate
#define FLASH_PORT              (0)                 ///< Flash connected to USART0
#define FLASH_CS_MODE           (SPI_CS_FIXED)
//#define FLASH_CS_LINE           ()

///< Reset pin not connected to MCU
///< define below is a dummy pin (unconnected pin 32 on MCU)
#define FLASH_RESET_PIN         (AVR32_PIN_PA24)    ///< DUMMY Reset pin for the
                                                    ///< dataflash module
#define FLASH_USES_USPI

#define ENABLE_USPI0

#define USPI0_PDCA_CHANNEL_RX   (5)
#define USPI0_PDCA_CHANNEL_TX   (4)
#define USPI0_PDCA_RX_IRQ       (AVR32_PDCA_IRQ_5)  ///< IRQ must reflect channel
#define USPI0_PDCA_TX_IRQ       (AVR32_PDCA_IRQ_4)  ///< IRQ must reflect channel
#define USPI0_INT_LEVEL         (AVR32_INTC_INT2)

#define USPI0_SCK_PIN           (AVR32_USART0_CLK_1_PIN)
#define USPI0_SCK_FUNCTION      (AVR32_USART0_CLK_1_FUNCTION)
/// \note The MISO pin, when in SPI master mode must be connected to the RXD pin
///       of the particular USART port
#define USPI0_MISO_PIN          (AVR32_USART0_RXD_1_PIN)
#define USPI0_MISO_FUNCTION     (AVR32_USART0_RXD_1_FUNCTION)
/// \note The MOSI pin, when in SPI master mode must be connected to the TXD pin
///       of the particular USART port
#define USPI0_MOSI_PIN          (AVR32_USART0_TXD_1_PIN)
#define USPI0_MOSI_FUNCTION     (AVR32_USART0_TXD_1_FUNCTION)
/// \note The NSS pin, when in SPI master mode must be connected to the RTS pin
///       of the particular USART port
#define USPI0_NSS_PIN           (AVR32_USART0_RTS_1_PIN)
#define USPI0_NSS_FUNCTION      (AVR32_USART0_RTS_1_FUNCTION)

//-------------------------------------------------------------------------------------------------
/// USART1: IrDA Debug interface (UART / IrDA)

///< Debug
#define IRDA_UART               (1)

///< UART1
#define ENABLE_UART1

#define UART1_PDCA_CHANNEL_RX   (7)
#define UART1_PDCA_CHANNEL_TX   (6)
#define UART1_PDCA_RX_IRQ       (AVR32_PDCA_IRQ_7)  ///< IRQ must reflect channel
#define UART1_PDCA_TX_IRQ       (AVR32_PDCA_IRQ_6)  ///< IRQ must reflect channel
#define UART1_INT_LEVEL         (AVR32_INTC_INT1)   ///< Interrupt level

#define UART1_RXD_PIN           (AVR32_USART1_RXD_1_PIN)
#define UART1_RXD_FUNCTION      (AVR32_USART1_RXD_1_FUNCTION)
#define UART1_TXD_PIN           (AVR32_USART1_TXD_1_PIN)
#define UART1_TXD_FUNCTION      (AVR32_USART1_TXD_1_FUNCTION)

#define IRDA_SD_PIN             (AVR32_PIN_PD13)

// Custom buffer lengths
#define UART1_TX_CUST_LEN       (1024)
#define UART1_RX_CUST_LEN       (1024)

//-------------------------------------------------------------------------------------------------
/// USART2: Unused

//-------------------------------------------------------------------------------------------------
/// USART3: PTC Display connector (UART)    -   Future use

#define PTC_UART               (3)

///< UART3
#define PTC_BAUDRATE            (115200)
#define ENABLE_UART3

#define UART3_PDCA_CHANNEL_RX   (11)
#define UART3_PDCA_CHANNEL_TX   (10)
#define UART3_PDCA_RX_IRQ       (AVR32_PDCA_IRQ_11)  ///< IRQ must reflect channel
#define UART3_PDCA_TX_IRQ       (AVR32_PDCA_IRQ_10)  ///< IRQ must reflect channel
#define UART3_INT_LEVEL         (AVR32_INTC_INT1)   ///< Interrupt level

#define UART3_RXD_PIN           (AVR32_USART3_RXD_2_PIN)
#define UART3_RXD_FUNCTION      (AVR32_USART3_RXD_2_FUNCTION)
#define UART3_TXD_PIN           (AVR32_USART3_TXD_2_PIN)
#define UART3_TXD_FUNCTION      (AVR32_USART3_TXD_2_FUNCTION)


// Custom buffer lengths
#define UART3_TX_CUST_LEN       (1024)
#define UART3_RX_CUST_LEN       (1024)

//-------------------------------------------------------------------------------------------------
/// USART3: GPS connector (UART)    -   Future use

#ifdef SCASII_OC_TAG
#define GPS_STANDBY_PIN			(AVR32_PIN_PC12)
#define GPS_RESET_PIN			(AVR32_PIN_PA10)
#endif

#define GPS_UART                (4)
#define GPS_BAUDRATE            (9600)
#define ENABLE_UART4

#define UART4_PDCA_CHANNEL_RX   (13)
#define UART4_PDCA_CHANNEL_TX   (12)
#define UART4_PDCA_RX_IRQ       (AVR32_PDCA_IRQ_13)  ///< IRQ must reflect channel
#define UART4_PDCA_TX_IRQ       (AVR32_PDCA_IRQ_12)  ///< IRQ must reflect channel
#define UART4_INT_LEVEL         (AVR32_INTC_INT1)   ///< Interrupt level

#define UART4_RXD_PIN           (AVR32_USART4_RXD_2_PIN)
#define UART4_RXD_FUNCTION      (AVR32_USART4_RXD_2_FUNCTION)
#define UART4_TXD_PIN           (AVR32_USART4_TXD_2_PIN)
#define UART4_TXD_FUNCTION      (AVR32_USART4_TXD_2_FUNCTION)

// Custom buffer lengths
#define UART4_TX_CUST_LEN       (1024)
#define UART4_RX_CUST_LEN       (1024)

#ifdef __cplusplus
};
#endif

#endif	// _HWDEFS_H_