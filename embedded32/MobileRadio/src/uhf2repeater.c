/// \file uhf2repeater.c
/// This is the header file of the uhf2repeater module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2012/06/26
/// - Module       : UHF2REPEATER
/// - Description  : uhf2repeater interface header
///
/// \details This module handles UHF2 messages on a Mobile Radio configured as a 
///          UNIT_TYPE_UHF2_REPEATER. The specific DLL types that get repeated is configurable.
///
/// \note Confluence link: https://jira.parsec.co.za:8446/display/PRJCASII/UHF2REPEATER+Module
///
/// \author Michael Manthey (MM)
/// \version $Id: uhf2repeater.c,v 1.3 2012-06-29 12:00:19+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "uhf2repeater.h"

#include "common.h"         ///< Common defines such as TRUE, FALSE etc
#include "board.h"          ///< HW definitions
#include "ca_msg.h"         ///< SCAS II CA Message Module
#include "uhf2.h"           ///< SCAS II UHF2 Module
#include "cc1101.h"         ///< CC1101 RF transceiver interface module

//-------------------------------------------------------------------------------------------------
// Defines
#define REPEATER_TIMESLOT_SIZE_MS   (50)            ///< Timeslot size dictates the retry interval
#define REPEATER_NUM_RETRIES        (3)             ///< Number of retries when not sending 
                                                    ///  messages with HIGH_PRIORITY (otherwise
													///  messages are retried indefinitely)

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;       ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// Global variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
static void uhf2repeater_TransmitPacket(MESSAGES_UHF2Msg *rxMsg);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void UHF2REPEATER_Init() {
	// Get the current packet control settings from CC1101 transceiver
	U8 pktctrl1Val = CC1101_ReadReg(CC1101_PKTCTRL1);
	
	// Disable hardware address matching by setting  ADR_CHK[1:0] bitfield in PKTCTRL1 to 0 (0b00)
	pktctrl1Val &= (~CC1101_ADR_CHK_MASK | CC1101_ADR_CHK_OFF);     // pktctrl1Val &= 0b11111100
	CC1101_WriteReg(CC1101_PKTCTRL1, pktctrl1Val);
}

//-------------------------------------------------------------------------------------------------
void UHF2REPEATER_ProcessMessage(MESSAGES_UHF2Msg *rxMsg)
{
	/// Based on the dllType of the received message, and the configuration settings of this 
	/// UHF2 Repeater, transmit (i.e. repeat) the received message if applicable
	switch(rxMsg->dllType) {
	    case MESSAGES_UHF2_DLL_TYPE_BROADCAST:
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
	    case MESSAGES_UHF2_DLL_TYPE_CA:
		    // Regardless of the uhf2RepeaterType, put UHF2 messages on CA_MSG buffer
			CA_MSG_PutCaMsg(rxMsg);     ///< RANGETAG will process these messages as a CA_SLAVE
			
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				case REPEATER_CA_ONLY:
				case REPEATER_CA_SCASII:
				case REPEATER_CA_P2:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
	    case MESSAGES_UHF2_DLL_TYPE_OTA:
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
	    case MESSAGES_UHF2_DLL_TYPE_SCASII:
		case MESSAGES_UHF2_DLL_TYPE_SCASII_LONG:
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				case REPEATER_SCASII_ONLY:
				case REPEATER_CA_SCASII:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
	    case MESSAGES_UHF2_DLL_TYPE_SCASII_RU:
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				case REPEATER_RU_ONLY:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
	    case MESSAGES_UHF2_DLL_TYPE_P2_SHORT:
		    switch(g_configBlock.uhf2RepeaterType) {
                case REPEATER_ALL:
				case REPEATER_P2_ONLY:
				case REPEATER_CA_P2:
				    uhf2repeater_TransmitPacket(rxMsg);
					break;
				default:
				    break;
		    }
			break;
		default:
		    // Do nothing
	        break;
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function transmits a message over UHF2 as quickly as possible
/// \param rxMsg [in] A pointer to the message which must be transmitted
void uhf2repeater_TransmitPacket(MESSAGES_UHF2Msg *rxMsg)
{
	UHF2_SendMessage(UHF2_GetPrimaryChannel(),
	                 rxMsg,                     ///< Pointer to the message to be transmitted
					 UHF2_TYPE_HIGH_PRIORITY,   ///< Send message immediately and retry indefinitely
					 REPEATER_TIMESLOT_SIZE_MS, ///< Timeslot size dictates the retry interval
					 REPEATER_NUM_RETRIES,      ///< Will not be used for UHF2_TYPE_HIGH_PRIORITY
					 TIMER_Now32());            ///< Will not be used for UHF2_TYPE_HIGH_PRIORITY
}