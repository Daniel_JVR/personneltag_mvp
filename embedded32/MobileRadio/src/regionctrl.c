/// \file regionctrl.c
/// This is the implementation file of the regionctrl module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2013/05/27
/// - Module        : REGIONCTRL
/// - Description   : regionctrl implementation file
///
/// \detail	The REGIONCTRL module manages messages sent by a UNIT_TYPE_RCU (Region Control Unit).
///         The message structure is defined in REGIONCTRL and is used for sending commands to 
///         nearby CA devices, which they need to act on based on their distance from the RCU.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: regionctrl.c,v 1.3 2014-06-25 18:38:50+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "regionctrl.h"
#include "timer.h"
#include "config_block_uc3.h"
#include "msghandler.h"
#include "uhf2_msghandler.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define REGIONCTRL_COMMAND_TX_INTERVAL_MS   (500)   // The rate at which commands are sent over UHF2

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct _REGIONCTRL_Info {
	U32 cmdTxTimestamp;     // Timestamp of the last time a command was transmitted
}ATTR_PACKED REGIONCTRL_Info;

//-------------------------------------------------------------------------------------------------
// Global variables
REGIONCTRL_Info regionctrlLib = {0};

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;   ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void REGIONCTRL_Service(void)
{
	U8 *genericCmdMsg;                          // Pointer used for sending message over UHF2
	REGIONCTRL_RegionCtrlMsg cmdMsg = {{0}};
	
	if(TIMER_Elapsed32(regionctrlLib.cmdTxTimestamp) > TIMER_MS(REGIONCTRL_COMMAND_TX_INTERVAL_MS)) {
		// Reset the timer
		regionctrlLib.cmdTxTimestamp = TIMER_Now32();
		
		// It is time to transmit a command
	    switch(g_configBlock.regionCtrlCommand) {
			// Simple commands with zero payload
	    	case ENTER_DISCREET_MODE:
			case ENTER_STOP_MODE:
			case ENTER_CRAWL_MODE:
			    cmdMsg.srcID = g_configBlock.deviceID;
				// No need to populate destID
				cmdMsg.minDist = g_configBlock.regionCtrlMinDist_cm;
				cmdMsg.maxDist = g_configBlock.regionCtrlMaxDist_cm;
				cmdMsg.cmd = g_configBlock.regionCtrlCommand;
				cmdMsg.cmdLen = LEN_SIMPLE_COMMAND;
				// No need to populate cmdData since length is zero
                
				// Build the packet
	            genericCmdMsg = MSGHANDLER_BuildPacket(MESSAGES_DEST_UHF2,
					                                   MESSAGES_TYPE_REGION_CTRL,
										               MESSAGES_LEN_REGION_CTRL,
										               (U8*)&(cmdMsg));
                // Transmit the packet over UHF2
                UHF2_MSGHANDLER_TransmitUHF2Packet(MESSAGES_DEST_MU,
					                               MSGHANDLER_PREAMBLE_CRC_LEN + MESSAGES_LEN_REGION_CTRL,
									               genericCmdMsg,
									               0xFF);
	    		break;
			case MINER_GLOBAL_DEREG:
			    // An RCU does not send an RCU Command when configured to trigger MINER_GLOBAL_DEREG
				// In stead, the range result messages sent to Miner Units will indicate in the usual
				// exclusion bits whether the device must be excluded
				break;
			case NO_COMMAND:
	    	default:
	    		// Do nothing
				break;
	    }
	}
}

//-------------------------------------------------------------------------------------------------
U8 REGIONCTRL_ExcludeMinerUnitGlobally(DeviceID *muId, S16 distance)
{
	// Handle the case where a Region Control Unit (RCU) is configured to trigger global
    // deregistration on nearby Miner Units
    if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_RCU) {
	    // Handle the MINER_GLOBAL_DEREG case
	    if(g_configBlock.regionCtrlCommand == MINER_GLOBAL_DEREG) {
		    if((distance > g_configBlock.regionCtrlMinDist_cm) && (distance < g_configBlock.regionCtrlMaxDist_cm)) {
			    return TRUE;
		    }
	    }
    }
	
	// If this point is reached, the specified Miner Unit must not be globally excluded by this RCU
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 