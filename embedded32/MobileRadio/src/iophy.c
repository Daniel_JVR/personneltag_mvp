/// \file iophy.c
/// This is the implementation file of the iophy module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/09/13
/// - Module        : IOPHY
/// - Description   : iophy implementation
///
/// \details  This module contains functions that control Mobile Radio inputs and outputs on the 
///           hardware level.
///
/// \author Michael Manthey (MM)
/// \version $Id: iophy.c,v 1.10 2013-06-14 10:56:18+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: [1] 8-bit SCASII Miner Unit IOPHY
///                       Michael Manthey
///                       
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "iophy.h"
#include "gpio.h"       ///< ASF GPIO module for manipulating microcontroller pins

#include "board.h"
#include "scasdbg.h"

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Global variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void IOPHY_Init(void)
{
    ///< Initialize data direction and default states for indicator LEDs
	gpio_configure_pin(VU_LED_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	gpio_configure_pin(LU_LED_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	gpio_configure_pin(HU_LED_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	gpio_configure_pin(EXCL_LED_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);

	
	///< Initialize data direction and default states for debugging LEDs
	gpio_configure_pin(DLED1_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
	gpio_configure_pin(DLED2_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_HIGH);
	
    ///< Initialise data direction and default state for Digital Output (DO1)
	gpio_configure_pin(DO1_PIN, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);
	
	///< Initialise data direction and default state for TP42
	gpio_configure_pin(TEST_PIN_TP42, GPIO_DIR_OUTPUT | GPIO_INIT_LOW);

    ///< Initialise data direction for Digital Input (DI1)
	gpio_configure_pin(DI1_PIN, GPIO_DIR_INPUT & GPIO_PULL_UP);

    ///< Turn on heartbeat LED so that it illuminates while the system initialises 
	///  (i.e. before heartbeat LED starts to blink)
    IOPHY_RunLED(IO_ON);

}

//-------------------------------------------------------------------------------------------------
void IOPHY_DLED1(IOState state)
{
   if(state == IO_ON) {
      gpio_set_pin_low(DLED1_PIN);
   } else {
      gpio_set_pin_high(DLED1_PIN);
   }
}

//-------------------------------------------------------------------------------------------------
void IOPHY_DLED2(IOState state)
{
   if(state == IO_ON) {
      gpio_set_pin_low(DLED2_PIN);
   } else {
      gpio_set_pin_high(DLED2_PIN);
   }
}
//-------------------------------------------------------------------------------------------------
void IOPHY_RunLED(IOState state)
{
   IOPHY_DLED1(state);
}

//-------------------------------------------------------------------------------------------------
void IOPHY_VehicleWarningLED(IOState state)
{
   if(state == IO_ON) 
   {
		#ifdef SCASII_OC_TAG
		gpio_set_pin_low(VU_LED_PIN);
		#else
		gpio_set_pin_high(VU_LED_PIN);
		#endif	  
   } 
   else 
   {				   
	   #ifdef SCASII_OC_TAG
	   gpio_set_pin_high(VU_LED_PIN);
	   #else
	   gpio_set_pin_low(VU_LED_PIN);
	   #endif
   }       
}

//-------------------------------------------------------------------------------------------------
void IOPHY_LocoWarningLED(IOState state)
{
   if(state == IO_ON) {		
	   #ifdef SCASII_OC_TAG
	   gpio_set_pin_low(LU_LED_PIN);
	   #else
	   gpio_set_pin_high(LU_LED_PIN);
	   #endif
   } else {
	   #ifdef SCASII_OC_TAG
	   gpio_set_pin_high(LU_LED_PIN);
	   #else
	   gpio_set_pin_low(LU_LED_PIN);
	   #endif
   }       
}

//-------------------------------------------------------------------------------------------------
///< \note See DEVSCASII-124: The HU and Excl LED's must be switched around. HU yellow, Excl amber
void IOPHY_HazardWarningLED(IOState state)
{
   if(state == IO_ON) 
   {
//      gpio_set_pin_high(EXCL_LED_PIN);  ///< See DEVSCASII-124
		#ifdef SCASII_OC_TAG
		gpio_set_pin_low(EXCL_LED_PIN);
		#else
		gpio_set_pin_high(EXCL_LED_PIN);
		#endif	  
   } 
   else 
   {
//      gpio_set_pin_low(EXCL_LED_PIN);   ///< See DEVSCASII-124
		#ifdef SCASII_OC_TAG
		gpio_set_pin_high(EXCL_LED_PIN);
		#else
		gpio_set_pin_low(EXCL_LED_PIN);
		#endif  
   }
}

//-------------------------------------------------------------------------------------------------
///< \note See DEVSCASII-124: The HU and Excl LED's must be switched around. HU yellow, Excl amber
void IOPHY_WrkrExclDrvrDeregLED(IOState state)
{
   if(state == IO_ON) 
   {
//      gpio_set_pin_high(HU_LED_PIN);    ///< See DEVSCASII-124
		#ifdef SCASII_OC_TAG
		gpio_set_pin_low(HU_LED_PIN);
		#else
		gpio_set_pin_high(HU_LED_PIN);
		#endif	  
   } 
   else 
   {
//      gpio_set_pin_low(HU_LED_PIN);     ///< See DEVSCASII-124
		#ifdef SCASII_OC_TAG
		gpio_set_pin_high(HU_LED_PIN);
		#else
		gpio_set_pin_low(HU_LED_PIN);
		#endif	  
   }
}

//-------------------------------------------------------------------------------------------------
void IOPHY_DigitalOut1(IOState state)
{
   //if(state == IO_ON) {
      //gpio_set_pin_high(DO1_PIN);
   //} else {
      //gpio_set_pin_low(DO1_PIN);
   //}
}

void IOPHY_DigitalOut1_Buzz(IOState state)
{
	if(state == IO_ON) {
		gpio_set_pin_high(DO1_PIN);
		} else {
		gpio_set_pin_low(DO1_PIN);
	}
}

//-------------------------------------------------------------------------------------------------
void IOPHY_DigitalOut1Toggle(void)
{
    //gpio_toggle_pin(DO1_PIN);
}

void IOPHY_DigitalOut1Toggle_Buzz(void)
{
    gpio_toggle_pin(DO1_PIN);
}

//-------------------------------------------------------------------------------------------------
void IOPHY_TestPinTP42(IOState state)
{
   if(state == IO_ON) {
      gpio_set_pin_high(TEST_PIN_TP42);
   } else {
      gpio_set_pin_low(TEST_PIN_TP42);
   }
}

//-------------------------------------------------------------------------------------------------
IOState IOPHY_DigitalIn1(void) {
    if(gpio_get_pin_value(DI1_PIN)) {
        return IO_ON;
    } else {
        return IO_OFF;
    }
}

//-------------------------------------------------------------------------------------------------
IOState IOPHY_EmergencyStopBtn(void) {
    if(gpio_get_pin_value(DI1_PIN)) {
        return IO_OFF;
    } else {
        return IO_ON;
    }
}

//-------------------------------------------------------------------------------------------------
void IOPHY_SetAllWarnleds(IOState state) {
    IOPHY_VehicleWarningLED(state);
	IOPHY_LocoWarningLED(state);
	IOPHY_HazardWarningLED(state);
	IOPHY_WrkrExclDrvrDeregLED(state);
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
// none
