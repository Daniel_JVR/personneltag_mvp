/// \file btuctrl.c
/// This is the implementation file of the btuctrl module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/03/17
/// - Module        : BTUCTRL
/// - Description   : btuctrl implementation file
///
/// \detail	BTUCTRL
///
///
/// \author Michael Manthey (MM)
/// \version $Id: btuctrl.c,v 1.0 2014-03-28 11:16:57+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "btuctrl.h"
#include "sysconfig_uc3.h"
#include "timer.h"
#include "calog.h"

#include <string.h>

//-------------------------------------------------------------------------------------------------
// Defines
#define BTUCTRL_MAX_NUM_VEHICLES    (50)    // BTUCTRL can hold info about max 50 vehicle systems
#define BTUCTRL_POLL_INTERVAL_MS    (1000)
#define BTUCTRL_OPMODE_INTERVAL_MS  (2000)

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
// \struct BTUCTRL_VehicleInfo Defines the list of variables held for each detected vehicle system
//          and used for storing the state of requested brake tests.
typedef struct _BTUCTRL_VehicleInfo {
	DeviceID vehId;             // Device ID of vehicle system
	DeviceID operatorId;        // Device ID of the vehicle operator (if applicable)
	BRAKETEST_TestStates state; // State of BTU commanded brake testing on the vehicle system
	U32 lastCmdTimestamp;       // Timestamp when the last brake test command was sent to vehicle
	U8 rssi;                    // Averaged RSSI value of detected vehicle
	S16 distanceCm;             // Latest proximity estimation with the vehicle system
	
} ATTR_PACKED BTUCTRL_VehicleInfo;

//-------------------------------------------------------------------------------------------------
// \struct BTUCTRL_RuntimeVariables Defines the list of variables held for achieving the 
//          requirements of the BTUCTRL module
//  \note The vehInfo array is an unsorted array. Entries are added to the bottom of the array until
//          the array is full, after which no further entries are added until an entry is removed.
typedef struct _BTUCTRL_RuntimeVariables {
	BTUCTRL_VehicleInfo vehInfo[BTUCTRL_MAX_NUM_VEHICLES];
	U8 numEntries;              // The number of vehicle system entries currently in the vehInfo array
} ATTR_PACKED BTUCTRL_RuntimeVariables;

//-------------------------------------------------------------------------------------------------
// Global variables
static BTUCTRL_RuntimeVariables btuLib;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;       ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
void btuctrl_InsertTableEntry(BTUCTRL_VehicleInfo *vehInfo);
void btuctrl_RemoveTableEntry(DeviceID id);
U8 btuctrl_FindTableEntry(DeviceID id, U8 *foundPosi);

void btuctrl_SetVehTestState(U8 indexPosi, BRAKETEST_TestStates newState);

void btuctrl_SendBrakeTestCommand(DeviceID destId);
void btuctrl_LogBrakeTestUnitEvent(BRAKETEST_TestStates state, DeviceID vehicleId, DeviceID operatorId);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void BTUCTRL_Init(void)
{
	memset((void*)&(btuLib), 0x00, sizeof(BTUCTRL_RuntimeVariables));
}

//-------------------------------------------------------------------------------------------------
void BTUCTRL_Service(void)
{
	// Scan through the vehicle table and check whether a brake test command must be sent
	for(U8 i = 0; i < btuLib.numEntries; i++) {
		switch(btuLib.vehInfo[i].state) {
			case BTU_CMD_IDLE:
			    // Check whether the vehicle's proximity fulfills the requirements
				if((g_configBlock.btuUsesRanging == FALSE) || ((btuLib.vehInfo[i].distanceCm > 0) && (btuLib.vehInfo[i].distanceCm < g_configBlock.proximityZone3_cm))) {
				    // Transmit a brake test command to this vehicle every second
				    if(TIMER_Elapsed32(btuLib.vehInfo[i].lastCmdTimestamp) > TIMER_MS(BTUCTRL_POLL_INTERVAL_MS)) {
					    btuctrl_SendBrakeTestCommand(btuLib.vehInfo[i].vehId);				
	                    // Update the timestamp
	                    btuLib.vehInfo[i].lastCmdTimestamp = TIMER_Now32();
				    }
				}
				break;
			
			case BTU_CMD_ISSUED_TO_OPERATOR:
			case BTU_CMD_DENIED_NOT_OPERATIONAL:
			    // Transmit a brake test command to this vehicle every 2 seconds
				if(TIMER_Elapsed32(btuLib.vehInfo[i].lastCmdTimestamp) > TIMER_MS(BTUCTRL_OPMODE_INTERVAL_MS)) {
					btuctrl_SendBrakeTestCommand(btuLib.vehInfo[i].vehId);
	                // Update the timestamp
	                btuLib.vehInfo[i].lastCmdTimestamp = TIMER_Now32();
				}
				break;
			
			case BTU_CMD_ACKNOWLEDGED_BY_OPERATOR:
			case BTU_CMD_DENIED_CMD_PENDING:
			case BTU_CMD_DENIED_EXEMPT:
			case  BTU_CMD_DENIED_CONFIG:
			    // Do nothing. This BTU does not have to continue sending brake test commands to
				// this vehicle until the next time in comes into range of the BTU.
				break;
				
			default:
				break;
		}
	}
}

//-------------------------------------------------------------------------------------------------
void BTUCTRL_P2MsgProcess(P2MESSAGES_ShortMessage *p2Msg)
{
	BTUCTRL_ResponseMessage *response;
	U8 foundPosi;
	
	switch(p2Msg->type) {
		case P2MESSAGES_TYPE_BTU_TEST_RESPONSE:
		    response = (BTUCTRL_ResponseMessage*)&(p2Msg->payload[0]);
			// Check whether the message was received from a vehicle in the vehicle table
			if(btuctrl_FindTableEntry(response->vehId, &foundPosi) == TRUE) {
				// Update the operator ID for this vehicle system
				btuLib.vehInfo[foundPosi].operatorId = response->operatorId;
				btuctrl_SetVehTestState(foundPosi, response->responseState);
			}
			break;
			
		default:
		    // Do not process the message
			break;
	}
}

//-------------------------------------------------------------------------------------------------
void BTUCTRL_UpdateProximityInfo(DeviceID id, U8 rssi, S32 distanceCm)
{
	if((distanceCm >= 0) && (id.IDByte1 == UNIT_TYPE_VMU)) {
		btuLib.vehInfo[0].distanceCm = 0;
	}
	
	U8 foundPosi;
	// Check whether the device is a vehicle type
	switch(id.IDByte1) {
		case UNIT_TYPE_VMU:
		case UNIT_TYPE_LMU:
		case UNIT_TYPE_LSU:
		    // Check whether the entry is already in the table
			if(btuctrl_FindTableEntry(id, &foundPosi) == TRUE) {
				// Update the table entry's info
				btuLib.vehInfo[foundPosi].rssi = rssi;
				btuLib.vehInfo[foundPosi].distanceCm = distanceCm;
			} else {
				// Add the entry to the table if there is space
				BTUCTRL_VehicleInfo vehInfo = {{0},{0},0,0};
				vehInfo.vehId = id;
				vehInfo.state = BTU_CMD_IDLE;
				vehInfo.rssi = rssi;
				vehInfo.distanceCm = distanceCm;
				btuctrl_InsertTableEntry(&vehInfo);
			}
			break;
		default:
			break;
	}
}

//-------------------------------------------------------------------------------------------------
void BTUCTRL_DeviceTimeout(DeviceID id)
{
	btuctrl_RemoveTableEntry(id);
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function adds a vehicle to the vehInfo table
void btuctrl_InsertTableEntry(BTUCTRL_VehicleInfo *vehInfo)
{
	// Check whether there is space in the table for the new entry
	if(btuLib.numEntries < BTUCTRL_MAX_NUM_VEHICLES) {
		btuLib.vehInfo[btuLib.numEntries] = *vehInfo;
		btuLib.numEntries++;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function removes a vehicle from the vehInfo table
void btuctrl_RemoveTableEntry(DeviceID id)
{
	U8 foundPosi;
	
	// Find the table entry
	if(btuctrl_FindTableEntry(id, &foundPosi) == TRUE) {
		// Move all entries after the found device in the table up one position
		for(U8 i = foundPosi; i < (btuLib.numEntries - 1); i++) {
			btuLib.vehInfo[i] = btuLib.vehInfo[i + 1];
		}
		// Decrement the numEntries since one entry was found and removed
		btuLib.numEntries--;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function finds a vehicle in the vehInfo table
U8 btuctrl_FindTableEntry(DeviceID id, U8 *foundPosi)
{
	// Iterate through the table to find the specified device
	for(U8 i = 0; i < btuLib.numEntries; i++) {
		if(SYSCONFIG_DeviceIdCompare(&(btuLib.vehInfo[i].vehId), &id) == SUCCESS) {
			*foundPosi = i;
			return TRUE;
		}
	}
	// Device was not found
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function updates the test state of a vehicle in the vehInfo table. When a state
///         change occurs, the event is logged (if applicable). 
void btuctrl_SetVehTestState(U8 indexPosi, BRAKETEST_TestStates newState)
{
	// Check whether the state is actually changing
	if(btuLib.vehInfo[indexPosi].state != newState) {
		// Handle state change event based on the new state
		switch(newState) {
			case BTU_CMD_IDLE:
			    // Do nothing in this case
				break;
				
			case BTU_CMD_DENIED_NOT_OPERATIONAL:
			case BTU_CMD_DENIED_CMD_PENDING:
			case BTU_CMD_DENIED_EXEMPT:
			case BTU_CMD_DENIED_CONFIG:
			    // Log the fact that the vehicle validly rejected the test request
				btuctrl_LogBrakeTestUnitEvent(newState, btuLib.vehInfo[indexPosi].vehId, btuLib.vehInfo[indexPosi].operatorId);
				break;
				
			case BTU_CMD_ISSUED_TO_OPERATOR:
			    // Log acceptance of the brake test command
				btuctrl_LogBrakeTestUnitEvent(newState, btuLib.vehInfo[indexPosi].vehId, btuLib.vehInfo[indexPosi].operatorId);
				break;
				
			case BTU_CMD_ACKNOWLEDGED_BY_OPERATOR:
			    // Log compliance by the vehicle operator
				btuctrl_LogBrakeTestUnitEvent(newState, btuLib.vehInfo[indexPosi].vehId, btuLib.vehInfo[indexPosi].operatorId);
				break;
				
			default:
				break;
		}
		
		// Update the vehicle's state in the table
		btuLib.vehInfo[indexPosi].state = newState;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function sends a brake test command over UHF2 (in Protocol 2 format) to a nearby
///         vehicle system.
void btuctrl_SendBrakeTestCommand(DeviceID destId)
{
	// Build a Protocol 2 packet
	P2MESSAGES_ShortMessage p2Msg;
	
	p2Msg.destID = destId;
	p2Msg.srcID = g_configBlock.deviceID;
	p2Msg.destPort = P2MESSAGES_PORT_SCASII_OPTEST;
	p2Msg.srcPort = P2MESSAGES_PORT_SCASII_OPTEST;
	p2Msg.type = P2MESSAGES_TYPE_BTU_TEST_COMMAND;
	
	// Set the payload of the Protocol 2 packet
	BTUCTRL_CommandMessage cmdMsg = {{0},0};
	cmdMsg.btuId = g_configBlock.deviceID;
	cmdMsg.strict = g_configBlock.btuStrict;
	memcpy((void*)&(p2Msg.payload[0]), (void*)&(cmdMsg), sizeof(BTUCTRL_CommandMessage));
	
	// Send the packet
	UHF2_MSGHANDLER_TransmitP2ShortMsgDirect(&p2Msg, sizeof(BTUCTRL_CommandMessage));
	
}

//-------------------------------------------------------------------------------------------------
/// \brief This function logs a brake test unit even based on the provided parameters.
void btuctrl_LogBrakeTestUnitEvent(BRAKETEST_TestStates state, DeviceID vehicleId, DeviceID operatorId)
{
	// Create the log variable
	CALOG_BrakeTestUnitLog newLog;
	newLog.testState = state;
	newLog.vehicleId = vehicleId;
	newLog.operatorId = operatorId;
	// Write the log to dataflash memory
	CALOG_Write(BRAKE_TEST_UNIT_EVENT, (U8*)&newLog, BRAKE_TEST_UNIT_LEN);
}