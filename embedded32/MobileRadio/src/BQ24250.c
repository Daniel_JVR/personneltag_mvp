/// \file BQ24250.c
/// This is the implementation file of the BQ24250 module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2016/01/19
/// - Module        : BQ24250
/// - Description   : TI BQ24250 battery charger module
///
/// \detail	BQ24250
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "BQ24250.h"
#include "scasdbg.h"

#include "hwdefs.h"

#include "irdahandler.h"        ///< IrDA handler for the UC3 series
#include "string.h"
#include "gpio.h"

#include "status_codes.h"
#include "timer.h"
#include "twim.h"
//-------------------------------------------------------------------------------------------------
// Defines

				
//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Local variables
                             
static U32 bqTimerSnapshot = 0;


//-------------------------------------------------------------------------------------------------
// Global variables


//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
bq24250FaultEnum BQ24250_CheckFault(void);

bq24250ChargeControllerStatus BQ24250_ChargerStatus(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void BQ24250_Init_GPIO(void)
{
	// Enable the Battery charger pins
	gpio_enable_gpio_pin(BCHRG_CE_PIN);
	gpio_configure_pin(BCHRG_CE_PIN, (GPIO_DIR_OUTPUT | GPIO_INIT_LOW));
	gpio_enable_gpio_pin(BCHRG_INT_PIN);
	gpio_configure_pin(BCHRG_INT_PIN, (GPIO_DIR_INPUT | GPIO_PULL_UP));
}

status_code_t BQ24250_Init(void)
{
	status_code_t twi_status = 0;
	twim_package_t	bqComsPacket;
	char	bqTxData = 0;
	char	bqLastFault = 0;
	
	// Setup the I2C transfer packet.
	bqComsPacket.addr[0] = BQ24250_I2C_REG1;
	bqComsPacket.addr_length = 1;
	bqComsPacket.buffer		= &bqTxData;
	bqComsPacket.chip		= BQ24250_I2C_ADDRESS;
	bqComsPacket.length		=	1;
	bqComsPacket.no_wait	=	1;
	
	// Get a snapshot of the current time
	bqTimerSnapshot = TIMER_Now32();
	
		//Check if the device is available
	if(twim_probe (&AVR32_TWIM0, BQ24250_I2C_ADDRESS) != STATUS_OK)
	{
		return ERR_IO_ERROR;
	}
	
	//read out all the faults. Stop reading on recurring fault
	while(BQ24250_CheckFault() != NO_FAULT && bqLastFault != bqTxData)
	{
		bqLastFault = bqTxData;
	}
	
	// Set data transfer address to regiser 1
	bqTxData	=	(char)(1 << REG1_WD_EN);	//	Enable the watchdog timer
	twi_status = twim_write_packet (&AVR32_TWIM0,	&bqComsPacket);	
	
	// Set data transfer address to regiser 2
	bqComsPacket.addr[0] = BQ24250_I2C_REG2;
	// Set charge current to 1.5A, enable STAT pin, enable TERM function and enable Charging
	bqTxData = (1 << REG2_IN_ILIM2 |  1 << REG2_EN_STAT | 1 << REG2_EN_TERM);
	twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);
	
	// Set data transfer address to regiser 3
	bqComsPacket.addr[0] = BQ24250_I2C_REG3;
	// Set charge voltage 4.18V
	bqTxData = (1 << REG3_VBATREG_5 | 1 << REG3_VBATREG_1 );
	twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);
	
	// Set data transfer address to regiser 4
	bqComsPacket.addr[0] = BQ24250_I2C_REG4;
	// Set charge current to 900mA with 50mA charge termination current
	bqTxData = (1 << REG4_ICHG_3 | 1 << REG4_ITERM_1 );
	twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);
	
	// Set data transfer address to regiser 6
	bqComsPacket.addr[0] = BQ24250_I2C_REG6;
	// Set charge safety timer to 9 hours, TS enable
	//bqTxData = (1 << REG6_TMR_1 | 1 << REG6_TS_EN );
	bqTxData = (1 << REG6_TMR_1 );
	twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);

	// Set data transfer address to regiser 7
	bqComsPacket.addr[0] = BQ24250_I2C_REG7;
	// Set OVP to 10.5V
	bqTxData = (1 << REG7_VOVP_2 | 1 << REG7_VOVP_1 | 1 << REG7_VOVP_0 );
	twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);
	
	return STATUS_OK;	
}

status_code_t BQ24250_Service(void)
{	
	status_code_t twi_status = 0;
	twim_package_t	bqComsPacket;
	char	bqRxData = 0;
	char	bqTxData = 0;
	
	// Reset the Watchdog every 30 sec (50 sec timeout).
	if(TIMER_Elapsed32(bqTimerSnapshot) > TIMER_S(30))
	{
		// Setup the I2C transfer packet.
		bqComsPacket.addr[0] = BQ24250_I2C_REG1;
		bqComsPacket.addr_length = 1;
		bqComsPacket.buffer		= &bqRxData;
		bqComsPacket.chip		= BQ24250_I2C_ADDRESS;
		bqComsPacket.length		=	1;
		bqComsPacket.no_wait	=	1;
			
		twi_status = twim_read_packet (&AVR32_TWIM0, &bqComsPacket);
		// check if the watchdog has expired. This will cause the config to be reset to default
		if(bqRxData & REG1_WD_FAULT)
		{
			//re-initialise the d
			BQ24250_Init();
		}
		// Make sure the Watch Dog Enable bit is set. 
		// The Write to this register resets the Watchdog timer. This is required for so taht the settings are maintained.
		bqTxData = (1 << REG1_WD_EN);
		bqComsPacket.buffer		= &bqTxData;
		twim_write_packet(&AVR32_TWIM0,	&bqComsPacket);
		
		// Check for faults
		BQ24250_CheckFault();
		//check the status of the charge controller
		BQ24250_ChargerStatus();
		
		// get the current time
		bqTimerSnapshot = TIMER_Now32();			

	}
	
	return (bqRxData & REG1_FAULT_MASK);
}

bq24250FaultEnum BQ24250_CheckFault(void)
{
	status_code_t twi_status = 0;
	twim_package_t	bqComsPacket;
	char	bqRxData = 0;
	
	// Setup the I2C transfer packet.
	bqComsPacket.addr[0] = BQ24250_I2C_REG1;
	bqComsPacket.addr_length = 1;
	bqComsPacket.buffer		= &bqRxData;
	bqComsPacket.chip		= BQ24250_I2C_ADDRESS;
	bqComsPacket.length		=	1;
	bqComsPacket.no_wait	=	1;
	
	twi_status = twim_read_packet (&AVR32_TWIM0, &bqComsPacket);
	
	if(twi_status == STATUS_OK)
	{
		switch (bqRxData & REG1_FAULT_MASK){
			case BQ_FAULT_NO_FAULT:
				SCASDBG_PrintAscii("BQ24250 - NO_FAULT\r\n");
				return NO_FAULT;
				break;
				
			case BQ_FAULT_VIN_OVP:
				SCASDBG_PrintAscii("BQ24250 - VIN_OVP\r\n");
				return	VIN_OVP;
				break;
			
			case BQ_FAULT_VIN_UVLO:
				SCASDBG_PrintAscii("BQ24250 - VIN_UVLO\r\n");
				return	VIN_UVLO;
				break;

			case BQ_FAULT_SLEEP:
				SCASDBG_PrintAscii("BQ24250 - SLEEP\r\n");
				return	SLEEP;
				break;
				
			case BQ_FAULT_BAT_TEMP_FAULT:
				SCASDBG_PrintAscii("BQ24250 - TEMP_FAULT\r\n");
				return	TEMP_FAULT;
				break;
				
			case BQ_FAULT_BAT_OVP:
				SCASDBG_PrintAscii("BQ24250 - BAT_OVP\r\n");
				return	BAT_OVP;
				break;
				
			case BQ_FAULT_THERM_SHDN:
				SCASDBG_PrintAscii("BQ24250 - THERM_SHDN\r\n");
				return	THERM_SHDN;
				break;
				
			case BQ_FAULT_TMR_FAULT:
				SCASDBG_PrintAscii("BQ24250 - TMR_FAULT\r\n");
				return	TMR_FAULT;
				break;
				
			case BQ_FAULT_NO_BATTERY:
				SCASDBG_PrintAscii("BQ24250 - NO_BATTERY\r\n");
				return	NO_BATTERY;
				break;
				
			case BQ_FAULT_ISET_SHORT:
				SCASDBG_PrintAscii("BQ24250 - ISET_SHORT\r\n");
				return	ISET_SHORT;
				break;
				
			case BQ_FAULT_IN_FAULT:
				SCASDBG_PrintAscii("BQ24250 - IN_FAULT\r\n");
				return	IN_FAULT;			
				break;
				
			default:
				SCASDBG_PrintAscii("BQ24250 - UNKNOWN_ERROR\r\n");
				return	UNKNOWN_ERROR;
				break;
		}
		
	}
	return NO_FAULT;
}

bq24250ChargeControllerStatus BQ24250_ChargerStatus(void)
{
	status_code_t twi_status = 0;
	twim_package_t	bqComsPacket;
	char	bqRxData = 0;
	
	// Setup the I2C transfer packet.
	bqComsPacket.addr[0] = BQ24250_I2C_REG1;
	bqComsPacket.addr_length = 1;
	bqComsPacket.buffer		= &bqRxData;
	bqComsPacket.chip		= BQ24250_I2C_ADDRESS;
	bqComsPacket.length		=	1;
	bqComsPacket.no_wait	=	1;
	
	twi_status = twim_read_packet (&AVR32_TWIM0, &bqComsPacket);
	
	if(twi_status == STATUS_OK)
	{
		switch (bqRxData & REG1_STAT_MASK)
		{
			case BQ_CHARGE_CNTRL_READY:
				SCASDBG_PrintAscii("BQ24250 - CHARGE_CNTRL_READY\r\n");
				return	CHARGE_CNTRL_READY;
				break;
				
			case BQ_CHARGE_CNTRL_CHARGING:
				SCASDBG_PrintAscii("BQ24250 - CHARGE_CNTRL_CHARGING\r\n");
				return	CHARGE_CNTRL_CHARGING;
				break;
				
			case BQ_CHARGE_CNTRL_DONE:
				SCASDBG_PrintAscii("BQ24250 - CHARGE_CNTRL_DONE\r\n");
				return	CHARGE_CNTRL_DONE;
				break;
				
			case BQ_CHARGE_CNTRL_FAULT:
				SCASDBG_PrintAscii("BQ24250 - CHARGE_CNTRL_FAULT\r\n");
				return	CHARGE_CNTRL_FAULT;
				break;
				
			default:
				SCASDBG_PrintAscii("BQ24250 - UNKNOWN_STATE\r\n");
				return	UNKNOWN_STATE;
				break;
			
		}
		return	((bqRxData >> 4) & REG1_STAT_MASK);
		
	}
	return	UNKNOWN_STATE;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function