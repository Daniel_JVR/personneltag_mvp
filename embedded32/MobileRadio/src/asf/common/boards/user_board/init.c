/// \file init.c
/// This is the implementation file of the init module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/08/28
/// - Module        : INIT
/// - Description   : Board-specific initialisation implementation
///
/// \author Michael Manthey (MM)
/// \version $Id: init.c,v 1.29 2015-02-17 08:51:02+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files

#include <asf.h>                    ///< All drivers that have been imported from the ASF
#include <board.h>                  ///< Standard board header file
#include <conf_board.h>             ///< User board configuration template
#include <string.h>

#include "timer.h"                  ///< Timer module used for measuring delays, etc.
#include "ioctl.h"                  ///< IOCTL Module for the Mobile Radio
#include "iophy.h"                  ///< IOPHY Module for the Mobile Radio
#include "sysconfig_uc3.h"          ///< Configuration parameters
#include "cc1101.h"                 ///< CC1101 Module header file
#include "uhf1.h"                   ///< UHF1 Module header file
#include "uhf2.h"                   ///< UHF2 Module header file
#include "rangetag.h"               ///< RANGETAG Module for the Mobile Radio
#include "ver.h"                    ///< Mobile Radio version description file
#include "sw_maintenance_uc3.h"     ///< SCAS II SW maintenance module
#include "msgprocess.h"             ///< MSGPROCESS Module header file
#include "diag_terminal.h"          ///< DIAG_TERMINAL used for debugging/configuration over IrDA
#include "irdahandler.h"            ///< IRDAHANDLER Module to use USART1 in IrDA mode
#include "cbit.h"                   ///< Continuous built-in test module header file
#include "calog.h"                  ///< CALOG Module header file
#include "quadrant.h"
#include "gpio.h"
#include "uhf2repeater.h"
#include "mrtest.h"
#include "ewphandler.h"
#include "paging.h"
#include "ptcstatus.h"
#include "uhf2_msghandler.h"
#include "serialhandler.h"
#include "msgdispatch.h"
#include "captod.h"
#include "gps.h"
#include "lcd.h"
#include "twim.h"
#include "BQ24250.h"
#include "M24SR04.h"

#include "scasdbg.h"

//-------------------------------------------------------------------------------------------------
// Defines

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;   ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// C function prototypes
void init_SmartStart(void);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
/// This function initialises the Mobile Radio
U8 board_init(void)
{
	U8 errCnt = 0;                                  // Used to count failed module initializations
	
	status_code_t twi_status = 0;
		
	twim_options_t twi0_config;
	twim_options_t twi1_config;
	
	twi0_config.pba_hz = FOSC0;
	twi0_config.speed = TWI_STD_MODE_SPEED;
	twi0_config.chip = (0x6A);
	
	twi1_config.pba_hz = FOSC0;
	twi1_config.speed = TWI_STD_MODE_SPEED;
	twi1_config.chip = (0x19);
		
	static const gpio_map_t TWIM0_GPIO_MAP = {
		{TWI0_TWD_PIN, TWI0_TWD_FUNCTION},          // TWD0
		{TWI0_TWCK_PIN, TWI0_TWCK_FUNCTION}         // TWCK0
	};
		
	static const gpio_map_t TWIM1_GPIO_MAP = {
		{TWI1_TWD_PIN, TWI1_TWD_FUNCTION},          // TWD1
		{TWI1_TWCK_PIN, TWI1_TWCK_FUNCTION}		    // TWCK1
	};
	
	// Initialise the interrupt controller
	INTC_init_interrupts();
	
	/// The bootloader has already configured and enabled PLL0, disable PLL before reconfiguring it	
	// 1. Set the main clock source to internal RC
	sysclk_set_source(SYSCLK_SRC_RCSYS);
	// 2. Disable PLL0
	pll_disable(0);

	// Configure the clocks using PLL0 - scale 12MHz crystal up to 96MHz then divide down to 48MHz
	/// \note Configuration for the system clocks are set in conf_clock.h with
	///       CONFIG_SYSCLK_SOURCE set to SYSCLK_SRC_PLL0
	sysclk_init();
	init_SmartStart();
	if(TIMER_Init() == FAILURE) {
		errCnt++;
	}
	IOCTL_Init();                                   // -> IOPHY_Init()
	
	EWPHANDLER_Init();
	PAGING_Init();
	PTCSTATUS_Init();
	
	// Reset the initReport stored in the MRTEST module
	MRTEST_ResetReport();
	
	// Check DI1_PIN which is connected to TP51 - Pulled low by test station to indicate test mode
	if(IOPHY_DigitalIn1() == IO_OFF) {
	    // Update MRTEST state
		MRTEST_SetState(TEST_MODE_ACTIVE);
		// Disable the WDT
		wdt_disable();
		// Give test station sufficient time to measure TP42
 		TIMER_DELAY_MS(500);
		
		// Assert TP42 - Test station will monitor this pin to detect a programmed Mobile Radio
		IOPHY_TestPinTP42(IO_ON);
		
		// Now allow test sequence to measure all power supply voltages, including 2V5 regulator
		gpio_set_pin_high(NANO_NSHDN_PIN);
		// Wait for DI1 to be pulled high before continuing with board initialisation
		while(IOPHY_DigitalIn1() == IO_OFF);
		// Shut down NanoLOC power supply
		gpio_set_pin_low(NANO_NSHDN_PIN);
	}
	
	IOPHY_DLED2(IO_ON);

	if(FLASH_Init() == FAILURE) {
		MRTEST_SetInitResultDataFlash(FAILURE);
		errCnt++;
	} else {
		MRTEST_SetInitResultDataFlash(SUCCESS);
	}
	
	SYSCONFIG_Init(&g_configBlock); // -> FLASH_Init() --> USPI_Init()
                                    // Even if SYSCONFIG_Init() returns FAILURE, continue with software execution
	                                // (load default config). Initialisation status of SYSCONFIG should be checked 
									// elsewhere using SYSCONFIG_GetInitState();

	wdt_clear();

	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_MU) {
		IOCTL_SweepWarnLedsBlocking();
	} else {
		IOCTL_SweepWarnLedsBlockingNoCaplamp();
	}

	///< Toggle the STATUS LED on the Hazard Unit enclosure a couple of times before CALOG_Init();
	//if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_HU) {
		//IOCTL_PulseDigOut1Blocking();
	//}
	
	wdt_clear();

	if(CALOG_Init() == FAILURE) { // -> FLASH_Init()
 		errCnt++;
	}
	wdt_clear();

	if(SWMAINT_Init() == FAILURE) {
		errCnt++;
	}
	wdt_clear();
	

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 
	// Initialise modules used for messaging
	// MSGDISPATCH needs to know which messages will be accepted by the Mobile Radio
	MSGDISPATCH_RegisterAcceptedDestination(MESSAGES_DEST_MU);
	MSGDISPATCH_RegisterAcceptedDestination(MESSAGES_DEST_UHF2);
	MSGDISPATCH_RegisterAcceptedDestination(MESSAGES_DEST_INTERNAL);
	MSGDISPATCH_RegisterAcceptedDestination(MESSAGES_DEST_DIRECT_DEBUG_NO_CRC);
	
	// SERIALHANDLER needs to initialise all the serial interfaces on the Mobile Radio
	if(SERIALHANDLER_Init() == FAILURE) {
	    errCnt++;
	}
	
	// UHF2_MSGHANDLER needs to initialise the UHF2 interface
	if(UHF2_MSGHANDLER_Init() == FAILURE) {
	    errCnt++;
	}
	wdt_clear();
	
	// Notify MRTEST module of the initialisation result of UHF2 module after initialising UHF2_MSGHANDLER
	MRTEST_SetInitResultUHF2(UHF2_GetInitResult());
	
	// MSGPROCESS must be initialised to ensure that it registers for receiving packets
	// from the MSGDISPATCH module
	if(MSGPROCESS_Init() == FAILURE) {
	    errCnt++;
	}
    wdt_clear();

	// Enable all interrupts.
    Enable_global_interrupt();

	if(RANGETAG_Init() == FAILURE) {                // -> CA_MSG_Init();
                                                    // -> UHF1_Init() --> NTRXInit();
		MRTEST_SetInitResultUHF1(FAILURE);
		errCnt++;
	} else {
		MRTEST_SetInitResultUHF1(SUCCESS);
	}
	
	// Disable low power mode if running in test mode
	if(MRTEST_GetState() == TEST_MODE_ACTIVE) {
		RANGETAG_DisableLowPowerMode();
	}
	wdt_clear();
	

	CBIT_Init();                                    // -> ADC_Init();
	wdt_clear();
	
	CAPTOD_Init();
	
	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_UHF2_REPEATER) {
		UHF2REPEATER_Init();
	}

	// Configure the HMATRIX for rapid DMA transfers
    #if (defined AVR32_HMATRIX)
      AVR32_HMATRIX.mcfg[AVR32_HMATRIX_MASTER_CPU_INSN] = 0x1;
    #elif (defined AVR32_HMATRIXB)
      AVR32_HMATRIXB.mcfg[AVR32_HMATRIXB_MASTER_CPU_INSN] = 0x1;
    #endif

	// Enable all interrupts.
    //Enable_global_interrupt();

	IOPHY_DLED2(IO_OFF);
	IOPHY_TestPinTP42(IO_OFF);  // De-assert TP42 - Test station will monitor this pin to determine
	                            //                  whether initialisation has been completed

	SCASDBG_SetIrdaOutput(TRUE);
	SCASDBG_PrintAscii("Init complete - SCASDBG functional..\r\n");
	
	//Initialise the BQ24250 GPIOs
	BQ24250_Init_GPIO();
	
	// Assign GPIO to TWIM0
	gpio_enable_module(TWIM0_GPIO_MAP,sizeof(TWIM0_GPIO_MAP) / sizeof(TWIM0_GPIO_MAP[0]));
	// Enable and initialise TWIM0
	twim_master_enable(&AVR32_TWIM0);
	twi_status = twim_master_init(&AVR32_TWIM0, &twi0_config);
	
	// initialize the BQ24250 battery charger
	BQ24250_Init();
	
	NFC_EEPROM_init();  
		
	///// Enable the Accelerometer IC pins
	//gpio_enable_gpio_pin(ACC_INT1_PIN);
	//gpio_configure_pin(ACC_INT1_PIN, (GPIO_DIR_INPUT | GPIO_PULL_UP));
	//gpio_enable_gpio_pin(ACC_INT2_PIN);
	//gpio_configure_pin(ACC_INT2_PIN, (GPIO_DIR_INPUT | GPIO_PULL_UP));
	//
	//// Assign GPIO to TWIM1
	//gpio_enable_module(TWIM1_GPIO_MAP,sizeof(TWIM1_GPIO_MAP) / sizeof(TWIM1_GPIO_MAP[0]));
	//
	//wdt_clear();
	//// Enable and initialize TWIM1	
	//twim_master_enable(&AVR32_TWIM1);
	//
	//twi_status = twim_master_init(&AVR32_TWIM1, &twi1_config);
	//if(twi_status == TWI_SUCCESS)
	//{
		//SCASDBG_PrintAscii("TWI1 INIT SUCCESS.\r\n");
	//}
	//else
	//{
		//SCASDBG_PrintAscii("TWI1 INIT FAIL.\r\n");
	//}
	
	GPS_Init();
	LCD_Init();

	if(errCnt > 0) {
		return FAILURE;
	} else {
		return SUCCESS;
	}

}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function performs a Smart Start for the processor, ensuring that external ICs are
///        in a safe state to ensure fail-safe start up
void init_SmartStart(void)
{
	///< Setup the nSHDN pin of the nanoLOC and switch the 2V5 regulator off initially
	gpio_configure_pin(NANO_NSHDN_PIN, GPIO_DIR_OUTPUT);
	gpio_set_pin_low(NANO_NSHDN_PIN);
	
#ifdef USE_AST_CALENDAR
    // Start OSC_32KHZ only if AST calendar is used
	scif_osc32_opt_t opt;
	opt.mode = SCIF_OSC_MODE_2PIN_CRYSTAL_HICUR;
	opt.startup = AVR32_SCIF_OSCCTRL32_STARTUP_8192_RCOSC;
	scif_start_osc32(&opt,true);
#endif

	// Setup WDT to reset system if an initialisation step takes more than 5 seconds
    wdt_opt_t wdtOptions = {
	    .cssel = WDT_CLOCK_SOURCE_SELECT_RCSYS,     ///< Select the 32kHz oscillator as clock source
		.dar   = false,                             ///< After a watchdog reset, the WDT will still be enabled
		.fcd   = false,                             ///< The flash calibration will be redone after a watchdog reset
		.mode  = WDT_BASIC_MODE,                    ///< The WDT is in basic mode, only PSEL time is used.
		.sfv   = false,                             ///< WDT Control Register is not locked
		.us_timeout_period  = 5000000               ///< Time out value of 5 seconds
	};
    wdt_enable(&wdtOptions);
}