/// \file iophy.h
/// This is the header file of the iophy module for the SCASII
/// - Project      : Schauenburg Collision Avoidance System II (SCASII)
/// - Company      : Parsec
/// - Client       : Schauenburg
/// - Date created : 2011/09/13
/// - Module       : IOPHY
/// - Description  : iophy interface header
///
/// \details  This module contains functions that control Mobile Radio inputs and outputs on the 
///           hardware level.
///
/// \author Michael Manthey (MM)
/// \version $Id: iophy.h,v 1.6 2013-06-14 10:56:18+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------

#ifndef _IOPHY_H_
#define _IOPHY_H_

//-------------------------------------------------------------------------------------------------
// Include files
#include "compiler.h"       ///< Commonly used types and macros
#include "ioctl.h"          ///< IOCTL module header file which defines the IOState enum
#include "board.h"          ///< Includes hardware definitions for the Mobile Radio PCB

#ifdef __cplusplus
extern "C" {    // C++
#endif

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// Class definitions
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
/// \brief This function initialises the IOPHY module, and sets all output pins in default state 
void IOPHY_Init(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the DLED1 (debugging LED) on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_DLED1(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the DLED2 (debugging LED) on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_DLED2(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the run LED on or off. (note: DLED1 is used as run LED)
/// \param state [in]The state to set it to (on/off)
void IOPHY_RunLED(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the vehicle warning LED on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_VehicleWarningLED(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the loco warning LED on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_LocoWarningLED(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the hazard warning LED on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_HazardWarningLED(IOState state);

//-------------------------------------------------------------------------------------------------
/// This function sets the worker exculsion/driver deregistration LED on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_WrkrExclDrvrDeregLED(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the digital output on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_DigitalOut1(IOState state);

void IOPHY_DigitalOut1_Buzz(IOState state);
//-------------------------------------------------------------------------------------------------
/// \brief This function toggles the digital output
void IOPHY_DigitalOut1Toggle(void);
void IOPHY_DigitalOut1Toggle_Buzz(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets the test pin on TP42 on or off
/// \param state [in] The state to set it to (on/off)
void IOPHY_TestPinTP42(IOState state);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the state of digital input 1
/// \return The state of the digital input, HIGH -> IO_ON; LOW = IO_OFF
IOState IOPHY_DigitalIn1(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the state of Emergency Stop button, which pulls down DI1 when 
///        pressed
/// \return The state of the stop button, HIGH -> IO_OFF; LOW = IO_ON
IOState IOPHY_EmergencyStopBtn(void);

//-------------------------------------------------------------------------------------------------
/// \brief This function sets all four warning LED's on the MR on or off
/// \param state The desired state of the warning LED's, on or off
void IOPHY_SetAllWarnleds(IOState state);

#ifdef __cplusplus
};
#endif

#endif	// _IOPHY_H_
