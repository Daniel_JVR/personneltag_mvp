/// \file slinkmon.c
/// This is the implementation file of the slinkmon module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/12/15
/// - Module        : SLINKMON
/// - Description   : slinkmon implementation file
///
/// \detail	slinkmon
///
///
/// \author Michael Manthey (MM)
/// \version $Id: slinkmon.c,v 1.1 2014-12-19 12:13:07+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "slinkmon.h"
#include "common.h"
#include "ca_msg.h"
#include "msghandler.h"

#include "strbuilder.h"
#include "ptcctrl.h"

#include <string.h>

//-------------------------------------------------------------------------------------------------
// Defines
#define SLINKMON_MAX_NUM_ENTRIES        (4)

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct _SLINKMON_SLinkInfo {
	DeviceID id;
	S16 latestRssidBm;
	U32 lastSeenTs;
} ATTR_PACKED SLINKMON_SLinkInfo;

//-------------------------------------------------------------------------------------------------
// Structures
typedef struct _SLINKMON_SLinkArray {
	U8 numEntries;
	SLINKMON_SLinkInfo entries[SLINKMON_MAX_NUM_ENTRIES];
} ATTR_PACKED SLINKMON_SLinkArray;

//-------------------------------------------------------------------------------------------------
// Global variables
static SLINKMON_SLinkArray slinkArray = {0};

//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes
U8 slinkmon_FindDevice(DeviceID id, U8* foundPosi);
void slinkmon_InsertNewSLink(SLINKMON_SLinkInfo info);
void slinkmon_RemoveOldestEntry(void);
S16 slinkmon_GetRssiValue(U8 nominalRssi);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void SLINKMON_PrintArray(void)
{
	static U8 builtStr[255] = {0};
		
	// Remove entries which have been in the array for longer than 10 minutes
	for(U8 i = 0; i < slinkArray.numEntries; i++) {
		if(TIMER_Elapsed32(slinkArray.entries[i].lastSeenTs) > TIMER_S(300)) {
			// Now remove the entry by moving all subsequent entries up one position
	        for(U8 k = i; k < (slinkArray.numEntries - 1); k++) {
		        slinkArray.entries[k] = slinkArray.entries[k+1];
	        }
	        slinkArray.numEntries--;
		}
	}
		
	STRBUILDER_ResetString(builtStr);
		
	if(slinkArray.numEntries == 0) {
		STRBUILDER_AddText(builtStr, "\nNo S-Links Detected Yet\n");
		
	} else {
		for(U8 i = 0; i < slinkArray.numEntries; i++) {
			//STRBUILDER_AddText(builtStr, "0x");
			STRBUILDER_AddHex(builtStr, 5, (U8*)&(slinkArray.entries[i].id), FALSE, ",");
			STRBUILDER_AddText(builtStr, ", ");
			if(slinkArray.entries[i].latestRssidBm >= 0) {
				
			    STRBUILDER_AddUnsignedDecimal(builtStr, slinkArray.entries[i].latestRssidBm, TRUE, 2);
				
			} else {
				STRBUILDER_AddText(builtStr, "-");
			    STRBUILDER_AddUnsignedDecimal(builtStr, (U32)(0 - slinkArray.entries[i].latestRssidBm), TRUE, 2);
				
			}
			STRBUILDER_AddText(builtStr, "dBm");
			STRBUILDER_AddText(builtStr, ", ");
			STRBUILDER_AddUnsignedDecimal(builtStr, 1 + TIMER_Elapsed32(slinkArray.entries[i].lastSeenTs) / TIMER_S(1), TRUE, 3);
			STRBUILDER_AddText(builtStr, "s");
			STRBUILDER_AddLineFeed(builtStr);
		}
	}
	
	// Also send the string to the PTC Display so that it can be displayed
	PTCCTRL_MSG_AdHocDisplayContent adHocMsg = {0};
	adHocMsg.invert = FALSE;
	adHocMsg.persist = FALSE;
	adHocMsg.durationMs = 2000;
	adHocMsg.dismissable = FALSE;
	adHocMsg.futureUse = 0;
	memcpy(adHocMsg.content, builtStr, STRBUILDER_GetStringLength(builtStr) + 1);
	MSGHANDLER_TransmitPacket(MESSAGES_DEST_IRDA, MESSAGES_TYPE_PTC_AD_HOC_STRING, 11 + STRBUILDER_GetStringLength(builtStr) + 1, (U8*)&adHocMsg);
	////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC, MESSAGES_TYPE_PTC_AD_HOC_STRING, 11 + STRBUILDER_GetStringLength(builtStr) + 1, (U8*)&adHocMsg);
}

//-------------------------------------------------------------------------------------------------
void SLINKMON_UHF2PacketReceived(MESSAGES_UHF2Msg *rxMsg)
{
    ca_msg_Beacon *beaconMsg;                   // Beacon
    ca_msg_Wrap *wrapMsg;                       // CAM, CAACK, SSM and SSAM.
	
	// Check whether this is a message from an S-Link (cannot really check agents)
	switch(rxMsg->dllType) {
        case MESSAGES_UHF2_DLL_TYPE_CA:
			// Check for beacons and CAM messages sent by an S-Link
			switch(rxMsg->payload[0]) {
				
                // Collision avoidance beacon message
                case CA_MSG_BEACON:
				    beaconMsg = (ca_msg_Beacon*)rxMsg;
					if(beaconMsg->field.srcAddress.IDByte1 == UNIT_TYPE_SAP) {
						// We received a message from an S-Link
						SLINKMON_SLinkMessageReceived(beaconMsg->field.srcAddress, rxMsg->rssi_lqi[0]);
					}
                    break;

                // Collision avoidance message - address filtering
				// Collision avoidance ACK - address filtering
                case CA_MSG_CAM:
                case CA_MSG_CAACK:
                    wrapMsg = (ca_msg_Wrap*)rxMsg;
					if(wrapMsg->field.srcAddress.IDByte1 == UNIT_TYPE_SAP) {
						// We received a message from an S-Link
						SLINKMON_SLinkMessageReceived(wrapMsg->field.srcAddress, rxMsg->rssi_lqi[0]);
					}
                    break;
			}					
			break;      // MESSAGES_UHF2_DLL_TYPE_CA
			
        case MESSAGES_UHF2_DLL_TYPE_P2_SHORT:
			// Check for any message transmitted by an S-Link
			
			break;      // MESSAGES_UHF2_DLL_TYPE_P2_SHORT
	}    		
}

//-------------------------------------------------------------------------------------------------
void SLINKMON_SLinkMessageReceived(DeviceID slinkID, U8 rssi)
{
	// Add this device to the list of S-Links. Remove oldest entry if necessary.
	static SLINKMON_SLinkInfo info = {{0}};
	
	U8 foundPosi;
	if(slinkmon_FindDevice(slinkID, &foundPosi) == TRUE) {
		// Update the S-Link's info
		slinkArray.entries[foundPosi].latestRssidBm = slinkmon_GetRssiValue(rssi);
		slinkArray.entries[foundPosi].lastSeenTs = TIMER_Now32();
	} else {
		// Add the new S-Link
		info.id = slinkID;
		info.lastSeenTs = TIMER_Now32();
		info.latestRssidBm = slinkmon_GetRssiValue(rssi);
		slinkmon_InsertNewSLink(info);
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 
U8 slinkmon_FindDevice(DeviceID id, U8* foundPosi)
{
	for(U8 i = 0; i < slinkArray.numEntries; i++) {
		if(SYSCONFIG_DeviceIdCompare(&id, &(slinkArray.entries[i].id)) == SUCCESS) {
			*foundPosi = i;
			return TRUE;
		}
	}
	
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function 
void slinkmon_InsertNewSLink(SLINKMON_SLinkInfo info)
{
	if(slinkArray.numEntries == SLINKMON_MAX_NUM_ENTRIES) {
		// Remove oldest entry first
		slinkmon_RemoveOldestEntry();
	}
		
    // Add the new entry
    slinkArray.entries[slinkArray.numEntries] = info;
    slinkArray.numEntries++;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function 
void slinkmon_RemoveOldestEntry(void)
{
	// Check whether there are any entries to remove
	if(slinkArray.numEntries > 0) {
		// Find the oldest entry
		U32 oldestTs = slinkArray.entries[0].lastSeenTs;
		U8 oldestPosi = 0;
		for(U8 i = 0; i < slinkArray.numEntries; i++) {
			if(TIMER_Elapsed32(slinkArray.entries[i].lastSeenTs) > TIMER_Elapsed32(oldestTs)) {
				oldestPosi = i;
				oldestTs = slinkArray.entries[i].lastSeenTs;
			}
		}
		// Now remove the oldest entry by moving all subsequent entries up one position
		for(U8 i = oldestPosi; i < (slinkArray.numEntries - 1); i++) {
			slinkArray.entries[i] = slinkArray.entries[i+1];
		}
		slinkArray.numEntries--;
		
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function 
S16 slinkmon_GetRssiValue(U8 nominalRssi)
{
	return ((U8)((U16)nominalRssi + 128) / 2) - 138;
}