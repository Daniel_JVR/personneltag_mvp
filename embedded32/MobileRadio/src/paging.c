/// \file paging.c
///
/// \details For testing/demonstration purposes only

/// \author Michael Manthey (MM)
/// \version $Id: paging.c,v 1.16 2015-01-27 09:15:59+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "paging.h"         ///< PAGING module header file

#include "common.h"         ///< Common defines such as TRUE, FALSE etc
#include "board.h"          ///< HW definitions
#include "timer.h"          ///< SCAS II TIMER module
#include "msghandler.h"
#include "uhf2_msghandler.h"
#include "ewphandler.h"

#include <string.h>         ///< For memcpy(), memset(), etc.

// Debugging
#include "ioctl.h"

//-------------------------------------------------------------------------------------------------
// Defines
#define PAGING_SPACE_CHAR               (0x20)
#define PAGING_BUFFER_SIZE              (8)         // Only 8 pages will be queued for display on
                                                    // the PTC Display
#define PAGING_POST_RESPONSE_WAIT_S     (3)         // Wait more 3 seconds after receiving a page 
                                                    // response, before sending a new page

#define MESSAGE_QUEUE_RETRY_INTERVAL_MS (3000)      // Retry sending messages to the Paging Agent
                                                    // every 3 seconds while in range of a SAP

//-------------------------------------------------------------------------------------------------
// Enums
//-------------------------------------------------------------------------------------------------
// Each entry in the page buffer is either pending or has been sent and awaits acknowledgment
typedef enum _PAGING_PageStatus {
	PAGE_TX_PENDING,        // Page must still be sent to PTC
	PAGE_SENT,              // Page has been sent to PTC
	PAGE_ACKNOWLEDGED,      // Page acknowledged by PTC. Not sure if it will always be acknowledged
} PAGING_PageStatus;

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
typedef struct _PAGING_PageBufferEntry {
	P2MESSAGES_P2PageToken token;
	DeviceID srcID;
	PTCCTRL_MSG_DisplayMessage msg;
	PAGING_PageStatus status;
	U32 txTimestamp;
} ATTR_PACKED PAGING_PageBufferEntry;

//-------------------------------------------------------------------------------------------------
typedef struct _PAGING_PageBuffer {
	PAGING_PageBufferEntry pages[PAGING_BUFFER_SIZE];
	U32 lastResponseTimestamp;
	U8 numEntries;
} ATTR_PACKED PAGING_PageBuffer;

//-------------------------------------------------------------------------------------------------
typedef struct _PAGING_P2MsgQueueEntry {
	P2MESSAGES_P2PageToken token;
	U8 payloadLen;
	P2MESSAGES_ShortMessage p2Msg;
	U32 lastTxTimestamp;
} ATTR_PACKED PAGING_P2MsgQueueEntry;

//-------------------------------------------------------------------------------------------------
typedef struct _PAGING_P2MsgQueue {
	PAGING_P2MsgQueueEntry messages[PAGING_BUFFER_SIZE];
	U8 numEntries;
} ATTR_PACKED PAGING_P2MsgQueue;

//-------------------------------------------------------------------------------------------------
typedef struct _PAGING_Lib {
	U32 pageId;
	PAGING_PageBuffer buffer;
	PAGING_P2MsgQueue msgQueue;
	TIMER_TimeStamp displayedTime;
	U8 callSeq;                     // A sequence number held for use when sending Call messages to 
	                                // the Paging Agent
} ATTR_PACKED PAGING_Lib;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;       ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// Global variables
static PAGING_Lib pagingLib = {0};

//-------------------------------------------------------------------------------------------------
// C function prototypes
//-------------------------------------------------------------------------------------------------
// Paging buffer related functions (Buffer of pages sent to PTC Display over UART)
U8 paging_FindBufferedPageById(U32 pageId, U8 *foundPosi);
U8 paging_FindBufferedPageByToken(P2MESSAGES_P2PageToken token, U8 *foundPosi);
void paging_QueuePageToPTC(P2MESSAGES_P2PageToken *token, DeviceID srcID,  PTCCTRL_MSG_DisplayMessage *msg);
void paging_SendQueuedPage(U8 bufferIndex);
U8 paging_UserResponsePending(void);

// Paging queue related functions (Queue of UHF2 messages destined to the Paging agent)
U8 paging_FindQueuedMessage(P2MESSAGES_P2PageToken token, U8 *foundPosi);
void paging_QueueMessageToPagingAgent(P2MESSAGES_P2PageToken *token, U8 payloadLen, P2MESSAGES_ShortMessage *message);
void paging_DequeueMessageToPagingAgent(P2MESSAGES_P2PageToken *token);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
U8 PAGING_Init(void)
{
    // Register callback function
	EWPHANDLER_RegisterCallback(&PAGING_P2PageMessageReceived);
	
	return SUCCESS;
}
	
//-------------------------------------------------------------------------------------------------
void PAGING_Service(void)
{
	// Check whether a user response is pending for a page previously sent to the PTC
	if(paging_UserResponsePending() == FALSE) {
		
		// \todo Check whether a PTC is actually present (i.e. check for healthy communication)
		// For example: if(PTCPRESENCE_IsPtcPresent() == TRUE) {...}
		
		// No response pending - Scan through page buffer, send next unsent page
		// Note: scan in reverse order since the buffer is sorted "oldest page at the bottom"
	    for(U8 i=pagingLib.buffer.numEntries; i > 0; i--) {
		    if(pagingLib.buffer.pages[i-1].status == PAGE_TX_PENDING) {
			    if(TIMER_Elapsed32(pagingLib.buffer.lastResponseTimestamp) > TIMER_S(PAGING_POST_RESPONSE_WAIT_S)) {
				    paging_SendQueuedPage(i-1);
					// Break out of the for loop to avoid sending multiple queued pages to PTC
					break;
			    }
		    }
	    }
	}
	
	// \todo Check the PTC's current status to ensure that it is currently showing the page we
	// expect it to show etc.
	
	// Check whether in range of a SAP and send any pending messages to the Paging Agent
	if(TRUE) { // \todo make this dependent on SAP proximity
		for(U8 i = 0; i < pagingLib.msgQueue.numEntries; i++) {
			if(TIMER_Elapsed32(pagingLib.msgQueue.messages[i].lastTxTimestamp) > TIMER_MS(MESSAGE_QUEUE_RETRY_INTERVAL_MS)) {
				pagingLib.msgQueue.messages[i].lastTxTimestamp = TIMER_Now32();
				UHF2_MSGHANDLER_TransmitP2ShortMsgDirect(&(pagingLib.msgQueue.messages[i].p2Msg), pagingLib.msgQueue.messages[i].payloadLen);
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void PAGING_MessageProcess(MESSAGES_PacketG2 *packet)
{
	PTCCTRL_MSG_PresetMessage *presetMsg;
	PTCCTRL_MSG_UserResponse *respMsg;
	P2MESSAGES_ShortMessage *p2Blob;
	P2MESSAGES_P2CallMessage *p2Call;
	
	switch(packet->msgType) {
		case MESSAGES_TYPE_PTC_KEEP_ALIVE_REQ:
		case MESSAGES_TYPE_PTC_KEEP_ALIVE_RES:
		    // Handled in the PTCSTATUS module
			break;
			
		case MESSAGES_TYPE_PTC_DISPLAY_MSG:
		    // This message is usually transmitted to the PTC and not received from the PTC
			break;
			
		case MESSAGES_TYPE_PTC_SEND_PRESET_MSG:
		    // A user selected a preset message to be sent to the paging agent
			// Update the unique token portion of the preset message before queuing the message
			// for delivery to the Paging Agent (when in range of a SAP)
			presetMsg = (PTCCTRL_MSG_PresetMessage*)&(packet->data);
			p2Blob = (P2MESSAGES_ShortMessage*)&(presetMsg->blob);
			// Update the srcID of the blob
			p2Blob->srcID = g_configBlock.deviceID;
			// Update the unique token of the P2CallMessage portion of the blob
			p2Call = (P2MESSAGES_P2CallMessage*)&(p2Blob->payload);
			p2Call->token.sourceDeviceID = g_configBlock.deviceID;
			p2Call->token.unixTimestamp = TIMER_GetUnixTime();
			p2Call->token.sequence = pagingLib.callSeq++;       // Also increment the sequence number
			
			// The blob is now ready for transmission to the Paging Agent, put it on the queue
			paging_QueueMessageToPagingAgent(&(p2Call->token), sizeof(P2MESSAGES_P2CallMessage), p2Blob);
			
			// \todo Notify the PTC Display that the preset message (call message) has successfully been queued
			// for delivery to the Paging Agent
			
			break;
			
		case MESSAGES_TYPE_PTC_USER_RESPONSE:
		    // A user responded to a page that was sent to the PTC Display
			respMsg = (PTCCTRL_MSG_UserResponse*)&(packet->data);
			
			PAGING_UserResponseReceived(respMsg);
			
			break;
			
		default:
			break;
	}
}

//-------------------------------------------------------------------------------------------------
void PAGING_P2PageMessageReceived(P2MESSAGES_ShortMessage *p2PageMsg)
{
	// Pointers used to interpret the received message payload
	P2MESSAGES_GenericP2PageMessage *ewpMsg;
	P2MESSAGES_P2PageDeliveryAck *ackMsg;
	
	switch(p2PageMsg->type) {
		case P2MESSAGES_TYPE_EARLY_WARN_PAGE:
		    // Create access pointer to the received Protocol 2 early warning / page message
	        ewpMsg = (P2MESSAGES_GenericP2PageMessage *)&(p2PageMsg->payload);
		
	        // Create a display message to send to the SCAS II PTC
	        static PTCCTRL_MSG_DisplayMessage dispMessage;
	        dispMessage.uniqueId = pagingLib.pageId++;
	        dispMessage.msgClass = PTCCTRL_NOTICE;  // \todo Should really be derived from p2PageMsg
	        dispMessage.responseType = ewpMsg->actions.userResponseType;
	        dispMessage.messageTimeoutSeconds = ewpMsg->warnDurationS;
	        memcpy((char*)&(dispMessage.text), (char*)&(ewpMsg->str), P2MESSAGES_GENERIC_MSG_MAX_LEN);
		
	        // Add the page to the page buffer 
	        paging_QueuePageToPTC(&(ewpMsg->token), p2PageMsg->srcID, &dispMessage);
			
			// If this is a gas warning message
			if((ewpMsg->actions.threatType == 0b0001) || (ewpMsg->actions.threatType == 0b0010) || (ewpMsg->actions.threatType == 0b0011)) {
				// For now, trigger gas alarm indication on MR caplamp LED 
				// Indication should actually be selected based on pedestrian action etc.
				IOCTL_SetGasWarning(IO_BLINKING, ewpMsg->warnDurationS);
			} else {
				// For a demonstration of Cap Lamp LED triggering when a page is received, simulate a gas 
	            // warning here
	            IOCTL_SetGasWarning(IO_ON, ewpMsg->warnDurationS);
			}
			break;
			
		case P2MESSAGES_TYPE_EARLY_WARN_PAGE_RESPONSE_ACK:
		    ackMsg = (P2MESSAGES_P2PageDeliveryAck *)&(p2PageMsg->payload);
			paging_DequeueMessageToPagingAgent(&(ackMsg->token));
			break;
			
		case P2MESSAGES_TYPE_CALL_MESSAGE_ACK:
		    ackMsg = (P2MESSAGES_P2PageDeliveryAck *)&(p2PageMsg->payload);
			paging_DequeueMessageToPagingAgent(&(ackMsg->token));
			break;
			
		default:
			break;
	}
	
}

//-------------------------------------------------------------------------------------------------
void PAGING_UserResponseReceived(PTCCTRL_MSG_UserResponse *response)
{
	U8 foundPosi;
	
	pagingLib.buffer.lastResponseTimestamp = TIMER_Now32();
	
	// Find the page in the buffer
	if(paging_FindBufferedPageById(response->uniqueId, &foundPosi) == FALSE) {
		// The page is not in the buffer
		return;
	}
	 
	// Update the page status to PAGE_ACKNOWLEDGED
	pagingLib.buffer.pages[foundPosi].status = PAGE_ACKNOWLEDGED;
	
	// Check whether a response must be sent to the Paging Agent over UHF2
	if(pagingLib.buffer.pages[foundPosi].msg.responseType != P2MESSAGES_PAGE_RESPONSE_NONE) {
		
		// Build the response packet
		P2MESSAGES_ShortMessage p2Msg;
		p2Msg.destID = pagingLib.buffer.pages[foundPosi].srcID;
		p2Msg.srcID = g_configBlock.deviceID;
		p2Msg.destPort = P2MESSAGES_PORT_PAGING;
		p2Msg.srcPort = P2MESSAGES_PORT_PAGING;
		p2Msg.type = P2MESSAGES_TYPE_EARLY_WARN_PAGE_RESPONSE;
		
		// The payload of a P2MESSAGES_TYPE_EARLY_WARN_PAGE_RESPONSE message is P2MESSAGES_P2PageResponse
	    P2MESSAGES_P2PageResponse p2MsgPayload;
		p2MsgPayload.token = pagingLib.buffer.pages[foundPosi].token;
		p2MsgPayload.respTimestamp = TIMER_GetUnixTime();
		p2MsgPayload.response = response->response;
		
		// Copy the payload to the p2Msg variable
		memcpy((U8*)&(p2Msg.payload), (U8*)&(p2MsgPayload), sizeof(P2MESSAGES_P2PageResponse));
		
		// Queue the response for transmission over UHF2 when within range of a SAP
		
		paging_QueueMessageToPagingAgent(&(p2MsgPayload.token), sizeof(P2MESSAGES_P2PageResponse), &p2Msg);
			
		// \todo Notify the PTC Display that the response message has successfully been queued
		// for delivery to the Paging Agent
		
	}
}

//-------------------------------------------------------------------------------------------------
U8 PAGING_GetPendingDisplayMessageCount(void)
{
	U8 retVal = 0;
	
	for(U8 i = 0; i < pagingLib.buffer.numEntries; i++) {
		if(pagingLib.buffer.pages[i].status == PAGE_TX_PENDING) {
			retVal++;
		}
	}
	
	return retVal;
}

//-------------------------------------------------------------------------------------------------
U8 PAGING_GetQueuedP2MessageCount(void)
{
	return pagingLib.msgQueue.numEntries;
}


//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function searches for a specific pageId in the page buffer
/// \param pageId The unique page ID to search for in the page buffer
/// \param foundPosi [in] A pointer to the variable to store the index posi in, if found
/// \return TRUE if page was found, FALSE otherwise
U8 paging_FindBufferedPageById(U32 pageId, U8 *foundPosi)
{
	for(U8 i=0; i < pagingLib.buffer.numEntries; i++) {
		if(pagingLib.buffer.pages[i].msg.uniqueId == pageId) {
			*foundPosi = i;
			return TRUE;
		}
	}
	// pageID not found
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function searches for a specific token in the page buffer
/// \param token The unique token to search for in the page buffer
/// \param foundPosi [in] A pointer to the variable to store the index posi in, if found
/// \return TRUE if page was found, FALSE otherwise
U8 paging_FindBufferedPageByToken(P2MESSAGES_P2PageToken token, U8 *foundPosi)
{
	for(U8 i=0; i < pagingLib.buffer.numEntries; i++) {
		if(memcmp((U8*)&(pagingLib.buffer.pages[i].token), (U8*)&(token), sizeof(P2MESSAGES_P2PageToken)) == 0) {
			*foundPosi = i;
			return TRUE;
		}
	}
	// token not found
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function puts a page on the page buffer to be transmitted to the SCAS II PTC at an
///         appropriate time.
void paging_QueuePageToPTC(P2MESSAGES_P2PageToken *token, DeviceID srcID, PTCCTRL_MSG_DisplayMessage *msg)
{
	U8 foundPosi;
	U8 found = paging_FindBufferedPageByToken(*token, &foundPosi);
	
	// Add the page to the top of the page buffer, if not already in buffer
	if((found == FALSE) || ((found == TRUE) && (pagingLib.buffer.pages[foundPosi].status == PAGE_ACKNOWLEDGED))) {
		// Move all entries in the buffer down 1 position
		for(U8 i=(PAGING_BUFFER_SIZE-1); i > 0; i--) {
			pagingLib.buffer.pages[i] = pagingLib.buffer.pages[i-1];
		}
		
		// Add new page to the top of the buffer
		pagingLib.buffer.pages[0].status = PAGE_TX_PENDING;
		pagingLib.buffer.pages[0].token = *token;
		pagingLib.buffer.pages[0].srcID = srcID;
		pagingLib.buffer.pages[0].msg = *msg;
		
		// Increment numEntries if necessary
		if(pagingLib.buffer.numEntries < PAGING_BUFFER_SIZE) {
			pagingLib.buffer.numEntries++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function transmits a page in the page buffer to the PTC display
/// \param bufferIndex Index position in buffer of the page which must be transmitted
void paging_SendQueuedPage(U8 bufferIndex)
{
	// Send the message to the SCAS II PTC
	////MSGHANDLER_TransmitPacket(MESSAGES_DEST_MR_PTC, 
	////                            MESSAGES_TYPE_PTC_DISPLAY_MSG, 
	////						    sizeof(PTCCTRL_MSG_DisplayMessage),
	////						    (U8*)&(pagingLib.buffer.pages[bufferIndex].msg));

	pagingLib.buffer.pages[bufferIndex].status = PAGE_SENT;
	pagingLib.buffer.pages[bufferIndex].txTimestamp = TIMER_Now32();
	
	// For a demonstration of Cap Lamp LED triggering when a page is received, simulate a gas 
	// warning here
	// IOCTL_SetGasWarning(IO_ON, pagingLib.buffer.pages[bufferIndex].msg.messageTimeoutSeconds);
}

//-------------------------------------------------------------------------------------------------
/// \brief This function searches for page in the page buffer with status PAGE_SENT, meaning that
///         we are waiting for a user response to the page
/// \return TRUE if a response is expected from the user, FALSE otherwise
U8 paging_UserResponsePending(void)
{
	for(U8 i=0; i < pagingLib.buffer.numEntries; i++) {
		if(pagingLib.buffer.pages[i].status == PAGE_SENT) {
			return TRUE;
		}
	}
	// pageID not found
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function searches for a specific token in the message queue
/// \param token The unique token to search for in the message queue
/// \param foundPosi [in] A pointer to the variable to store the index posi in, if found
/// \return TRUE if message was found, FALSE otherwise
U8 paging_FindQueuedMessage(P2MESSAGES_P2PageToken token, U8 *foundPosi)
{
	for(U8 i=0; i < pagingLib.msgQueue.numEntries; i++) {
		if(memcmp((U8*)&(pagingLib.msgQueue.messages[i].token), (U8*)&(token), sizeof(P2MESSAGES_P2PageToken)) == 0) {
			*foundPosi = i;
			return TRUE;
		}
	}
	// token not found
	return FALSE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function puts a message on the message queue to be transmitted to the Paging Agent 
///         when within range of a SAP.
void paging_QueueMessageToPagingAgent(P2MESSAGES_P2PageToken *token, U8 payloadLen, P2MESSAGES_ShortMessage *message)
{
	U8 foundPosi;
	
	// Add the message to the top of the message queue, if not already in queue
	if(paging_FindQueuedMessage(*token, &foundPosi) == FALSE) {
		// Move all entries in the queue down 1 position
		for(U8 i=(PAGING_BUFFER_SIZE-1); i > 0; i--) {
			pagingLib.msgQueue.messages[i] = pagingLib.msgQueue.messages[i-1];
		}
		
		// Add new message to the top of the queue
		pagingLib.msgQueue.messages[0].token = *token;
		pagingLib.msgQueue.messages[0].payloadLen = payloadLen;
		pagingLib.msgQueue.messages[0].p2Msg = *message;
		pagingLib.msgQueue.messages[0].lastTxTimestamp = TIMER_Now32() - TIMER_MS(MESSAGE_QUEUE_RETRY_INTERVAL_MS);
		
		// Increment numEntries if necessary
		if(pagingLib.msgQueue.numEntries < PAGING_BUFFER_SIZE) {
			pagingLib.msgQueue.numEntries++;
		}
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function removes a message from the message queue
void paging_DequeueMessageToPagingAgent(P2MESSAGES_P2PageToken *token)
{
	U8 foundPosi;
	
	// Find the message in the queue
	if(paging_FindQueuedMessage(*token, &foundPosi) == TRUE) {
		// Remove the message by moving all lower entries up one position
		for(U8 i = foundPosi; i < (pagingLib.msgQueue.numEntries - 1); i++) {
			pagingLib.msgQueue.messages[i] = pagingLib.msgQueue.messages[i+1];
		}
		pagingLib.msgQueue.numEntries--;
	} else {
		// Message was not found in the queue
	}		
	
}