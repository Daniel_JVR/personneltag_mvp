/// \file caectrl.c
/// This is the implementation file of the caectrl module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2014/04/11
/// - Module        : CAECTRL
/// - Description   : caectrl implementation file
///
/// \detail	Collision Avoidance Extender (CAE) Control Module.
///         This is the implementation file for the CAECTRL module which runs on the Mobile
///         Radio PCB. It takes care of broadcasting ranging results to other nearby CA Masters and
///         also relaying range result messages between CA Masters and nearby Miner Units.
///
///
/// \author Michael Manthey (MM)
/// \version $Id: caectrl.c,v 1.1 2014-04-16 07:59:03+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "caectrl.h"
#include "uhf2_msghandler.h"
#include "msghandler.h"

#include <string.h>

//-------------------------------------------------------------------------------------------------
// Defines
// none

//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Global variables
// none

//-------------------------------------------------------------------------------------------------
// External variables
//-------------------------------------------------------------------------------------------------
extern ConfigBlock g_configBlock;       ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// C function prototypes
// none

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
void CAECTRL_RangingResultUpdate(DeviceID targetId, VISIBILITY_CaVisibility targetVisibility, U8 targetAveRssi, S16 targetDist)
{
	// Create a blank CAE result message
    CAECTRL_RangingResultMessage resultMsg;
	memset((void*)&resultMsg, 0x00, sizeof(CAECTRL_RangingResultMessage));
	
	// Populate the result message with the supplied parameters
	resultMsg.caeDeviceId = g_configBlock.deviceID;
	resultMsg.targetId = targetId;
	resultMsg.targetVisibility = targetVisibility;
	resultMsg.targetAveRssi = targetAveRssi;
	resultMsg.targetDist = targetDist;
	
	// Broadcast the result message over UHF2 using Protocol 2
	UHF2_MSGHANDLER_TransmitP2ShortMsg(UHF2_MSGHANDLER_BroadcastID(), 
	                                   P2MESSAGES_PORT_SCASII_CA_RESULTS,
									   P2MESSAGES_TYPE_CAE_RESULT,
									   sizeof(CAECTRL_RangingResultMessage),
									   (U8*)&resultMsg);
}

//-------------------------------------------------------------------------------------------------
void CAECTRL_LegacyDeviceUpdate(DeviceID targetId, VISIBILITY_CaVisibility targetVisibility, U8 targetAveRssi)
{
	// Create a blank CAE result message
    CAECTRL_RangingResultMessage resultMsg;
	memset((void*)&resultMsg, 0x00, sizeof(CAECTRL_RangingResultMessage));
	
	// Populate the result message with the supplied parameters
	resultMsg.caeDeviceId = g_configBlock.deviceID;
	resultMsg.targetId = targetId;
	resultMsg.targetVisibility = targetVisibility;
	resultMsg.targetAveRssi = targetAveRssi;
	resultMsg.targetDist = -1;                      // Distance not available for legacy devices
	
	// Broadcast the result message over UHF2 using Protocol 2
	UHF2_MSGHANDLER_TransmitP2ShortMsg(UHF2_MSGHANDLER_BroadcastID(), 
	                                   P2MESSAGES_PORT_SCASII_CA_RESULTS,
									   P2MESSAGES_TYPE_CAE_RESULT,
									   sizeof(CAECTRL_RangingResultMessage),
									   (U8*)&resultMsg);
	
}

//-------------------------------------------------------------------------------------------------
void CAECTRL_P2MsgProcess(P2MESSAGES_ShortMessage *p2Msg)
{	
	CAECTRL_RangeResultTransferMessage *transferMsg;
	
	// Process based on type of message received
	switch(p2Msg->type) {
		case P2MESSAGES_TYPE_CAE_TRANSFER_RANGE_RESULT:
		    // Simply transmit the supplied MESSAGES_TYPE_RANGE_RESULT message out over UHF2
			transferMsg = (CAECTRL_RangeResultTransferMessage*)&(p2Msg->payload[0]);
			
	        U8 *resultMsgUhf2 = MSGHANDLER_BuildPacket(MESSAGES_DEST_MU,
					                               MESSAGES_TYPE_RANGE_RESULT,
										           MESSAGES_LEN_RANGE_RESULT,
										           (U8*)&(transferMsg->rangeResultMsg));

            UHF2_MSGHANDLER_TransmitUHF2Packet(MESSAGES_DEST_MU,
					                           MSGHANDLER_PREAMBLE_CRC_LEN + MESSAGES_LEN_RANGE_RESULT,
									           resultMsgUhf2,
									           transferMsg->rangeResultMsg.destID.IDByte5);
		
			break;
			
		default:
		    // Do not process the message
			break;
	}
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function 