/// \file M24SR04.c
/// This is the implementation file of the M24SR04 module for SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Schauenburg Systems
/// - Client        : Schauenburg
/// - Date created  : 2016/02/01
/// - Module        : M24SR04
/// - Description   : M24SR04 RFID IC module
///
/// \detail	M24SR04
///
///
/// \author Rossen Ivanov (RPI)
/// \version
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
// Include files
#include "M24SR04.h"
#include "scasdbg.h"

#include "hwdefs.h"

#include "irdahandler.h"        ///< IrDA handler for the UC3 series
#include "string.h"
#include "gpio.h"

#include "status_codes.h"
#include "timer.h"
#include "twim.h"
//-------------------------------------------------------------------------------------------------
// Defines


//-------------------------------------------------------------------------------------------------
// Enums
// none

//-------------------------------------------------------------------------------------------------
// Structures
// none

//-------------------------------------------------------------------------------------------------
// Local variables



//-------------------------------------------------------------------------------------------------
// Global variables


//-------------------------------------------------------------------------------------------------
// External variables
// none

//-------------------------------------------------------------------------------------------------
// C function prototypes

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------

#define I2C_INSTANCE			&g_mss_i2c1
#define EEPROM_I2C_CLK_FREQ		MSS_I2C_PCLK_DIV_120	//MSS_I2C_PCLK_DIV_256	MSS_I2C_PCLK_DIV_160

// Variables
eeprom_ctx_t	eeprom_ctx;
eeprom_ctx_t   *eeprom_ctx_p = 0;

void load_defaults(void);
status_code_t WR_Eeprom_String(uint16_t write_address, uint8_t *data, uint8_t buff_size, uint8_t i2c_address);
status_code_t RD_Eeprom_String(uint16_t read_address, uint8_t *data, uint8_t buff_size, uint8_t i2c_address);

/************************************************************************/
/* Name	: Setup EEPROM_init												*/
/*																		*/
/* Description : Setup EEPROM for the first time.						*/
/*																		*/
/* Inputs:	void	:-													*/
/*																		*/
/* Outputs:	void	:-													*/
/*																		*/
/*	brief writes the eeprom at startup if no values are written into	*/
/* 	it to 																*/
/************************************************************************/
uint32_t NFC_EEPROM_init(void)
{
	uint32_t status = 0;
	char node_id_temp[9];
	char device_id_temp[9];

	/// Enable the RFID IC pins
	gpio_enable_gpio_pin(RFID_RF_DIS_PIN);
	gpio_configure_pin(RFID_RF_DIS_PIN, (GPIO_DIR_OUTPUT | GPIO_INIT_HIGH));
	gpio_enable_gpio_pin(RFID_INT_PIN);
	gpio_configure_pin(RFID_INT_PIN, (GPIO_DIR_INPUT | GPIO_PULL_UP));


	// check if the device acknowledges its address.
	if(twim_probe (&AVR32_TWIM0, M24SR04_I2C_ADDRESS) != STATUS_OK)
	{
		SCASDBG_PrintAscii("M24SR04_I2C_ADDRESS - NACK\r\n");
	}
	else
	{
		SCASDBG_PrintAscii("M24SR04_I2C_ADDRESS - ACK\r\n");
	}

	//EEPROM_EN_OFF;
	//delay(WRITE_DELAY);
	//EEPROM_EN_ON;
	//delay(WRITE_DELAY);
//
	//eeprom_ctx_p = (eeprom_ctx_t*)&eeprom_ctx;
	//memset(&eeprom_ctx, 0x00, sizeof(eeprom_ctx_t));
//
	//eeprom_ctx.status.mode = EEPROM_IDLE;
//
	//eeprom_ctx.i2c_address = SECURITY_ADDRESS;											// gebruik eers security space I2C adres
	//MSS_I2C_init(I2C_INSTANCE, eeprom_ctx.i2c_address, EEPROM_I2C_CLK_FREQ);
	//NVIC_SetPriority(I2C0_IRQn, 3);
//
	//memcpy(eeprom_ctx_p->buff, (uint8_t*)&eeprom_ctx.password, 4);
	//eeprom_ctx_p->buff[4] = VALIDATE_PASSWORD;
	//memcpy((uint8_t*)&eeprom_ctx.buff[5], (uint8_t*)&eeprom_ctx.password, 4);
//
	//EEPROM_write(PASSWORD_ADDRESS, eeprom_ctx_p->buff, 9, DONTVERIFY);					// skryf eers password
	//status = EEPROM_polling_service();
//
	//status = EEPROM_read(E2_ADDR_UID, (uint8_t*)&eeprom_ctx.uid, sizeof(uint64_t));		// lees RFID
	//status = EEPROM_polling_service();
//
	//status = EEPROM_read(E2_ADDR_WRITE_LOCK_BITS, (uint8_t*)&eeprom_ctx.write_lockbits, sizeof(uint16_t));		// lees sekuriteit bits - nie nodig nie
	//status = EEPROM_polling_service();
//
	//eeprom_ctx.i2c_address = USER_ADDRESS;												// verander die I2C adres na user space
	//MSS_I2C_init(I2C_INSTANCE, eeprom_ctx.i2c_address, EEPROM_I2C_CLK_FREQ);
//
	//status = EEPROM_read_nxdosi();														// lees eers om te bepaal of dit 'n virgin is of nie
	//nxdosi_ctx.index.uid = eeprom_ctx.uid;
//
	//if((nxdosi_ctx.index.device_id == 0xFFFFFFFF) || (nxdosi_ctx.index.device_id == 0x00000000) || 			// ons neem aan dis 'n virgin
	//(nxdosi_ctx.service.software_version == 0xFFFF) || (nxdosi_ctx.service.model_type == 0xFFFF) || \
	//(nxdosi_ctx.service.manufacture_date == 0xFFFFFFFF))
	//{
		//load_defaults();
		//status = EEPROM_write_nxdosi();
//
		//memset(&state_machine, 0x00, sizeof(state_machine_t));
		//state_machine.app_sts.operating_mode = FACTORY_MODE;
		//state_machine.flash_sts.usage = 0x00000000;
		//state_machine.bootl_sts.iap_vector = DEFAULT_IAPVECTOR;
		//state_machine.bootl_sts.application_vector = DEFAULT_APPVECTOR;
		//status = EEPROM_save_state_machine();
	//}
//
	//snprintf(device_id_temp, 9, "%08d\0", nxdosi_ctx.index.device_id);			// maak seker die regte node_id ord op die eeprom gestoor
	//strncpy(node_id_temp, nxdosi_ctx.config.radio.node_id, 8);
	//if(strcmp(device_id_temp, node_id_temp))
	//{
		//strncpy(nxdosi_ctx.config.radio.node_id, device_id_temp, 8);
	//}
//
	//if(nxdosi_ctx.service.software_version != SOFTWARE_VERSION)
	//{
		//nxdosi_ctx.service.software_version = SOFTWARE_VERSION;
		//EEPROM_write(SERVICE_ADDR, (uint8_t*)&nxdosi_ctx.service, sizeof(service_t), VERIFY);
		//status = EEPROM_polling_service();
	//}
//
	//status = EEPROM_read_state_machine();
	//if(state_machine.bootl_sts.iap_vector == 0xFFFFFFFF || state_machine.bootl_sts.iap_vector == 0x00000000)
	//{
		//state_machine.bootl_sts.iap_vector = DEFAULT_IAPVECTOR;
		//EEPROM_write(BOOTL_STS_ADDR, (uint8_t*)&state_machine.bootl_sts, sizeof(bootloader_status_t), VERIFY);
		//status = EEPROM_polling_service();
	//}
//
	//if(state_machine.bootl_sts.application_vector == 0xFFFFFFFF || state_machine.bootl_sts.application_vector == 0x00000000)
	//{
		//state_machine.bootl_sts.application_vector = DEFAULT_APPVECTOR;
		//EEPROM_write(BOOTL_STS_ADDR, (uint8_t*)&state_machine.bootl_sts, sizeof(bootloader_status_t), VERIFY);
		//status = EEPROM_polling_service();
	//}
//
	//if((eeprom_ctx.status.mode == EEPROM_WRITE_UNSUCCESS) || (eeprom_ctx.status.mode == EEPROM_READ_UNSUCCESS))
	//status = 1;

	return status;
}
//
//uint32_t EEPROM_read_state_machine(void)
//{
	//uint32_t status = 0;
//
	//status = EEPROM_read(BOOTL_CTRL_ADDR, (uint8_t*)&state_machine.bootl_ctrl, sizeof(bootloader_control_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(BOOTL_STS_ADDR, (uint8_t*)&state_machine.bootl_sts, sizeof(bootloader_status_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(ERR_CTRL_ADDR, (uint8_t*)&state_machine.err_ctrl, sizeof(error_control_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(ERR_STS_ADDR, (uint8_t*)&state_machine.err_sts, sizeof(error_status_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(APP_CTRL_ADDR, (uint8_t*)&state_machine.app_ctrl, sizeof(application_control_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(APP_STS_ADDR, (uint8_t*)&state_machine.app_sts, sizeof(application_status_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(SNR_CTRL_ADDR, (uint8_t*)&state_machine.snsr_ctrl, sizeof(sensor_control_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(SNR_STS_ADDR, (uint8_t*)&state_machine.snsr_sts, sizeof(sensor_status_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(RADIO_CTRL_ADDR, (uint8_t*)&state_machine.radio_ctrl, sizeof(radio_control_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(RADIO_STS_ADDR, (uint8_t*)&state_machine.radio_sts, sizeof(radio_status_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(FLASH_STS_ADDR, (uint8_t*)&state_machine.flash_sts, sizeof(flash_status_t));
	//status = EEPROM_polling_service();
//
	//status = EEPROM_read(ON_THE_AIR_ADDR, (uint8_t*)&state_machine.on_the_air, sizeof(on_the_air_t));
	//status = EEPROM_polling_service();
//
	//return status;
//}
//
//uint32_t EEPROM_save_state_machine(void)
//{
	//uint32_t status = 0;
//
	//EEPROM_write(BOOTL_CTRL_ADDR, (uint8_t*)&state_machine.bootl_ctrl, sizeof(bootloader_control_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(BOOTL_STS_ADDR, (uint8_t*)&state_machine.bootl_sts, sizeof(bootloader_status_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(ERR_CTRL_ADDR, (uint8_t*)&state_machine.err_ctrl, sizeof(error_control_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(ERR_STS_ADDR, (uint8_t*)&state_machine.err_sts, sizeof(error_status_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(APP_CTRL_ADDR, (uint8_t*)&state_machine.app_ctrl, sizeof(application_control_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(APP_STS_ADDR, (uint8_t*)&state_machine.app_sts, sizeof(application_status_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(SNR_CTRL_ADDR, (uint8_t*)&state_machine.snsr_ctrl, sizeof(sensor_control_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(SNR_STS_ADDR, (uint8_t*)&state_machine.snsr_sts, sizeof(sensor_status_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(RADIO_CTRL_ADDR, (uint8_t*)&state_machine.radio_ctrl, sizeof(radio_control_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(RADIO_STS_ADDR, (uint8_t*)&state_machine.radio_sts, sizeof(radio_status_t), VERIFY);
	//status = EEPROM_polling_service();
	//
	//EEPROM_write(FLASH_STS_ADDR, (uint8_t*)&state_machine.flash_sts, sizeof(flash_status_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(ON_THE_AIR_ADDR, (uint8_t*)&state_machine.on_the_air, sizeof(on_the_air_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//return status;
//}
//
//uint32_t EEPROM_read_nxdosi(void)
//{
	//uint32_t status = 0;
//
	//status = EEPROM_read(INDEX_ADDR, (uint8_t*)&nxdosi_ctx.index, sizeof(index_t));
	//status = EEPROM_polling_service();
//
	//status = EEPROM_read(SERVICE_ADDR, (uint8_t*)&nxdosi_ctx.service, sizeof(service_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(CONFIG_PROFILE_ADDR, (uint8_t*)&nxdosi_ctx.config.profile, sizeof(profile_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(CONFIG_RADIO_ADDR, (uint8_t*)&nxdosi_ctx.config.radio, sizeof(digi_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(CONFIG_NETWORK_ADDR, (uint8_t*)&nxdosi_ctx.config.network, sizeof(network_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(GLOBAL_REC_REF_ADDR, (uint8_t*)&nxdosi_ctx.global_record.reference, sizeof(reference_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(GLOBAL_REC_REC_ADDR, (uint8_t*)&nxdosi_ctx.global_record.recording, sizeof(recording_t));
	//status = EEPROM_polling_service();
	//
	//status = EEPROM_read(GLOBAL_REC_ALLOC_ADDR, (uint8_t*)&nxdosi_ctx.global_record.allocation, sizeof(allocation_t));
	//status = EEPROM_polling_service();
	//
	//return status;
//}
//
//uint32_t EEPROM_write_nxdosi(void)
//{
	//uint32_t status = 0;
//
	//EEPROM_write(INDEX_ADDR, (uint8_t*)&nxdosi_ctx.index, sizeof(index_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(SERVICE_ADDR, (uint8_t*)&nxdosi_ctx.service, sizeof(service_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(CONFIG_PROFILE_ADDR, (uint8_t*)&nxdosi_ctx.config.profile, sizeof(profile_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(CONFIG_RADIO_ADDR, (uint8_t*)&nxdosi_ctx.config.radio, sizeof(digi_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(CONFIG_NETWORK_ADDR, (uint8_t*)&nxdosi_ctx.config.network, sizeof(network_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(GLOBAL_REC_REF_ADDR, (uint8_t*)&nxdosi_ctx.global_record.reference, sizeof(reference_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(GLOBAL_REC_REC_ADDR, (uint8_t*)&nxdosi_ctx.global_record.recording, sizeof(recording_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//EEPROM_write(GLOBAL_REC_ALLOC_ADDR, (uint8_t*)&nxdosi_ctx.global_record.allocation, sizeof(allocation_t), VERIFY);
	//status = EEPROM_polling_service();
//
	//return status;
//}
//
//void load_defaults(void)
//{
	//memset(&nxdosi_ctx, 0x00, sizeof(nxdosi_ctx_t));
//
	//nxdosi_ctx.index.device_id							= 00000001;
	//nxdosi_ctx.index.uid								= eeprom_ctx.uid;
//
	//nxdosi_ctx.service.calibration.mic_sensitivity		= DEFAULT_MIC_SENSITIVITY;	//0.000000325	//16.25mV/Pa
	//nxdosi_ctx.service.calibration.filter_dc_offset		= DEFAULT_OFFSET;
//
	//nxdosi_ctx.service.software_version					= SOFTWARE_VERSION;
	//nxdosi_ctx.service.reset_password					= RESET_PASSWORD;
//
	//nxdosi_ctx.config.profile.version					= DEFAULT_CONFIG;
	//nxdosi_ctx.config.profile.criteria.criterion_level	= DEFAULT_CRITERION_LEVEL;
	//nxdosi_ctx.config.profile.criteria.criterion_time	= DEFAULT_CRITERION_TIME;
	//nxdosi_ctx.config.profile.criteria.exchange_rate	= DEFAULT_EXCHANGE_RATE;
	//nxdosi_ctx.config.profile.criteria.threshold_level	= DEFAULT_TRESHOLD_LEVEL;
	//nxdosi_ctx.config.profile.criteria.data_log_rate	= DEFAULT_SAMPLE_RATE;
//
	//SET_LAEQ_T(nxdosi_ctx.config.profile.sensor_selection);
	//SET_LEX(nxdosi_ctx.config.profile.sensor_selection);
	//SET_LAS(nxdosi_ctx.config.profile.sensor_selection);
	//SET_LCS(nxdosi_ctx.config.profile.sensor_selection);
	//SET_DOSE(nxdosi_ctx.config.profile.sensor_selection);
	//SET_LCPEAK(nxdosi_ctx.config.profile.sensor_selection);
	//SET_LZPEAK(nxdosi_ctx.config.profile.sensor_selection);
	//SET_DB_TEMP(nxdosi_ctx.config.profile.sensor_selection);
	//SET_WB_TEMP(nxdosi_ctx.config.profile.sensor_selection);
	//SET_HUMIDITY(nxdosi_ctx.config.profile.sensor_selection);
	//SET_PRESSURE(nxdosi_ctx.config.profile.sensor_selection);
	//SET_BATTV(nxdosi_ctx.config.profile.sensor_selection);
	//SET_PA2H(nxdosi_ctx.config.profile.sensor_selection);
	//SET_RESERVED_1(nxdosi_ctx.config.profile.sensor_selection);
	//SET_RESERVED_2(nxdosi_ctx.config.profile.sensor_selection);
//
	//nxdosi_ctx.config.profile.trigger.method			= NETWORK_AUTO;
	//nxdosi_ctx.config.profile.trigger.minimum_recording_length = 3600;
	//nxdosi_ctx.config.profile.trigger.radio_timeout		= 300;
//
	//nxdosi_ctx.config.profile.warning.in_use			= ENABLED;
	//nxdosi_ctx.config.profile.warning.method			= CONTINUOUS;
	//nxdosi_ctx.config.profile.warning.lex_level			= 85.00;
//
	//memset(&nxdosi_ctx.config.radio, '0', sizeof(radio_ctx_t));
	//strncpy(nxdosi_ctx.config.radio.node_id, 			"00000001", 8);
	//strncpy(nxdosi_ctx.config.radio.pan_id, 			"00000001", 8);		//verander moontlik na config netwerk vir fabriek opstelling
	//strncpy(&nxdosi_ctx.config.radio.power_mode, 		"0", 1);
	//strncpy(&nxdosi_ctx.config.radio.power_level, 		"0", 1);
	//strncpy(&nxdosi_ctx.config.radio.sleep_mode, 		"1", 1);
	//strncpy(nxdosi_ctx.config.radio.packet_timeout, 	"19", 2);			//waarde in ascii hex
	//strncpy(&nxdosi_ctx.config.radio.guard_time, 		"2", 1);
	//strncpy(&nxdosi_ctx.config.radio.polling_rate, 		"5", 1);
	//strncpy(nxdosi_ctx.config.radio.sleep_time, 		"1388", 4);			//waarde in ascii hex
	//strncpy(&nxdosi_ctx.config.radio.baudrate, 			"7", 1);
//
	//nxdosi_ctx.config.network.baudrate					= 115200;
	//nxdosi_ctx.config.network.modbus_addr				= 0x01;
	//nxdosi_ctx.config.network.modbus_mtu				= DEFAULT_MODBUS_MTU;
//}
//
//uint32_t EEPROM_polling_service(void)
//{
	//uint32_t status = 0;
//
	//while((eeprom_ctx.status.mode != EEPROM_IDLE) && (!status))
	//{
		//status = EEPROM_service();
		//if(eeprom_ctx.status.mode == EEPROM_WRITE_BUSY)
		//delay(1);
	//}
//
	//return status;
//}
//
///************************************************************************/
///* Name	: Setup EEPROM													*/
///*																		*/
///* Description :service write operations								*/
///*																		*/
///* Inputs:	void														*/
///*																		*/
///* Outputs:	EEPROM_status_t												*/
///*																		*/
///************************************************************************/
//uint32_t EEPROM_service(void)
//{
	//mss_i2c_status_t status = MSS_I2C_IN_PROGRESS;
	//uint32_t ret = 0;
	//uint32_t read_error = 0;
	//static uint8_t error_wr_timeout = 3;
	//static uint8_t active_length = 0;
	//static uint16_t length_remainder = 0;
	//static uint16_t write_address = 0;
	//static uint32_t count = 0;
	//static uint32_t buffer_pos = 0;
	//static uint8_t service_counter = 0;
	//static uint32_t retry = 0;
//
	//if(!EEPROM_EN)
	//{
		//EEPROM_EN_ON;
		//service_counter = 0;
		//return ret;
	//}
	//else if(service_counter < WRITE_DELAY)
	//{
		//service_counter ++;
		//return ret;
	//}
	//else
	//{
		//service_counter = 0;
	//}
//
	//status = MSS_I2C_get_status(I2C_INSTANCE);
	//switch(status)
	//{
		//case MSS_I2C_IN_PROGRESS :
		//service_counter = 0;
		//break;
//
		//case MSS_I2C_SUCCESS :
		//switch(eeprom_ctx.status.mode)
		//{
			//case EEPROM_WRITE_BUSY :
			//{
				//if(count == 0)
				//{
					//write_address	 = eeprom_ctx.write_address;
					//length_remainder = eeprom_ctx.length;
					//active_length	 = EEPROM_PAGE_SIZE - (write_address % EEPROM_PAGE_SIZE);
					//buffer_pos		 = 0;	// address for the string pointer
					//count++;
				//}
				//else	// the first write cycle
				//{
					//// ingeval dit somehow nie meer hoog is nie
					//// if the length of the string that must be written
					//// fits in the length that are left of the 32 bytes
					//// then the length to be writen is the length
					//// of the string
//
					//if(active_length >= length_remainder)		// last write of string
					//{
						//active_length = length_remainder;		// length to write = to what is left
					//}
//
					//// Write the string to the eeprom
					//error_wr_timeout = 5;
					//while(error_wr_timeout)
					//{
						//status = WR_Eeprom_String(write_address, (uint8_t*)&eeprom_ctx.buff[buffer_pos], active_length, eeprom_ctx.i2c_address);
						//if(status == MSS_I2C_FAILED)
						//{
							//error_wr_timeout --;
							//EEPROM_EN_OFF;
							//delay(WRITE_DELAY*2);
							//EEPROM_EN_ON;
							//delay(WRITE_DELAY*2);
							//MSS_I2C_init(I2C_INSTANCE, eeprom_ctx.i2c_address, EEPROM_I2C_CLK_FREQ);
						//}
						//else
						//{
							//error_wr_timeout = 0;
						//}
					//}
//
					//switch(status)
					//{
						//case MSS_I2C_IN_PROGRESS :
						//eeprom_ctx.status.mode = EEPROM_WRITE_BUSY;
						//break;
//
						//case MSS_I2C_FAILED :
						//eeprom_ctx.status.mode = EEPROM_WRITE_UNSUCCESS;
						//count = 0;		// Setup conditions for next call to eeprom write
						//ret = 1;
						//break;
//
						//case MSS_I2C_SUCCESS :
						//eeprom_ctx.status.mode = EEPROM_WRITE_BUSY;
						//break;
//
						//default :
						//break;
					//}
//
					//if(active_length >= length_remainder)		// last write of string
					//{
						//count = 0;								// Setup conditions for next call to eeprom write
						//buffer_pos = 0;							// address for the string pointer
//
						//eeprom_ctx.status.write = CLEAR;		// Last write break.
						//eeprom_ctx.status.mode = EEPROM_WRITE_SUCCESS;
					//}
//
					//// if the length that are left to write is bigger than the length left avaliable
					//// the maximum length (32bytes) is written to the eeprom and the new length is calculated
					//// the new start address is also calculated
					//if(active_length < length_remainder)
					//{
						//write_address			= write_address + active_length;	// Set up the next start address of eeprom
						//length_remainder		= length_remainder - active_length;	// Length of sting that stil needs to be wr
						//buffer_pos				= buffer_pos + active_length;		// New start address for th string pointer
						//active_length			= EEPROM_PAGE_SIZE;					// will check it before the write cycle
						//eeprom_ctx.status.mode 	= EEPROM_WRITE_BUSY;
					//}
				//}
			//}
			//break;
//
			//case EEPROM_WRITE_SUCCESS :
			//if(eeprom_ctx.status.mode != EEPROM_READ_BUSY && eeprom_ctx.control.verify)
			//{
				//memset(&eeprom_ctx.buff_cmp, 0xFF, sizeof(eeprom_ctx.buff_cmp));
				//read_error = EEPROM_read(eeprom_ctx.write_address, eeprom_ctx.buff_cmp, eeprom_ctx.length);
				//if(read_error)
				//{
					//if(retry < 3)
					//{
						//retry ++;
						//eeprom_ctx.status.mode = EEPROM_READ_BUSY;
					//}
					//else
					//{
						//eeprom_ctx.status.mode = EEPROM_READ_UNSUCCESS;
						//retry = 0;
					//}
				//}
				//else
				//{
					//eeprom_ctx.status.mode = EEPROM_READ_BUSY;
					//retry = 0;
				//}
			//}
			//else
			//{
				//eeprom_ctx.control.write = CLEAR;
				//eeprom_ctx.status.mode = EEPROM_IDLE;
			//}
			//break;
//
			//case EEPROM_READ_BUSY :
			//if(eeprom_ctx.control.write && eeprom_ctx.control.verify)
			//{
				//eeprom_ctx.control.write = CLEAR;
				//eeprom_ctx.control.verify = CLEAR;
//
				//read_error = memcmp(&eeprom_ctx.buff_cmp, &eeprom_ctx.buff, eeprom_ctx.length);
				//if(read_error)
				//{
					//eeprom_ctx.status.mode = EEPROM_WRITE_UNSUCCESS;
				//}
				//else
				//{
					//eeprom_ctx.status.mode = EEPROM_IDLE;
				//}
			//}
			//else
			//{
				//eeprom_ctx.status.mode = EEPROM_IDLE;
			//}
			//break;
//
			//case EEPROM_IDLE :
			//if(eeprom_ctx.control.qued_write)
			//EEPROM_write(eeprom_ctx.qued_write_address, eeprom_ctx.qued_buff_p, eeprom_ctx.qued_lenght, VERIFY);
			//break;
//
			//case EEPROM_WRITE_UNSUCCESS :
			//case EEPROM_READ_UNSUCCESS :
			//ret = 1;
			//break;
//
			//default :
			//break;
		//}
		//break;
//
		//case MSS_I2C_FAILED :
		//EEPROM_EN_OFF;
		//service_counter = 0;
		//MSS_I2C_init(I2C_INSTANCE, eeprom_ctx.i2c_address, EEPROM_I2C_CLK_FREQ);
		//ret = 0;
		//break;
//
		//default :
		//break;
	//}
//
	//return ret;
//}
//
///************************************************************************************/
///* Name	: EEPROM_write																*/
///*																					*/
///* Description : Read PIN1 from EEPROM												*/
///*																					*/
///* Inputs:	uint16_t write_address, uint16_t length, uint8_t *string				*/
///*																					*/
///* Outputs:	uint32_t																*/
///*																					*/
///************************************************************************************/
//void EEPROM_write(uint16_t write_address, uint8_t *string, uint16_t length, uint8_t verify)
//{
	//if(eeprom_ctx.status.mode == EEPROM_IDLE)
	//{
		//if(eeprom_ctx.control.qued_write)
		//{
			//eeprom_ctx.control.qued_write = CLEAR;
		//}
		//eeprom_ctx.control.write = SET;
		//eeprom_ctx.write_address = write_address;
		//eeprom_ctx.length		 = length;
		//eeprom_ctx.status.mode	 = EEPROM_WRITE_BUSY;
		//if(verify)
		//eeprom_ctx.control.verify = SET;
		//
		//memcpy(eeprom_ctx_p->buff, string, eeprom_ctx.length);
	//}
	//else
	//{
		//eeprom_ctx.control.qued_write = SET;
		//eeprom_ctx.qued_write_address = write_address;
		//eeprom_ctx.qued_lenght		  = length;
		//eeprom_ctx.qued_buff_p		  = string;
		//if(verify)
		//eeprom_ctx.control.verify = SET;
	//}
//}
//
///************************************************************************/
///* Name	: EEPROM_read()													*/
///*																		*/
///* Description : Read from EEPROM										*/
///*																		*/
///* Inputs:	uint16_t read_address, uint16_t length, uint8_t *string		*/
///*																		*/
///* Outputs:	uint32_t													*/
///*																		*/
///************************************************************************/
//uint32_t EEPROM_read(uint16_t read_address, uint8_t *string, uint16_t length)
//{
	//mss_i2c_status_t status = MSS_I2C_IN_PROGRESS;
	//uint8_t error_rd_timeout = 0;
	//uint32_t ret = 0;
//
	//delay(2);
//
	//eeprom_ctx.status.mode = EEPROM_READ_BUSY;
	//eeprom_ctx.read_address = read_address;
	//eeprom_ctx.length = length;
//
	//error_rd_timeout = 5;
	//while(error_rd_timeout)
	//{
		//status = RD_Eeprom_String(eeprom_ctx.read_address, string, (uint8_t)eeprom_ctx.length, eeprom_ctx.i2c_address);
		//if(status == MSS_I2C_FAILED)
		//{
			//error_rd_timeout --;
			//EEPROM_EN_OFF;
			//delay(WRITE_DELAY*2);
			//EEPROM_EN_ON;
			//delay(WRITE_DELAY*2);
			//MSS_I2C_init(I2C_INSTANCE, eeprom_ctx.i2c_address, EEPROM_I2C_CLK_FREQ);
			//ret = 1;
		//}
		//else
		//{
			//error_rd_timeout = 0;
			//ret = 0;
		//}
	//}
//
	//return ret;
//}
//
///************************************************************************/
///* Name	: u8 WR_Eeprom_String(u16AdrSta, u8DataSta[], u8DataLn)			*/
///*																		*/
///* Description : Writes the data string (Max 32 Bytes) to the eeprom	*/
///*																		*/
///* Inputs:	u16AdrSta: The start adress to write to						*/
///*			u8DataSta: The start byte of the data array					*/
///*			u8DataLn: Number of data bytes to transmit					*/
///*																		*/
///* Outputs:	Message Status: 0 = Busy, 1 = OK, 3 = DataLn error			*/
///*																		*/
///************************************************************************/
//mss_i2c_status_t WR_Eeprom_String(uint16_t write_address, uint8_t *data, uint8_t buff_size, uint8_t i2c_address)
//{
	//mss_i2c_status_t status = MSS_I2C_IN_PROGRESS;
	//uint8_t	page[EEPROM_PAGE_SIZE + 2] = {0};
//
	//if (buff_size > EEPROM_PAGE_SIZE)
	//{
		//return MSS_I2C_IN_PROGRESS;
	//}
//
	//page[0] = write_address >> 8;
	//page[1] = write_address;
//
	//memcpy(&page[2], data, buff_size);
//
	//MSS_I2C_write(I2C_INSTANCE, i2c_address, page, (buff_size + 2), MSS_I2C_RELEASE_BUS);
	//status = MSS_I2C_wait_complete(I2C_INSTANCE);
	//
	//return status;
//}
//
///************************************************************************/
///* Name	: u8 RD_Eeprom_String(u16AdrSta, u8DataSta[], u8DataLn)			*/
///*																		*/
///* Description : Reads a string (max = 8192 bytes) from eeprom via I2C	*/
///*																		*/
///* Inputs:	u16AdrSta: The start adress to read from					*/
///*			u8DataSta: The start byte of the data array					*/
///*			u8DataLn: Number of data bytes to receive					*/
///*																		*/
///* Outputs:	Message Status: 0 = Timed Out, 1 = OK, 3 = DataLn error		*/
///*																		*/
///************************************************************************/
//mss_i2c_status_t RD_Eeprom_String(uint16_t read_address, uint8_t *data, uint8_t data_size, uint8_t i2c_address)
//{
	//mss_i2c_status_t status = MSS_I2C_IN_PROGRESS;
	//uint8_t buff[2];
//
	//buff[0] = read_address >> 8;
	//buff[1] = read_address;
//
	//MSS_I2C_write(I2C_INSTANCE, i2c_address, buff, 0x02, MSS_I2C_HOLD_BUS);
	//status = MSS_I2C_wait_complete(I2C_INSTANCE);
//
	//MSS_I2C_read(I2C_INSTANCE, i2c_address, data, data_size, MSS_I2C_RELEASE_BUS);
	//status = MSS_I2C_get_status(I2C_INSTANCE);
//
	//return status;
//}