/// \file rangetag.c
/// This is the implementation file of the rangetag module for the SCASII
/// - Project       : Schauenburg Collision Avoidance System II (SCASII)
/// - Company       : Parsec
/// - Client        : Schauenburg
/// - Date created  : 2011/09/16
/// - Module        : RANGETAG
/// - Description   : rangetag implementation
///
/// \details This module is responsible for processing all collision avoidance messages received
///          over UHF1 and UHF2. It maintains a warning table of all devices detected in the
///          mobile radio's vicinity, and uses rssi measurements and range result messages to
///          issue warnings for both SCAS I devices (no nanoLOC) and SCAS II devices (nanoLOC
///          equipped). It also performs logging of all collision avoidance events such as UHF2
///          registration (RSSI over threshold) and UHF1 registration (distance within zone 2),
///          worker exclusion and driver deregistration additions, termination and timeouts, etc.
///
/// \note    This module replaces CAALGO for the Mobile Radio in order to keep collision avoidance
///          information in a single module. The only UHF2 messages received/transmitted by the
///          Mobile Radio are:
///          - CA_MSG_BEACON: Received from all CA masters
///          - CA_MSG_CAACK: Received from Fixed Units, after which FU beacons must be ignored for
///            a determined period of time.
///          - CA_MSG_CAM: Sent to CA masters whenever average RSSI is measured over threshold
///
/// \author Michael Manthey (MM)
/// \version $Id: rangetag.c,v 1.48 2015-03-05 11:50:28+02 michaelm Exp $
//-------------------------------------------------------------------------------------------------
/// \note References: [1]
///
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Include files
#include "rangetag.h"

#include "timer.h"              ///< Timer Module
#include "ca_msg.h"             ///< UHF2 CA message handler
#include "beacon.h"             ///< Beacon Module header file
#include "uhf1.h"               ///< UHF1 Module
#include "ioctl.h"              ///< IOCTL module for the Mobile Radio, to manipulate warning LED's
#include "iophy.h"              ///< IOPHY module for the Mobile Radio, to manipulate debug LED's
#include "calog.h"              ///< CALOG module used to log UHF1 and UHF2 events
#include "config_block_uc3.h"   ///< Configuration settings
#include <string.h>             ///< Used for memcmp and memset
#include "gpio.h"
#include "rangealgo.h"
#include "uhf2_msghandler.h"
#include "cc1101.h"
#include "regionctrl.h"
#include "btuctrl.h"
#include "caectrl.h"
#include "cbit.h"
#include "gps.h"
#include "scasdbg.h"

//-------------------------------------------------------------------------------------------------
// Defines

#define RANGETAG_MAX_COUNT   (CONFIG_MAX_CA_DEVICES)  ///< Defines the maximum number of devices
                                                      ///  that can be stored in the warning table.

#define RSSI_AVE_COUNT              (10)    ///< Defines the number of RSSI measurements to use for
                                            ///  averaging.

#define RANGETAG_UHF1_BUFFER_LEN    (5)    ///< The length of a circular buffer which holds UHF1_DataMsg's

#define BLINK_PERIOD                TIMER_MS(1000)  ///< Used for pulsing the warning LED's when the
                                                    ///  Mobile Radio's CA visibility is set to FALSE.

#define RANGETAG_CAM_RETRY_COUNT    (0)

#define RANGETAG_RANGING_TIMEOUT_S	(5)

#define RELAY_OVER_THRESHOLD_HYSTERESIS_MS  (3000)

#define MIN_BEACON_CYCLES_BEFORE_TIMEOUT    (3)


//-------------------------------------------------------------------------------------------------
// Enums
// RANGETAG_RangingState
typedef enum _RANGETAG_RangingState {
	RANGETAG_RANGING_IDLE,
	RANGETAG_RANGING_BUSY,
	RANGETAG_RANGING_WAIT,		/// Entered after RANGING_BUSY, to allow a delay before next ranging cycle
} RANGETAG_RangingState;

typedef enum _RANGETAG_RelayActuators {
	RANGETAG_ALL_UNIT_TYPES         = 0,
	RANGETAG_ALL_VEHICLES           = 1,
	RANGETAG_TRACKLESS_VEHICLES     = 2,
	RANGETAG_TRACK_BOUND_VEHICLES   = 3,
	RANGETAG_PEDESTRIANS_ONLY       = 4,
	RANGETAG_RELAY_DISABLED         = 5,
	RANGETAG_WHEN_OPERATIONAL       = 6,
} RANGETAG_RelayActuators;

//-------------------------------------------------------------------------------------------------
// Structures
//-------------------------------------------------------------------------------------------------
/// \struct Warning Table containing all devices detected on UHF2 and UHF1
typedef struct _RANGETAG_DeviceInfo {
	DeviceID id;                    ///< The SCAS II device ID of this device

	VISIBILITY_CaVisibility caVisibility;   ///< Stores the quadrant configuration installed on each table
	                                        ///  entry (only if the Mobile Radio transmits BEACON's and
									        ///  receives CAM's). Also indicates whether the remote device
									        ///  is globally excluded.
											
    U16 beaconCycleTimeMs;          ///< Set to zero if no beacon is received from the device
	U8 beaconIgnore;                ///< Flag indicating whether beacons from this specific device
	                                ///  must not be responded to using a CAM message
	U16 beaconIgnoreTimeS;          ///< The duration in seconds for which beacons from this device
	                                ///  must not be responded to.
	U32 ts_beaconIgnore;            ///< Timestamp from when beacons must be ignored

	U8       nanoLOC_equipped;      ///< Boolean value representing whether this device is
	                                ///  equipped with nanoLOC. Used mainly for backwards
								    ///  compatibility and error identification.

	U32      ts_enterTable;          ///< Stores the timestamp when a device first enters
	                                ///  the warning table.

	U32      ts_UHF2;               ///< Stores the last timestamp when a beacon from this device
	                                ///  was received over UHF2.

	U8       flagAboveThrUHF2;      ///< Boolean value indicating whether remote device RSSI over threshold

    U32      ts_aboveThrUHF2;       ///< Stores the last timestamp when UHF2 RSSI was above
	                                ///  threshold. Used for logging.

	U32      ts_UHF1;               ///< Stores the last timestamp when a range result message was
	                                ///  received for this device over UHF1.

    U32      ts_zone1UHF1;          ///< Stores the last timestamp when this device's distance was
	                                ///  measured within zone 1 over UHF1. Can be used for logging.

	U8       flagZone2UHF1;         ///< Boolean value indicating whether inside zone 2 of remote device

	U32      ts_zone2UHF1;          ///< Stores the last timestamp when this device's distance was
	                                ///  measured within zone 2 over UHF1. Used for logging.

	U8       rssi[RSSI_AVE_COUNT+1];///< An array of RSSI values measured over UHF2, with the last
	                                ///  entry in the array representing the average RSSI value.

    U8      nomTxPower;             ///< The nominal transmit power of the device (UHF2).
	                                ///  Not really necessary to store, received with each CA_MSG

	U8      rssiAboveThr;           ///< Flag indicating whether the average RSSI of this device
	                                ///  is over the g_configblock threshold (TRUE or FALSE).

	S32      distance;              ///< Stores the distance measured to this device over UHF1.

	VISIBILITY_WrkrExclByte wrkrExcl;   ///< Boolean value indicating whether this device is currently
	                                    ///  excluding the Mobile Radio as a worker.

	U32      ts_wrkrExcl;           ///< Stores the last timestamp when worker exclusion was
	                                ///  confirmed.

    VISIBILITY_DrvrExclByte drvrDereg;  ///< Boolean value indicating whether this device is currently
	                                    ///  excluding the Mobile Radio as it's driver.

	U32      ts_drvrDereg;          ///< Stores the last timestamp when driver deregistration was
	                                ///  confirmed.
									
	U8       overRelayThresholdFlag;
	U32      overRelayThresholdTs;

	GPS_Data gpsData;				/// The GPS data of this device	
	
	S32	gpsDistance;				/// Stores the distance measured to this device using GPS coordinates
	
	S32 finalLatitude;				// final coordinates of the threat determined by taking ranging results into account signed decimal degrees 1e7
	S32 finalLongitude;				// final coordinates of the threat determined by taking ranging results into account signed decimal degrees 1e7
	S32 finalDistance;				// Final distance chosen between GPS and Ranging

	MESSAGES_SpatialZone deviceZone;

} ATTR_PACKED RANGETAG_DeviceInfo;

//-------------------------------------------------------------------------------------------------
/// \struct A circular buffer used for storing data messages received over UHF1. RANGETAG_Service()
///         polls this buffer to process UHF1 data messages.
typedef struct _RANGETAG_UHF1DataMsgBuffer {
	UHF1_DataMsg        buffer[RANGETAG_UHF1_BUFFER_LEN];
	U8                  head;
	U8                  tail;
} ATTR_PACKED RANGETAG_UHF1DataMsgBuffer;

//-------------------------------------------------------------------------------------------------
/// \struct Warning Table containing all devices detected on UHF2 and UHF1
typedef struct _RANGETAG_WarnTable {
    U8 warnCount;                                       ///< Number of devices in warning table
	U8 beaconIgnore;                                    ///< Flag indicating whether beacons from
	                                                    ///  FU's must be ignored (TRUE or FALSE).
	time_t ts_beaconIgnore;                             ///< Timestamp set when beaconIgnore is set
	                                                    ///  to TRUE.
	U16 ignoreBeaconTimeOut_s;                          ///< Parameter sent along with CAACK messages
	                                                    ///  from Fixed Units, specifying duration for
													    ///  which FU beacons must be ignored.
    RANGETAG_DeviceInfo entries[RANGETAG_MAX_COUNT];    ///< Array of device info, i.e. the content
	                                                    ///  of the warning table.
	RANGETAG_RangingState rangingState;					///< Stores the current ranging state
	U32 rangingStartTs;									///< Stores timestamp when ranging started
	U32 rangingEndTs;									///< Stores timestamp when ranging completed
	U8 rangingCycleSeq;
	U8 lowPowerEnabled;                                 ///< Indicates wheter NanoLOC must be shut down
	                                                    ///  when zero threats are detected on UHF2
	U8 nanoLocEnabled;                                  ///< Indicates whether nanoLOC (2V5 regulator)
	                                                    ///  is enabled or not (for power saving)
} ATTR_PACKED RANGETAG_WarnTable;

//-------------------------------------------------------------------------------------------------
// External variables
extern ConfigBlock g_configBlock;   ///< Global configuration items of unit

//-------------------------------------------------------------------------------------------------
// Global variables
static RANGETAG_WarnTable warnTable;    ///< The warning table containing all ranging and collision
                                        ///  avoidance data for other devices in the vicinity.
static U8 errorCounter = 0;

static RANGETAG_UHF1DataMsgBuffer msgBufferUHF1;

static U8 mrCaEnabledState = TRUE;      ///< Indicates whether the Mobile Radio is enabled in terms
                                        ///  of collision avoidance.

static RANGETAG_AirInterfaceRole uhf2Role;
static RANGETAG_AirInterfaceRole uhf1Role;

static U8 forceEnabled = FALSE;

//-------------------------------------------------------------------------------------------------
// C function prototypes
U8      rangetag_GetDevicePosi(DeviceID id, U8 *posi);
void    rangetag_TableUpdateUHF2(MESSAGES_UHF2MsgType msgType, DeviceID id, U8 rssi, U8 nomTxPower, MESSAGES_UHF2Msg *rxMsg);
void    rangetag_TableRemove(U8 posi);
U8      rangetag_TableFindLeastPowerful(void);
U16     rangetag_GetUHF2TimeoutForDevice(RANGETAG_DeviceInfo *deviceInfo);

void    rangetag_HandleTimeouts(void);

U8      rangetag_RssiToPower(U8 rssi, U8 nomTxPower);
double  rangetag_RssiToDbm(U8 rssi);
U8      rangetag_CalcAvgRssi(U8 *rssiArray);

U8 rangetag_UHF1DataBufferCount(void);
U8 rangetag_GetUHF1DataMsg(UHF1_DataMsg *msg);

U8 rangetag_GetRequiredRelayState(U8 tableEntry);

void rangetag_CalculateClosestDistance(RANGETAG_DeviceInfo *entry);
void rangetag_DetermineThreatZone(RANGETAG_DeviceInfo *entry);

//-------------------------------------------------------------------------------------------------
// Module Implementation - External Functions
//-------------------------------------------------------------------------------------------------
/// This function initialises the RANGETAG module
U8 RANGETAG_Init(void)
{
	/// Clear the warning table
	memset(&warnTable, 0, sizeof(RANGETAG_WarnTable));
	/// Set the rangingState to IDLE
	warnTable.rangingState = RANGETAG_RANGING_IDLE;

	/// Initialise CA_MSG (UHF2) and UHF1
	if(CA_MSG_Init() == FAILURE) {
	    return FAILURE;
	} else if(UHF1_Init() == FAILURE) {
	    return FAILURE;
	}

	///< UHF1 has been successfully initialised and is currently enabled
	warnTable.nanoLocEnabled = TRUE;
	// Low power mode enabled by default. Can be disabled using RANGETAG_DisableLowPowerMode()
	warnTable.lowPowerEnabled = TRUE;
	
	// DEBUGGING PARAMETERS
	//g_configBlock.deviceID.IDByte1 = UNIT_TYPE_MU;
	//g_configBlock.rangingPeriod_ms = 1000;
	//g_configBlock.rssiThresholdMU = 0xF8;
	//g_configBlock.proximityZone3_cm = 2500;
	//g_configBlock.proximityZone2_cm = 5000;
	//g_configBlock.proximityZone1_cm = 7500;
	//g_configBlock.relTxPower = 0x1E;		///< 0x0F --> -20dBm	|	0x1E --> -15dBm

	// Set the device class according to DeviceID
	switch(g_configBlock.deviceID.IDByte1) {
	    case UNIT_TYPE_TU:
		case UNIT_TYPE_LMU:
		case UNIT_TYPE_VMU:
		case UNIT_TYPE_RCU:
		case UNIT_TYPE_CA_EXTENDER:
		
			// All these device types perform both device discovery (UHF2) and ranging (UHF1)
			uhf2Role = RANGETAG_CA_MASTER;
			uhf1Role = RANGETAG_CA_MASTER;
			break;
		
		case UNIT_TYPE_BTU:
		
			// BTUs always perform device discovery (UHF2) but ranging (UHF1) is optional
			uhf2Role = RANGETAG_CA_MASTER;
			if(g_configBlock.btuUsesRanging == TRUE) {
				uhf1Role = RANGETAG_CA_MASTER;
			} else {
				uhf1Role = RANGETAG_CA_SLAVE;
			}
			
			break;
		
		case UNIT_TYPE_TU_UHF2:
		    // UHF2 Test Units perform device discovery (UHF2) but not ranging (UHF1)
			uhf2Role = RANGETAG_CA_MASTER;
			uhf1Role = RANGETAG_CA_SLAVE;
			break;
			
		case UNIT_TYPE_HU:
		case UNIT_TYPE_CU:
		    
			// Hazard Units perform both device discovery (UHF2) and ranging (UHF1)
	        uhf2Role = RANGETAG_CA_MASTER;
			uhf1Role = RANGETAG_CA_MASTER;
			
			break;
			
		case UNIT_TYPE_MU:
		case UNIT_TYPE_UHF2_REPEATER:
		default:
		    // All other devices are considered as CA slaves (UHF2 and UHF1)
		    uhf2Role = RANGETAG_CA_SLAVE;
			uhf1Role = RANGETAG_CA_SLAVE;
			break;
	}
	
	if(g_configBlock.deviceID.IDByte1 != UNIT_TYPE_MU) {
		// DIG_OUT1 pin must be used to toggle a relay on all unit types except Miner Units
		IOCTL_SetDigOut1Function(IOCTL_FUNC_HU_RELAY);
	}
	
	// Limit UHF2 transmit power on devices that are CA Masters on UHF2
	// This can be used to reduce UHF2 traffic in lamprooms or to create a smaller zone around
	// the unit for device discovery purposes
	if(uhf2Role == RANGETAG_CA_MASTER) {
		// Check that a valid value is stored in the ConfigBlock
        if((g_configBlock.relTxPower >= CC1101_PATABLE_MIN) && (g_configBlock.relTxPower <= CC1101_PATABLE_MAX)) {
		    ///< Put CC1101 into IDLE state
		    CC1101_Strobe(CC1101_SIDLE);
		    TIMER_DELAY_US(100);
		    ///< Write the new TX power value to CC1101
		    CC1101_WriteReg(CC1101_PATABLE, g_configBlock.relTxPower);
		    ///< Perform calibration
		    CC1101_Strobe(CC1101_SCAL); // NOTE: if freq. calibration is not performed the radio will not function
	    }
	}
	
	// Enable or disable sending of beacons based on the device's uhf2 role
	switch(uhf2Role) {
		case RANGETAG_CA_MASTER:
		    BEACON_Enable();
			break;
		case RANGETAG_CA_SLAVE:
		    BEACON_Disable();
			break;
		default:
		    BEACON_Disable();
			break;
	}
    
	// Initialise RANGEALGO only if the device is a CA Master on UHF1
	if(uhf1Role == RANGETAG_CA_MASTER) {
	    RANGEALGO_Init();
	}
	
	// Initialise the BTUCTRL module on Brake Test Units (BTUs)
	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
		BTUCTRL_Init();
	}

	return SUCCESS;
}

//-------------------------------------------------------------------------------------------------
/// This function services the RANGETAG module
void RANGETAG_Service(void)
{
	// Temporary variables for storing information received over UHF2
	DeviceID rxDeviceID;
    MESSAGES_UHF2MsgType rxMsgID;
    U8 rxRssi;
    U8 nomTxPower;
	MESSAGES_UHF2Msg rxMsg;
	U8 i;
	static U32 now;                                         ///< Used for timing
	U8  tempNanoLocFlag = FALSE;            ///< Used to determine whether nanoLOC must be enabled or disabled
	U8  tempHuRelayFlag = FALSE;            ///< Used to determine whether HU Relay must be activated or not
	
	
	// Manage servicing of the BEACON module based on UHF2 role
	if(uhf2Role == RANGETAG_CA_MASTER) {
		//#ifdef HAULTRUCK
		BEACON_Service();
		//#endif
	}
	
	// Manage servicing of UHF1 related modules based on UHF1 role
	if(uhf1Role == RANGETAG_CA_MASTER) {
		RANGEALGO_Service();        // Includes UHF1_Service()
	} else {
		UHF1_Service();
	}
	
	// Manage the Ranging state machine on UHF1 CA Masters
	if(uhf1Role == RANGETAG_CA_MASTER) {
		if (warnTable.rangingState == RANGETAG_RANGING_IDLE) {
			RANGETAG_StartRanging();
			warnTable.rangingStartTs = TIMER_Now32();
		} else if(warnTable.rangingState == RANGETAG_RANGING_WAIT) {
			///< Check whether the post-ranging waiting period has elapsed
			if (TIMER_Elapsed32(warnTable.rangingEndTs) > TIMER_MS(g_configBlock.rangingPeriod_ms)) {
				warnTable.rangingState = RANGETAG_RANGING_IDLE;
			}
		} else if(warnTable.rangingState == RANGETAG_RANGING_BUSY) {
			///< Check whether the total ranging time-out period has elapsed
			if (TIMER_Elapsed32(warnTable.rangingStartTs) > TIMER_S(RANGETAG_RANGING_TIMEOUT_S)) {
				warnTable.rangingState = RANGETAG_RANGING_IDLE;
			}
		}
	}
	
	// Service the REGIONCTRL module on Region Control Units (RCUs)
	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_RCU) {
		// Service the sending of region control messages
		REGIONCTRL_Service();
	}
	
	// Service the BTUCTRL module on Brake Test Units (BTUs)
	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
		// Service the sending of region control messages
		BTUCTRL_Service();
	}
	
	// Now handle reception of CA messages on UHF2 by polling the CA_MSG module
	CA_MSG_Service();

	///< Check the CA_MSG message queue for new CA messages
	rxMsgID = CA_MSG_GetMsg(&rxDeviceID, &rxRssi, &nomTxPower, &rxMsg);
	while(rxMsgID != CA_MSG_NO_MESSAGE) {
		if(mrCaEnabledState == TRUE) {
		    /// Only interested in beacon messages and CAACK messages
		    switch(rxMsgID) {
		        case CA_MSG_BEACON:
				case CA_MSG_LMU_BEACON:
				    /// Toggle the HU LED when a TU beacon is received
					if((rxDeviceID.IDByte1 == UNIT_TYPE_TU) || (rxDeviceID.IDByte1 == UNIT_TYPE_TU_UHF2)) {
					    IOCTL_ToggleHazardLed();
					}
					////SCASDBG_PrintAscii("Beacon Received\r\n");
				    rangetag_TableUpdateUHF2(rxMsgID, rxDeviceID, rxRssi, nomTxPower, &rxMsg);
				    break;
				case CA_MSG_CAM:
				case CA_MSG_LMU_CAM:
					////SCASDBG_PrintAscii("CAM Received\r\n");
				    rangetag_TableUpdateUHF2(rxMsgID, rxDeviceID, rxRssi, nomTxPower, &rxMsg);
				    break;
			    case CA_MSG_CAACK:
			        /// CAACK was received from a Fixed Unit
			        /// Temporarily ignore beacons from Fixed Units
					rangetag_TableUpdateUHF2(rxMsgID, rxDeviceID, rxRssi, nomTxPower, &rxMsg);
				    //warnTable.beaconIgnore = TRUE;
				    //warnTable.ts_beaconIgnore = TIMER_LongTimeNow();
				    break;
		        default:
			        break;
		    }
		}
	    rxMsgID = CA_MSG_GetMsg(&rxDeviceID, &rxRssi, &nomTxPower, &rxMsg);
	}

    ///< Check the UHF1 message buffer for new range result messages
	while(rangetag_UHF1DataBufferCount() != 0) {
		UHF1_DataMsg rxMsg;
		rangetag_GetUHF1DataMsg(&rxMsg);

		/// Only interested in range result messages
        switch(rxMsg.msgType) {
            case UHF1_TYPE_RANGE_RESULT:
			    // This message should no longer be received on UHF1 and is no longer supported
				break;
		    default:
				break;
		}
	}

	///< Service warning table timeouts (UHF2, UHF1, wrkrExcl, drvrDereg, beaconIgnore)
	rangetag_HandleTimeouts();

	///< Update warning LED's and exclusion status
	IOCTL_WarnBits warnBits;
	memset(&warnBits, 0, sizeof(IOCTL_WarnBits));
	
	U8 excluded = FALSE;            // Indicates whether this device is currently being excluded
	                                // by another device

	U8 globallyExcluded = FALSE;    // Indicates whether this device is currently being excluded
	                                // by another device which requires it to be excluded from
									// ALL other SCAS II devices

	// Scan through entire warning table and update warning bits accordingly
    for(i=0; i < warnTable.warnCount; i++) {
		RANGETAG_DeviceInfo *entry = &(warnTable.entries[i]);
		
		// Update the temporary relay state variable based on the properties of this warnTable entry
		tempHuRelayFlag |= rangetag_GetRequiredRelayState(i);		

		///< Check whether NanoLOC should be enabled/disabled based on table entries
		if((entry->id.IDByte2 & 0x01) == 0x01) {
			switch(entry->id.IDByte1) {
			    case UNIT_TYPE_TU:
			        ///< Check whether ranging has been completed with this device
				    if(entry->flagZone2UHF1 == FALSE)
				    {
					    // NanoLOC must be enabled
					    tempNanoLocFlag = TRUE;
				    }
				    break;
			    case UNIT_TYPE_TU_UHF2:
			        ///< Do not enable nanoLOC due to this device type
				    break;
			    default:
			        ///< Enable nanoLOC for all other device types
			        tempNanoLocFlag = TRUE;
				    break;
		    }
		}

        if((entry->wrkrExcl.bits.excluded == FALSE) && (entry->drvrDereg.bits.excluded == FALSE)) {
			if((entry->nanoLOC_equipped && (entry->distance < g_configBlock.proximityZone2_cm) && (entry->distance > 0)) ||
				((entry->nanoLOC_equipped == FALSE) && entry->rssiAboveThr) ) {
				switch(warnTable.entries[i].id.IDByte1) {
                    case UNIT_TYPE_LMU:
                    case UNIT_TYPE_LSU:
                        if(warnBits.bit.SingleLoco == 0) {
                            warnBits.bit.SingleLoco = 1;
                        } else {
                            warnBits.bit.MultipleLoco = 1;
                        }
                    break;
                    case UNIT_TYPE_VMU:
                        if(warnBits.bit.SingleVehicle == 0) {
                            warnBits.bit.SingleVehicle = 1;
                        } else {
                            warnBits.bit.MultipleVehicle = 1;
                        }
                    break;
                    case UNIT_TYPE_HU:
                    case UNIT_TYPE_CU:
                        warnBits.bit.Hazard = 1;
                    break;
                    case UNIT_TYPE_MU:
                        // Do nothing, MU's should not be displayed
                    break;
                    case UNIT_TYPE_TU:
                        warnBits.bit.TestUnit = 1;
                    break;
                    case UNIT_TYPE_FU:
                        warnBits.bit.FixedUnit = 1;
                    break;
                    case UNIT_TYPE_IO_CARRIER:
					case UNIT_TYPE_UHF2_REPEATER:
					case UNIT_TYPE_RCU:
                    default:
                        // Do nothing
                    break;
                }
			}
			if(warnTable.entries[i].id.IDByte1 == UNIT_TYPE_TU_UHF2) {
				///< Always indicate a UHF2 Test Unit on warning LED's
				warnBits.bit.TestUnit = 1;
			}

        } else {
            if(entry->wrkrExcl.bits.excluded == TRUE) {
                warnBits.bit.wrkrExcl = 1;
				excluded = TRUE;
            } else if(entry->drvrDereg.bits.excluded == TRUE) {
                warnBits.bit.drvrDereg = 1;
				excluded = TRUE;
            }
			
			// Update the global exclusion status of this device
			if((entry->wrkrExcl.bits.globalDereg == TRUE) || (entry->drvrDereg.bits.globalDereg == TRUE)) {
				globallyExcluded = TRUE;
			}
        }
    }
	
	// Update the global exclusion status of this device
	CA_MSG_SetExclusionStatus(excluded, globallyExcluded);
	
	if(forceEnabled == TRUE) {
		tempNanoLocFlag = TRUE;
	}

	///< Update NanoLOC power state if power management is enabled
	if((warnTable.lowPowerEnabled == TRUE) && (warnTable.nanoLocEnabled != tempNanoLocFlag)) {
		///< NanoLOC status must be updated
		if(tempNanoLocFlag == TRUE) {
			UHF1_Restart();
		} else {
			UHF1_Shutdown();
		}
		warnTable.nanoLocEnabled = tempNanoLocFlag;
	}
	
	// Check whether relay state must be set according to board initialisation status
	if(g_configBlock.huRelayActuators == RANGETAG_WHEN_OPERATIONAL) {
		if(CBIT_GetBoardInitStatus() == SUCCESS) {
			tempHuRelayFlag = TRUE;
		} else {
			tempHuRelayFlag = FALSE;
		}
	}
	
	// Update relay state if this device is not configured as a Miner Unit
	if(g_configBlock.deviceID.IDByte1 != UNIT_TYPE_MU) {
	    IOCTL_SetDigOut1Active(tempHuRelayFlag);
	}
	
	IOCTL_UpdateWarning(warnBits);

	/// If this Mobile Radio has its mrCaEnabledState set to FALSE, pulse the warning LED's
	/// intermittently
	if((mrCaEnabledState == FALSE) && (TIMER_Elapsed32(now) > BLINK_PERIOD)) {
		now = TIMER_Now32();
		IOCTL_PulseWarnLedsBlocking();
	}
	
	//static int counter = 0;
	//
	//if (counter == 0) {
		//// Print out the warning table
		////SCASDBG_PrintAscii("Warning Table: \r\n");
		//for (int i = 0; i < warnTable.warnCount; i++) {
			//////SCASDBG_PrintAscii("Id: ");
			//////SCASDBG_PrintU32ToAscii(warnTable.entries[i].id.IDByte5, FALSE);
			////SCASDBG_PrintAscii("\r\n");
			////SCASDBG_PrintAscii("Distance: ");
			////SCASDBG_PrintU32ToAscii(warnTable.entries[i].distance, FALSE);
			////SCASDBG_PrintAscii("\r\n");
			////SCASDBG_PrintAscii("Zone: ");
			////SCASDBG_PrintU32ToAscii(warnTable.entries[i].deviceZone, FALSE);
			////SCASDBG_PrintAscii("\r\n");
		//}
		////SCASDBG_PrintAscii("\r\n");
		//counter = 10000;
	//}
	//else {counter--;}	
}

//-------------------------------------------------------------------------------------------------
U8 RANGETAG_PutUHF1DataMsg(UHF1_DataMsg *msg)
{
	if(rangetag_UHF1DataBufferCount() == RANGETAG_UHF1_BUFFER_LEN) {
		return FAILURE;                                         /// The buffer is already full...
	} else {

		msgBufferUHF1.buffer[msgBufferUHF1.head] = *msg;                  /// Add message to buffer

		msgBufferUHF1.head = (msgBufferUHF1.head + 1) % RANGETAG_UHF1_BUFFER_LEN;  /// increment the head
		return SUCCESS;
	}
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_SetCaEnabledState(U8 newVal)
{
	mrCaEnabledState = newVal;
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_TableUpdateUHF1(MESSAGES_RangeResult *rangeResult)
{
	U8 rxDevicePosi;

	///< Turn DLED2 on while updating the table due to ranging results
	IOPHY_DLED2(IO_ON);

	/// Check whether the device is already in the warning table
	if(rangetag_GetDevicePosi(rangeResult->srcID, &rxDevicePosi) == SUCCESS) {

		/// If found, update the device entry in the warning table
		RANGETAG_DeviceInfo *entry = &(warnTable.entries[rxDevicePosi]);
		
		///< Update the last seen timestamp on UHF2 since this result was received over UHF2.
		///  We do not know whether it was above RSSI threshold or not though.
		entry->ts_UHF2 = TIMER_LongTimeNow();

		///< ranging results from Test Units must be assumed to be in zone 2 to ensure LED switches
		///  on and CAM responses are not sent to TU until TU times out on UHF2.
		if(rangeResult->srcID.IDByte1 == UNIT_TYPE_TU) {
			if(rangeResult->distance < g_configBlock.proximityZone2_cm) {
				entry->distance = rangeResult->distance;
			} else {
			    entry->distance = g_configBlock.proximityZone2_cm - 1;  ///< Force inside zone2
			}
		} else {
			entry->distance = rangeResult->distance;
		}


		entry->ts_UHF1 = TIMER_LongTimeNow();

		/// Update zone information used for logging
		if(entry->distance < g_configBlock.proximityZone2_cm) {
			/// Log this event if applicable
			//if(TIMER_LongTimeElapsed(entry->ts_zone2UHF1) > g_configBlock.slaveCATimeout_s) {
			if(entry->flagZone2UHF1 == FALSE) {
				CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_2,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_ADD,
				    .finalDist = entry->distance,
					.globalExclusion = FALSE        // Not applicable for CA_ADD event
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
			entry->ts_zone2UHF1 = TIMER_LongTimeNow();
			entry->flagZone2UHF1 = TRUE;
		}

		if(entry->distance < g_configBlock.proximityZone1_cm) {
			entry->ts_zone1UHF1 = TIMER_LongTimeNow();
		}

		/// Update worker exclusion status
		if(rangeResult->wrkrExcl.bits.excluded == TRUE) {
			/// Log worker exclusion if applicable
			if(entry->wrkrExcl.bits.excluded == FALSE) {
				CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_3,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_WORKER_EXCL_ADD,
				    .finalDist = entry->distance,
					.globalExclusion = rangeResult->wrkrExcl.bits.globalDereg
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
			entry->wrkrExcl = rangeResult->wrkrExcl;
			entry->ts_wrkrExcl = TIMER_LongTimeNow();
		} else {
			if(entry->wrkrExcl.bits.excluded == TRUE) {
			    /// Log worker exclusion termination
				CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_3,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_WORKER_EXCL_TERMINATE,
				    .finalDist = entry->distance,
					.globalExclusion = entry->wrkrExcl.bits.globalDereg
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
			entry->wrkrExcl.byte = FALSE;    // Reset the wrkrExcl byte
			entry->ts_wrkrExcl = 0;
		}

		/// Update driver deregistration status
		if(rangeResult->drvrDereg.bits.excluded == TRUE) {
			if(entry->drvrDereg.bits.excluded == FALSE) {
				CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_3,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_DRIVER_DEREG_ADD,
				    .finalDist = entry->distance,
					.globalExclusion = rangeResult->drvrDereg.bits.globalDereg
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
			entry->drvrDereg = rangeResult->drvrDereg;
			entry->ts_drvrDereg = TIMER_LongTimeNow();
		} else {
			if(entry->drvrDereg.bits.excluded == TRUE) {
				CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_3,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_DRIVER_DEREG_TERMINATE,
				    .finalDist = entry->distance,
					.globalExclusion = entry->drvrDereg.bits.globalDereg
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
			entry->drvrDereg.byte = FALSE;   // Reset the drvrDereg byte
			entry->ts_drvrDereg = 0;
		}
	} else {
		/// This case should never occur.
		/// Increment an error counter here to detect any exceptions.
		errorCounter++;
	}

	IOPHY_DLED2(IO_OFF);
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_StartRanging(void)
{
	U8 counter = 0;
	MESSAGES_TagEntry tagEntry;              ///< Tag table entry msg
	U8 dontCare = 0;

	///< Flush the RangeAlgo table
	RANGEALGO_Flush();

	warnTable.rangingCycleSeq++;

	///< Transmit the relevant warnTable entries to RangeAlgo module
	for(U8 i = 0; i < warnTable.warnCount; i++) {
        ///< Check whether the entry should be ranged with
        if((warnTable.entries[i].nanoLOC_equipped == TRUE) && (warnTable.entries[i].flagAboveThrUHF2)) {
			// Process data into temporary struct
            memcpy(&(tagEntry.numEntry), &counter, sizeof(counter));
            memcpy(&(tagEntry.tagID), (U8*)&(warnTable.entries[i].id), sizeof(DeviceID));
            memcpy(&(tagEntry.resolution), &dontCare, sizeof(U8));
			tagEntry.caVisibility = warnTable.entries[i].caVisibility;
	        RANGEALGO_TableUpdate(&tagEntry);

			counter++;
        } else if(warnTable.entries[i].flagAboveThrUHF2 == TRUE) {
			if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_CA_EXTENDER) {
				// For each device not equipped with NanoLOC but over UHF2 threshold, notify CAECTRL module
			    CAECTRL_LegacyDeviceUpdate(warnTable.entries[i].id, warnTable.entries[i].caVisibility, warnTable.entries[i].rssi[RSSI_AVE_COUNT]);
			}
        }
	}

	if(counter > 0) {
		warnTable.rangingState = RANGETAG_RANGING_BUSY;
		RANGEALGO_PerformRanging(warnTable.rangingCycleSeq);
	} else {
		///< Skip the RANGING_BUSY state if no ranging will take place
		warnTable.rangingState = RANGETAG_RANGING_WAIT;
	}
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_ConcludeRanging(void)
{
	///< This function currently does nothing but start the next cycle of ranging
	warnTable.rangingState = RANGETAG_RANGING_WAIT;
	warnTable.rangingEndTs = TIMER_Now32();
	gpio_toggle_pin(LU_LED_PIN);
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_TableUpdateRangeAlgo(U8 *rangingData)
{
	U8 foundPosi;
	///< Cast received ranging result into a rangingEntry
    MESSAGES_RangingEntry rangingEntry;
    memcpy(&rangingEntry, rangingData, sizeof(rangingEntry));


	///< Update the corresponding entry in the warning table. Reject the result if the entry is not
	///  found in the warning table
	if(rangetag_GetDevicePosi(rangingEntry.tagID, &foundPosi) == SUCCESS) {
		
		// If RANGEALGO calculated a valid ranging result for this device, update the entry's distance
		if(rangingEntry.aveDistance > 0) {
			RANGETAG_DeviceInfo *entry = &(warnTable.entries[foundPosi]);
			
			entry->distance = rangingEntry.aveDistance;
			
			////SCASDBG_PrintAscii("\r\nNew Ranging Unit Distance: ");
			////SCASDBG_PrintU32ToAscii(entry->distance, FALSE);
			////SCASDBG_PrintAscii("\r\n");
			rangetag_CalculateClosestDistance(entry);
			rangetag_DetermineThreatZone(entry);
			// TODO must calculate new coordinates now if the closest distance has changed
			
			// Update last seen flags
			entry->ts_UHF1 = TIMER_LongTimeNow();

		    /// Update zone information used for logging
		    if(entry->distance < g_configBlock.proximityZone2_cm) {
			    /// Log this event if applicable
			    //if(TIMER_LongTimeElapsed(entry->ts_zone2UHF1) > g_configBlock.slaveCATimeout_s) {
			    if(entry->flagZone2UHF1 == FALSE) {
				    CALOG_UHF1Log newLog = {
	                    .deviceID = entry->id,
	                    .zone = CA_ZONE_2,
				        .quad = QUADRANT_DEFAULT,
	                    .type = CA_ADD,
				        .finalDist = entry->distance,
						.globalExclusion = FALSE        // Not applicable to CA_ADD event
                    };
                    CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			    }
			    entry->ts_zone2UHF1 = TIMER_LongTimeNow();
			    entry->flagZone2UHF1 = TRUE;
		    }

		    if(entry->distance < g_configBlock.proximityZone1_cm) {
			    entry->ts_zone1UHF1 = TIMER_LongTimeNow();
		    }
			
			// Notify BTUCTRL of the entry's updated info
		    if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
			    BTUCTRL_UpdateProximityInfo(entry->id, entry->rssi[RSSI_AVE_COUNT], entry->distance);
		    }
		}
		
		///< On CAEs, broadcast the ranging result to all nearby CA Master devices (e.g. vehicles)
		if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_CA_EXTENDER) {
			// Only broadcast valid ranging results for UHF1 equipped devices
			if(warnTable.entries[foundPosi].distance > 0) {
				CAECTRL_RangingResultUpdate(warnTable.entries[foundPosi].id, 
				                            warnTable.entries[foundPosi].caVisibility, 
											warnTable.entries[foundPosi].rssi[RSSI_AVE_COUNT], 
											warnTable.entries[foundPosi].distance);
			}
		///< Send a ranging result message over UHF2 to Miner Units within zone2	
		} else if((warnTable.entries[foundPosi].id.IDByte1 == UNIT_TYPE_MU) &&
	       (warnTable.entries[foundPosi].distance > 0) &&
	       (warnTable.entries[foundPosi].distance < g_configBlock.proximityZone2_cm)) {

	        ///< Use UHF2 for transmitting results to Miner Units
            MESSAGES_RangeResult resultMsg;
            resultMsg.srcID = g_configBlock.deviceID;
            resultMsg.destID = warnTable.entries[foundPosi].id;
            resultMsg.distance = warnTable.entries[foundPosi].distance;
            resultMsg.wrkrExcl.byte = FALSE;
            resultMsg.drvrDereg.byte = FALSE;
			
			// Check whether the range result message should indicate global exclusion
			if(REGIONCTRL_ExcludeMinerUnitGlobally(&(warnTable.entries[foundPosi].id), warnTable.entries[foundPosi].distance) == TRUE) {
				resultMsg.wrkrExcl.bits.excluded = TRUE;
			    resultMsg.wrkrExcl.bits.globalDereg = TRUE;
			}
			
            U8 *resultMsgUhf2;
            resultMsgUhf2 = MSGHANDLER_BuildPacket(MESSAGES_DEST_MU,
					                                MESSAGES_TYPE_RANGE_RESULT,
										            MESSAGES_LEN_RANGE_RESULT,
										            (U8*)&resultMsg);
			UHF2_MSGHANDLER_TransmitUHF2Packet(MESSAGES_DEST_MU,
					                        MSGHANDLER_PREAMBLE_CRC_LEN + MESSAGES_LEN_RANGE_RESULT,
									        resultMsgUhf2,
									        warnTable.entries[foundPosi].id.IDByte5);
	    }		
	}
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_DisableLowPowerMode(void)
{
	warnTable.lowPowerEnabled = FALSE;
	// Ensure that NanoLOC is enabled
	UHF1_Restart();
	warnTable.nanoLocEnabled = TRUE;
}

//-------------------------------------------------------------------------------------------------
U8 RANGETAG_GetDeviceDist(DeviceID id, S32 *dist)
{
	U8 foundPosi;
	U8 retVal = FALSE;
    
	// Check whether the device is in the warnTable
	if(rangetag_GetDevicePosi(id, &foundPosi) == SUCCESS) {
		retVal = TRUE;
		*dist = warnTable.entries[foundPosi].distance;
	}
	
	return retVal;
}

//-------------------------------------------------------------------------------------------------
U8 RANGETAG_GetMaxSignalPercentageByType(UnitType type)
{
	U8 tempVal = 0;
	
	// Get the maximum average RSSI of the device of specified type
	for(U8 i=0; i < warnTable.warnCount; i++) {
		if(warnTable.entries[i].id.IDByte1 == type) {
			if(warnTable.entries[i].rssi[RSSI_AVE_COUNT] > tempVal) {
				tempVal = warnTable.entries[i].rssi[RSSI_AVE_COUNT];
			}
		}
	}
	
	// Translate the normalised signal strength to a percentage (simple conversion 0% represents 
	// -138dBm, 100% represents -10.5dBm). This is the span of signal strengths detectable by CC1101.
	double percent = tempVal / 2.55;    // (Tempval / 255) * 100
	
	return (U8)percent;
}

//-------------------------------------------------------------------------------------------------
void RANGETAG_ForceUHF1Enabled(U8 state) 
{
	forceEnabled = state;
}

//-------------------------------------------------------------------------------------------------
U8 RANGETAG_TableFindMostPowerfulByType(UnitType type, DeviceID *devId)
{
	/// The most powerful table entry is considered to be the entry with the highest average RSSI.
	/// Note: There may be special cases when this will be an incorrect assumption. Directly after
	///       a device is inserted in the table, its average RSSI will be very small (one RSSI
	///       measurement divided by RSSI_AVE_COUNT). If two new devices are contending to enter
	///       the warning table while it is full, they may each only be in the table for one beacon
	///       period at a time.

	/// Scan through table to find the most powerful device entry
	U8 found = FALSE;
	U8 maxIdx = 0;
	U8 i;
	U8 maxAvgRssi;
	for(i=0; i < warnTable.warnCount; i++) {
		if(warnTable.entries[i].id.IDByte1 == type) {
			if(found == FALSE) {
				// Set the first entry found
				maxAvgRssi = warnTable.entries[i].rssi[RSSI_AVE_COUNT];
				maxIdx = i;
				found = TRUE;
			} else {
		      if(warnTable.entries[i].rssi[RSSI_AVE_COUNT] > maxAvgRssi) {
			     maxAvgRssi = warnTable.entries[i].rssi[RSSI_AVE_COUNT];
			     maxIdx = i;
			  }
		    }
				
		}
	}
	
	if(found == TRUE) {
		devId->IDByte1 = warnTable.entries[maxIdx].id.IDByte1;
		devId->IDByte2 = warnTable.entries[maxIdx].id.IDByte2;
		devId->IDByte3 = warnTable.entries[maxIdx].id.IDByte3;
		devId->IDByte4 = warnTable.entries[maxIdx].id.IDByte4;
		devId->IDByte5 = warnTable.entries[maxIdx].id.IDByte5;
	}
	
	return found;
}

//-------------------------------------------------------------------------------------------------
// Module Implementation - Internal Functions
//-------------------------------------------------------------------------------------------------
/// \brief This function searches for a DeviceID in the warning table. If the device is found, the
///        found position is assigned to the 'U8 *posi' function parameter.
/// \param id [in] the DeviceID to search for in the warning table.
/// \param posi [out] the memory address of a variable in which to store the found position.
/// \return SUCCESS if DeviceID was found, FAILURE otherwise.
U8 rangetag_GetDevicePosi(DeviceID id, U8 *posi)
{
	U8 i;

	for(i=0; i < warnTable.warnCount; i++) {
		if(memcmp(&(id.IDByte1), &(warnTable.entries[i].id.IDByte1), sizeof(DeviceID)) == 0) {
			*posi = i;
			return SUCCESS;
		}
	}

	return FAILURE;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function updates an entry in the warning table based on UHF2 beacons. If the device
///        is not yet in the table, a new entry is created.
/// \param msgType The type of message which is triggering the table update
/// \param id [in] the DeviceID to search for and update/insert in the warning table.
/// \param rssi [in] the RSSI measurement received over UHF2.
/// \param nomTxPower [in] the nominal TX power of the device from which a beacon was received.
/// \param rxMsg A pointer to the full UHF2 message received
void rangetag_TableUpdateUHF2(MESSAGES_UHF2MsgType msgType, DeviceID id, U8 rssi, U8 nomTxPower, MESSAGES_UHF2Msg *rxMsg)
{
	U8 rxDevicePosi;
	U8 counter;         /// Used in for loops
	U8 aboveThr = FALSE;
	ca_msg_Beacon *beaconMsg;
	ca_msg_CAM *camMsg;
	ca_msg_CAACK *caackMsg;
	RANGETAG_DeviceInfo *entry;
	U8 limitedTable = FALSE;        ///< Used to decide whether Miner Units should only be added to 
	                                ///  warnTable if their initial RSSI is above threshold
    U8 ignoreEntry = FALSE;         ///< Boolean flag indicating wether a newly detected potential 
	                                ///  warnTable entry must be ignored

	/// Check whether the device is already in the warning table
	if(rangetag_GetDevicePosi(id, &rxDevicePosi) == SUCCESS) {
		/// If found, update the device entry in the warning table
		entry = &(warnTable.entries[rxDevicePosi]);
		entry->nomTxPower = nomTxPower;

		/// Move rssi array entries up one position
        for(counter=RSSI_AVE_COUNT-1; counter>0; counter--) {
            entry->rssi[counter] = entry->rssi[counter-1];
        }
		/// Place normalised RSSI in array
        entry->rssi[0] = rangetag_RssiToPower(rssi, nomTxPower);

		/// Calculate and store the average RSSI
        entry->rssi[RSSI_AVE_COUNT] = rangetag_CalcAvgRssi(&(entry->rssi[0]));

		if((msgType == CA_MSG_CAM) || (msgType == CA_MSG_LMU_CAM)) {
			camMsg = (ca_msg_CAM*)rxMsg;
			entry->caVisibility = camMsg->field.caVisibility;
			entry->gpsData.latitude = camMsg->field.gpsData.latitude;
			entry->gpsData.longitude = camMsg->field.gpsData.longitude;
			entry->gpsData.altitude = camMsg->field.gpsData.altitude;
			entry->gpsData.speed = camMsg->field.gpsData.speed;
			if (entry->gpsData.latitude != 0 && entry->gpsData.longitude !=0) {
				entry->gpsDistance = 0;
				entry->gpsDistance = GPS_GetDistance(entry->gpsData.latitude, entry->gpsData.longitude);
				////SCASDBG_PrintAscii("\r\nNew GPS Distance: ");
				////SCASDBG_PrintU32ToAscii(entry->gpsDistance, FALSE);
				////SCASDBG_PrintAscii("\r\n");
				rangetag_CalculateClosestDistance(entry);
				rangetag_DetermineThreatZone(entry);
			}	
			//SCASDBG_PrintAscii("CAM Updated Entry: \r\n");
			//SCASDBG_PrintAscii("Id: ");
			//SCASDBG_PrintU8ToAscii(entry->id.IDByte5, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Zone: ");
			//SCASDBG_PrintU8ToAscii(entry->deviceZone, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Latitude: ");
			//SCASDBG_PrintU32ToAscii((U32)entry->gpsData.latitude, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Longitude: ");
			//SCASDBG_PrintU32ToAscii((U32)entry->gpsData.longitude, FALSE);
			//SCASDBG_PrintAscii("\r\n\r\n");
		}
		else if (msgType == CA_MSG_BEACON) {
			beaconMsg = (ca_msg_Beacon*)rxMsg;
			entry->gpsData.latitude = beaconMsg->field.gpsData.latitude;
			entry->gpsData.longitude = beaconMsg->field.gpsData.longitude;
			entry->gpsData.altitude = beaconMsg->field.gpsData.altitude;
			entry->gpsData.speed = beaconMsg->field.gpsData.speed;
			if (entry->gpsData.latitude != 0 && entry->gpsData.longitude !=0) {
				entry->gpsDistance = 0;
				entry->gpsDistance = GPS_GetDistance(entry->gpsData.latitude, entry->gpsData.longitude);
				//SCASDBG_PrintAscii("\r\nNew GPS Distance: ");
				//SCASDBG_PrintU32ToAscii(entry->gpsDistance, FALSE);
				//SCASDBG_PrintAscii("\r\n");
				rangetag_CalculateClosestDistance(entry);
				rangetag_DetermineThreatZone(entry);
			}	
			//SCASDBG_PrintAscii("BEACON Updated Entry: \r\n");
			//SCASDBG_PrintAscii("Id: ");
			//SCASDBG_PrintU8ToAscii(entry->id.IDByte5, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Zone: ");
			//SCASDBG_PrintU8ToAscii(entry->deviceZone, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Latitude: ");
			//SCASDBG_PrintU32ToAscii((U32)entry->gpsData.latitude, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Longitude: ");
			//SCASDBG_PrintU32ToAscii((U32)entry->gpsData.longitude, FALSE);
			//SCASDBG_PrintAscii("\r\n\r\n");
		}
		
		// Only update the device's UHF2 timestamp if it is not globally deregistered by another device
		// Note: if the device is globally excluded, it could be because of this RCU, also handle that case here.
		if((entry->caVisibility.bits.excludedGlobally == FALSE) || (REGIONCTRL_ExcludeMinerUnitGlobally(&(entry->id), entry->distance) == TRUE)) {
			entry->ts_UHF2 = TIMER_LongTimeNow();
		}
		
		// Notify BTUCTRL of the entry's updated info
		if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
			BTUCTRL_UpdateProximityInfo(entry->id, entry->rssi[RSSI_AVE_COUNT], entry->distance);
		}
		
	} else {
		/// Else, add the device as a new entry in the warning table
		RANGETAG_DeviceInfo newEntry;
		memset(&newEntry, 0, sizeof(RANGETAG_DeviceInfo));
		newEntry.id = id;
		newEntry.ts_enterTable = TIMER_LongTimeNow();
		newEntry.ts_UHF2 = TIMER_LongTimeNow();
		newEntry.nomTxPower = nomTxPower;

		/// Add RSSI value to array
		newEntry.rssi[0] = rangetag_RssiToPower(rssi, nomTxPower);
		newEntry.rssi[RSSI_AVE_COUNT] = rangetag_CalcAvgRssi(&(newEntry.rssi[0]));

		/// Check whether this device is nanoLOC_equipped
		if((newEntry.id.IDByte2 & 0x01) == 0x01) {
			newEntry.nanoLOC_equipped = TRUE;
		} else {
			newEntry.nanoLOC_equipped = FALSE;
		}
        
		// Set the caVisibility value if available
		if((msgType == CA_MSG_CAM) || (msgType == CA_MSG_LMU_CAM)) {
			camMsg = (ca_msg_CAM*)rxMsg;
			newEntry.caVisibility = camMsg->field.caVisibility;
			newEntry.gpsData.latitude = camMsg->field.gpsData.latitude;
			newEntry.gpsData.longitude = camMsg->field.gpsData.longitude;
			newEntry.gpsData.altitude = camMsg->field.gpsData.altitude;
			newEntry.gpsData.speed = camMsg->field.gpsData.speed;
			if (newEntry.gpsData.latitude != 0 && newEntry.gpsData.longitude !=0) {
				newEntry.gpsDistance = 0;
				newEntry.gpsDistance = GPS_GetDistance(newEntry.gpsData.latitude, newEntry.gpsData.longitude);
				//SCASDBG_PrintAscii("\r\nNew GPS Distance: ");
				//SCASDBG_PrintU32ToAscii(newEntry.gpsDistance, FALSE);
				//SCASDBG_PrintAscii("\r\n");
				rangetag_CalculateClosestDistance(&newEntry);
				rangetag_DetermineThreatZone(&newEntry);
			}	
			//SCASDBG_PrintAscii("CAM New Entry: \r\n");
			//SCASDBG_PrintAscii("Id: ");
			//SCASDBG_PrintU8ToAscii(newEntry.id.IDByte5, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Zone: ");
			//SCASDBG_PrintU8ToAscii(newEntry.deviceZone, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Latitude: ");
			//SCASDBG_PrintU32ToAscii((U32)newEntry.gpsData.latitude, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Longitude: ");
			//SCASDBG_PrintU32ToAscii((U32)newEntry.gpsData.longitude, FALSE);
			//SCASDBG_PrintAscii("\r\n\r\n");		
		}
		else if (msgType == CA_MSG_BEACON) {
			beaconMsg = (ca_msg_Beacon*)rxMsg;
			newEntry.gpsData.latitude = beaconMsg->field.gpsData.latitude;
			newEntry.gpsData.longitude = beaconMsg->field.gpsData.longitude;
			newEntry.gpsData.altitude = beaconMsg->field.gpsData.altitude;
			newEntry.gpsData.speed = beaconMsg->field.gpsData.speed;
			if (newEntry.gpsData.latitude != 0 && newEntry.gpsData.longitude !=0) {
				newEntry.gpsDistance = 0;
				newEntry.gpsDistance = GPS_GetDistance(newEntry.gpsData.latitude, newEntry.gpsData.longitude);
				//SCASDBG_PrintAscii("\r\nNew GPS Distance: ");
				//SCASDBG_PrintU32ToAscii(newEntry.gpsDistance, FALSE);
				//SCASDBG_PrintAscii("\r\n");
				rangetag_CalculateClosestDistance(&newEntry);
				rangetag_DetermineThreatZone(&newEntry);
			}	
			//SCASDBG_PrintAscii("BEACON New Entry: \r\n");
			//SCASDBG_PrintAscii("Id: ");
			//SCASDBG_PrintU8ToAscii(newEntry.id.IDByte5, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Zone: ");
			//SCASDBG_PrintU8ToAscii(newEntry.deviceZone, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Latitude: ");
			//SCASDBG_PrintU32ToAscii((U32)newEntry.gpsData.latitude, FALSE);
			//SCASDBG_PrintAscii("\r\n");
			//SCASDBG_PrintAscii("Longitude: ");
			//SCASDBG_PrintU32ToAscii((U32)newEntry.gpsData.longitude, FALSE);
			//SCASDBG_PrintAscii("\r\n\r\n");		
		}
		
		// Now, only add the device if it is not globally deregistered
		if(newEntry.caVisibility.bits.excludedGlobally == FALSE) {
			switch(g_configBlock.deviceID.IDByte1) {
		        case UNIT_TYPE_TU:
			    case UNIT_TYPE_TU_UHF2:
			    case UNIT_TYPE_LMU:
			    case UNIT_TYPE_VMU:
			        //limitedTable = TRUE;
					limitedTable = FALSE;
				    break;
			    default:
			        limitedTable = FALSE;
		            break;
		    }
		
		    // If limitedTable is TRUE, only add new device to table if it is already over RSSI threshold
		    if(((limitedTable == TRUE) && (newEntry.rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdMU)) ||
		        limitedTable == FALSE) {
			    // If the warnTable is full, check whether weakest entry must be removed based on RSSI prioritisation
			    if(warnTable.warnCount == RANGETAG_MAX_COUNT) {
				    /// Check the average RSSI of the "least powerful" warnTable entry
				    U8 remPos = rangetag_TableFindLeastPowerful();
				    if(warnTable.entries[remPos].rssi[RSSI_AVE_COUNT] < newEntry.rssi[RSSI_AVE_COUNT]) {
					    // Replace the weakest entry in the warnTable with the newEntry
					    rangetag_TableRemove(remPos);
				        IOCTL_ToggleHazardLed();        // For debugging
				    } else {
					    // Space will not be allocated for the new entry since it has a low RSSI value
					    ignoreEntry = TRUE;
				    }
			    }
			
			    // If the new entry must be added to the warnTable, add it at the bottom
			    if(ignoreEntry == FALSE) {
				    warnTable.entries[warnTable.warnCount] = newEntry;

			        // Update pointer to the current entry, which will be used next for aboveThr checking
			        entry = &(warnTable.entries[warnTable.warnCount]);

			        warnTable.warnCount++;
					
					// Notify BTUCTRL of the new entry on BTUs
					if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
						BTUCTRL_UpdateProximityInfo(newEntry.id, newEntry.rssi[RSSI_AVE_COUNT], newEntry.distance);
					}
			    }
		    } else {
			    // Ignore the new entry based on RSSI level being too low while limitedTable is TRUE
			    ignoreEntry = TRUE;
		    }
		} else {
			// Ignore the new entry based on its global deregistration status
			ignoreEntry = TRUE;
		}
	}
	
	// Perform remaining tasks only if the table entry must not be ignored
	if(ignoreEntry == FALSE) {
		/// Check whether average RSSI is over threshold.
	    switch (entry->id.IDByte1) {
		    case UNIT_TYPE_LMU:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdLMU);
            break;
            case UNIT_TYPE_LSU:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdLSU);
            break;
            case UNIT_TYPE_VMU:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdVMU);
            break;
            case UNIT_TYPE_HU:
            case UNIT_TYPE_CU:
			case UNIT_TYPE_RCU:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdHU);
            break;
            case UNIT_TYPE_MU:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdMU);
            break;
            case UNIT_TYPE_TU:
		    case UNIT_TYPE_TU_UHF2:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdTU);
            break;
            case UNIT_TYPE_FU:
		    case UNIT_TYPE_AID:
		    case UNIT_TYPE_SAP:
		    case UNIT_TYPE_UHF2_REPEATER:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdFU);
            break;
            case UNIT_TYPE_IO_CARRIER:
            default:
                aboveThr = (entry->rssi[RSSI_AVE_COUNT] > g_configBlock.rssiThresholdFU);
            break;
	    }
        
		// If the entry's average RSSI is above threshold, CAM messages must be sent in response to BEACON messages, and 
		// the event may need to be logged
		// NEW: Always return a message to share GPS positions, but only flag certain above RSSI threshold entries
	    // if(aboveThr) {
		    ///< CAM messages must only be sent to Test Units if the Test Unit has not successfully ranged with this Mobile Radio
	        U8 doNotCam = FALSE;
	        if((entry->id.IDByte1 == UNIT_TYPE_TU) && (entry->flagZone2UHF1 == TRUE)) {
		        ///< This test unit has already successfully ranged with the Mobile Radio, do not send CAM response
		        doNotCam = TRUE;
	        } else if(entry->id.IDByte1 == UNIT_TYPE_TU_UHF2) {
		        ///< Do not send CAM messages to the UHF2 test unit.
		        doNotCam = TRUE;
	        } else if(((entry->id.IDByte1 == UNIT_TYPE_FU) || (entry->id.IDByte1 == UNIT_TYPE_AID) || (entry->id.IDByte1 == UNIT_TYPE_SAP)) && (warnTable.beaconIgnore == TRUE)) {
				// global ignore of beacons active, do not CAM
				doNotCam = TRUE;
			} else if(entry->beaconIgnore == TRUE) {
				// beacons from this specific table entry must not be responded to
				doNotCam = TRUE;
			}
			
			// Send CAM message if appropriate
			if(((msgType == CA_MSG_BEACON) || (msgType == CA_MSG_LMU_BEACON)) && (doNotCam == FALSE)) {
			    /// rxMsg contains any parameters sent along with the last beacon msg received from
			    /// this table entry
			    beaconMsg = (ca_msg_Beacon*)rxMsg;
			    /// For backwards compatibility with previous BEACON messages which did not include beaconCycleTime_ms etc.,
			    /// check that the received beacon message has the correct (updated) length
			    if((msgType == CA_MSG_LMU_BEACON) || (beaconMsg->field.preamble.length == (CA_MSG_BEACON_LEN) + MESSAGES_UHF2_PREAMBLE_LEN - 1)) {
				    CA_MSG_SetResponseParams(beaconMsg->field.beaconCycleTime_ms,
				                             beaconMsg->field.beaconCycleReserve_ms,
										     beaconMsg->field.timeSlotSize_ms,
										     TRUE);
				    CA_MSG_SendMsg(CA_MSG_CAM, entry->id, g_configBlock.relTxPower);
				    //CA_MSG_SendMsgAdvanced(CA_MSG_CAM, entry->id, g_configBlock.relTxPower, TRUE, responseDelay_ms, RANGETAG_CAM_RETRY_COUNT);
			    } else {
				    /// Fall back to the old way of sending CAM messages
				    /// Trick CA_MSG into using the old random back-off retry mechanism
				    CA_MSG_SetResponseParams(0,0,0, FALSE);
				    CA_MSG_SendMsg(CA_MSG_CAM, entry->id, g_configBlock.relTxPower);
			    }
		    }
		    /// Log the UHF2 registration event if applicable
		    if(entry->flagAboveThrUHF2 == FALSE) {
                CALOG_UHF2Log newLog = {
	                .deviceID = entry->id,
	                .rssi = entry->rssi[RSSI_AVE_COUNT],
	                .type = CA_ADD
                };
                CALOG_Write(CA_UHF2_EVENT, (U8*)&newLog, CA_UHF2_LEN);
            }
			
			// Update beacon ignore times and cycle times for devices which transmit beacons and CAACKs
			if((msgType == CA_MSG_BEACON) || (msgType == CA_MSG_LMU_BEACON)) {
				
				beaconMsg = (ca_msg_Beacon*)rxMsg;
				entry->beaconCycleTimeMs = beaconMsg->field.beaconCycleTime_ms;
				
			} else if(msgType == CA_MSG_CAACK) {
				
				caackMsg = (ca_msg_CAACK *)rxMsg;
				
				// If this is a CAACK from a 0xF_ type device and ignoreAllBeacons field is set
				if(((entry->id.IDByte1 == UNIT_TYPE_FU) || (entry->id.IDByte1 == UNIT_TYPE_AID) || (entry->id.IDByte1 == UNIT_TYPE_SAP)) && (caackMsg->field.ignoreAllBeacons == TRUE)) {
					warnTable.beaconIgnore = TRUE;
				    warnTable.ts_beaconIgnore = TIMER_Now32();
					warnTable.ignoreBeaconTimeOut_s = caackMsg->field.beaconIgnoreTime_s;
				} else {
					// Ignore future beacons from this specific device only
					entry->beaconIgnore = TRUE;
					entry->beaconIgnoreTimeS = caackMsg->field.beaconIgnoreTime_s;
					entry->ts_beaconIgnore = TIMER_Now32();
				}
			}

			if (aboveThr) {
				/// Update aboveThr timestamp
				entry->ts_aboveThrUHF2 = TIMER_LongTimeNow();
				entry->flagAboveThrUHF2 = TRUE;
			}		
	    //}		
	    /// Update table entry's rssiAboveThr variable. This flag is only cleared when timeout occurs.
	    entry->rssiAboveThr |= aboveThr;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief Converts the received rssi parameter read from CC1101 to a value between 0 and 255.
//          * 0 represents -138.0dBm
//          * 1 represents -137.5dBm
//          * 2 represents -137.0dBm
//          * ...
//          * 255 represents -10.5dBm
//          
/// \param rssi [in] Received RSSI value in CC1101 register
/// \param nomTxPower [in] Nominal TX power of the sending device (currently not used)
/// \return Integer representation of RSSI value where retVal = 2 * (dBm + 138), or 
///                                                       dBm = (0.5 * retVal) - 138
U8 rangetag_RssiToPower(U8 rssi, U8 nomTxPower)
{
	// Simply add 128 to the two's compliment RSSI value reported by CC1101
    return (U8)((U16)rssi + 128);
}

//-------------------------------------------------------------------------------------------------
/// \brief Converts the normalised rssi parameter from an unsigned value between 0 and 255 to a 
///         signed value in dBm.
/// \param rssi [in] Normalised RSSI value where 0 represents -138dBm and 255 represents -10.5dBm
/// \return Signed representation of RSSI value in dBm
double rangetag_RssiToDbm(U8 rssi)
{
	// dBm = (0.5 * rssi) - 138
    return (0.5 * rssi) - 138.0;
}

//-------------------------------------------------------------------------------------------------
/// \brief Averages the RSSI values stored in an array in each warning table entry.
/// \param rssiArray [inout] A pointer to the RSSI array in the warning table which must be
///        averaged. The result will be stored in the last position of the array.
/// \return The average of the array.
U8 rangetag_CalcAvgRssi(U8 *rssiArray)
{
	U16 accumulator;
	U8 validCnt;
    U8 i;

    accumulator = 0;
	validCnt = 0;
    for(i=0; i < RSSI_AVE_COUNT; i++, rssiArray++) {
		if((*rssiArray) > 0) {
			accumulator += (*rssiArray);
			validCnt++;
		}

    }
	if(validCnt > 0) {
		return (U8)(accumulator / validCnt);
	} else {
	    return 0;
	}

}

//-------------------------------------------------------------------------------------------------
/// \brief This function removes an entry from the warning table.
/// \param posi [in] The index of the device in the warning table which must be removed.
void rangetag_TableRemove(U8 posi)
{
	///< Remove the entry from the table by moving all subsequent entries up one position and
	///  decrementing the warnCount counter.
	U8 i;
	
	// Notify BTUCTRL of the entry's removal
	if(g_configBlock.deviceID.IDByte1 == UNIT_TYPE_BTU) {
		BTUCTRL_DeviceTimeout(warnTable.entries[posi].id);
	}
	
	for(i=posi; i < (warnTable.warnCount - 1); i++) {
		warnTable.entries[i] = warnTable.entries[i+1];
	}
	warnTable.warnCount--;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function determines the least significant entry in the warning table, and is
///        typically used when the warning table is full but a new entry must be inserted into the
///        table.
/// \return The array index of the least powerful table entry.
U8 rangetag_TableFindLeastPowerful(void)
{
	/// The least powerful table entry is considered to be the entry with the lowest average RSSI.
	/// Note: There may be special cases when this will be an incorrect assumption. Directly after
	///       a device is inserted in the table, its average RSSI will be very small (one RSSI
	///       measurement divided by RSSI_AVE_COUNT). If two new devices are contending to enter
	///       the warning table while it is full, they may each only be in the table for one beacon
	///       period at a time.

	/// Scan through table to find the least powerful device entry
	U8 i;
	U8 retVal = 0;
	U8 minAvgRssi = warnTable.entries[0].rssi[RSSI_AVE_COUNT];  // Init with first entry's value
	for(i=0; i < warnTable.warnCount; i++) {
		if(warnTable.entries[i].rssi[RSSI_AVE_COUNT] < minAvgRssi) {
			minAvgRssi = warnTable.entries[i].rssi[RSSI_AVE_COUNT];
			retVal = i;
		}
	}

	return retVal;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function calculates the appropriate timeout period to use for detecting when a 
///         specific device is no longer detectable on UHF2.
U16 rangetag_GetUHF2TimeoutForDevice(RANGETAG_DeviceInfo *deviceInfo)
{
	// Check whether beacon cycle time of device is known
	if(deviceInfo->beaconCycleTimeMs != 0) {
		
		// Check whether beacon cycle time is long enough to overrule configured slave CA timeout
		if(((deviceInfo->beaconCycleTimeMs * MIN_BEACON_CYCLES_BEFORE_TIMEOUT) / 1000) > g_configBlock.slaveCATimeout_s) {
			return (deviceInfo->beaconCycleTimeMs * MIN_BEACON_CYCLES_BEFORE_TIMEOUT) / 1000;
		}
	}
	
	// If this point is reached, return the configured slave CA timeout duration
	return g_configBlock.slaveCATimeout_s;
}

//-------------------------------------------------------------------------------------------------
/// \brief This function checks and handles all timeouts for warning and logging purposes. The
///        following timestamps are processed:
///        -ts_UHF2         Remove device from table if no UHF2 comms occurred for specified time.
///        -ts_UHF1         Do nothing (future use) if no range results received for specified time.
///        -ts_aboveThrUHF2 Log event as an exit from UHF2 if under threshold for specified time.
///        -ts_zone2UHF1    Log event as an exit from UHF1 if distance > zone 2 for specified time.
///        -ts_beaconIgnore Stop ignoring beacons from Fixed Units after specified time.
///        -ts_drvrDereg    Clear driver deregistration status if not confirmed for specified time.
///        -ts_wrkrExcl     Clear worker exclusion status if not confirmed for specified time.
void rangetag_HandleTimeouts(void)
{
	U8 i;   /// Used in for loops

    // Handle the beacon ignore timeout
	if( (warnTable.beaconIgnore) && (TIMER_Elapsed32(warnTable.ts_beaconIgnore) > TIMER_S(warnTable.ignoreBeaconTimeOut_s)) ) {
		warnTable.beaconIgnore = FALSE;
		warnTable.ts_beaconIgnore = 0;
	}

    // For each warnTable entry, check the applicable timeouts
	for(i=0; i < warnTable.warnCount; i++) {
		RANGETAG_DeviceInfo *entry = &(warnTable.entries[i]);
        
		// Check whether Worker Exclusion should time out
		if( (entry->wrkrExcl.bits.excluded) && (TIMER_LongTimeElapsed(entry->ts_wrkrExcl) > g_configBlock.slaveCATimeout_s) ) {
			CALOG_UHF1Log newLog = {
	            .deviceID = entry->id,
	            .zone = CA_ZONE_3,
				.quad = QUADRANT_DEFAULT,
	            .type = CA_WORKER_EXCL_TIMEOUT,
				.finalDist = 0,
				.globalExclusion = entry->wrkrExcl.bits.globalDereg
            };
            CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			entry->wrkrExcl.byte = FALSE;       // Reset the wrkrExcl byte
			entry->ts_wrkrExcl = 0;
		}
        
		// Check whether driver deregistration should time out
		if( (entry->drvrDereg.bits.excluded) && (TIMER_LongTimeElapsed(entry->ts_drvrDereg) > g_configBlock.slaveCATimeout_s) ) {
			CALOG_UHF1Log newLog = {
	            .deviceID = entry->id,
	            .zone = CA_ZONE_3,
				.quad = QUADRANT_DEFAULT,
	            .type = CA_DRIVER_DEREG_TIMEOUT,
				.finalDist = 0,
				.globalExclusion = entry->drvrDereg.bits.globalDereg
            };
            CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			entry->drvrDereg.byte = FALSE;      // Reset the drvrDereg byte
			entry->ts_drvrDereg = 0;
		}
        
		// Check whether the device is still confirmed to be within zone2 threshold distance
		if((entry->flagZone2UHF1) && (TIMER_LongTimeElapsed(entry->ts_zone2UHF1) > g_configBlock.slaveCATimeout_s)) {
			///< Do not allow Test Units to time out from zone 2; TU's only time out on UHF2
			if(entry->id.IDByte1 != UNIT_TYPE_TU) {
				entry->flagZone2UHF1 = FALSE;
			    if(TIMER_LongTimeElapsed(entry->ts_UHF1) > g_configBlock.slaveCATimeout_s) {
				    /// Special case, this MR has not received range result messages from this specific device
				    /// for slaveCATimeout_s seconds. This means the MR did not simply move >25m away from the
				    /// device, but the device has been switched off or malfunctioned. The warning for this
				    /// device must be cleared, which can be done by setting the distance to zero.
				    entry->distance = 0;
			    }
			    /// Log the exit from zone 2
			    CALOG_UHF1Log newLog = {
	                .deviceID = entry->id,
	                .zone = CA_ZONE_2,
				    .quad = QUADRANT_DEFAULT,
	                .type = CA_TIMEOUT,
				    .finalDist = 0,         /// Final distance field not meaningful here
					.globalExclusion = FALSE        // Not applicable to CA_TIMEOUT event
                };
                CALOG_Write(CA_UHF1_EVENT, (U8*)&newLog, CA_UHF1_LEN);
			}
		}
        
		// Check the UHF2 above threshold timeout
		if((entry->flagAboveThrUHF2) && (TIMER_LongTimeElapsed(entry->ts_aboveThrUHF2) > rangetag_GetUHF2TimeoutForDevice(entry))) {
			entry->flagAboveThrUHF2 = FALSE;
			/// Log the exit from UHF2
			CALOG_UHF2Log newLog = {
	            .deviceID = entry->id,
	            .rssi = 0,              /// RSSI not meaningful here
	            .type = CA_TIMEOUT
            };
            CALOG_Write(CA_UHF2_EVENT, (U8*)&newLog, CA_UHF2_LEN);
			entry->rssiAboveThr = FALSE;
		}
        
		// Check the UHF2 detection timeout
		if(TIMER_LongTimeElapsed(entry->ts_UHF2) > rangetag_GetUHF2TimeoutForDevice(entry)) {
			/// Remove the device from the warning table if no UHF2 comms occurred for specified time
			rangetag_TableRemove(i);
		}
		
		// Check the device specific beacon ignore timeout if applicable
		if(entry->beaconIgnore == TRUE) {
			if(TIMER_Elapsed32(entry->ts_beaconIgnore) > TIMER_S(entry->beaconIgnoreTimeS)) {
				entry->beaconIgnore = FALSE;
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function returns the number of entries in the UHF1 data message buffer
/// \return the number of entries in the UHF1 data message buffer
U8 rangetag_UHF1DataBufferCount(void)
{
	if(msgBufferUHF1.head >= msgBufferUHF1.tail) {
		return msgBufferUHF1.head - msgBufferUHF1.tail;
	} else {
		return msgBufferUHF1.tail - msgBufferUHF1.head;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function retrieves a single UHF1 data message from the buffer.
/// \param msg [out] Pointer to the memory address of the UHF1 message
/// \return SUCCESS if message was retrieved, FAILURE if buffer is empty
U8 rangetag_GetUHF1DataMsg(UHF1_DataMsg *msg)
{
	if(rangetag_UHF1DataBufferCount() == 0) {
	    return FAILURE;                         /// The buffer contains no messages to retrieve
	} else {
		*msg = msgBufferUHF1.buffer[msgBufferUHF1.tail];
		msgBufferUHF1.tail = (msgBufferUHF1.tail + 1) % RANGETAG_UHF1_BUFFER_LEN;
		return SUCCESS;
	}
}

//-------------------------------------------------------------------------------------------------
/// \brief This function evaluates configurable relay toggling parameters on Hazard Units to
///         determine whether relay toggling must be activated or not based on the ranging results
///         of a specific device in the warnTable.
/// \param tableEntry The index position of the warnTable entry
/// \return The resulting relay state due to the ranging results of the specified warnTable entry
U8 rangetag_GetRequiredRelayState(U8 tableEntry)
{
	U8 retVal = FALSE;              // Initially assume relay toggling not required 
    
	// Check whether the detected unit is within relay activating distance
	if((warnTable.entries[tableEntry].distance > 0) && 
	   (warnTable.entries[tableEntry].distance <= g_configBlock.huRelayThreshold_cm)) {
		// Device is within the configurable threshold distance
		warnTable.entries[tableEntry].overRelayThresholdFlag = TRUE;
		warnTable.entries[tableEntry].overRelayThresholdTs = TIMER_Now32();
	} else if((g_configBlock.huRelayThreshold_cm == 0) && (warnTable.entries[tableEntry].flagAboveThrUHF2 == TRUE)) {
		// The configurable threshold distance was set to zero - fall back to RSSI method
		warnTable.entries[tableEntry].overRelayThresholdFlag = TRUE;
		warnTable.entries[tableEntry].overRelayThresholdTs = TIMER_Now32();
	} else {
		// Device not over relay threshold. Check whether over threshold timeout has occurred
		if(warnTable.entries[tableEntry].overRelayThresholdFlag == TRUE) {
			if(TIMER_Elapsed32(warnTable.entries[tableEntry].overRelayThresholdTs) > TIMER_MS(RELAY_OVER_THRESHOLD_HYSTERESIS_MS)) {
			    warnTable.entries[tableEntry].overRelayThresholdFlag = FALSE;
			}
		}
	}
	
	if(warnTable.entries[tableEntry].overRelayThresholdFlag == TRUE) {
		// Check whether the detected unit type is supposed to activate the HU relay output
		switch(warnTable.entries[tableEntry].id.IDByte1) {
			case UNIT_TYPE_LMU:
			case UNIT_TYPE_LSU:
			    if((g_configBlock.huRelayActuators == RANGETAG_ALL_UNIT_TYPES) || 
				   (g_configBlock.huRelayActuators == RANGETAG_ALL_VEHICLES) ||
				   (g_configBlock.huRelayActuators == RANGETAG_TRACK_BOUND_VEHICLES)) {
				    
					retVal = TRUE;
			    }
				break;
			case UNIT_TYPE_VMU:
			    if((g_configBlock.huRelayActuators == RANGETAG_ALL_UNIT_TYPES) || 
				   (g_configBlock.huRelayActuators == RANGETAG_ALL_VEHICLES) ||
				   (g_configBlock.huRelayActuators == RANGETAG_TRACKLESS_VEHICLES)) {
				    
					retVal = TRUE;
			    }
				break;
			case UNIT_TYPE_MU:
			    if((g_configBlock.huRelayActuators == RANGETAG_ALL_UNIT_TYPES) || 
				   (g_configBlock.huRelayActuators == RANGETAG_PEDESTRIANS_ONLY)) {
				    
					retVal = TRUE;
			    }
				break;
			default:
			    retVal = FALSE;
				break;
		}
	}
	
	return retVal;
}

void rangetag_CalculateClosestDistance(RANGETAG_DeviceInfo *entry) {
	//SCASDBG_PrintAscii("Ranging Distance: ");
	//SCASDBG_PrintU32ToAscii((U32)entry->distance, FALSE);
	//SCASDBG_PrintAscii("\r\n");
	//SCASDBG_PrintAscii("GPS Distance: ");
	//SCASDBG_PrintU32ToAscii((U32)entry->gpsDistance, FALSE);
	//SCASDBG_PrintAscii("\r\n");
	if ((entry->gpsDistance > 0) && (entry->distance <= 0)) {
		entry->finalDistance = entry->gpsDistance;
		entry->finalLatitude = entry->gpsData.latitude;
		entry->finalLongitude = entry->gpsData.longitude;
		//SCASDBG_PrintAscii("Choose GPS distance\r\n");
		//SCASDBG_PrintAscii("Final Distance: ");
		//SCASDBG_PrintU32ToAscii((U32)entry->finalDistance, FALSE);
		//SCASDBG_PrintAscii("\r\n");
	}
	else if (((entry->gpsDistance > 0) && (entry->distance > 0)) && (entry->gpsDistance < entry->distance)) {
		entry->finalDistance = entry->gpsDistance;
		entry->finalLatitude = entry->gpsData.latitude;
		entry->finalLongitude = entry->gpsData.longitude;
		//SCASDBG_PrintAscii("Choose GPS distance\r\n");
		//SCASDBG_PrintAscii("Final Distance: ");
		//SCASDBG_PrintU32ToAscii((U32)entry->finalDistance, FALSE);
		//SCASDBG_PrintAscii("\r\n");
		return;
	}
	else {	
		//TODO if it gets here, it means the nanoloc range is closest, so determine new coordinates based on this distance
		entry->finalDistance = entry->distance;
		entry->finalLatitude = entry->gpsData.latitude;
		entry->finalLongitude = entry->gpsData.longitude;
	
		//SCASDBG_PrintAscii("Choose Ranging distance\r\n");
		//SCASDBG_PrintAscii("Final Distance: ");
		//SCASDBG_PrintU32ToAscii((U32)entry->finalDistance, FALSE);
		//SCASDBG_PrintAscii("\r\n");
	}	
	
	return;
}

void rangetag_DetermineThreatZone(RANGETAG_DeviceInfo *entry) {
	// Check closest zones first moving outwards
	if (entry->finalDistance > 0) {
		if (entry->finalDistance < g_configBlock.proximityZone3_cm) {
			entry->deviceZone = MESSAGES_SPATIAL_ZONE_3;
		}
		else if (entry->finalDistance < g_configBlock.proximityZone2_cm) {
			entry->deviceZone = MESSAGES_SPATIAL_ZONE_2;
		}
		else if (entry->finalDistance < g_configBlock.proximityZone1_cm) {
			entry->deviceZone = MESSAGES_SPATIAL_ZONE_1;
		}
		else {
			entry->deviceZone = MESSAGES_SPATIAL_ZONE_0;
		}			
	}	
	else {
		entry->deviceZone = MESSAGES_SPATIAL_ZONE_0;
	}
	
	//SCASDBG_PrintAscii("Zone: ");
	//SCASDBG_PrintU32ToAscii((U32)entry->deviceZone, FALSE);
	//SCASDBG_PrintAscii("\r\n");
}

void RANGETAG_GetWarningTable(RANGETAG_Warning_Table_Entry *buffer, U8 *threatCount) {
	for (int i = 0; i < warnTable.warnCount; i++) {		
		buffer[i].latitude = warnTable.entries[i].finalLatitude;
		buffer[i].longitude = warnTable.entries[i].finalLongitude;
		buffer[i].zone = warnTable.entries[i].deviceZone;
		buffer[i].threatType = warnTable.entries[i].id.IDByte1;
	}
	
	*threatCount = warnTable.warnCount;
	
	return;
}