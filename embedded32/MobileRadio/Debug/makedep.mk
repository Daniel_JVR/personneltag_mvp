################################################################################
# Automatically-generated file. Do not edit or delete the file
################################################################################

..\..\common\lib_crc.c

..\lib\adc.c

..\lib\beacon.c

..\lib\calog.c

..\lib\ca_msg.c

..\lib\cc1101.c

..\lib\ewphandler.c

..\lib\flashdump.c

..\lib\flash_uc3.c

..\lib\irdahandler.c

..\lib\messages_ptcslave.c

..\lib\msgdispatch.c

..\lib\msghandler.c

..\lib\pllcheck.c

..\lib\rangealgo.c

..\lib\scasdbg.c

..\lib\spi_uc3.c

..\lib\strbuilder.c

..\lib\sw_maintenance_uc3.c

..\lib\sysconfig_uc3.c

..\lib\sysvar.c

..\lib\timer.c

..\lib\uart_uc3.c

..\lib\uhf1.c

..\lib\uhf2.c

..\lib\uhf2mux.c

..\lib\uhf2_msghandler.c

..\lib\uspi_uc3.c

..\nanoLOC\phy\ntrxinit.c

..\nanoLOC\phy\ntrxiqpar.c

..\nanoLOC\phy\ntrxranging.c

..\nanoLOC\phy\ntrxutil.c

..\nanoLOC\phy\phy.c

..\nanoLOC\portation\hwclock.c

..\nanoLOC\portation\nnspi.c

src\asf\avr32\drivers\adcifa\adcifa.c

src\asf\avr32\drivers\ast\ast.c

src\asf\avr32\drivers\eic\eic.c

src\asf\avr32\drivers\flashc\flashc.c

src\asf\avr32\drivers\gpio\gpio.c

src\asf\avr32\drivers\intc\exception.S

src\asf\avr32\drivers\intc\intc.c

src\asf\avr32\drivers\pdca\pdca.c

src\asf\avr32\drivers\pm\pm_uc3c.c

src\asf\avr32\drivers\pm\power_clocks_lib.c

src\asf\avr32\drivers\scif\scif_uc3c.c

src\asf\avr32\drivers\spi\spi.c

src\asf\avr32\drivers\tc\tc.c

src\asf\avr32\drivers\twim\twim.c

src\asf\avr32\drivers\usart\usart.c

src\asf\avr32\drivers\wdt\wdt4.c

src\asf\avr32\utils\debug\print_funcs.c

src\asf\avr32\utils\startup\startup_uc3.S

src\asf\common\boards\user_board\init.c

src\asf\common\services\clock\uc3c\osc.c

src\asf\common\services\clock\uc3c\pll.c

src\asf\common\services\clock\uc3c\sysclk.c

src\BQ24250.c

src\btuctrl.c

src\buzzerCtrl.c

src\caectrl.c

src\captod.c

src\cbit.c

src\gps.c

src\ioctl.c

src\iophy.c

src\lcd.c

src\M24SR04.c

src\main.c

src\mrtest.c

src\msgprocess.c

src\msgrouter.c

src\p2msgprocess.c

src\paging.c

src\ptcstatus.c

src\rangetag.c

src\regionctrl.c

src\serialhandler.c

src\slinkmon.c

src\uhf2repeater.c

