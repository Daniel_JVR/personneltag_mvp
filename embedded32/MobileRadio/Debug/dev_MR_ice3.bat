REM Chip Erase

"C:\Program Files (x86)\Atmel\Atmel Studio 6.2\atbackend\atprogram.exe" -i jtag -d at32uc3c1256c chiperase

REM Bootloader

"C:\Program Files (x86)\Atmel\Atmel Studio 6.2\atbackend\atprogram.exe" -t jtagice3 -i jtag -d at32uc3c1256c -v -cl 7mhz program -e -f "D:\Schauenburg\SCAS_OC\trunk\embedded32\elf\bootLoader\bootLoader-MobileRadio.elf"

REM Mobile Radio

"C:\Program Files (x86)\Atmel\Atmel Studio 6.2\atbackend\atprogram.exe" -t jtagice3 -i jtag -d at32uc3c1256c -v -cl 7mhz program -e -f "D:\scas_opencast\scas_opencast\mvp\mr\trunk\embedded32\MobileRadio\Debug\MobileRadio.elf"
